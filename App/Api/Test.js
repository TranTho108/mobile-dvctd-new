import {SERVER_URL} from '../Config/server'
import axios from 'axios'


export const fetchTest = async(status) => {
    return await axios.get(`${SERVER_URL}/GetByStatus?limit=20&status=${status}`)
        .then(function (response) {
			console.log(response.data)
            return response.data
        })
        .catch(function (error) {
            console.log(error)
            return []
        });
}

export const fetchTest2 = async(id) => {
    return await axios.get(`${SERVER_URL}/ViewByID?id=${id}`)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            console.log(error)
        });
}

export const fetchTest3 = async(searchkey) => {
    return await axios.get(`${SERVER_URL}/Search?key=${searchkey}`)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            console.log(error)
            return []
        });
}

export const fetchTest6 = async(comment,id,parentid,userName) => {
    return await axios({
        method: 'post',
        url: `${SERVER_URL}/CommentByID`,
        data: JSON.stringify(
            {	
                gopyid:id,
                name:userName,
                comment:comment,
                parentid:parentid
            }
        ),
        headers: {'Content-Type': 'application/json'},
      })
    .then(response => {
        return response.status
    })
}

export const fetchNewComment = async(id) => {
    return await axios.get(`${SERVER_URL}/ViewByID?id=${id}`)
    .then(function (response) {
        var data2 = JSON.parse(response.data)
        return data2
    })
    .catch(function (error) {
        console.log(error)
    });
}