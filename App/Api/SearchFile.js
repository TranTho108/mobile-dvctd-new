import {TKHS_URL} from '../Config/server'
const SearchInfo = async () =>{
const xmlReqBody = `
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ServiceLayThongTinHoSo xmlns="http://schemas.microsoft.com/sharepoint/soap/">
      <tukhoa>CTYQM000010</tukhoa>
    </ServiceLayThongTinHoSo>
  </soap:Body>
</soap:Envelope>
`
await fetch(TKHS_URL, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'text/xml'
        },
        body: xmlReqBody
      })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson)
      })
      .catch((err) => console.log(err))
}