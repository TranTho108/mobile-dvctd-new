import { AsyncStorage } from 'react-native';
import {requestGET, requestPOST} from "../Api/Basic"
import {DVC_URL} from '../Config/server'

const SearchFileByCode = async(MaHoSo) => {
  var file = await requestGET(`${DVC_URL}/SearchDocByKey/${MaHoSo}`)
  var data1 = file.data?file.data.DSHoSo:[]
  return data1;
  }

module.exports = SearchFileByCode;