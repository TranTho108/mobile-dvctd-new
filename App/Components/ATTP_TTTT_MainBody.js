import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/ATTP_TTTT_MainBodyStyles"
import images from "../Themes/Images"
import Modal from "react-native-modal";
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import {ATTP_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'


export default class ATTP_TTTT_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          data: [],
          loading: true,
          visibleModal: false,
          detail: []
        };
      }

    componentWillMount = async() => {
      var data1 = await requestGET(`${ATTP_URL}/posts`)
      var data2 = data1.data?data1.data:[]
      this.setState({data: data2, loading: false})
    }

    _renderItem = ({item,index}) => {
    return(
        <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#FC7D2E', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='bullhorn' color='#fff' size={18} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
            <Text style={{fontSize: 16, fontWeight: 'bold', padding: 5}}>{item.Title}</Text>
            <View style={{flexDirection: 'row', padding: 5}}><Text style={{color: '#757575', width: 80}}>Nội dung</Text><Text style={{color: '#757575', fontWeight: 'bold', flex: 1}}>{item.Content}</Text></View>
        </View>
        </TouchableOpacity>
    )
    }

    navigateToMap = () => {
      var data = this.state.data
      var dataMap = []
      data.map((item) => {
          dataMap.push({name: item.Name, address: item.Address, location: {latitude: Number(item.Latitude),longitude: Number(item.Longitude)}})
      })
      this.props.navigation.navigate('MapViewScreen', {data: dataMap})
    }

    _renderModalContent = () => {
      if(this.state.detail){
      const {detail} = this.state
      return(
        <View style={styles.content2}>
          <ScrollView style={{padding: 20}}>
            <Text style={styles.title5}>Chi tiết bài viết</Text>
            <Text style={styles.section}>Tiêu đề</Text>
            <Text style={styles.label}>{detail.Title}</Text>
            <Text style={styles.section}>Nội dung</Text>
            <Text style={styles.label}>{detail.Content}</Text>
            <Text style={styles.section}>Mô tả</Text>
            <Text style={styles.label}>{detail.Description}</Text>
            <Text style={styles.section}></Text>
            <Text style={styles.label}></Text>
          </ScrollView>
          <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
               <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
          </TouchableOpacity>
        </View>
      )
      }
    }

    renderBody(){
      if(!this.state.loading){
        return(
          <View style={{flex: 1}}>
          <FlatList 
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{padding: 10}}
            ListEmptyComponent={EmptyFlatlist}
        />
        </View>
        )
      }
      else{
        return(
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#FC7D2E' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#FC7D2E'}}>Đang lấy dữ liệu</Text>
          </View>
        )
      }
    }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Thông tin tuyên truyền</Text>
          <Icon name='map' color='transparent' containerStyle={{paddingEnd: 20}} />
        </View>
        {this.renderBody()}
        <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
    </View>
    );
  }
}
