import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DL_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import {DL_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'

export default class DL_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          loading: true,
          data: []
        };
      }

      componentWillMount = async() => {
        var body = {"maxa": ''}
        var data1 = await requestPOST(`${DL_URL}/GetLoaiHinhDuLichs`, body)
        var data2 = data1.data?data1.data:[]
        this.setState({data: data2, loading: false})
      }

      renderBody(){
        if(!this.state.loading){
          return(
            <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        flexWrap: 'wrap',
                        flex: 1,
                        paddingTop: 50
                    }}>
                    {this.state.data.map(item => (
                      <View key={item.ID} style={{width: '33%'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("DL_ListScreen", {TieuDe: item.TieuDe, ID: item.ID})} style={styles.view2}>
                            <Image source={images.background.placeholder} style={{height: 40, width: 40}} />
                            <Text style={{textAlign: 'center', paddingTop: 10, height: 60}}>{item.TieuDe}</Text>
                        </TouchableOpacity>
                        </View>
                    ))}
                </View> 
          )
        }
        else{
          return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator size='large' style={{}} color='#9aca40' />
              <Text style={{textAlign: 'center', paddingTop: 10, color: '#9aca40'}}>Đang lấy dữ liệu</Text>
            </View>
          )
        }
      }

  render() {
    return (
        <View style={styles.container}>
            <ImageBackground source={images.background.DL_bg} style={{flex: 3/10}}>
            <View style={styles.linearGradient1}>
              <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
              <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            </ImageBackground>
            <View style={{flex: 7/10}}>
                <View style={styles.view1}>
                    <Text style={styles.title}>DU LỊCH</Text>
                </View>
                {this.renderBody()}
            </View>
        </View>
    );
  }
}
