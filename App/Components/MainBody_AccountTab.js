import React, { Component } from "react";
import { View, AsyncStorage, ImageBackground, ScrollView, TouchableOpacity, RefreshControl, FlatList, Image, StatusBar, Alert, ToastAndroid} from "react-native";
import {Text, Button, Icon, Divider, Avatar} from "react-native-elements";
import styles from "./Styles/MainBody_AccountTabStyles.js"
import images from "../Themes/Images"
import axios from "axios"
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { StackActions, NavigationActions } from 'react-navigation';
import firebase from 'react-native-firebase';
import ToggleSwitch from './ToggleSwitch';
import TouchID from 'react-native-touch-id';
import Share from 'react-native-share';
var moment = require('moment')
moment.locale('vi')
import Toast, {DURATION} from 'react-native-easy-toast'
import {CD_URL} from '../Config/server'

const optionalConfigObject = {
    title: 'Xác thực vân tay', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Thất bại', // Android
    cancelText: 'Hủy', // Android
    fallbackLabel: '', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
  };

export default class MainBody_AccountTab extends Component {

    constructor(props) {
        super(props);
      
        this.state = {
          namekey: '',
          userName: '',
          userInfo: '',
          phoneText: '',
          checkXacThucVanTay: false,
          userToken: '',
          deviceToken: ''
        };
    }

    componentWillMount = async() => {
        var userToken = await AsyncStorage.getItem("userToken")
        var deviceToken = await AsyncStorage.getItem("deviceToken")
        if(userToken){
            userInfo = await AsyncStorage.getItem("userInfo")
            info = JSON.parse(userInfo)
            var b = info.fullName.split('').reverse().join('')
            var name = b.charAt(b.indexOf(' ') -1)
            var name2 = info.fullName.charAt(0).toUpperCase()
            var t1 = info.phoneNumber.substring(0,4)
            var t2 = info.phoneNumber.substring(4,7)
            var t3 = info.phoneNumber.substring(7,10)
            this.setState({namekey: name+name2, userToken: userToken, deviceToken: deviceToken, userName: info.fullName, userInfo: info, phoneText: t1 + ' ' + t2 + ' ' + t3})
        }
        const value = await AsyncStorage.getItem('XacThucVanTay');
        var value1 = JSON.parse(value)
        if (value1) {
            this.setState({checkXacThucVanTay: true})
        }
    }

    XuLyVanTay = (value) => {
        TouchID.isSupported()
          .then(this.authenticate(value))
          .catch(error => {
            console.log('TouchID not supported');
            //AlertIOS.alert('Authenticated Successfully');
          });
      };
    
    setXacThucVanTay = async (value) => {
        try {
          await AsyncStorage.setItem('XacThucVanTay', JSON.stringify(value));
          this.setState({checkXacThucVanTay: value})
        } catch (e) {
          console.log('loi' + e);
        }
      };
    
    authenticate = (value) => {
        return TouchID.authenticate(
          'Xác thực dấu vân tay của bạn để tiếp tục',
          optionalConfigObject,
        )
          .then(success => {
            //AlertIOS.alert('Authenticated Successfully');
            this.refs.toast.show('Xác thực thành công', ToastAndroid.LONG)
            this.setXacThucVanTay(value);
          })
          .catch(error => {
            this.refs.toast.show('Xác thực thất bại', ToastAndroid.LONG)
            console.log(error);
            //AlertIOS.alert(error.message);
          });
      };

    openShare = () => {
        const options = {
            title: 'Chia sẻ ứng dụng',
            url: 'https://apps.apple.com/us/app/tandan-c/id1479508017',
        };
        Share.open(options)
        .then((res) => { console.log(res) })
        .catch((err) => { err && console.log(err); });
    }

    alertLogout = () => {
        Alert.alert(
          'Đăng xuất',
          'Bạn có chắc muốn đăng xuất không? Toàn bộ dữ liệu cá nhân trong ứng dụng sẽ bị xóa',
          [
            {text: 'ĐÓNG', onPress: () => console.log('Thoat')},
            {text: 'ĐĂNG XUẤT', onPress: () => this.buttonLogout()},
          ],
          {cancelable: false},
        );
      }
      buttonLogout = async() => {
        var body = {'token': this.state.userToken, 'tokenDevice': this.state.deviceToken}
        await axios(`${CD_URL}/LogoutUser`, {
          method: 'POST',
          data: body,
        })
        await AsyncStorage.removeItem('userToken')
        await AsyncStorage.removeItem('studentID')
        await AsyncStorage.removeItem('tokenYT')
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
        });
        firebase.messaging().unsubscribeFromTopic('DVCTD');
        setTimeout(() => {this.props.navigation.dispatch(resetAction)}, 500)
      }
    
  render() {
    return (
        <View style={styles.container}>
            <View style={{paddingTop: 30}}>
                <Text style={{fontWeight: 'bold', fontSize: 24, padding: 10}}>Tài khoản</Text>
            </View>
            <View style={{flex: 1}}>
                <ScrollView>
                <Divider style={{backgroundColor: '#F6F6F6', height: 8}} />
                <TouchableOpacity onPress={() => this.props.navigation.navigate("InfoScreen")} style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Avatar titleStyle={{color: '#fff'}} overlayContainerStyle={{backgroundColor: '#22A2D3'}} rounded title={this.state.namekey} size={60} />
                        <View style={{paddingStart: 15,}}>
                            <Text style={{fontWeight: 'bold', fontSize: 16}}>{this.state.userName}</Text>
                            <Text style={{color: '#939393', paddingTop: 5, fontSize: 16}}>{this.state.phoneText}</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={30} color='#969696' />
                </TouchableOpacity>
                <Divider style={{backgroundColor: '#F6F6F6', height: 8}} />
                <TouchableOpacity style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name='cog' type="font-awesome" color='#999999' />
                        <View style={{paddingStart: 15, alignItems: 'center'}}>
                            <Text style={styles.text_title}>Cài đặt</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={30} color='#969696' />
                </TouchableOpacity>
                <TouchableOpacity style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} activeOpacity={0.6}>
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{paddingEnd: 15}}>
                    <Icon name="fingerprint" type="font-awesome" color="#422680" />
                    </View>
                    <Text style={styles.text_title}>Xác thực vân tay</Text>
                </View>
                {/* <Icon
                    name="chevron-right"
                    size={18}
                    color="#52575D"
                    style={{marginEnd: 10}}
                    /> */}
                <ToggleSwitch
                    style={{}}
                    isOn={this.state.checkXacThucVanTay}
                    onColor="#2196F3"
                    label=""
                    labelStyle={{color: 'black', fontWeight: '900'}}
                    size="small"
                    onToggle={isOn => {
                    this.XuLyVanTay(isOn);
                    }}
                />
                </TouchableOpacity>
                <TouchableOpacity style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name='life-ring' type="font-awesome" color='#999999' />
                        <View style={{paddingStart: 15, alignItems: 'center'}}>
                            <Text style={styles.text_title}>Trung tâm trợ giúp</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={30} color='#969696' />
                </TouchableOpacity>
                <TouchableOpacity style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name='info-circle' type="font-awesome" color='#999999' />
                        <View style={{paddingStart: 15, alignItems: 'center'}}>
                            <Text style={styles.text_title}>Thông tin ứng dụng</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={30} color='#969696' />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.openShare()} style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name='share-alt' type="font-awesome" color='#999999' />
                        <View style={{paddingStart: 15, alignItems: 'center'}}>
                            <Text style={styles.text_title}>Chia sẻ ứng dụng</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={30} color='#969696' />
                </TouchableOpacity>
                <Divider style={{backgroundColor: '#F6F6F6', height: 8}} />
                <TouchableOpacity onPress={() => {this.alertLogout()}} style={{padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name='sign-out-alt' type="font-awesome" color='#999999' />
                        <View style={{paddingStart: 15, alignItems: 'center'}}>
                            <Text style={styles.text_title}>Đăng xuất</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={30} color='#969696' />
                </TouchableOpacity>
                </ScrollView>
            </View>
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
