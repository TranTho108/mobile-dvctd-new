import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ToastAndroid, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Image} from "react-native-elements";
import styles from "./Styles/SignUp_PassTabStyles"
import images from '../Themes/Images'
import LinearGradient from 'react-native-linear-gradient';
import axios from "axios"
import {CD_URL} from '../Config/server'
import * as Animatable from 'react-native-animatable';
import {parseString} from "react-native-xml2js"
import { StackActions, NavigationActions } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
import Toast, {DURATION} from 'react-native-easy-toast'

export default class SignUp_PassTab extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          pass : '',
          loading: false,
          success: false,
          error: false,
          repass: '',
          ID: this.props.ID,
          res: null
        };
      }

    checkContent = async() => {
      if(this.state.pass && this.state.repass){
        if(this.state.pass !== this.state.repass){
          this.refs.toast.show('Mật khẩu nhập lại không đúng', ToastAndroid.LONG)
        }
        else{
          this.setState({loading: true})
          var a = await axios({
            method: 'POST',
            url: `${CD_URL}/CreateUserRegister`,
            data: JSON.stringify(
                {	
                  user: this.state.ID,
                  pass: this.state.pass
                }
            ),
            headers: {'Content-Type': 'application/json'},
          })
          .then(response => {
              console.log(response.data)
              return response.data
          })
          .catch(() => this.refs.toast.show('Lỗi kết nối', ToastAndroid.LONG + setTimeout(() => {this.setState({loading: false})}, 1000), ToastAndroid.LONG))
          if(a){
            if(a.error.code == 200){
              setTimeout(() => this.setState({loading: false, success: true, res: a}), 1000)
            }
            else{
              setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show(a.error.userMessage, ToastAndroid.LONG)}, 1000)
            }
          }
          else{
            setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show('Lỗi kết nối', ToastAndroid.LONG)}, 1000)
          }
          
        }
      }
      else{
        this.refs.toast.show('Vui lòng nhập đầy đủ thông tin', ToastAndroid.LONG)
      }
    }

    loginByPass = async(a) => {
      await AsyncStorage.setItem("userToken",a.data.token)
      await AsyncStorage.setItem("userInfo",JSON.stringify(a.data))
      this.refs.toast.show('Đăng nhập thành công', ToastAndroid.LONG)
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'AppNavigation' })],
      });
      SplashScreen.show()
      this.props.navigation.dispatch(resetAction)
    }

    handleChange(page,data){
      this.props.handleChangePage(page);
      this.props.handleChangeData(data)
    }

    renderBody = () => {
      if(this.state.success){
        return(
          <View style={styles.container}>
            <Image source={images.background.checked} style={{padding: 10, width: 120, height: 120}} />
            <Text style={{padding: 10, fontSize: 16, fontWeight: 'bold'}}>Tài khoản được tạo thành công</Text>
            <Button
                title='Đăng nhập'
                buttonStyle={{borderRadius: 20,paddingStart: 100,paddingEnd: 100}}
                containerStyle={{marginTop: 30,height: 50}}
                onPress={() => this.loginByPass(this.state.res)}
                type='outline'
            />
          </View>
        )
      }
      else{
        return(
          <View style={styles.container}>
            <Image source={images.background.key} style={{padding: 10, width: 120, height: 120}} />
            <Text style={{padding: 10, fontSize: 16, fontWeight: 'bold'}}>Tạo Mật Khẩu</Text>
            <Text style={{padding: 5, textAlign: 'center', paddingStart: 20, paddingEnd: 20}}>Mật khẩu phải chứa 6-8 ký tự và không bao gồm các ký tự đặc biệt</Text>
            <TextInput secureTextEntry placeholderTextColor='#9E9E9E' style={{color: '#757575', borderBottomWidth: 0.5, borderBottomColor: '#757575', margin: 10, alignSelf: 'stretch'}} value={this.state.pass} placeholder='Nhập mật khẩu' onChangeText={(text) => this.setState({pass: text})}/>
            <TextInput secureTextEntry placeholderTextColor='#9E9E9E' style={{color: '#757575', borderBottomWidth: 0.5, borderBottomColor: '#757575', margin: 10, alignSelf: 'stretch'}} value={this.state.repass} placeholder='Nhập lại mật khẩu' onChangeText={(text) => this.setState({repass: text})}/>
            {this.renderButtonLogin()}
          </View>
        )
      }
    }

    renderButtonLogin = () => {
      if(!this.state.loading){
        return(
          <Button
          title='Tiếp tục'
          buttonStyle={{borderRadius: 20,paddingStart: 100,paddingEnd: 100}}
          containerStyle={{marginTop: 30,height: 50}}
          onPress={() => this.checkContent()}
      />
        )
      }
      else{
        return(
          <Animatable.View animation='fadeIn' style={{alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{marginTop: 30}} color='#1E88E5' />
            <Text style={{textAlign: 'center', paddingTop: 10}}>Vui lòng đợi trong giây lát</Text>
          </Animatable.View>
        )
      }
    }


  render() {
    return (
        <View style={styles.container}>
          {this.renderBody()}
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
