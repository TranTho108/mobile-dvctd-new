import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements'

import LinearGradient from 'react-native-linear-gradient';

import styles from './Styles/YT_BHYT_TabBarStyles'

class MainBody_TabBar extends React.Component {
  icons = [];
  constructor(props) {
    super(props);
    this.icons = [];
  }

  textStyle = (active,i) => {
    return {
      fontSize: 10,
      color: active === i ? '#CB1F2F' : '#A9AAAE'
    }
  }

  render() {
    var logo = ['home', 'mail', 'map', 'person']
    return <View style={[styles.tabs, this.props.style, ]}>
              <LinearGradient
                colors={['transparent', 'rgba(0,0,0,0.1)']}
                style={{
                    left: 0,
                    right: 0,
                    height: 1,
                    top: -1,
                    position: 'absolute'
                }}
              />
      {this.props.tabs.map((tab, i) => {
        icon = logo[i]
        return <TouchableOpacity key={tab} onPress={() => this.props.goToPage(i)} style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Icon
            name={icon}
            size={28}
            color={this.props.activeTab === i ? '#CB1F2F' : '#A9AAAE'}
            ref={(icon) => { this.icons[i] = icon; }}
          />
          <Text style={this.textStyle(this.props.activeTab,i)}>{tab}</Text>
        </TouchableOpacity>;
      })}
    </View>;
  }
}

export default MainBody_TabBar;