import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, TouchableOpacity, Image, Platform, Linking, ImageBackground,Animated} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DGN_DetailBodyStyles"
import images from "../Themes/Images"
import Modal from "react-native-modal";
import SwipeableGmail from "./SwipeableGmail"
import {  RectButton } from 'react-native-gesture-handler';


export default class DGN_DetailBody extends Component {

    constructor () {
        super()
        this.state = {
            name: '',
            data: [],
            visibleModal: false,
            detail: [],
            history_DGN: []
        }
    }

    componentWillMount = async() => {
        var name = this.props.navigation.getParam('name')
        var data = this.props.navigation.getParam('data')
        var history_DGN = await AsyncStorage.getItem('history_DGN')
        history_DGN1 = history_DGN?JSON.parse(history_DGN):[]
        this.setState({name: name, data: data, history_DGN: history_DGN1})
    }

    dialCall = (number) => {
        let phoneNumber = '';
        if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
        else {phoneNumber = `telprompt:${number}`; }
        Linking.openURL(phoneNumber);
     };

     addPID = async(PID) => {
        var history_DGN = await AsyncStorage.getItem('history_DGN')
        var arr = history_DGN ? JSON.parse(history_DGN) : [];
        arr.push(PID)
        var arr1 = [...new Set([...arr])]
        this.setState({history_DGN: arr1})
        AsyncStorage.setItem('history_DGN', JSON.stringify(arr1));
     }

     removePID = async(PID) => {
        var arr = this.state.history_DGN
        var data = this.state.data
        var i = arr.indexOf(PID);
        if (i != -1) {
          arr.splice(i,1);
        }
        await AsyncStorage.setItem('history_DGN', JSON.stringify(arr));
        this.setState({history_DGN: arr})
        if(this.state.name === 'Yêu thích'){
            var j = data.findIndex((i) => {return i.PID === PID})
            data.splice(j,1)
            this.setState({data: data})
        }
     }

     renderEmpty = () => {
        return (
          <View style={{padding: 10, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Không có dữ liệu</Text>
          </View>
        )
      }
    renderFavorite = (PID) => {
        if(this.state.history_DGN.includes(PID)){
            return(
                <Icon onPress = {() => this.removePID(PID)} size={30} name='favorite' color='#f44336' />
            )
          }
          else{
            return(
                <Icon onPress = {() => this.addPID(PID)} size={30} name='favorite-border' />
            )
          }
    }
    
    SwipeableRow = ({ item, index }) => {
        return (
            <SwipeableGmail item={item}>
              <RectButton style={styles.rectButton} onPress={() => this.setState({visibleModal: true, detail: item})}>
                <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', flex: 1}}>
                        <Image resizeMode='stretch' style={{width: 60, height: 60}} source={{uri: item.img}} />
                        <View>
                            <Text style={{fontSize: 16, paddingStart: 10, padding: 5,color: '#212121'}}>{item.name}</Text>
                            <Text style={{fontSize: 12, paddingStart: 10, color: '#9E9E9E'}}>{item.phone}</Text>
                        </View>
                    </View>
                    <View style={{height: 40, width: 40, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent', borderWidth: 1, borderColor: '#9E9E9E'}}>
                        <Icon size={18} type='font-awesome' name='info' color='#9E9E9E' />
                    </View>
                </TouchableOpacity>
                </RectButton>
            </SwipeableGmail>
          );
      }

      _renderModalContent = () => {
        if(this.state.detail){
        const {detail} = this.state
        return(
          <View style={styles.content2}>
            <ImageBackground imageStyle={{}} source={images.background.dgn_bg} style={{height: 150, justifyContent: 'flex-end', padding: 10}}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                        <Image resizeMode='cover' source={{uri: detail.img}} style={{width: 80, height: 80, borderRadius: 40}} />
                        <Text style={{padding: 10, fontWeight: 'bold'}}>{detail.name}</Text>
                    </View>
                    {this.renderFavorite(detail.PID)}
                </View>
            </ImageBackground>
            <View style={{padding: 20}}>
                <Text style={styles.section}>SĐT</Text>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.label}>{detail.phone}</Text>
                    <TouchableOpacity onPress={() => this.dialCall(detail.phone)} style={{alignItems: 'center', justifyContent:'center', height: 50, width: 50, borderRadius: 50, backgroundColor: '#4CAF50'}}>
                        <Icon size={24} name='phone' color='#fff' />
                    </TouchableOpacity>
                </View>
                <Text style={styles.section}>Thông tin</Text>
                <Text style={styles.label}></Text>
            </View>
            <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
                 <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 10, alignSelf: 'flex-end'}}>ĐÓNG</Text>
            </TouchableOpacity>
          </View>
        )
        }
      }

     _renderItem = ({item}) => {
        return(
                <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', flex: 1}}>
                        <Image resizeMode='stretch' style={{width: 60, height: 60}} source={{uri: item.img}} />
                        <View>
                            <Text style={{fontSize: 16, paddingStart: 10, padding: 5,color: '#212121'}}>{item.name}</Text>
                            <Text style={{fontSize: 12, paddingStart: 10, color: '#9E9E9E'}}>{item.phone}</Text>
                        </View>
                    </View>
                    <View style={{height: 40, width: 40, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent', borderWidth: 1, borderColor: '#9E9E9E'}}>
                        <Icon size={18} type='font-awesome' name='info' color='#9E9E9E' />
                    </View>
                </TouchableOpacity>
        )
    }

    

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}><Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='white' /><Text style={styles.title}>{this.state.name}</Text></View>
            <FlatList 
                data={this.state.data}
                renderItem={this.SwipeableRow}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={this.renderEmpty}
                ItemSeparatorComponent={() => <View style={styles.separator} />}
                extraData={this.state}
            /> 
            <Modal
                onBackdropPress={() => this.setState({visibleModal: false})}
                backdropTransitionOutTiming={0}
                isVisible={this.state.visibleModal}
                style={{margin: 0}}
                hideModalContentWhileAnimating={true}>
            {this._renderModalContent()}
            </Modal>
        </View>
    );
  }
}