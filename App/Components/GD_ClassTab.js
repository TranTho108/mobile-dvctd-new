import React, { Component } from "react";
import { View, AsyncStorage, FlatList, TouchableOpacity, Platform, Linking, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/GD_ClassTabStyles"
import * as Animatable from 'react-native-animatable'
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'

export default class GD_ClassTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        data: [],
        dataStudent: [],
        dataTeacher: [],
        listGV: false,
        loading: true
    }
  }

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem('userToken')
    var NamHocID = await AsyncStorage.getItem('NamHocID')
    var HocKyID = await AsyncStorage.getItem('HocKyID')
    var studentID = await AsyncStorage.getItem('studentID')
    var body = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID, 'HocKyID': HocKyID}
    var data1 = await requestPOST(`${GD_URL}/DanhSachHocSinhTrongLops`, body)
    var data2 = data1.data?data1.data:[]
    var data3 = await requestPOST(`${GD_URL}/GiaoVienGiangDay`, body)
    var data4 = data3.data?data3.data:[]
    this.setState({loading: false, dataStudent: data2,dataTeacher: data4, data: data2})
  }

  changeData = (data) => {
    if(data === 'HS'){
      this.setState({data: this.state.dataStudent, listGV: false})
    }
    else{
      this.setState({data: this.state.dataTeacher, listGV: true})
    }
  }

  dialCall = (number) => {
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else {phoneNumber = `telprompt:${number}`; }
    Linking.openURL(phoneNumber);
 };


  renderGT = (gt) => {
    if(gt === 'Nam'){
      return(
        <Icon name='mars' type='font-awesome' size={24} color='#3F51B5' />
      )
    }
    else{
      return(
        <Icon name='venus' type='font-awesome' size={24} color='#E91E63' />
      )
    }
  }

  renderCall = (phone) => {
    if(phone){
      return(
        <Icon name='phone' type='font-awesome' size={24} color='#757575' onPress={() => this.dialCall(phone)} />
      ) 
    }
  }
    
  _renderItem = ({item,index}) => {
    return(
      <View style={{marginTop: 10, padding: 10, flexDirection: 'row',justifyContent: 'space-between'}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{height: 50,width: 50, borderRadius: 30, backgroundColor: '#2AA5FF', justifyContent: 'center', alignItems: 'center'}}>
            <Icon type='font-awesome' name='user' color='#fff' size={18} />
          </View>
          <View>
            <View style={{flex: 1, paddingStart: 10, alignItems: 'center', flexDirection: 'row'}}>
              <Text style={{paddingEnd: 10}}>{item.Ten}</Text>
              {this.renderGT(item.GioiTinh)}
            </View>
          </View>
        </View>
        <View style={{paddingTop: 10}}>
          {this.renderCall(item.phone)}
        </View>
      </View>
    )
  }

  _renderItem1 = ({item,index}) => {
    return(
      <View style={{marginTop: 10, padding: 10, flexDirection: 'row',justifyContent: 'space-between'}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{height: 50,width: 50, borderRadius: 30, backgroundColor: '#2AA5FF', justifyContent: 'center', alignItems: 'center'}}>
            <Icon type='font-awesome' name='chalkboard-teacher' color='#fff' size={18} />
          </View>
          <View>
            <View style={{flex: 1, paddingStart: 10, alignItems: 'center', flexDirection: 'row'}}>
              <Text style={{paddingEnd: 10}}>{item.HoTen}</Text>
              {this.renderGT(item.GioiTinh)}
            </View>
            <Text style={{color: '#757575',paddingStart: 10}}>{item.MonHoc}</Text>
          </View>
        </View>
        <View style={{paddingTop: 10}}>
          {this.renderCall(item.SoDienThoai)}
        </View>
      </View>
    )
  }

  renderHead = () => {
    if(!this.state.listGV){
      var male = 0
      this.state.dataStudent.map((i) => {if(i.GioiTinh === 'Nam'){male ++}})
      return(
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 20, paddingStart: 20}}>
          <Animatable.View animation='bounceInLeft' delay={100}>
            <Text style={{fontSize: 20,color: 'black'}}>Danh sách học sinh</Text>
            <Text style={{paddingTop: 5, color: '#757575'}}>{male} Nam - {this.state.dataStudent.length - male} Nữ / {this.state.dataStudent.length} Học sinh</Text>
          </Animatable.View>
          <Animatable.View animation='fadeInLeft' delay={100}>
          <TouchableOpacity onPress={() => {this.changeData('GV')}} style={{flexDirection: 'row', padding: 10, paddingStart: 20, paddingEnd: 20, backgroundColor: '#2AA5FF', borderTopLeftRadius: 30, borderBottomLeftRadius: 30, justifyContent: 'center', alignItems: 'center', }}>
            <Icon name='chalkboard-teacher' type='font-awesome' color='#fff' size={18} />
            <Text style={{color: '#fff'}}>    Giáo viên</Text>
          </TouchableOpacity>
          </Animatable.View>
        </View>
      )
    }
    else{
      var male = 0
      this.state.dataTeacher.map((i) => {if(i.GioiTinh === 'Nam'){male ++}})
      return(
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 20, paddingStart: 20}}>
          <Animatable.View animation='bounceInLeft' delay={100}>
            <Text style={{fontSize: 20,color: 'black'}}>Danh sách giáo viên</Text>
            <Text style={{paddingTop: 5, color: '#757575'}}>{male} Thầy - {this.state.dataTeacher.length - male} Cô/ {this.state.dataTeacher.length} Giáo viên</Text>
          </Animatable.View>
          <Animatable.View animation='fadeInLeft' delay={100}>
          <TouchableOpacity onPress={() => {this.changeData('HS')}} style={{flexDirection: 'row', padding: 10, paddingStart: 20, paddingEnd: 20, backgroundColor: '#2AA5FF', borderTopLeftRadius: 30, borderBottomLeftRadius: 30, justifyContent: 'center', alignItems: 'center', }}>
            <Icon name='user' type='font-awesome' color='#fff' size={18} />
            <Text style={{color: '#fff'}}>    Học sinh</Text>
          </TouchableOpacity>
          </Animatable.View>
        </View>
      )
    }
  }
    
  render() {
    return (
        <View style={styles.container}>
          {this.renderHead()}
          <View style={{padding: 20}}>
            {this.state.loading?
              <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
              </View>
            :
              this.state.listGV?
                <FlatList 
                  data={this.state.data}
                  renderItem={this._renderItem1}
                  keyExtractor={(item, index) => index.toString()}
                  extraData={this.state}
                />
              :
                <FlatList 
                  data={this.state.data}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index.toString()}
                  extraData={this.state}
                />
            }
          </View>
        </View>
    );
  }
}
