import React, { Component } from "react";
import { View, AsyncStorage, FlatList, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Tooltip} from "react-native-elements";
import styles from "./Styles/GD_MenuTabStyles"
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import Modal from 'react-native-modal';
import RadioForm from 'react-native-simple-radio-button';
var moment = require('moment')
moment.locale('vi')

export default class GD_TKB_MainBody extends Component {

  constructor(props) {
    super(props);

    this.state = {
        loading: true,
        dataTKBs: [],
    }
  }

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem('userToken')
    var NamHocID = await AsyncStorage.getItem('NamHocID')
    var HocKyID = await AsyncStorage.getItem('HocKyID')
    var studentID = await AsyncStorage.getItem('studentID')
    var body1 = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID, 'HocKyID': HocKyID}
    var data3 = await requestPOST(`${GD_URL}/ThoiKhoaBieu`, body1)
    var data4 = data3.data?data3.data:[]
    this.setState({loading: false, dataTKBs: data4})
  }

  _renderItem = ({item,index}) => {
    return(
      <View>
        <Text style={{textAlign: 'center', padding: 10, fontWeight: 'bold', fontSize: 16}}>Thứ {item.Thu}</Text>
        <FlatList 
          data={item.List}
          renderItem={this._renderItem1}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={EmptyFlatlist}
        />
      </View>
    )
  }

  _renderItem1 = ({item,index}) => {
    return(
      <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, borderBottomColor: '#e8e8e8', borderBottomWidth: 1, padding: 5}}>
        <Text>Tiết {item.TietHoc}</Text>
        <Text>{item.MonHoc}</Text>
      </View>
    )
  }

  renderBody = () => {
    if(!this.state.loading){
      return(
        <View style={{flex: 1}}>
            <FlatList 
              data={this.state.dataTKBs}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
              contentContainerStyle={{padding: 10}}
            />
          </View>
      )
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
        </View>
      )
    }
  }
    
  render() {
    return (
        <View style={styles.container}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#2AA5FF', padding: 15, paddingTop: 35}}>
            <Icon onPress={() => {this.props.navigation.goBack()}} name='chevron-left' type='font-awesome' color='#fff' size={24} />
            <Text style={styles.text3}>Thời khoá biểu</Text>
            <Icon name='arrow-left' type='font-awesome' color='transparent' size={24} />
          </View>
          {this.renderBody()}
        </View>
    );
  }
}
