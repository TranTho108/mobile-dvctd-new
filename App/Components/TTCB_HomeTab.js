import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, ActivityIndicator, TouchableOpacity, ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/TTCB_HomeTabStyles"
import {requestPOST, requestGET} from '../Api/Basic'
import {CB_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import Modal from "react-native-modal";

export default class TTCB_HomeTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dataInformation: [],
      loading: true,
      detail: [],
      visibleModal: false
    };
  }

  componentWillMount = async() => {
    var dataInformation = await requestGET(`${CB_URL}/information`)
    this.setState({dataInformation: dataInformation.data, loading: false})
  }

  _renderModalContent = () => {
    if(this.state.detail){
    const {detail} = this.state
    var MĐ = detail.Level == 0?'Thông tin':'Cảnh báo'
    return(
      <View style={styles.content2}>
        <ScrollView style={{padding: 20}}>
          <Text style={styles.title5}>Chi tiết cảnh báo</Text>
          <Text style={styles.section}>Tên</Text>
          <Text style={styles.label}>{detail.Title}</Text>
          <Text style={styles.section}>Đơn vị</Text>
          <Text style={styles.label}>{detail.Organization}</Text>
          <Text style={styles.section}>Lĩnh vực</Text>
          <Text style={styles.label}>{detail.Category}</Text>
          <Text style={styles.section}>Mức độ</Text>
          <Text style={styles.label}>{MĐ}</Text>
          <Text style={styles.section}>Nội dung</Text>
          <Text style={styles.label}>{detail.Content}</Text>
          <Text style={styles.section}>Thời gian</Text>
          <Text style={styles.label}>{detail.CreatedAt}</Text>
          <Text style={styles.section}></Text>
          <Text style={styles.label}></Text>
        </ScrollView>
        <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
             <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
        </TouchableOpacity>
      </View>
    )
    }
  }

  _renderItem = ({item}) => {
    return(
      <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{backgroundColor: '#fff', borderRadius: 8, borderWidth: 0.4, borderColor: '#e8e8e8', padding: 10, margin: 5}}>
        <View style={{flexDirection: 'row', paddingBottom: 10, alignItems: 'center'}}>
          <Icon color='#37BCB3' name={item.CategoryIcon} type='font-awesome' size={24} containerStyle={{width: 40}} />
          <View>
            <Text style={{fontWeight:'600'}}>{item.Organization}</Text>
            <Text>{item.CreatedAt}</Text>
          </View>
        </View>
        <View style={{paddingStart: 10}}>
        <Text style={{fontWeight: '600', color: 'black', fontSize: 16}}>{item.Title}</Text>
        <View style={{flexDirection: 'row', paddingTop: 10}}>
          <Icon type='font-awesome' name='map-marker-alt' color='#e8e8e8' size={16} />
          <Text style={{paddingStart: 10}}>{item.LocationName}</Text>
        </View>
        </View>
      </TouchableOpacity>
    )
  }

  renderBody = () => {
    if(!this.state.loading){
      return(
        <FlatList
          showsVerticalScrollIndicator={false}
          data = {this.state.dataInformation}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
          ListEmptyComponent={EmptyFlatlist}
        />
      )
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#D84315' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#D84315'}}>Đang lấy dữ liệu</Text>
        </View>
      )
    }
  }

  render() {
    return (
        <View style={styles.container}>
          {this.renderBody()}
          <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
        </View>
    );
  }
}
