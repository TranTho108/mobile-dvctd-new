import React, { Component } from 'react'
import { View,TouchableOpacity, ImageBackground, AsyncStorage, ToastAndroid, StatusBar, Dimensions, Alert, ScrollView} from 'react-native'
import {Text, Button, Icon, Avatar} from "react-native-elements";
import styles from './Styles/SideMenuStyles'
import Modal from "react-native-modal";
import SplashScreen from 'react-native-splash-screen'
import { StackActions, NavigationActions } from 'react-navigation';
import firebase from 'react-native-firebase';
import Toast, {DURATION} from 'react-native-easy-toast'

const height = Math.max(Dimensions.get('window').height, Dimensions.get('screen').height)
export default class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      namekey: '',
      userName: '',
      userInfo: [],
      visibleModal1: false
    }
  }

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem("userToken")
    if(userToken){
        userInfo = await AsyncStorage.getItem("userInfo")
        info = JSON.parse(userInfo)
        var b = info.fullName.split('').reverse().join('')
        var name = b.charAt(b.indexOf(' ') -1)
        var name2 = info.fullName.charAt(0).toUpperCase()
        this.setState({namekey: name+name2, userName: info.fullName, userInfo: info})
    }
  }

  render () {
    return (
        <ScrollView style={styles.container}>
            <ImageBackground resizeMode='cover' style={{width: null, height: 150}} source={{uri: 'https://img4.goodfon.com/wallpaper/nbig/a/8b/vector-blue-squares-abstract-background-geometric.jpg'}}>
                <View style={{paddingTop: 35, paddingStart: 20, justifyContent: 'center'}}>
                <Avatar onPress={() => this.props.navigation.navigate('InfoScreen')} titleStyle={{color: '#fff'}} overlayContainerStyle={{backgroundColor: '#43A047'}} rounded title={this.state.namekey} size={60} />
                </View>
            </ImageBackground>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name='home' type="font-awesome" /><Text style={styles.title1}>Trang chủ</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("DVC_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="users" type="font-awesome" /><Text style={styles.title1}>Dịch vụ hành chính công</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("PAHT_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="mail-bulk" type="font-awesome" /><Text style={styles.title1}>Phản ánh hiện trường</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("DL_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="umbrella-beach" type="font-awesome" /><Text style={styles.title1}>Du lịch</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("TTCB_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="exclamation-triangle" type="font-awesome" /><Text style={styles.title1}>Thông tin cảnh báo</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("TĐTM_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="phone-volume" type="font-awesome" /><Text style={styles.title1}>Tổng đài thông minh</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("DN_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="clipboard-list" type="font-awesome" /><Text style={styles.title1}>Điện nước</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("GD_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="graduation-cap" type="font-awesome" /><Text style={styles.title1}>Giáo dục</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("YT_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="hospital-alt" type="font-awesome" /><Text style={styles.title1}>Y tế</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("NN_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="tractor" type="font-awesome" /><Text style={styles.title1}>Nông nghiệp</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("GCTT_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="chart-line" type="font-awesome" /><Text style={styles.title1}>Giá cả thị trường</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("MT_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="cannabis" type="font-awesome" /><Text style={styles.title1}>Môi trường</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => this.props.navigation.navigate("ATTP_MainScreen")}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="user-shield" type="font-awesome" /><Text style={styles.title1}>An toàn thực phẩm </Text></TouchableOpacity>

            <TouchableOpacity  style={styles.viewBorder} onPress={() => {this.refs.toast.show('Chức năng đang được triển khai', ToastAndroid.LONG)}}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="life-ring" type="font-awesome" /><Text style={styles.title1}>Hỗ trợ</Text></TouchableOpacity>
            <TouchableOpacity style={styles.view1} onPress={() => {this.refs.toast.show('Chức năng đang được triển khai', ToastAndroid.LONG)}}><Icon containerStyle={styles.icon1} size={24} color='#9E9E9E' name="user-tag" type="font-awesome" /><Text style={styles.title1}>Điều khoản</Text></TouchableOpacity>
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </ScrollView>
    )
  }
}

