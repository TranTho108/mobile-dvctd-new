import React, { Component } from "react";
import { Platform,View, TextInput, Dimensions, Picker, TouchableOpacity, Image, FlatList, ScrollView, ToastAndroid, AsyncStorage, ActivityIndicator,Modal,Animated, Keyboard, ImageBackground} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_TKHS_SearchBodyStyle"
import {requestGET, requestPOST} from "../Api/Basic"
import * as Animatable from 'react-native-animatable';
import EmptyFlatlist from './EmptyFlatlist'
import LinearGradient from 'react-native-linear-gradient';
import SearchFileByCode from "../Api/GetFileByCode"
import Toast, {DURATION} from 'react-native-easy-toast'
 
import images from '../Themes/Images'

export default class DVC_TKHS_SearchBody extends Component {
  constructor(props) {
  super(props);

	this.state = {
      donvi: '',
      code: '',
      empty: false,
      data: [],
      loading: false,
      history: [],
    }
  }
  componentWillMount= async() => {
    
  }

  componentDidMount(){
    this.subs = [
    this.props.navigation.addListener('didFocus', () => {
      this._getHistory()
      if(this.props.navigation.state.params.code){
      this.setState({code : this.props.navigation.state.params.code})
    }
  })]
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  SearchFile = async() => {
    Keyboard.dismiss()
      if(this.state.code.length == 0){
        this.refs.toast.show(`Nhập mã hồ sơ`, ToastAndroid.LONG)
      }
      else if(this.state.code.length < 5 || this.state.code.length > 26){
        this.refs.toast.show(`Mã hồ sơ phải chứa từ 5-26 ký tự`, ToastAndroid.LONG)
      }
      else{
        this.setState({data: [],loading: true})
        var data = await SearchFileByCode(this.state.code)
        if(data.length == 0){
          this.refs.toast.show(`Không tìm thấy hồ sơ`, ToastAndroid.LONG);
          this.setState({loading: false})
        }
        else{
          this.refs.toast.show(`Đã tìm thấy `+ data.length +` hồ sơ. Nhấn vào hồ sơ để xem chi tiết`, ToastAndroid.LONG);
          this.setState({loading: false, data: data})
        }
      }
}

  PushItem = async (MaHoSo,MaDonVi) => {
    /*const xmlReqBody = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">   <soap:Body>     <SaveToken xmlns="http://schemas.microsoft.com/sharepoint/soap/">       <maHoSo>`+ MaHoSo +`</maHoSo><maDonVi>`+ MaDonVi +`</maDonVi><token>`+ this.state.token +`</token>     </SaveToken>   </soap:Body> </soap:Envelope>`
      await axios('http://dichvucong.thanhhoa.gov.vn/_layouts/tandan/dvc/DVCService.asmx?wsdl', {
        method: 'POST',
        headers: {
          "Content-Type": 'text/xml; charset="utf-8"',
        },
        data: xmlReqBody,
        dataType: "xml",
      })
      .then((response)=> console.log(response))
      .catch((err)=> console.log(err))*/
    await AsyncStorage.getItem('history')
    .then((history) => {
        var arr = history ? JSON.parse(history) : [];
        arr.push(MaHoSo)
        var arr1 = [...new Set([...arr])]
        this.setState({history: arr1})
        console.log(JSON.stringify(arr1))
      AsyncStorage.setItem('history', JSON.stringify(arr1));
      this.refs.toast.show(`Đã thêm hồ sơ vào danh sách theo dõi`, ToastAndroid.LONG);
  })
    .catch((err)=> console.log(err))
  }

  _getHistory = async() => {
    await AsyncStorage.getItem('history')
    .then((history) => {
        const arr = history ? JSON.parse(history) : [];
        this.setState({history: arr})
  })
    .catch((err)=> console.log(err))
  }

  _checkItem = (MaHoSo,MaDonVi) => {
    if(this.state.history.includes(MaHoSo)){
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#FF7043', padding: 5, alignItems: 'center', flexDirection: 'row',alignSelf: 'stretch', justifyContent: 'center'}} onPress = {() => this._deleteItem(MaHoSo)}>
            <Icon 
              name='delete-sweep'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}> BỎ THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
    else{
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#03A9F4', padding: 5, alignItems: 'center', flexDirection: 'row',alignSelf: 'stretch', justifyContent: 'center'}} onPress = {() => this.PushItem(MaHoSo,MaDonVi)}>
            <Icon 
              name='near-me'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}>  THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
  }

  _deleteItem = async (MaHoSo) => {
    var arr = this.state.history
    var i = arr.indexOf(MaHoSo);
    if (i != -1) {
      arr.splice(i,1);
    }
    await AsyncStorage.setItem('history', JSON.stringify(arr));
    this.refs.toast.show(`Đã bỏ theo dõi hồ sơ`, ToastAndroid.LONG);
    this.setState({history: arr})
  }
 
  showLoading = () => {
    if(this.state.loading){
        return ( <ActivityIndicator size='large' style={{marginTop: 20}} color='#1E88E5' />)
    }
    else{
      return (
        <TouchableOpacity onPress={() => this.SearchFile()} style={{height: 60, width: 60, borderRadius: 60, backgroundColor: '#00897B', alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: 10, marginBottom: 10}} >
          <Icon name='search' color='#fff' />
        </TouchableOpacity>
      )
    }
    }

  _renderItem = ({item,index}) => {
    return (
      <Animatable.View delay={index*300} animation='fadeInRight'>
      <TouchableOpacity onPress = {() => {this.props.navigation.navigate("DVC_TKHS_DetailScreen", {data: item})}} style={{flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#e8e8e8', paddingBottom: 10}} >
      <View>
        <View style={{backgroundColor: '#00897B', margin: 10, borderRadius: 5, alignItems: 'center'}}><Icon name='file-alt' type='font-awesome' color='#fff' containerStyle={{padding: 20}} /></View>
      </View>
      <View style={{margin: 10, flex: 1}}>
        <Text style={{fontWeight: 'bold', fontSize: 20,color: 'black'}}>{item.MaHoSo}</Text>
        <Text style={{ color: 'black', paddingTop: 10}}>{item.TenHoSo}</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}><Icon color='#e8e8e8' size={20} name='user-alt' type='font-awesome' /><Text style={{paddingStart: 5}}>{item.ChuHoSo}</Text></View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}><Icon color='#e8e8e8' size={20} name='calendar-alt' type='font-awesome' /><Text style={{paddingStart: 5}}>{item.NgayTiepNhan}</Text></View>
        </View>
        <View style= {{ alignItems: 'center', paddingTop: 10, alignSelf: 'stretch'}}>
        {this._checkItem(item.MaHoSo, item.MaDonVi)}
    </View>
      </View>
    </TouchableOpacity>
    </Animatable.View>
    )}

  render() { 
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <ImageBackground resizeMode='cover' source={images.background.trongdong} style={{flex:1}} imageStyle= {{opacity:0.1}} >
          <View style={styles.linearGradient1}><Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='white' /><Text style={styles.title}>Tìm kiếm hồ sơ</Text></View>
          <LinearGradient
            colors={['rgba(0,0,0,0.1)', 'transparent']}
            style={{
                left: 0,
                right: 0,
                height: 5,
            }}
          />
          <ScrollView style={styles.container}>
        <TextInput
            autoCapitalize="none"
            style={styles.textInput}
            placeholder="Nhập mã hồ sơ"
            onChangeText={(code) => this.setState({code: code})}
            value={this.state.code}
            underlineColorAndroid={'transparent'}
        />
        <View style={styles.qrscan}><Text>Hoặc quét mã BR</Text>
        <TouchableOpacity style={{height:40, width:50,borderRadius: 2,borderColor: "#e8e8e8",borderWidth: 0.5,marginLeft: 10}} onPress={() => this.props.navigation.navigate("DVC_TKHS_ScanScreen")} >  
            <Image 
               source={{uri: "https://static.thenounproject.com/png/74445-200.png"}} 
               style={{height:40, width:50}} />
          </TouchableOpacity>
        </View>
        {this.showLoading()}
          <FlatList
                data={this.state.data}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                //ListEmptyComponent={this.showEmptyListView()}
                extraData={this.state}
        />
        </ScrollView>
        <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
      </ImageBackground>
        </View>
    );
  }
}
