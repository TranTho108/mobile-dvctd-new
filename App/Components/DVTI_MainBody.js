import React, { Component } from "react";
import {View,ScrollView,Animated,Image,Dimensions,TouchableOpacity,StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/DVTI_MainBodyStyles'
import Modal from 'react-native-modal';

import MapView,{MarkerAnimated, PROVIDER_GOOGLE} from "react-native-maps";

const markers = [
  {
    coordinate: {
      latitude: 21.0265214,
      longitude: 105.8280145,
    },
    title: "Cửa hàng xăng dầu Hòa Nam",
    loai: "Trạm xăng",
    phone: '+84 90 347 96 19',
    DiaChi: "154 Hào Nam, Chợ Dừa, Đống Đa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipNPCn_nmTMWzcwH1xeJc-61pqv-M3QefBNGs2Z_=w426-h240-k-no" },
  },
  {
    coordinate: {
      latitude: 21.0193221,
      longitude: 105.8323932,
    },
    title: "Cửa Hàng Xăng Dầu Khâm Thiên",
    loai: "Trạm xăng",
    phone: '+84 90 235 12 19',
    DiaChi: "233 Khâm Thiên, Thổ Quan, Đống Đa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipNQjMQawfKa3zHBHgcFttt99kJSG2PKpq-ngZ8t=w426-h240-k-no" },
  },
  {
    coordinate: {
      latitude: 21.0261822,
      longitude: 105.8318272,
    },
    title: "Trường Cao đẳng Y tế Hà Nội",
    loai: "Trường học",
    phone: '+84 87 567 66 19',
    DiaChi: "35 Đoàn Thị Điểm, Quốc Tử Giám, Đống Đa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipNYZPWorxvUH1ad2_N-rMAPNHneVRh1ZkB9Zouy=w408-h305-k-no" },
  },
  {
    coordinate: {
      latitude: 21.0204288,
      longitude: 105.8379722,
    },
    title: "Trường Tiểu học Văn Chương",
    loai: "Trường học",
    phone: '+84 68 657 96 12',
    DiaChi: "323 Ngõ Văn Chương, Văn Chương, Đống Đa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipNUl_J2XIniEZ4zDkLW7VnCU4aut9CI6w-peDeZ=w408-h306-k-no" },
  },
  {
    coordinate: {
      latitude: 21.0174443,
      longitude: 105.8348541,
    },
    title: "Trường Tiểu học La Thành",
    loai: "Trường học",
    phone: '+84 65 234 22 33',
    DiaChi: "ngách 79 Thổ Quan, Đống Đa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipPKETUFI9YnDITLAfrqcVvcq84A9isdTw3ua8mj=w408-h725-k-no" },
  },
  {
    coordinate: {
      latitude: 21.025689,
      longitude: 105.8353275,
    },
    title: "Bệnh viện đa khoa tư nhân Tràng An",
    loai: "Bệnh viện",
    phone: '+84 85 756 96 78',
    DiaChi: "59 Ngõ Thông Phong, Quốc Tử Giám, Đống Đa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipNRMJrc48unuFcgMWXe9CcYoiZff869fb3GkrGN=w408-h507-k-no" },
  },
  {
    coordinate: {
      latitude: 21.028135,
      longitude: 105.8385864,
    },
    title: "Bệnh viện Da liễu Hà Nội",
    loai: "Bệnh viện",
    phone: '+84 23 443 53 89',
    DiaChi: "79B Phố Nguyễn Khuyến, Văn Miếu, Đống Đa, Hà Nội 115715, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipO8ERZvgPG8vZHs4FvFU5rjPhuWUJ5sONi9ZoRp=w408-h394-k-no" },
  },
  {
    coordinate: {
      latitude: 21.0192671,
      longitude: 105.8279206,
    },
    title: "Nhà hàng Nấm Việt Hà Thành",
    loai: "Nhà hàng",
    phone: '+84 91 555 53 00',
    DiaChi: "11 Ô Chợ Dừa, Chợ Dừa, Hà Nội, Việt Nam",
    image: { uri: "https://lh5.googleusercontent.com/p/AF1QipMGM3I6k2ceBDzTs_S6cq_R9jYgqzorW8ThsRrz=w408-h286-k-no" },
  },
]

const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = width - 80;

export default class DVTI_MainBody extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      region: {
        latitude: markers[0].coordinate.latitude,
        longitude: markers[0].coordinate.longitude,
        latitudeDelta: 0.004,
        longitudeDelta: 0.004,
      },
      markers: markers,
      visibleModal1: false,
      loaiDisplay: 'Lọc theo'
      }
    }

  componentWillMount() {
    this.index = 0;
    this.animation = new Animated.Value(0);
  }
  componentDidMount() {
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here
    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.2); // animate 30% away from landing on the next item
      if (index >= this.state.markers.length) {
        index = this.state.markers.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }

      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          const { coordinate } = this.state.markers[index];
          this.map.animateToRegion(
            {
              ...coordinate,
              latitudeDelta: this.state.region.latitudeDelta,
              longitudeDelta: this.state.region.longitudeDelta,
            },
            350
          );
        }
      }, 10);
    });
  }

  changePlace = (loai) => {
    if(loai !== 'Tất cả'){
      var markers1 = markers.filter(i => i.loai === loai)
      setTimeout(() => {
          this.map.animateToRegion(
            {
              latitude: markers1[0].coordinate.latitude,
              longitude: markers1[0].coordinate.longitude,
              latitudeDelta: 0.004,
              longitudeDelta: 0.004,
            },
            350
          );
      }, 10);
      this.setState({visibleModal1: false, loaiDisplay: loai, markers: markers1})
    }
    else{
      setTimeout(() => {
        this.map.animateToRegion(
          {
            latitude: markers[0].coordinate.latitude,
            longitude: markers[0].coordinate.longitude,
            latitudeDelta: 0.004,
            longitudeDelta: 0.004,
          },
          350
        );
    }, 10);
      this.setState({visibleModal1: false, loaiDisplay: loai, markers: markers})
    }
  }

  renderModalContent1 = () => {
    return(
      <View style={styles.content2}>
        <StatusBar backgroundColor="rgba(0,0,0,0.8)" barStyle= 'dark-content' translucent={true} />
        <View style={{padding: 20}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity onPress={() => this.changePlace('Tất cả')} style={{alignItems: 'center', justifyContent: 'center',}}>
            <View style={{alignItems: 'center', justifyContent: 'center', height: 60, width: 60, borderRadius: 60, backgroundColor: '#E5454C'}}>
              <Icon type='font-awesome' name='globe' color='#fff' />
            </View>
            <Text style={{paddingTop: 5}}>Tất cả</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changePlace('Trạm xăng')} style={{alignItems: 'center', justifyContent: 'center',}}>
            <View style={{alignItems: 'center', justifyContent: 'center', height: 60, width: 60, borderRadius: 60, backgroundColor: '#F538A0'}}>
              <Icon type='font-awesome' name='gas-pump' color='#fff' />
            </View>
            <Text style={{paddingTop: 5}}>Trạm xăng</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changePlace('Nhà hàng')} style={{alignItems: 'center', justifyContent: 'center',}}>
            <View style={{alignItems: 'center', justifyContent: 'center', height: 60, width: 60, borderRadius: 60, backgroundColor: '#FAA33F'}}>
              <Icon type='font-awesome' name='utensils' color='#fff' />
            </View>
            <Text style={{paddingTop: 5}}>Nhà hàng</Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-evenly', paddingTop: 10}}>
            <TouchableOpacity onPress={() => this.changePlace('Trường học')} style={{alignItems: 'center', justifyContent: 'center',}}>
            <View style={{alignItems: 'center', justifyContent: 'center', height: 60, width: 60, borderRadius: 60, backgroundColor: '#885931'}}>
              <Icon type='font-awesome' name='graduation-cap' color='#fff' />
            </View>
            <Text style={{paddingTop: 5}}>Trường học</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changePlace('Bệnh viện')} style={{alignItems: 'center', justifyContent: 'center',}}>
            <View style={{alignItems: 'center', justifyContent: 'center', height: 60, width: 60, borderRadius: 60, backgroundColor: '#2196F3'}}>
              <Icon type='font-awesome' name='hospital' color='#fff' />
            </View>
            <Text style={{paddingTop: 5}}>Bệnh viện</Text>
            </TouchableOpacity>

          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'flex-end', alignSelf: 'stretch', padding: 10}}>
          <TouchableOpacity onPress={() => {this.setState({visibleModal1: false})}}>
            <Text style={{fontSize: 14, fontWeight: 'bold', color: '#2196F3'}}>ĐÓNG</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH,
        index * CARD_WIDTH,
        ((index + 1) * CARD_WIDTH),
      ];
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: "clamp",
      });
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0, 1, 0],
        extrapolate: "clamp",
      });
      return { scale, opacity };
    });

    return (
      <View style={styles.container}>
        <MapView
          ref={map => this.map = map}
          initialRegion={this.state.region}
          style={styles.container}
        >
          {this.state.markers.map((marker, index) => {
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale,
                },
              ],
            };
            const opacityStyle = {
              opacity: interpolations[index].opacity,
            };
            return (
              <MarkerAnimated
                tracksViewChanges={false}
                key={index} 
                coordinate={marker.coordinate}
                style={[styles.markerWrap, opacityStyle]}
              >
              </MarkerAnimated> 
            );
          })}
        </MapView>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{padding: 5, position: 'absolute', top: 35, left: 10, flexDirection: 'row', borderRadius: 5, borderWidth: 0.5, borderColor: '#e8e8e8', alignItems: 'center'}}>
            <Icon size={18} containerStyle={{paddingEnd: 10}} name='chevron-left' type='font-awesome' color='#424242' />
            <Text style={{color: '#424242'}}>Quay lại</Text>
          </TouchableOpacity>
          <View style={{position: 'absolute', bottom: 310, left: 10, flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch'}}>
            <Text style={styles.modalText}>Các tiện ích xung quanh</Text>
          </View>
          <TouchableOpacity onPress={() => this.setState({visibleModal1: true})} style={{position: 'absolute', bottom: 315, right: 10,flexDirection: 'row', alignItems: 'center'}}>
              <Icon type='font-awesome' name='pen' size={16} color='#2196F3' />
              <Text style={{color: '#2196F3', fontWeight: 'bold'}}> Lọc theo</Text>
            </TouchableOpacity>
          <Modal
            style={{ margin: 0, flex: 1 }}
            backdropColor="rgba(0,0,0,0.8)"
            transparent={true}
            //backdropOpacity={0.8}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={0}
            animationOutTiming={0}
            backdropTransitionInTiming={600}
            backdropTransitionOutTiming={600}
            isVisible={this.state.visibleModal1}
            deviceHeight={height+100}>
          {this.renderModalContent1()}
          </Modal>
        <Animated.ScrollView
          horizontal
          scrollEventThrottle={1}
          showsHorizontalScrollIndicator={false}
          snapToInterval={CARD_WIDTH+10}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
          contentContainerStyle={styles.endPadding}
        >
          {this.state.markers.map((marker, index) => (
            <View style={styles.card} key={index}>
              <Image
                source={marker.image}
                style={styles.cardImage}
                resizeMode="cover"
              />
              <View style={styles.textContent}>
                <Text style={styles.cardtitle}>{marker.title}</Text>
                <Text style={styles.cardloai}>{marker.loai}</Text>
                <View style={styles.view3}>
                    <Icon type='font-awesome' name='map-marker-alt' size={18} color='#9E9E9E' containerStyle={{width: 25}} />
                    <Text style={styles.cardDescription}>{marker.DiaChi}</Text>
                </View>
                <View style={styles.view3}>
                    <Icon type='font-awesome' name='phone' size={18} color='#9E9E9E' containerStyle={{width: 25}} />
                    <Text style={styles.cardDescription}>{marker.phone}</Text>
                </View>
              </View>
            </View>
          ))}
        </Animated.ScrollView>
      </View>
    );
  }
}
