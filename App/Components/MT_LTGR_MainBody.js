import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/MT_LTGR_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {CalendarList} from 'react-native-calendars';


export default class MT_LTGR_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = { 
          items: []
        };
      }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Lịch thu gom rác</Text>
          <Icon name='home' color='transparent' containerStyle={{paddingEnd: 20}} />
        </View>
        <View>
          <CalendarList
           current={'2019-12-31'} 
           pastScrollRange={24} 
           futureScrollRange={24}
          />
        </View>
    </View>
    );
  }
}
