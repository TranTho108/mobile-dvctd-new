import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ScrollView, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Tooltip} from "react-native-elements";
import styles from "./Styles/YT_TC_FigureTabStyles"
import {PHAC_DO} from '../Data/YT_TC_Data'
import Modal from "react-native-modal";
import Timeline from 'react-native-timeline-listview'

export default class YT_TC_FigureTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        data: []
    }
  }

  componentWillMount = () => {
    var data1 = PHAC_DO
    var data2 =[]
    data1.map((item) => 
    {
        data2.push(item.thang_tiem)
    }
    )
    this.setState({data: [...new Set(data2)].sort((a,b) => {return a-b})})
  }

  renderDetail(rowData, sectionID, rowID) {
    return (
        <Tooltip width={200} height={250} containerStyle={{flex: 1, padding: 10, justifyContent: 'center'}} popover={<Text style={{color: '#fff', flex: 1}}>{rowData.description1}</Text>}>
            <View>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>{rowData.title}</Text>
                <Text style={{color: '#4CAF50', fontStyle: 'italic'}}>{rowData.description}</Text>
            </View>
        </Tooltip>
    )
  }

  _renderItem = ({item,index}) => {
      var text = item/12 == 0?'Sơ sinh':item/12 < 1?`${item} tháng`:`${item/12} năm`
      var data2 = [{time: '', title: ``, description: ``}]
      PHAC_DO.map((item1) => {
          if(item1.thang_tiem == item){
              data2.unshift({time: '', title: `${item1.ten_khang_nguyen}`, description: `Mũi ${item1.thu_tu}/${item1.tong_so_mui}`, description1: `${item1.mo_ta}`})
          }
      })
      return(
          <View>
              <View style={{padding: 10, alignSelf: 'center', borderRadius: 10, width: 100, backgroundColor: '#565d67', alignItems: 'center', justifyContent: 'center', marginBottom: 20}}><Text style={{color: '#fff'}}>{text}</Text></View>
              <Timeline 
                columnFormat='two-column'
                data={data2}
                renderDetail={this.renderDetail}
                innerCircle={'dot'}
                circleColor='#9aca40'
                lineColor='#9aca40'
              />
          </View>
      )
  }
  

  render() {
    return (
        <View style={styles.container}>
            <FlatList
                data={this.state.data}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{padding: 10}}
            />
        </View>
    );
  }
}
