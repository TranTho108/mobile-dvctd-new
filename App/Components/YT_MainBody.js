import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';


export default class YT_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          
        };
      }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Y tế</Text>
          <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
        </View>
        <ScrollView>

            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("YT_BHYT_MainScreen")} style={styles.view1}>
                    <Image source={images.background.idcard} style={{width: 80, height: 80}} />
                    <Text style={{paddingTop: 10}}>Bảo hiểm y tế</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate("YT_TC_MainScreen")} style={styles.view1}>
                    <Image source={images.background.injection} style={{width: 80, height: 80}} />
                    <Text style={{paddingTop: 10}}>Tiêm chủng</Text>
                </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("YT_TYT_MainScreen")} style={styles.view1}>
                    <Image source={images.background.hospital} style={{width: 80, height: 80}} />
                    <Text style={{paddingTop: 10}}>Trạm y tế</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate("YT_DB_MainScreen")} style={styles.view1}>
                    <Image source={images.background.virus} style={{width: 80, height: 80}} />
                    <Text style={{paddingTop: 10}}>Dịch bệnh</Text>
                </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center'}}>

                <TouchableOpacity onPress={() => this.props.navigation.navigate("YT_QT_MainScreen")} style={styles.view1}>
                    <Image source={images.background.pharmacy} style={{width: 80, height: 80}} />
                    <Text style={{paddingTop: 10}}>Quầy thuốc</Text>
                </TouchableOpacity>
            </View>                
            
        </ScrollView>
    </View>
    );
  }
}
