import React, { Component } from "react";
import {View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, FlatList,} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import {requestGET, requestPOST} from "../Api/Basic"
import * as Animatable from 'react-native-animatable';
import {DVC_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import styles from "./Styles/DVC_TKHS_MainBodyStyles"

export default class DVC_TKHS_PersonalTab extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            data:[],
            loading: true,
        }
      }
    
        componentWillMount = async() => {
            var userToken = await AsyncStorage.getItem('userToken')
            var body = {'token': userToken}
            var data1 = await requestPOST(`${DVC_URL}/GetDocListCongDan`, body)
            var data = data1.data?data1.data:[]
            this.setState({data: data, loading: false})
        }

        _renderItem = ({item,index}) => {
            return (
              <Animatable.View delay={index*300} animation='fadeInRight'>
                <TouchableOpacity onPress = {() => {this.props.navigation.navigate("DVC_TKHS_DetailScreen", {data: item, followHide: 1})}} style={{flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#e8e8e8', paddingBottom: 10}} >
                  <View>
                    <View style={{backgroundColor: '#00897B', margin: 10, borderRadius: 5, alignItems: 'center'}}><Icon name='file-alt' type='font-awesome' color='#fff' containerStyle={{padding: 20}} /></View>
                  </View>
                  <View style={{margin: 10, flex: 1}}>
                    <Text style={{fontWeight: 'bold', fontSize: 20,color: 'black'}}>{item.MaHoSo}</Text>
                    <Text style={{ color: 'black', paddingTop: 10}}>{item.TenHoSo}</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10}}>
                      <View style={{flexDirection: 'row', alignItems: 'center'}}><Icon color='#e8e8e8' size={20} name='user-alt' type='font-awesome' /><Text style={{paddingStart: 5}}>{item.ChuHoSo}</Text></View>
                      <View style={{flexDirection: 'row', alignItems: 'center'}}><Icon color='#e8e8e8' size={20} name='calendar-alt' type='font-awesome' /><Text style={{paddingStart: 5}}>{item.NgayTiepNhan}</Text></View>
                    </View>
                  </View>
                </TouchableOpacity>
                </Animatable.View>
          )}
        

        renderBody = () => {
          if(!this.state.loading){
            return(
            <FlatList
                data={this.state.data}
                renderItem={(item,index) => this._renderItem(item,index)}
                keyExtractor={(item, index) => index.toString()}
                extraData={this.state}
                ListEmptyComponent={EmptyFlatlist}
            />
            )
          }
          else{
            return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator size='large' style={{}} color='#00897B' />
              <Text style={{textAlign: 'center', paddingTop: 10, color: '#00897B'}}>Đang lấy dữ liệu</Text>
            </View>
            )
          }
        }
    
      render() {
        return (
            <View style={styles.container}>
            {this.renderBody()}
            </View>
        );
      }
}
