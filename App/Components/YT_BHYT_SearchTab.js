import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ToastAndroid, TouchableOpacity, Keyboard, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_BHYT_SearchTabStyles"
import Toast, {DURATION} from 'react-native-easy-toast'

export default class YT_BHYT_SearchTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        maThe: '',
        hoTen: '',
        ngaySinh: '',
        isloading: false
    }
  }
  
  renderSearch = () => {
    if(this.state.isloading){
      return(
        <ActivityIndicator size='large' style={{marginTop: 20}} color='#1976D2' />
      )
    }
    else{
      return(
        <TouchableOpacity onPress={() => this.checkContent(this.state.maThe, this.state.hoTen, this.state.ngaySinh)} style={{height: 60, width: 60, borderRadius: 60, backgroundColor: '#9aca40', alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: 10}} >
          <Icon name='search' color='#fff' />
        </TouchableOpacity>
      )
    }
  }

  checkContent = (maThe,hoTen,ngaySinh) => {
    Keyboard.dismiss()
    if( !maThe || !hoTen || !ngaySinh){
        this.refs.toast.show('Vui lòng nhập đầy đủ thông tin', ToastAndroid.LONG)
    }
    else{
      this.setState({isloading: true})
      setTimeout(() => this.refs.toast.show('Không tìm thấy kết quả', ToastAndroid.LONG) + this.setState({isloading: false}), 1000)
    }
  }


  render() {
    return (
        <View style={styles.container}>
          <View style={{padding: 10}}>
          <View style={{flexDirection: 'row', alignItems: 'center',paddingBottom: 5,}}>
            <Icon type='font-awesome' name='id-card' size={20} containerStyle={{width: 35}} />
            <Text style={{fontWeight: '600', fontSize: 15}} >Mã thẻ</Text>
          </View>
          <TextInput placeholderTextColor='#e8e8e8' value={this.state.maThe} style={{color: '#424242', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8', margin: 10}} placeholder='Nhập mã thẻ' onChangeText={(text) => this.setState({maThe: text})}/>
          <View style={{flexDirection: 'row', alignItems: 'center',paddingBottom: 5,}}>
            <Icon type='font-awesome' name='user' size={20} containerStyle={{width: 35}} />
            <Text style={{fontWeight: '600', fontSize: 15}} >Họ tên</Text>
          </View>
          <TextInput placeholderTextColor='#e8e8e8' value={this.state.hoTen} style={{color: '#424242', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8', margin: 10}} placeholder='Nhập họ tên' onChangeText={(text) => this.setState({hoTen: text})}/>
          <View style={{flexDirection: 'row', alignItems: 'center',paddingBottom: 5,}}>
            <Icon type='font-awesome' name='calendar-day' size={20} containerStyle={{width: 35}} />
            <Text style={{fontWeight: '600', fontSize: 15}} >Ngày tháng năm sinh</Text>
          </View>
          <TextInput placeholderTextColor='#e8e8e8' value={this.state.ngaySinh} style={{color: '#424242', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8', margin: 10}} placeholder='Nhập ngày tháng năm sinh' onChangeText={(text) => this.setState({ngaySinh: text})}/>
          {this.renderSearch()}
          </View>
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
