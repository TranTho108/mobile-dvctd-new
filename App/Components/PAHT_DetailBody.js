import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ActivityIndicator,
  TextInput,
  ToastAndroid,
  StatusBar,
  KeyboardAvoidingView,
  Keyboard,
  AsyncStorage
} from 'react-native';
import {ButtonGroup, Icon, Image} from 'react-native-elements'
import styles from './Styles/PAHT_DetailBodyStyles'
import moment from 'moment'
import Video from 'react-native-video'
import {fetchTest2, fetchTest6, fetchNewComment} from "../Api/Test"
import {SERVER_URL, HOST} from '../Config/server'
import Modal from "react-native-modal";
moment.locale('vi')
import LinearGradient from 'react-native-linear-gradient';
import Toast, {DURATION} from 'react-native-easy-toast'
 
//<View style={{ alignItems: 'flex-start', backgroundColor: '#fff', height: 40, paddingStart: 10,}}><Icon name='arrow-back' size={34} color='black' onPress={() => this.props.navigation.goBack()}/></View>

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const num = Math.floor(width/130)

export default class PAHT_DetailBody extends Component {

  constructor () {
    super()
    this.state = {
      item: [],
      media: [],
      images: [],
      param: [],
      videos: [],
      paramkq: [],
      comment: [],
      danhgia: [],
      imageskq:[],
      videoskq:[],
      commentinput:'',
      loading: true,
      param3:[],
      visibleModal: false,
      child: [],
      modalcmt:[],
      modalparent: null,
      commentinputModal: ''
    }
  }

  componentWillMount = async() =>{
    if(this.props.navigation.getParam('item')){
      var id = this.props.navigation.getParam('item')
      console.log(id)
      var data1 = await fetchTest2(id).catch(() => this.refs.toast.show('Vui lòng kiểm tra kết nối mạng', ToastAndroid.LONG))
      var data2 = await JSON.parse(data1)
      console.log(data2)
      commentarr = JSON.parse(data2.comment)
      var parent = []
      var child = []
      commentarr.reverse()
      commentarr.forEach((item,index) => {
        if(!item.parentid){
          parent.push(item)
        }
        else{
          child.push(item)
        }
      })
      child.reverse()
        child.forEach((item1) => {
          index = parent.findIndex(x => x.id === Number(item1.parentid));
          parent.splice(index+1, 0, item1)
      })
      mediaguiarr = JSON.parse(data2.mediagui)
      kqdgarr = JSON.parse(data2.ketquadanhgia)
      mediakq = JSON.parse(data2.mediaketqua)
      this.setState({item: data2, comment: parent, danhgia: kqdgarr,child: child})
      var images = mediaguiarr.filter(function(item){
        return item.type === 'Image'
      })
      var videos = mediaguiarr.filter(function(item){
        return item.type === 'Video'
      })
      var imageskq = mediakq.filter(function(item){
        return item.type === 'Image'
      })
      var videoskq = mediakq.filter(function(item){
        return item.type === 'Video'
      })
      this.setState({images: images, videos: videos,imageskq: imageskq, videoskq: videoskq, loading: false})
    }
  }
  componentDidMount(){
  }

  _renderItem = (item) => {
    this.state.param.push({ 'url' : `${item.item.url}`})
    return (
        <Image resizeMethod='resize' style={styles.renderImage} source={{uri : `${item.item.url}`}} PlaceholderContent={<ActivityIndicator />}/>
    )
  }

  _renderItem6 = (item) => {
    this.state.paramkq.push({ 'url' : `${HOST}${item.item.url}`})
    return (
        <Image resizeMethod='resize' style={styles.renderImage} source={{uri : `${HOST}${item.item.url}`}} PlaceholderContent={<ActivityIndicator />}/>
    )
  }

  _renderItem1 = (item) => {
   const url = `${HOST}${item.item.url}`
   console.log(url)
    return (
      <TouchableOpacity onPress={() => {this.props.navigation.navigate('VideoScreen',{url: url})}} style={{height: 150, alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center', backgroundColor: 'black', margin: 20}}>
        <Icon type='font-awesome' name='film' size={34} color='white' />
      </TouchableOpacity>
    )
  }

  _renderItem2 = (ngay,noidung,coquan,danhgia,ketqua) => {
    if(coquan){
    let result = danhgia.map(a => a.count)
      return (
      <View style={{paddingTop: 10, paddingBottom: 20}}>
        <Text style={{ textAlign: 'center', fontSize: 16, color: 'black'}}>{coquan}</Text>
        <View style={{flexDirection: 'row', alignItems: 'center', paddingTop: 10, paddingStart: 5, justifyContent: 'center'}}><Text>{ngay}</Text></View>
        <View style={{borderBottomColor: '#e8e8e8', marginStart: 100, marginEnd: 100, borderBottomWidth: 0.5, height: 10, marginTop: 20, marginBottom: 20}}></View>
        <Text style={styles.text1}>{noidung}</Text>
        <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('GalleryScreen', {images: this.state.paramkq, title: coquan})}>
                <FlatList
                  //numColumns={width/100}
                  contentContainerStyle={styles.flatList}
                  data={this.state.imageskq}
                  renderItem={this._renderItem6}
                  keyExtractor={(item, index) => index.toString()}
                  //ListEmptyComponent={this.showEmptyListView()}
                />
        </TouchableOpacity>
        <FlatList
          data={this.state.videoskq}
          renderItem={this._renderItem1}
          keyExtractor={(item, index) => index.toString()}
          //ListEmptyComponent={this.showEmptyListView()}
        />   

      </View>
      )
    }
  }

  _renderItem3 = (comment) => {
    if(comment.length > 0){
      return (
        <FlatList
          data={this.state.comment}
          renderItem={(item) => this._renderItem4(item)}
          keyExtractor={(item, index) => index.toString()}
        //ListEmptyComponent={this.showEmptyListView()}
        />
      )
    }
  }

  renderReply = (parent) => {
    var child1 = this.state.child.filter(i => Number(i.parentid) === parent.id)
    child1.push(parent)
    child1.reverse()
    this.setState({modalcmt: child1, visibleModal: true})
  }


  _renderItem4 = (item) => {
    if(!item.item.parentid){
    return (
      <View style={{padding: 5}}>
        <View style={{backgroundColor: '#F5F5F5', borderRadius: 10, padding: 5}}><Text style={{fontSize: 14, color: 'black', fontWeight: '600'}}>  {item.item.name}</Text><Text style={{color: '#212121', padding: 5}}>{item.item.content}</Text></View>
        <View style={{flexDirection: 'row', padding: 5, alignItems: 'center'}}><Text style={{}}>{item.item.time}</Text><TouchableOpacity onPress={() => this.renderReply(item.item)}><Text style={{color: '#9E9E9E', paddingStart: 10, fontWeight: 'bold'}}>Phản hồi</Text></TouchableOpacity></View>
      </View>
    )}
    else {
      return (
        <View style={{padding: 5, paddingStart: 30}}>
          <View style={{backgroundColor: '#F5F5F5', borderRadius: 10, padding: 5}}><Text style={{fontSize: 14, color: 'black', fontWeight: '600'}}>  {item.item.name}</Text><Text style={{color: '#212121', padding: 5}}>{item.item.content}</Text></View>
          <Text style={{padding: 5}}>{item.item.time}</Text>
        </View>
      )
    }
  }

  _renderItem7 = (item) => {
    if(!item.item.parentid){
    return (
      <View style={{padding: 5}}>
        <View style={{backgroundColor: '#F5F5F5', borderRadius: 10, padding: 5}}><Text style={{fontSize: 14, color: 'black', fontWeight: '600'}}>  {item.item.name}</Text><Text style={{color: '#212121', padding: 5}}>{item.item.content}</Text></View>
        <View style={{flexDirection: 'row', padding: 5, alignItems: 'center'}}><Text style={{}}>{item.item.time}</Text></View>
      </View>
    )}
    else {
      return (
        <View style={{padding: 5, paddingStart: 30}}>
          <View style={{backgroundColor: '#F5F5F5', borderRadius: 10, padding: 5}}><Text style={{fontSize: 14, color: 'black', fontWeight: '600'}}>  {item.item.name}</Text><Text style={{color: '#212121', padding: 5}}>{item.item.content}</Text></View>
          <Text style={{padding: 5}}>{item.item.time}</Text>
        </View>
      )
    }
  }
  
  sendComment = async(comment,id, parentid) => {
    var userName = await AsyncStorage.getItem('userName')
    Keyboard.dismiss()
    if(comment !== ''){
    await fetchTest6(comment,id,parentid,userName).then((res) => {
      console.log(res)
      if(res){
        this.refs.toast.show('Gửi bình luận thành công', ToastAndroid.SHORT);
        this.setState({commentinput: '', commentinputModal: ''})
        if(parentid){
          this.state.modalcmt.push({id: null, name: userName, time: "Vừa xong", content: comment, parentid: parentid})
        }
      }
      else{
        this.refs.toast.show('Gửi bình luận thất bại', ToastAndroid.SHORT);
      }
    })
    await this.updateComment(id)
  }
  else{
    this.refs.toast.show('Vui lòng nhập bình luận', ToastAndroid.SHORT);
  }
  }

  updateComment = (id) => {
    fetchNewComment(id).then((data2) => {
      if(data2){
        commentarr = JSON.parse(data2.comment)
        var parent = []
        var child = []
        commentarr.reverse()
        commentarr.forEach((item,index) => {
          if(!item.parentid){
            parent.push(item)
          }
          else{
            child.push(item)
          }
        })
        child.reverse()
          child.forEach((item1) => {
            index = parent.findIndex(x => x.id === Number(item1.parentid));
            parent.splice(index+1, 0, item1)
        })
        this.setState({comment: parent, child: child})
      }
    })
  }

  renderBody(item,comment,danhgia,commentinput){
    if(this.state.loading){
      return (
        <View style={{flex: 1, justifyContent: 'center', backgroundColor: '#fff'}}>
          <ActivityIndicator />
        </View>
      );
    }
    else{
      return(
        <KeyboardAvoidingView style={{flex: 1}} behavior='padding' >
        <ScrollView 
          style={{}}
          keyboardDismissMode='on-drag'
          keyboardShouldPersistTaps='always'
        >
        <View style={styles.view2}><Icon name='bookmark' color='white' style={{}}/><Text style={{color: '#fff', padding: 5, fontSize: 13}}>Phản ánh hiện trường</Text></View>
        <View style={{padding: 20}}>
            <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold', paddingTop: 10}}>{item.tieude}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center', paddingBottom: 30}}><Icon name='account-circle'/><Text> Người dân</Text></View>
            
            <View style={{paddingTop: 20, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.8, minHeight: 60}}>
              <Text style={{fontSize: 18, color: '#212121',fontWeight: 'bold'}}>Nội dung</Text>
              <Text style={{fontSize: 16, color: '#424242', paddingTop: 10, paddingBottom: 20}}>{item.noidung}</Text>
            </View>

            <View style={{paddingTop: 20, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.8, minHeight: 60}}>
              <Text style={{fontSize: 18, color: '#212121',fontWeight: 'bold'}}>Hình ảnh</Text>
              <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('GalleryScreen', {images: this.state.param, title: item.tieude})}>
                <FlatList
                  //numColumns={width/100}
                  contentContainerStyle={styles.flatList}
                  data={this.state.images}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index.toString()}
                  numColumns={num}
                  //ListEmptyComponent={this.showEmptyListView()}
                />
              </TouchableOpacity>
            </View>

            <View style={{paddingTop: 20, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.8, minHeight: 60}}>
              <Text style={{fontSize: 18, color: '#212121',fontWeight: 'bold'}}>Video</Text>
              <FlatList
                  data={this.state.videos}
                  renderItem={this._renderItem1}
                  keyExtractor={(item, index) => index.toString()}
                  //ListEmptyComponent={this.showEmptyListView()}
              />            
            </View>

            <View style={{paddingTop: 20, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.8, minHeight: 60}}>
              <Text style={{fontSize: 18, color: '#212121',fontWeight: 'bold'}}>Kết quả xử lý</Text>
              {this._renderItem2(item.ngayxuly, item.noidungxuly, item.coquanxuly, danhgia, item.mediaketqua )}
            </View>

            <View style={{paddingTop: 20, minHeight: 60}}>
              <Text style={{fontSize: 18, color: '#212121',fontWeight: 'bold'}}>Bình luận ({comment.length})</Text>
              {this._renderItem3(comment)}
            </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TextInput
                autoCapitalize="none"
                style={styles.textInput}
                placeholder=" Nhập bình luận..."
                onChangeText={(commentinput) => this.setState({commentinput: commentinput})}
                value={commentinput}
                underlineColorAndroid={'transparent'}
              />
              <Icon
                      name='send'
                      color='#0078FF'
                      size={30}
                      onPress={() => this.sendComment(commentinput,item.id,null)}
              />
            </View>
      </ScrollView>
      </KeyboardAvoidingView>
      );
    }
  }

  renderModalContent = () => {
    console.log(this.state.modalcmt)
    return(
        <View style={{backgroundColor: 'white', flex: 1}}>
          <StatusBar barStyle="dark-content" backgroundColor={"#fff"} />
          <View style={{flexDirection: 'row', alignItems: 'center', padding: 10, borderBottomColor: '#f5f5f5', borderBottomWidth: 1, justifyContent: 'space-between', paddingTop: 32}}><Icon onPress={() =>  this.setState({visibleModal: false})} name='chevron-left' type='font-awesome' /><Text style={{color: 'black', fontWeight: '600'}}>Phản hồi</Text><Icon name='chevron-left' color='transparent' /></View>
          <FlatList
            style={{padding: 10}}
            data={this.state.modalcmt}
            renderItem={(item) => this._renderItem7(item)}
            keyExtractor={(item, index) => index.toString()}
          //ListEmptyComponent={this.showEmptyListView()}
        />
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TextInput
          autoFocus={true}
            autoCapitalize="none"
            style={styles.textInput}
            placeholder=" Nhập phản hồi..."
            onChangeText={(commentinputModal) => this.setState({commentinputModal: commentinputModal})}
            value={this.state.commentinputModal}
            underlineColorAndroid={'transparent'}
          />
          <Icon
            name='send'
            color='#0078FF'
            size={30}
            onPress={() => this.sendComment(this.state.commentinputModal,this.state.item.id, this.state.modalcmt[0].id)}
          />
          </View>
        </View>
    );
  }

  render() {
   console.log(num)
   const {item,comment,danhgia,commentinput} = this.state
   return (
    <View style={styles.container} onLayout={this._onLayout}>
      <View style={styles.linearGradient1}><Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='black' /><Text style={styles.title}>Chi tiết phản ánh</Text></View>
      <LinearGradient
        colors={['rgba(0,0,0,0.1)', 'transparent']}
        style={{
            left: 0,
            right: 0,
            height: 5,
        }}
      />
      {this.renderBody(item,comment,danhgia,commentinput)}
      <Modal
        avoidKeyboard={true}
        style={{ margin: 0}}
        transparent={true}
        hideModalContentWhileAnimating={true}
        backdropTransitionOutTiming={0}
        isVisible={this.state.visibleModal}
        onBackButtonPress={() => this.setState({ visibleModal: false })}
        onSwipeComplete={() => this.setState({visibleModal: false})}
      >
        {this.renderModalContent()}
      </Modal>
      <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
    </View>
    );
  }
}
