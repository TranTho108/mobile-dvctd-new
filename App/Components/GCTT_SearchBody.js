import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, FlatList, ToastAndroid, Keyboard} from "react-native";
import {Text, Button, Icon, Input} from "react-native-elements";
import styles from "./Styles/GCTT_SearchBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {requestGET, requestPOST} from "../Api/Basic"
import {GCTT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import axios from 'axios'
import Toast, {DURATION} from 'react-native-easy-toast'
import Modal from "react-native-modal";
import RadioForm from 'react-native-simple-radio-button';

export default class GCTT_SearchBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data:[],
        loading: false,
        tuKhoa: '',
        visibleModal1: false,
        detail: []
    }
  }

    componentWillMount = async() => {
        
    }

    searchText = async(tuKhoa) => {
        Keyboard.dismiss()
          if(this.state.tuKhoa.length == 0){
            this.refs.toast.show(`Nhập từ khoá`, ToastAndroid.LONG)
          }
          else{
            this.setState({data: [],loading: true})
            var body = {'keySearch': tuKhoa}
            var data1 = await requestPOST(`${GCTT_URL}/GCTTs/mGetListSearch`, body)
            var data2 = data1.data?data1.data:[]
            if(data2.length == 0){
              this.refs.toast.show(`Không tìm thấy mặt hàng`, ToastAndroid.LONG);
              this.setState({loading: false})
            }
            else{
              this.refs.toast.show(`Đã tìm thấy `+ data2.length +` mặt hàng`, ToastAndroid.LONG);
              this.setState({loading: false, data: data2})
            }
          }
    }

    showLoading = () => {
        if(this.state.loading){
            return ( <ActivityIndicator size='large' style={{marginTop: 20}} color='#2856C6' />)
        }
        else{
          return (
            <TouchableOpacity onPress={() => this.searchText(this.state.tuKhoa)} style={{height: 60, width: 60, borderRadius: 60, backgroundColor: '#2856C6', alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: 10, marginBottom: 10}} >
              <Icon name='search' color='#fff' />
            </TouchableOpacity>
          )
        }
        }
      
          _renderItem1 = ({item,index}) => {
            var TrungBinhGia = item.TrungBinhGia?item.TrungBinhGia.toLocaleString():0
            return(
              <TouchableOpacity onPress={() => this.setState({detail: item, visibleModal1: true})} style={{flexDirection: 'row', flex: 1, margin: 10, borderBottomColor: '#e8e8e8', borderBottomWidth: 1, padding: 5,}}>
                <Text style={styles.title1}>{item.TenMatHang}</Text>
                <Text style={styles.title3}>{TrungBinhGia}</Text>
                <Text style={styles.title3}>{item.DonViTinh}</Text>
                <Text style={styles.title1}>{item.DiaDiemKhaoSat}</Text>
              </TouchableOpacity>
            )
          }
      
        renderModalContent1 = () => {
            if(this.state.detail ){
            const {detail} = this.state
            var TuGia = detail.TuGia?detail.TuGia.toLocaleString():0
            var DenGia = detail.DenGia?detail.DenGia.toLocaleString():0
            var TrungBinhGia = detail.TrungBinhGia?detail.TrungBinhGia.toLocaleString():0
            return(
              <View style={styles.content2}>
                <ScrollView style={{padding: 20}}>
                  <Text style={styles.title5}>Chi tiết giá mặt hàng</Text>
                  <Text style={styles.section}>Tên mặt hàng</Text>
                  <Text style={styles.label}>{detail.TenMatHang}</Text>
                  <Text style={styles.section}>Từ giá</Text>
                  <Text style={styles.label}>{TuGia}</Text>
                  <Text style={styles.section}>Đến giá</Text>
                  <Text style={styles.label}>{DenGia}</Text>
                  <Text style={styles.section}>Trung bình giá</Text>
                  <Text style={styles.label}>{TrungBinhGia}</Text>
                  <Text style={styles.section}>Đơn vị tính</Text>
                  <Text style={styles.label}>{detail.DonViTinh}</Text>
                  <Text style={styles.section}>Địa điểm</Text>
                  <Text style={styles.label}>{detail.DiaDiemKhaoSat}</Text>
                  <Text style={styles.section}></Text>
                  <Text style={styles.label}></Text>
                </ScrollView>
                <TouchableOpacity onPress={() => {this.setState({visibleModal1: false})}}>
                     <Text style={{fontSize: 14, color: '#2089dc', fontWeight: '600', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
                   </TouchableOpacity>
              </View>
          );}
          }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
            <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
            <Text style={styles.title}>Tìm kiếm</Text>
            <Icon name='home' color='transparent' containerStyle={{paddingEnd: 20}} />
        </View>
        <View style={{}}>
          <View style={{flexDirection: 'row', alignItems: 'center', padding: 10}}>
            <Icon name='keyboard' size={24} containerStyle={{width: 35}} />
            <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 15}} >Từ khoá tra cứu </Text>
          </View>
          <TextInput autoCapitalize='none' placeholderTextColor='#CCCCCC' value={this.state.tuKhoa} style={{color: '#424242', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8', margin: 5, padding: 10}} placeholder='Nhập từ khoá' onChangeText={(text) => this.setState({tuKhoa: text})}/>
            {this.showLoading()}
            </View>
            {this.state.data.length>0?
            <>
              <View style={{flexDirection: 'row', margin: 10, borderBottomColor: '#e8e8e8', borderBottomWidth: 1, padding: 5}}>
                <Text style={[styles.title1, {fontWeight: 'bold'}]}>Mặt hàng</Text>
                <Text style={[styles.title3, {fontWeight: 'bold'}]}>Giá trung bình</Text>
                <Text style={[styles.title3, {fontWeight: 'bold'}]}>Đơn vị tính</Text>
                <Text style={[styles.title1, {fontWeight: 'bold'}]}>Địa điểm</Text>
              </View>
            </>
              :<></>}
            <FlatList 
              data={this.state.data}
              renderItem={this._renderItem1}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
            />
            <Modal
              backdropTransitionOutTiming={0}
              isVisible={this.state.visibleModal1}
              style={{margin: 0}}
              hideModalContentWhileAnimating={true}>
              {this.renderModalContent1()}
            </Modal>
        <Toast
            ref="toast"
            style={{backgroundColor:'rgba(0,0,0,0.6)'}}
            position='bottom'
            fadeInDuration={750}
            fadeOutDuration={1000}
        />
    </View>
    );
  }
}