import React, { Component } from "react";
import { View, Text, AsyncStorage, FlatList, Dimensions, ActivityIndicator, TouchableOpacity, TextInput, PermissionsAndroid, Platform, Animated, RefreshControl, Image, ToastAndroid, ImageBackground} from "react-native";
import { Button, Icon} from "react-native-elements";
import axios from "axios"
import styles from "./Styles/YT_DB_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import YT_DB_TabBar from './YT_DB_TabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import YT_DB_HomeTab from './YT_DB_HomeTab'
import YT_DB_HistoryTab from './YT_DB_HistoryTab'
import YT_DB_NotificationTab from './YT_DB_NotificationTab'
 
import images from '../Themes/Images'

export default class YT_DB_MainBody extends Component {

  constructor(props) {
    super(props);
    this.state = {
        
      }
    }

  render() {
    return (
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <View style={styles.linearGradient1}>
            <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
            <Text style={styles.title}>Dịch bệnh</Text>
            <Icon name='map' color='transparent' containerStyle={{paddingEnd: 20}} />
          </View>
            <ScrollableTabView
            style={{flex: 1}}
            tabBarPosition='bottom'
            initialPage={0}
            renderTabBar={() => <YT_DB_TabBar {...this.props}/>}
            >
              <View tabLabel="Dịch bệnh" style={styles.tabView}>
                <YT_DB_HomeTab {...this.props}/>
              </View>
              <View tabLabel="Lịch sử" style={styles.tabView}>
                <YT_DB_HistoryTab {...this.props}/>
              </View>
              <View tabLabel="Thông báo" style={styles.tabView}>
                <YT_DB_NotificationTab {...this.props}/>
              </View>
            </ScrollableTabView>
        </View>
    );
  }
}
