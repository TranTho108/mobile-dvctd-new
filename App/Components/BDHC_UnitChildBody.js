import React, { Component } from "react";
import {StyleSheet,View,ScrollView,StatusBar, ImageBackground, TouchableOpacity, ActivityIndicator, Platform, Animated} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/BDHC_UnitChildBodyStyles'
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import {MapData} from "../Data/MapData"
import {QLDB_URL, HOST_YT} from "../Config/server"
import {requestGET, requestPOST} from "../Api/Basic"

const HEADER_MAX_HEIGHT = 250;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class BDHC_UnitChildBody extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            loading: true,
            dataInfo: [],
            dataChild: []
        };
        
    }

  componentWillMount = async() => {
    var dataInfo = this.props.navigation.getParam('data')
    var data2 = await requestGET(`${QLDB_URL}/DiaBanApi/mGetChildByCode?AreaCode=${dataInfo.AreaCode}`)
    var dataChild = data2?data2:[]
    this.setState({dataInfo: dataInfo, dataChild: dataChild, loading: false})
  }

  _renderScrollViewContent = (dataInfo,dataChild) => {
    return (
        <View style={{padding: 15}}>
        <Text style={{color: '#FF9800'}}></Text>
        <Text style={{fontSize: 40, padding: 5, paddingLeft: 0}}>{dataInfo.AreaName}</Text>
        <View style={{flexDirection: 'row', paddingTop: 10, paddingBottom: 10}}>
            <View style={styles.view2}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon type='font-awesome' name='map' size={18} color='#FF9800' />
                    <Text style={{color: '#757575', paddingTop: 5}}>  Diện tích</Text>
                </View>
                <Text style={{fontWeight: 'bold', paddingTop: 10}}>{dataInfo.TuNhien_DiaHinh_DienTich} km2</Text>
            </View>
            <View style={styles.view2}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon type='font-awesome' name='user' size={18} color='#FF9800' />
                    <Text style={{color: '#757575', paddingTop: 5}}>  Dân số</Text>
                </View>
                <Text style={{fontWeight: 'bold', paddingTop: 10}}>{dataInfo.TongQuan_DanSo_Tong} người</Text>
            </View>
        </View>
        <Text style={{lineHeight: 30,}}>{dataInfo.TongQuan_GioiThieu}</Text>
        {dataChild.length>0?<>
        <Text style={{fontSize: 18, paddingTop: 30, paddingBottom: 10, fontWeight: '600'}}>Thuộc {dataInfo.AreaName}</Text>
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                flexWrap: 'wrap',
            }}>
            {dataChild.map(item => {
                var urlAnh = item.Url_AnhDaiDien?`${HOST_YT}${item.Url_AnhDaiDien.split(",")[0]}`:'https://vneconomy.mediacdn.vn/zoom/480_270/2019/3/29/tp-hcm-155384260439116565666-crop-1553842610657853611450.jpg'
                return(
                <TouchableOpacity key={item.ID} onPress={() => this.props.navigation.navigate('BDHC_UnitChildScreen', {data: item})} style={{justifyContent: 'center', padding: 5}}>
                    <ImageBackground source={{uri: urlAnh}} style={{height: 80, width: 100, overflow: 'hidden', borderTopLeftRadius: 10, borderBottomRightRadius: 20}} />
                    <Text style={{paddingTop: 10, fontWeight: '600', width: 100, height: 50}}>{item.AreaName}</Text>
                    <Text style={{paddingTop: 5, color: '#757575', paddingBottom: 10}}></Text>
                </TouchableOpacity>
            )})}
        </View>
        </>
        :
        <></>
        }
        </View>
    );
  }
  
  render() {
    const scrollY = Animated.add(
        this.state.scrollY,
        Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
      );
      const headerTranslate = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [0, -HEADER_SCROLL_DISTANCE],
        extrapolate: 'clamp',
      });
  
      const imageOpacity = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        outputRange: [1, 1, 0],
        extrapolate: 'clamp',
      });
      const imageTranslate = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE],
        outputRange: [0, 100],
        extrapolate: 'clamp',
      });
  
      const titleScale = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        outputRange: [1, 1, 0.8],
        extrapolate: 'clamp',
      });
      const titleTranslate = scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        outputRange: [0, 0, -8],
        extrapolate: 'clamp',
      });
    const {dataChild, dataInfo} = this.state
    var urlAnh1 = dataInfo.Url_AnhDaiDien?`${HOST_YT}${dataInfo.Url_AnhDaiDien.split(",")[0]}`:'https://vneconomy.mediacdn.vn/zoom/480_270/2019/3/29/tp-hcm-155384260439116565666-crop-1553842610657853611450.jpg'
   return (
    <View style={styles.container}>
        {this.state.loading?
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#212121' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#212121'}}>Đang lấy dữ liệu</Text>
        </View>
        :
            <View style={{flex: 1}}>
                <Animated.ScrollView
                    style={styles.fill}
                    scrollEventThrottle={1}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
                        { useNativeDriver: true },
                    )}
                    contentInset={{
                        top: HEADER_MAX_HEIGHT,
                      }}
                      contentOffset={{
                        y: -HEADER_MAX_HEIGHT,
                    }}
                    >
                        {this._renderScrollViewContent(dataInfo,dataChild)}
                    </Animated.ScrollView>
                <Animated.View
                pointerEvents="none"
                style={[
                    styles.header,
                    { transform: [{ translateY: headerTranslate }] },
                ]}
                >
                <Animated.Image
                    style={[
                    styles.backgroundImage,
                    {
                        opacity: imageOpacity,
                        transform: [{ translateY: imageTranslate }],
                    },
                    ]}
                    source={{uri: urlAnh1}}
                />
                </Animated.View>
                <Animated.View
                style={[
                    styles.bar,
                    {
                    transform: [
                        { scale: titleScale },
                        { translateY: titleTranslate },
                    ],
                    },
                ]}
                >
                <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
                </Animated.View>
            </View>
        }
    </View>
    );
  }
}
