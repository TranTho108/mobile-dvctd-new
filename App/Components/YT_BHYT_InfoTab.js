import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, ImageBackground, Image} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_BHYT_InfoTabStyles"
import images from "../Themes/Images"

export default class YT_BHYT_InfoTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
      userInfo: []
    }
  }

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem("userToken")
    if(userToken){
        userInfo = await AsyncStorage.getItem("userInfo")
        info = JSON.parse(userInfo)
        this.setState({userInfo: info})
    }
  }
    
  render() {
    return (
        <View style={styles.container}>

          <View style={styles.view1}>
            <View style={styles.view2}><Text style={{fontSize: 18, fontWeight: 'bold'}}>Thông tin thẻ</Text></View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Họ tên</Text>
                <Text style={styles.text2}>{this.state.userInfo.fullName}</Text>
            </View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Ngày sinh</Text>
                <Text style={styles.text2}>{this.state.userInfo.birthday}</Text>
            </View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Giới tính</Text>
                <Text style={styles.text2}>{this.state.userInfo.sex}</Text>
            </View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Địa chỉ</Text>
                <Text style={styles.text2}>{this.state.userInfo.address}</Text>
            </View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Nơi KCBBĐ</Text>
                <Text style={styles.text2}>01075</Text>
            </View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Hạn thẻ</Text>
                <Text style={styles.text2}>01/08/2019-31/12/2019</Text>
            </View>
            <View style={styles.view3}>
                <Text style={styles.text1}>Thời điểm đủ 5 năm liên tục</Text>
                <Text style={styles.text2}>01/02/2024</Text>
            </View>
          </View>
          <ImageBackground
            source={images.background.BHYT_MT}
            style={{height: 250, margin: 10, alignItems: 'flex-end', }}
            imageStyle={{borderRadius: 10}}
            resizeMode='stretch'
          >
          <View style={{paddingTop: 100, paddingRight: 50}}>
           <Text style={{fontSize: 20, paddingBottom: 5, color: '#00589B'}}>{this.state.userInfo.fullName}</Text>
           <Text style={{fontSize: 34, fontWeight: 'bold', color: '#00589B'}}>362 191 7474</Text>
          </View>
           <Text style={{paddingTop: 22, paddingRight: 10, color: '#00589B'}}>03/09/2019</Text>
          </ImageBackground>
          <ImageBackground 
            source={images.background.BHYT_MS}
            style={{height: 250, margin: 10,}}
            imageStyle={{borderRadius: 10}}
            resizeMode='stretch'
          />
        </View>
    );
  }
}
