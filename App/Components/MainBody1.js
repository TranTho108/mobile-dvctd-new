import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  StatusBar
} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/YT_BHYT_MainBodyStyles'
import LinearGradient from 'react-native-linear-gradient';
import MainBody_TabBar from './MainBody_TabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import MainBody_HomeTab from './MainBody_HomeTab'
import MainBody_NotifyTab from './MainBody_NotifyTab'
import MainBody_AccountTab from './MainBody_AccountTab'
import MainBody_MapTab from './MainBody_MapTab'
import firebase from 'react-native-firebase';
import SplashScreen from 'react-native-splash-screen'

export default class MainBody extends Component {

  componentDidMount = async() => {
    SplashScreen.hide()
    firebase.messaging().requestPermission()
    .then(() => {
      // User has authorised  
    })
    .catch(error => {
      // User has rejected permissions  
    });
  }
  
  render() {
   return (
    <View style={styles.container}>
        <ScrollableTabView
        style={{flex: 1}}
        tabBarPosition='bottom'
        initialPage={0}
        locked={true}
        renderTabBar={() => <MainBody_TabBar {...this.props}/>}
        >
        <View tabLabel="Trang chủ" style={styles.tabView}>
            <MainBody_HomeTab {...this.props}/>
        </View>
        <View tabLabel="Thông báo" style={styles.tabView}>
            <MainBody_NotifyTab {...this.props}/>
        </View>
        <View tabLabel="Nam Định" style={styles.tabView}>
            <MainBody_MapTab {...this.props}/>
        </View>
        <View tabLabel="Tài khoản" style={styles.tabView}>
            <MainBody_AccountTab {...this.props}/>
        </View>
        </ScrollableTabView>
    </View>
    );
  }
}
