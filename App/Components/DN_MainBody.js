import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  View,
  processColor,
  LayoutAnimation,
  ScrollView,
  Image,
  AsyncStorage
} from "react-native";
import {Text, Button, Icon, Avatar} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import DN_ElectricTab from './DN_ElectricTab'
import DN_WaterTab from './DN_WaterTab'
import styles from './Styles/DN_MainBodyStyles'
import {requestPOST, requestGET} from '../Api/Basic'
import {DN_URL} from '../Config/server'

import { LineChart } from "react-native-charts-wrapper";

const greenBlue = "rgb(26, 182, 151)";
const petrel = "rgb(59, 145, 153)";

export default class DN_MainBody extends Component {
  constructor() {
    super();

    this.state = {
      tab: 'điện',
      total: '',
      amount: '',
      totalD: '',
      amountD: '',
      totalN: '',
      amountN: '',
      loading: true,
      dv: 'kWh'
    };
  }


  componentWillMount = async() => {
    var soHoKhau = await AsyncStorage.getItem('soHoKhau')
    var dataD = await requestGET(`${DN_URL}/bill/services/1/households/${soHoKhau}/latest`)
    var dataN = await requestGET(`${DN_URL}/bill/services/2/households/${soHoKhau}/latest`)
    if(dataD.data){
      var BillD = dataD.data
      var totalD = BillD.MoneyPay.toLocaleString()
      var amountD = BillD.NewMeterNumber - BillD.OldMeterNumber
      this.setState({totalD: totalD, amountD: amountD, total: totalD, amount: amountD, loading: false})
    }
    else{
      this.setState({totalD: 0, amountD: 0, total: 0, amount: 0, loading: false})
    }
    if(dataN.data){
      var BillN = dataN.data
      var totalN = BillN.MoneyPay.toLocaleString()
      var amountN = BillN.NewMeterNumber - BillN.OldMeterNumber
      this.setState({totalN: totalN, amountN: amountN, loading: false})
    }
    else{
      this.setState({totalN: 0, amountN: 0, loading: false})
    }
  }

  _changeTab = ({i}) => {
    if(i == 0){
      this.setState({tab: 'diện', total: this.state.totalD,amount: this.state.amountD, dv: 'kWh'})
    }
    else{
        this.setState({tab: 'nước', total: this.state.totalN,amount: this.state.amountN, dv: 'm3'})
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
          <LinearGradient colors={[greenBlue,petrel]} style={styles.linearGradient2}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' />
              <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.title1}>{this.state.total}đ</Text>
            </View>
            <Text style={styles.section}>Tiền {this.state.tab}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={{flex: 1, maxWidth: 250}}>
                <Text style={styles.title2}>{this.state.amount} {this.state.dv}</Text>
                <Text style={styles.section}>Chỉ số tiêu thụ {this.state.tab}</Text>
              </View>
            </View>
          </LinearGradient>

          <ScrollableTabView
            style={{}}
            initialPage={0}
            tabBarPosition='top'
            tabBarActiveTextColor='rgb(26, 182, 151)'
            tabBarInactiveTextColor={'#757575'}
            tabBarUnderlineStyle={{backgroundColor: 'rgb(26, 182, 151)',}}
            onChangeTab={this._changeTab}
          >

            <ScrollView tabLabel="ĐIỆN" style={styles.tabView}>
              <DN_ElectricTab {...this.props}/>
            </ScrollView>
            <ScrollView tabLabel="NƯỚC" style={styles.tabView}>
              <DN_WaterTab {...this.props}/>
            </ScrollView>
          </ScrollableTabView>
      </View>
    );
  }
}
