import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements'

import LinearGradient from 'react-native-linear-gradient';

import styles from './Styles/DVC_TKHS_TabBarStyles'

class DVC_TKHS_TabBar extends React.Component {
  icons = [];
  constructor(props) {
    super(props);
    this.icons = [];
  }

  iconColor(progress) {
    const red = 59 + (204 - 59) * progress;
    const green = 89 + (204 - 89) * progress;
    const blue = 152 + (204 - 152) * progress;
    return `rgb(${red}, ${green}, ${blue})`;
  }

  textStyle = (active,i) => {
    return {
      fontSize: 10,
      color: active === i ? '#00897B' : 'rgb(204,204,204)'
    }
  }

  render() {
    var logo = ['user', 'clipboard-list']
    return <View style={[styles.tabs, this.props.style, ]}>
              <LinearGradient
                colors={['transparent', 'rgba(0,0,0,0.1)']}
                style={{
                    left: 0,
                    right: 0,
                    height: 2,
                    top: -2,
                    position: 'absolute'
                }}
              />
      {this.props.tabs.map((tab, i) => {
        icon = logo[i]
        return <TouchableOpacity key={tab} onPress={() => this.props.goToPage(i)} style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Icon
            name={icon}
            size={28}
            color={this.props.activeTab === i ? '#00897B' : 'rgb(204,204,204)'}
            ref={(icon) => { this.icons[i] = icon; }}
            type='font-awesome'
          />
          <Text style={this.textStyle(this.props.activeTab,i)}>{tab}</Text>
        </TouchableOpacity>;
      })}
    </View>;
  }
}

export default DVC_TKHS_TabBar;