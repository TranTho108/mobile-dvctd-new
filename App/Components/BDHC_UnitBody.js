import React, { Component } from "react";
import {StyleSheet,View,ScrollView,StatusBar, ImageBackground, TouchableOpacity} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/BDHC_UnitBodyStyles'
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import {MapData} from "../Data/MapData"
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';

export default class BDHC_UnitBody extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
        dataInfo: this.props.navigation.getParam('data'),
    };
}

  componentDidMount = async() => {
    
  }
  
  render() {
   const {dataInfo} = this.state
   return (
    <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='black' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Giới thiệu về {dataInfo.Area.AreaName}</Text>
          <Icon name='home' color='transparent' containerStyle={{paddingEnd: 20}} />
        </View>
        <ScrollableTabView
          style={{flex: 1}}
          tabBarPosition='top'
          initialPage={0}
          tabBarActiveTextColor='#212121'
          tabBarInactiveTextColor='#9E9E9E'
          tabBarUnderlineStyle={{height: 1, backgroundColor: '#FF9800'}}
          renderTabBar={() => <DefaultTabBar style={{height: 30}} />}
        >
          <View tabLabel="Tổng quan" style={styles.tabView}>
            <ScrollView style={{padding: 10}}>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Giới thiệu</Text>
              <Text style={{lineHeight: 30,}}>{dataInfo.TongQuan_GioiThieu}</Text>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Hành chính</Text>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Quốc gia</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_HanhChinh_QuocGia}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Vùng</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_HanhChinh_Vung}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Tỉnh lỵ</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_HanhChinh_TinhLy}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Trụ sở UBND</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_HanhChinh_TruSoUBND}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Phân chia hành chính</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_HanhChinh_PhanChiaHanhChinh}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Thành lậo</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_HanhChinh_ThanhLap}</Text>
                </View>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Dân số</Text>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Tổng cộng</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_DanSo_Tong}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Thành thị</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_DanSo_ThanhThi}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Nông thôn</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_DanSo_NongThon}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Mật độ</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_DanSo_MatDo}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Dân số</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TongQuan_DanSo_DanToc}</Text>
                </View>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Ảnh</Text>
            </ScrollView>
          </View>
          <View tabLabel="Tự nhiên" style={styles.tabView}>
            <ScrollView style={{padding: 10}}>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Địa hình</Text>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Diện tích</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TuNhien_DiaHinh_DienTich}</Text>
                </View>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Khí hậu</Text>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Đặc trưng</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TuNhien_KhiHau_DacTrung}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Mùa hè</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TuNhien_KhiHau_MuaHe}</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Mùa đông</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TuNhien_KhiHau_MuaDong}</Text>
                </View>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Khoáng sản</Text>
                <View style={{flexDirection: 'row', padding: 10, paddingLeft:0, borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5}}>
                  <Text style={{flex: 1/2}}>Khoáng sản</Text>
                  <Text style={{flex: 1/2}}>{dataInfo.TuNhien_KhoangSan}</Text>
                </View>
            </ScrollView>
          </View>
          <View tabLabel="Lịch sử" style={styles.tabView}>
            <ScrollView style={{padding: 10}}>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Lịch sử</Text>
              <Text style={{lineHeight: 30,}}>{dataInfo.LichSu}</Text>
            </ScrollView>
          </View>
          <View tabLabel="Kinh tế" style={styles.tabView}>
            <ScrollView style={{padding: 10}}>
              <Text style={{fontWeight: 'bold', padding: 10, paddingLeft: 0, fontSize: 16}}>Kinh tế</Text>
              <Text style={{lineHeight: 30,}}>{dataInfo.KinhTe}</Text>
            </ScrollView>
          </View>
        </ScrollableTabView>
    </View>
    );
  }
}
