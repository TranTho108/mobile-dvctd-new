import React, { Component } from "react";
import { View, AsyncStorage, FlatList, Animated, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/GD_PointTabStyles"
import Panel from './Panel'
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'

export default class GD_PointTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      dataPoint: []
    }
  }


  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem('userToken')
    var NamHocID = await AsyncStorage.getItem('NamHocID')
    var HocKyID = await AsyncStorage.getItem('HocKyID')
    var studentID = await AsyncStorage.getItem('studentID')
    var body = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID, 'HocKyID': HocKyID}
    var data1 = await requestPOST(`${GD_URL}/PhieuDiem`, body)
    var data2 = data1.data?data1.data:[]
    this.setState({dataPoint: data2, loading: false})
  }


  _renderItem = ({item,index}) => {
    return(
      <Panel title={item.TenMon} dtb={item.TBM}>
        <FlatList 
          data={item.Diems}
          renderItem={this._renderItem1}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
          ListEmptyComponent={EmptyFlatlist}
        />
      </Panel>
    )
  }

  _renderItem1 = ({item,index}) => {
    return(
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Text style={{padding: 10, width: 150, color: '#616161'}}>{item.LoaiDiem}</Text>
        <Text style={{paddingStart: 20, color: '#616161'}}>{item.DSDiem}</Text>
      </View>
    )
  }


renderData = () => {
  if(this.state.dataPoint.length > 0){
      return(
        <Animatable.View style={{padding: 5}} animation='fadeIn'>
        <View style={{alignSelf: 'stretch', flexDirection: 'row', backgroundColor: '#F5F5F5'}}>
        <View style={{flex: 1/4, paddingTop: 30, paddingBottom: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EEEEEE', borderRadius: 5}}>
          <Text style={{color: '#616161'}}>Trung bình</Text>
          <Text style={{color: '#2AA5FF', fontSize: 20, padding: 5, fontWeight: 'bold'}}>{this.state.dataPoint.TB}</Text>
        </View>
        <View style={{flex: 3/4, padding: 10, flexDirection: 'row', paddingTop: 30,}}>
          <View style={{flex: 1/2}}>
            <View style={{flexDirection: 'row'}}><Text style={{color: '#616161'}}>Học lực  </Text><Text>{this.state.dataPoint.HocLuc}</Text></View>
            <View style={{flexDirection: 'row', paddingTop: 10}}><Text style={{color: '#616161'}}>Hạnh kiểm  </Text><Text>{this.state.dataPoint.HanhKiem}</Text></View>
            <View style={{flexDirection: 'row', paddingTop: 10}}><Text style={{color: '#616161'}}>Danh hiệu  </Text><Text>{this.state.dataPoint.DanhHieu}</Text></View>
          </View>
          <View style={{flex: 1/2}}>
            <View style={{flexDirection: 'row'}}><Text style={{color: '#616161'}}>Vắng có phép  </Text><Text>0</Text></View>
            <View style={{flexDirection: 'row', paddingTop: 10}}><Text style={{color: '#616161'}}>Vắng không phép  </Text><Text>0</Text></View>
          </View>
        </View>
      </View>
    <FlatList 
      data={this.state.dataPoint}
      renderItem={this._renderItem}
      keyExtractor={(item, index) => index.toString()}
      extraData={this.state}
    />
    </Animatable.View>
      )
  }
  else{
    return(
    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 50}}>
      <Icon size={60} name='box-open' type='font-awesome' color='#e8e8e8' />
      <Text style={{width: 250, textAlign: 'center', paddingTop: 10}}>Không có dữ liệu</Text>
    </View>
    )
  }
}
  
  render() {
    return (
        <View style={styles.container}>
          <Text style={{fontSize: 20, color: 'black', paddingStart: 20, paddingTop: 20}}>Phiếu điểm</Text>
          {this.state.loading?
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
            </View>
            :
            this.renderData()
          }
        </View>
    );
  }
}
