import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Icon } from 'react-native-elements'
import styles from './Styles/YT_DB_TabBarStyles'

export default class YT_DB_TabBar extends React.Component {
  icons = [];
  constructor(props) {
    super(props);
    this.icons = [];
  }
  textStyle = (active, i) => {
    return {
      fontSize: 10,
      color: active === i ? '#9aca40' : 'rgb(204,204,204)'
    }
  }

  render() {
    var logo = ['diagnoses', 'notes-medical', 'bell']
    return <View style={[styles.tabs, this.props.style, ]}>
      {this.props.tabs.map((tab, i) => {
        icon = logo[i]

        return <TouchableOpacity key={tab} onPress={() => this.props.goToPage(i)} style={{ flex: 1, alignItems: 'center', justifyContent: 'center',}}>
          <Icon
            name={icon}
            size={28}
            color={this.props.activeTab === i ? '#9aca40' : 'rgb(204,204,204)'}
            ref={(icon) => { this.icons[i] = icon; }}
            type='font-awesome'
          />
          <Text style={this.textStyle(this.props.activeTab,i)}>{tab}</Text>
        </TouchableOpacity>;
      })}
    </View>;
  }
}