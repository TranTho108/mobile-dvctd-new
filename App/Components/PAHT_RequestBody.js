import React, { Component } from 'react'
import { PermissionsAndroid,Picker, ScrollView, Text, Image, View, TextInput, CheckBox, KeyboardAvoidingView, TouchableOpacity, ToastAndroid, StatusBar, Dimensions, ActivityIndicator, Keyboard} from 'react-native'
import { Images } from '../Themes'
import {ButtonGroup, Icon, Button} from 'react-native-elements'
import Modal from 'react-native-modal';
import {} from '../Api/Test'
import axios from 'axios'
import {SERVER_URL} from '../Config/server'
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import Toast, {DURATION} from 'react-native-easy-toast'
 
// Styles
import styles from './Styles/PAHT_RequestBodyStyles'

import Video from 'react-native-video';

import ImagePicker from 'react-native-image-crop-picker';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class PAHT_RequestBody extends Component {

  constructor() {
    super();
    this.state = {
      images: [],
      visibleModal: false,
      visibleModal_1: false,
      isLoading: false,
      hoten: '',
      sdt: '',
      tieude: '',
      noidung: '',
      email:'',
      diachi:''
    };
  }

  renderModalContent = () => (
    <View style={styles.content}>
    <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
      <Button
        type='clear'
        buttonStyle={{height: 40,}}
        containerStyle={styles.buttoninModal}
        onPress={this.pickMultiple.bind(this)}
        icon={
        <Icon
          name="photo-library"
          size={20}
          color="#2089dc"
        />
        }
        title=" Chọn từ thư viện"
      />
      <Button
        onPress={() => this.pickSingleWithCamera(false,mediaType='photo')}
        type='clear'
        buttonStyle={{height: 40}}
        containerStyle={styles.buttoninModal}
        icon={
        <Icon
          name="add-a-photo"
          size={20}
          color="#2089dc"
        />
        }
        title=" Chụp ảnh"
      />
      <Button
        type='clear'
        onPress={() => this.pickSingleWithCamera1(false, mediaType='video') + this.setState({visibleModal: false})}
        buttonStyle={{height: 40}}
        containerStyle={styles.buttoninModal}
        icon={
        <Icon
          name="video-call"
          size={20}
          color="#2089dc"
        />
        }
        title=" Quay video"
      />
      <Button
        onPress={() => this.setState({ visibleModal: false })}
        title="Thoát"
        buttonStyle={{height: 40}}
        containerStyle={styles.buttoninModal}
        type='clear'
        icon={
          <Icon
            name="exit-to-app"
            size={20}
            color="#2089dc"
          />
          }
      />
    </View>
  );

  renderModalContent_1 = () => {
    if(!this.state.isLoading){
      return(
      <Animatable.View animation='fadeIn' style={styles.content1}>
        <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
        <Icon
          containerStyle={{marginTop: 20}}
          name="check-circle"
          size={60}
          color="#28a745"
        />
        <Text style={{textAlign: 'center', color: 'black', fontSize: 16, margin: 10}}>Phản ánh được gửi thành công</Text>
        <Button
          onPress={() => {this.setState({visibleModal_1: false})}}
          buttonStyle={{height: 40, margin: 10, marginLeft: 20, marginRight: 20, backgroundColor: '#28a745', borderRadius: 5}}
          title=" ĐÓNG"
          containerStyle={{marginBottom: 20}}
        />        
      </Animatable.View>
      );}
    else{
      return(
        <View style={styles.content1}>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
          <ActivityIndicator size="large" color="black" />
          <Text style={{color: 'black', textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
      </View>
      );
    }
  }

  sendContent = async(hoten,email,sdt,tieude,noidung, diachi, images) => {
    //this.setState({isLoading: false})
    await this.fetchTest5(hoten,email,sdt,tieude,noidung, diachi, images)
  }


  fetchTest5 = async(hoten,email,sdt,tieude,noidung, diachi, images) => {
    var arr = ""
    count = 0
    await Promise.all(images.map(async (i) => {
    if(i.type == "video/mp4"){
        var data = new FormData();
        var uri = i.uri
        var namevideo = uri.substr(-20,20)
        arr = arr + "<media><type>Video</type><name>"+ namevideo +"</name><base64>"+ "/gopy/Video/test/" +"</base64></media>"
        console.log(arr)
        data.append("File", {
            uri: i.uri,
            name: namevideo,
            type: i.type
        });
        data.append("site","gopy");
        data.append("doclib","Video");
        data.append("subdir","test"); // thư mục upload
        data.append("useadmin","true");
        await axios({
            method: 'post',
            url: 'https://hatinhcity.gov.vn/_layouts/tandan/UploadControler.ashx',
            data: data,
            config: { headers: {'Content-Type': 'multipart/form-data' }},
            onUploadProgress: (p) => {
                console.log(p.loaded / p.total); 
                //this.setState({
                  //fileprogress: p.loaded / p.total
                //})
              }
            })
            .then(function (response) {
                //handle success
                console.log(response);
                count ++
            })
            .catch(function (err) {
                //handle error
                console.log(err);
            });
    }
    else{
      var base64 = i.data[0]
      var uri = i.uri
      var nameimage = uri.substr(-20,20)
      arr = arr + "<media><type>Image</type><name>"+ nameimage +"</name><base64>"+ "data:image/png;base64,"+ base64 +"</base64></media>"
      count ++
    }
    }));
      await console.log(count,images.length)
      var mediagui = `<data>` + arr + `</data>`
      var mediagui2 = encodeURIComponent(mediagui)
      console.log(mediagui2)
      var body = JSON.stringify({
        hoten: hoten,
        diachi: diachi,
        email: email,
        dienthoai: sdt,
        tieude: tieude,
        noidung: noidung,
        mediagui: mediagui2
    })
      await axios({
        method: 'post',
        url: `${SERVER_URL}/Send`,
        data: body,
        headers: {'Content-Type': 'application/json'},
      })
      .then(response => {
          console.log(response.data)
          this.setState({hoten: '', sdt: '', diachi: '', tieude: '', noidung: '',email: '', images: [], isLoading: false})
      })
}




  checkContent = (hoten,email,sdt,tieude,noidung, diachi, images) => {
    Keyboard.dismiss()
    if(!hoten || !sdt || !tieude || !noidung || !diachi){
      this.refs.toast.show('Vui lòng nhập đầy đủ thông tin', ToastAndroid.LONG);
    }
    else{
      this.setState({visibleModal_1: true, isLoading: true})
      this.sendContent(hoten,email,sdt,tieude,noidung, diachi,images)
    }
  }


  arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
      for(var j=i+1; j<a.length; ++j) {
          if(a[i].uri === a[j].uri)
              a.splice(j--, 1);
      }
    }

    return a;
  }
  
  pickMultiple() {
    ImagePicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      includeBase64: true,
    }).then(images => {
      var images1 = images.map(i => {
        return {uri: i.path, data: [i.data], type: i.mime};
      })
      var images2 = this.arrayUnique(this.state.images.concat(images1));
      this.setState({
        images: images2,
        visibleModal: false
      });
    })
    .catch(e => console.log(e));
  }


  renderVideo(video) {
    return (<View style={styles.viewVideo}>
      <Video source={{uri: video.uri, type: video.mime}}
         resizeMode='cover'
         style={{position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
          }}
         rate={1}
         paused={false}
         volume={1}
         muted={false}
         resizeMode={'cover'}
         onError={e => console.log(e)}
         onLoad={load => console.log(load)}
         repeat={true}
          />
          <Icon 
          name='check-box'
          onPress={() => this.cleanupSingleImage(video.uri)}
          size={28}
          color='#E53935'
          containerStyle={styles.iconVideo}
        />
     </View>);
  }

  cleanupSingleImage = (uri) => {
    this.setState({images: this.state.images.filter(function(item) {
      return item.uri !== uri
    })})
  }

  pickSingleWithCamera(cropping, mediaType) {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      mediaType,
      includeBase64: true
    }).then(i => {
      var image1 = {uri: i.path, data: [i.data], type: i.mime}
      var images2 = this.arrayUnique(this.state.images.concat(image1));
      this.setState({
        images: images2,
        visibleModal: false
      });
    }).catch(e => console.log(e));
  }

  pickSingleWithCamera1(cropping, mediaType) {
    ImagePicker.openCamera({
      mediaType: 'video',
    }).then(image => {
      var image1 = {uri: image.path, data: [],type: image.mime}
      var images1 = this.state.images.concat(image1)
      this.setState({
        images: images1,
        visibleModal: false
      });
    }).catch(e => console.log(e));
  }

  renderImage(image) {
    const uri = image.uri
    return (
      <View style= {{margin: 10}}>
        <Image style={styles.renderImage} source={image} />
        <Icon 
          name='check-box'
          onPress={() => this.cleanupSingleImage(uri)}
          size={28}
          color='#E53935'
          containerStyle={styles.iconImage}
        />
      </View>
      )
  }

  renderAsset(image) {
    console.log(image)
    if (image.type && image.type.toLowerCase().indexOf('video/') !== -1) {
      return this.renderVideo(image);
    }

    return this.renderImage(image);
  }


  
  render () {
    const {hoten,email,sdt,tieude,noidung,images, diachi} = this.state;
    return (
      
      <View style={styles.view1}>
        <KeyboardAvoidingView behavior='height'>
        <StatusBar backgroundColor='transparent' barStyle= 'dark-content' translucent={true} />
        <View style={styles.linearGradient1}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='black' />
            <Text style={styles.title}>Gửi phản ánh</Text>
          </View>
          <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', paddingEnd: 10}} onPress={() => {this.checkContent(hoten,email,sdt,tieude,noidung, diachi, images)}}>
              <Text style={{fontWeight: 'bold', color: '#D84315'}}>GỬI </Text>
              <Icon name='send' color='#D84315'/>
          </TouchableOpacity>
        </View>
        <LinearGradient
          colors={['rgba(0,0,0,0.1)', 'transparent']}
          style={{
              left: 0,
              right: 0,
              height: 5,
          }}
        />
      <ScrollView>

        <View style={styles.viewBody}>
          <Modal
            style={{ margin: 0, flex: 1 }}
            backdropColor="rgba(0,0,0,0.8)"
            transparent={true}
            //backdropOpacity={0.8}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={0}
            animationOutTiming={0}
            backdropTransitionInTiming={600}
            backdropTransitionOutTiming={600}
            isVisible={this.state.visibleModal_1}
            onBackButtonPress={() => this.setState({ visibleModal_1: false })}
            deviceHeight={height+100}
          >
          {this.renderModalContent_1()}
          </Modal>

          <View style={{}}>
            <Text style={{fontSize: 16, color: 'black'}}>Tổ chức, công dân</Text>
            <TextInput value={this.state.hoten} textContentType='name' style={{color: '#424242', height: 30}} placeholder='Nhập họ tên đầy đủ' onChangeText={(text) => this.setState({hoten: text})}/>
          </View>

          <View style={{}}>
            <Text style={{fontSize: 16, color: 'black'}}>Thông tin liên hệ</Text>
            <TextInput value={this.state.sdt} textContentType='telephoneNumber' maxLength={10} style={{height: 30}} placeholder='Nhập số điện thoại'  onChangeText={(text) => this.setState({sdt: text})}/>
            <TextInput value={this.state.email} textContentType='emailAddress' style={{height: 30}} placeholder='Nhập email'  onChangeText={(text) => this.setState({email: text})}/>
          </View>

          <View style={{}}>
            <Text style={{fontSize: 16, color: 'black'}}>Tiêu đề phản ánh</Text>
            <TextInput value={this.state.tieude} style={{height: 30}} placeholder='Nhập tiêu đề'  onChangeText={(text) => this.setState({tieude: text})}/>
          </View>

          <View style={{}}>
          <Text style={{fontSize: 16, color: 'black'}}>Nội dung phản ánh</Text>
            <TextInput returnKeyType='done' blurOnSubmit={true} onSubmitEditing={() => Keyboard.dismiss()} value={this.state.noidung} style={{flex: 1, height: 150}} placeholder='Nhập nội dung' multiline={true} textAlignVertical='top' onChangeText={(text) => this.setState({noidung: text})}/>
          </View>

          <View style={{}}>
            <View>
              <Text style={{fontSize: 16, color: 'black'}}>Vị trí phản ánh</Text>
              <View style={styles.viewLocation}>
                <Icon name='location-on' size={25}/>
                <TextInput value={this.state.diachi} onChangeText={(text) => this.setState({diachi: text})} textContentType='location' style={{flex: 1, height: 30}} placeholder='Nhập vị trí'/>
              </View>
              </View>

              <View style={{paddingTop: 30}}>
                <Text style={{fontSize: 16, color: 'black'}}>Thêm hình ảnh, video</Text>
                
              <Button
                  type='outline'
                  buttonStyle={{height: 50}}
                  containerStyle={{padding: 10}}
                  onPress={() => this.setState({ visibleModal: true })}
                  icon={
                  <Icon
                    name="add-a-photo"
                    size={16}
                    color="#2089dc"
                  />
                  }
                  title=" Thêm ảnh, video"
              />
              <Modal
                backdropTransitionOutTiming={0}
                isVisible={this.state.visibleModal}
                onSwipeComplete={() => this.setState({ visibleModal: false })}
                swipeDirection={['up', 'left', 'right', 'down']}
                style={styles.bottomModal}
                hideModalContentWhileAnimating={true}
                onBackButtonPress={() => this.setState({ visibleModal: false })}
                onBackdropPress={() => this.setState({ visibleModal: false })}
                deviceHeight={height+100}
              >
                {this.renderModalContent()}
              </Modal>
                
                <View style={styles.viewSelectedfile}>
                  {this.state.images ? this.state.images.map(i => <View key={i.uri}>{this.renderAsset(i)}</View>) : null}
                </View>
              </View>
            </View>
        </View>
      </ScrollView>
      </KeyboardAvoidingView>
      <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
      </View>

    )
  }
}
