import React, { Component } from "react";
import { View, AsyncStorage, FlatList, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Tooltip} from "react-native-elements";
import styles from "./Styles/GD_MenuTabStyles"
import moment from 'moment'
import 'moment/locale/vi'
moment.locale('vi')
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'

export default class GD_TKB_MainBody extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          dataNoti: [],
          loading: true,
          dataTKB: []
      }
    }
  
    componentWillMount = async() => {
      var userToken = await AsyncStorage.getItem('userToken')
      var NamHocID = await AsyncStorage.getItem('NamHocID')
      var studentID = await AsyncStorage.getItem('studentID')
      var body = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID}
      var data1 = await requestPOST(`${GD_URL}/ThongBao`, body)
      var data2 = data1.data?data1.data:[]
      this.setState({loading: false, dataNoti: data2})
    }
  
    _renderItem = ({item,index}) => {
      var date = moment(item.NgayThongBao).format('L')
      return(
        <View style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
          <View style={{height: 30,width: 30, borderRadius: 30, backgroundColor: '#2AA5FF', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='bell' color='#fff' size={14} /></View>
          <View style={{flex: 1, paddingStart: 10}}>
            <Text style={{fontSize: 14, color: 'black'}}>{item.TieuDe}</Text>
            <Text style={{color: '#757575'}}>{item.NoiDung}</Text>
            <Text style={{textAlign: 'right', fontStyle: 'italic',color: '#757575', fontSize: 12, paddingTop: 5}}>{date}</Text>
          </View>
        </View>
      )
    }
  
    renderBody = () => {
      if(!this.state.loading){
        return(
          <View style={{padding: 10}}>
              <FlatList 
                data={this.state.dataNoti}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={EmptyFlatlist}
              />
            </View>
        )
      }
      else{
        return(
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
          </View>
        )
      }
    }
      
    render() {
      return (
          <View style={styles.container}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#2AA5FF', padding: 15, paddingTop: 35}}>
              <Icon onPress={() => this.props.navigation.goBack()} name='chevron-left' type='font-awesome' color='#fff' size={24} />
              <Text style={styles.text3}>Thông báo</Text>
              <Icon name='arrow-left' type='font-awesome' color='transparent' size={24} />
            </View>
            {this.renderBody()}
          </View>
      );
    }
}
