import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, FlatList} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/GCTT_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {GCTT_DANHMUC} from '../Data/GCTT_Data'
import {requestGET, requestPOST} from "../Api/Basic"
import {GCTT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import axios from 'axios'


export default class GCTT_MainBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data:[],
        loading: true
    }
  }

    componentWillMount = async() => {
        var userToken = await AsyncStorage.getItem('userToken')
        var data1 = await requestGET(`${GCTT_URL}/NhomLinhVucMatHangs/mGetList`)
        var data = data1.data?data1.data:[]
        this.setState({data: data, loading: false})
    }

    renderEmpty = () => {
      if(this.state.data.length > 0){
        return(
          <ScrollView>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    flexWrap: 'wrap',
                }}>
                <View key={0} style={{width: '50%'}}>
                  <TouchableOpacity onPress={() => {this.props.navigation.navigate('GCTT_SearchScreen')}} style={styles.view1}>
                      <Image source={images.background.loupe} style={{width: 60, height: 60}} />
                      <Text style={{paddingTop: 10}}>Tìm kiếm</Text>
                  </TouchableOpacity>
                </View>
                {this.state.data.map(item => 
                (
                    <View key={item.ID} style={{width: '50%'}}>
                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('GCTT_NMH_MainScreen', {name: item.Ten, IDLVMH: item.ID})}} style={styles.view1}>
                        <Image source={images.background.box} style={{width: 60, height: 60}} />
                        <Text style={{paddingTop: 10}}>{item.Ten}</Text>
                    </TouchableOpacity>
                    </View>
                ))
                }
            </View>               
            
        </ScrollView>
        )
      }
      else{
        return(
          <EmptyFlatlist />
        )
      }
    }

    renderBody = () => {
      if(!this.state.loading){
        return(
          <>
          {this.renderEmpty()}
          </>
        )
      }
      else{
        return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2856C6' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#2856C6'}}>Đang lấy dữ liệu</Text>
        </View>
        )
      }
    }

    _renderItem = ({item}) => {
      return(
          <View key={item.id} style={{}}>
            <TouchableOpacity onPress={() => {}} style={styles.view1}>
                <Image source={images.background.box} style={{width: 60, height: 60}} />
                <Text style={{paddingTop: 10}}>{item.Ten}</Text>
            </TouchableOpacity>
            </View>
      )
  }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Giá cả thị trường</Text>
          <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
        </View>
        {this.renderBody()}
    </View>
    );
  }
}