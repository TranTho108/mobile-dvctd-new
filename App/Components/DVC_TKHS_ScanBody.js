import React, { Component } from "react";
import { View, PermissionsAndroid, Platform} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_TKHS_ScanBodyStyle"
import {RNCamera} from 'react-native-camera';
import { withNavigationFocus } from "react-navigation";

class DVC_TKHS_ScanBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      barcode: '',
      cameraType: 'back',
      text: 'Scan Barcode',
      torchMode: 'off',
      type: '',
    };
  }

  barcodeReceived(e) {
    console.log(e.data)
    if(e.data){
    this.props.navigation.navigate("DVC_TKHS_SearchScreen",{code: e.data});
    }
  }

  renderCamera() {
    const isFocused = this.props.navigation.isFocused();
 
    if (!isFocused) {
        return null;
    } else if (isFocused) {
        return (
          <View style={styles.container}>
				<RNCamera
					style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
					onFocusChanged={() => {}}
					onZoomChanged={() => {}}
					defaultTouchToFocus
					mirrorImage={false}
					barcodeFinderVisible={true}
					barcodeFinderWidth={280}
					barcodeFinderHeight={220}
					barcodeFinderBorderColor="red"
					barcodeFinderBorderWidth={2}
          onBarCodeRead={this.barcodeReceived.bind(this)}
          captureAudio={false}
				>
                <View style={styles.rectangleContainer}>
        <View style={styles.rectangle} />
      </View>
        </RNCamera>
          <View style={{position: 'absolute', top: 30, left: 10, height: 30}}><Icon style={{}} size={34} onPress={() => this.props.navigation.goBack()} name='keyboard-arrow-left' color='white' /></View>
          </View>
        )
    }
  }

  render() {
    return (
      <View style={styles.container}>
      {this.renderCamera()}
      </View>
    )
  }
}
export default withNavigationFocus(DVC_TKHS_ScanBody);