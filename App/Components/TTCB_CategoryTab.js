import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/TTCB_CategoryTabStyles"
import {requestPOST, requestGET} from '../Api/Basic'
import {CB_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'


export default class TTCB_CategoryTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dataCategories: [],
      loading: true
    };
  }

  componentWillMount = async() => {
    var dataCategories = await requestGET(`${CB_URL}/categories`)
    this.setState({dataCategories: dataCategories.data, loading: false})
  }


  _renderNoti = (Active,index) => {
    if(Active){
      return(
      <TouchableOpacity onPress={() => this.changeNotifi(index,Active)} style={{alignSelf: 'flex-end', flexDirection: 'row', backgroundColor: '#EEA80B', borderRadius: 5, alignItems: 'center', justifyContent: 'center', padding: 5, paddingStart: 15, paddingEnd: 15}}>
        <Icon size={18} type='font-awesome' color='#fff' name='bell' />
        <Text style={{color: '#fff', paddingStart: 5}}>Thông báo</Text>
      </TouchableOpacity>
      )
    }
    else{
      return(
        <TouchableOpacity onPress={() => this.changeNotifi(index,Active)} style={{alignSelf: 'flex-end', flexDirection: 'row', backgroundColor: '#E0E0E0', borderRadius: 5, alignItems: 'center', justifyContent: 'center', padding: 5, paddingStart: 15, paddingEnd: 15}}>
          <Icon size={18} type='font-awesome' color='#424242' name='bell-slash' />
          <Text style={{color: '#424242', paddingStart: 5}}>Thông báo</Text>
      </TouchableOpacity>
      )
    }
  }

  changeNotifi = (index,Active) => {
    this.state.dataCategories[index].Active = !Active
    this.setState({dataCategories: this.state.dataCategories})
  }

  _renderItem = ({item,index}) =>{
    return(
      <TouchableOpacity onPress={() => this.props.navigation.navigate('TTCB_ListScreen', {name: item.Title, ID: item.Id})} style={{padding: 10, flex: 1, paddingBottom: 0}}>
        <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}> 
          <View style={{alignItems: 'center', width: 40}}>
            <Icon color='#37BCB3' name={item.Icon} type='font-awesome' size={28} />
            <View style={{width: 20, height: 20, borderRadius: 15, backgroundColor: '#FFEB3B', alignItems: 'center', marginTop: 10, justifyContent: 'center'}}><Text style={{textAlign: 'center', color: '#fff', fontWeight: 'bold'}}>{item.Count}</Text></View>
          </View>
          <View style={{padding: 10, flex: 1}}>
            <Text style={styles.title}>{item.Title}</Text>
            <Text style={styles.description}>{item.Description}</Text>
          </View>
        </View>
        {this._renderNoti(item.Active,index)}
        <View style={{borderBottomColor: '#e8e8e8', borderBottomWidth: 0.5, marginTop: 20}} />
      </TouchableOpacity>
    )
  }

  renderBody = () => {
    if(!this.state.loading){
      return(
        <FlatList
          showsVerticalScrollIndicator={false}
          data = {this.state.dataCategories}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
          ListEmptyComponent={EmptyFlatlist}
        />
      )
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#D84315' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#D84315'}}>Đang lấy dữ liệu</Text>
        </View>
      )
    }
  }


  render() {
    return (
        <View style={styles.container}>
          {this.renderBody()}
        </View>
    );
  }
}
