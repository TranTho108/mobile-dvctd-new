import React, { Component } from "react";
import { View, ImageBackground,ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_DGHL_MainBodyStyles"
import ScrollableTabView from 'react-native-scrollable-tab-view';
import DVC_DGHL_SendTab from './DVC_DGHL_SendTab'
import DVC_DGHL_SearchTab from './DVC_DGHL_SearchTab'
import LinearGradient from 'react-native-linear-gradient';
import images from '../Themes/Images'

export default class DVC_DGHL_MainBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      page : 0,
      data: []
    };
  }

  componentWillMount = () =>{
    
  }

  changePage(page){
    this.setState({page: page});
  }

  changeData = (data) =>{
    this.setState({data: data})
  }

  render() {
    return (
        <View style={styles.container}>
          <ImageBackground resizeMode='cover' source={images.background.trongdong} style={{flex:1}} imageStyle= {{opacity:0.1}} >
          <View style={styles.linearGradient1}><Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='white' /><Text style={styles.title}>Đánh giá hài lòng</Text></View>
          <LinearGradient
            colors={['rgba(0,0,0,0.1)', 'transparent']}
            style={{
                left: 0,
                right: 0,
                height: 5,
            }}
          />
          <ScrollableTabView
            initialPage={0}
            tabBarPosition='bottom'
            page={this.state.page}
            locked={true}
            renderTabBar={() => <View />}
          >
            <ScrollView tabLabel="Nhập mã hồ sơ" style={styles.tabView}>
              <DVC_DGHL_SearchTab {...this.props} handleChangeData={this.changeData.bind(this)} handleChangePage={this.changePage.bind(this)}/>
            </ScrollView>
            <ScrollView tabLabel="Trả lời câu hỏi đánh giá" style={styles.tabView}>
              <DVC_DGHL_SendTab {...this.props} handleChangePage={this.changePage.bind(this)} data={this.state.data}/>
            </ScrollView>
          </ScrollableTabView>
          </ImageBackground>
        </View>
    );
  }
}
