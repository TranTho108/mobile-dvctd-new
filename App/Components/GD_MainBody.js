import React, { Component } from "react";
import {View,ScrollView,ActivityIndicator, FlatList, TouchableOpacity, AsyncStorage} from "react-native";
import {Text, Button, Icon, Avatar} from "react-native-elements";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import GD_ClassTab from './GD_ClassTab'
import GD_HomeTab from './GD_HomeTab'
import GD_MenuTab from './GD_MenuTab'
import GD_PointTab from './GD_PointTab'
import GD_TabBar from './GD_TabBar'
import {listData, detailData} from '../Data/GD_Data'
import Modal from 'react-native-modal';
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import RadioForm from 'react-native-simple-radio-button';

import styles from './Styles/GD_MainBodyStyles'
var moment = require('moment')
moment.locale('vi')


export default class GD_MainBody extends Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      student: [],
      listStudent: [],
      value: '',
      value1: '',
      visibleModal: false,
      visibleModal1: false,
      visibleModal2: false,
      namhocs: [],
      hockys: [],
      NamHoc: [],
      HocKy: [],
      userToken: '',
      studentID: '',
      Truong: '',
      Lop: ''
    };
  }

  componentWillMount = async() => {
    var studentID = await AsyncStorage.getItem('studentID')
    if(studentID){
      var studentInfo = await AsyncStorage.getItem('studentInfo')
      var NamHoc = await AsyncStorage.getItem('NamHoc')
      var HocKy = await AsyncStorage.getItem('HocKy')
      var Truong = await AsyncStorage.getItem('Truong')
      var Lop = await AsyncStorage.getItem('Lop')
      this.setState({student: JSON.parse(studentInfo), NamHoc: NamHoc, HocKy: HocKy, Truong: Truong, Lop: Lop})
      setTimeout(() => this.setState({loading: false,}), 1000)
    }
    else{
      var userToken = await AsyncStorage.getItem('userToken')
      var body = {'token': userToken}
      var data1 = await requestPOST(`${GD_URL}/GetHocSinhsByNguoiLienLacs`, body)
      var data2 = data1.data?data1.data:[]
      this.setState({listStudent: data2, userToken: userToken,visibleModal: true})
    }
  }

  changeStudent = async(item) => {
    this.setState({loading: true})
    await AsyncStorage.multiSet([
      ['studentID', item.ID.toString()],
      ['studentInfo', JSON.stringify(item)]
    ])
    var data1 = await requestGET(`${GD_URL}/GetNamHocs`)
    var data2 = data1.data?data1.data:[]
    var radio_props = []
    data2.forEach((i) => {
      radio_props.push({label: i.TieuDe, value: i.ID})
    })
    var data3 = await requestGET(`${GD_URL}/GetHocKys`)
    var data4 = data3.data?data3.data:[]
    var radio_props1 = []
    data4.forEach((i) => {
      radio_props1.push({label: i.TieuDe, value: i.ID})
    })
    this.setState({visibleModal: false, studentID: item.ID, namhocs: radio_props, hockys: radio_props1, student: item})
    setTimeout(() => this.setState({visibleModal1: true}), 1000)
  }


  showStudents = async() => {
    var userToken = await AsyncStorage.getItem('userToken')
    var body = {'token': userToken}
    var data1 = await requestPOST(`${GD_URL}/GetHocSinhsByNguoiLienLacs`, body)
    var data2 = data1.data?data1.data:[]
    this.setState({listStudent: data2,visibleModal: true})
  }

  setNamHoc = async() => {
    if(this.state.value && this.state.value1){
      var a = this.state.namhocs.findIndex(x => x.value === this.state.value)
      var b = this.state.hockys.findIndex(x => x.value === this.state.value1)
      var body = {'token': this.state.userToken, 'HocSinhID': this.state.studentID, 'NamHocID': this.state.value}
      var data1 = await requestPOST(`${GD_URL}/GetTruongLop`, body)
      var data2 = data1.data?data1.data:[]
      var Truong = data2[0]?data2[0].TruongHocTieuDe:''
      var Lop = data2[0]?data2[0].LopHocTieuDe:''
      await AsyncStorage.multiSet([
        ['NamHocID', this.state.value.toString()],
        ['HocKyID', this.state.value1.toString()],
        ['NamHoc', this.state.namhocs[a].label],
        ['HocKy', this.state.hockys[b].label],
        ['Truong', Truong],
        ['Lop', Lop],
      ])
      this.setState({visibleModal1: false, loading: false,Truong: Truong, Lop: Lop, NamHoc: this.state.namhocs[a].label, HocKy: this.state.hockys[b].label})
    }
  }

  _renderItem = ({item}) => {
    var date = moment(item.NgaySinh).format('L')
    return(
      <TouchableOpacity onPress={() => this.changeStudent(item)} style={{flexDirection: 'row', padding: 10, margin: 10, backgroundColor: '#2AA5FF', borderRadius: 10}}>
      <View style={{height: 40, width: 40, borderRadius: 50, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
        <Icon type='font-awesome' name='user' size={18} color='#fff' />
      </View>
      <View style={{paddingStart: 10}}>
        <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>{item.Ten}</Text>
        <Text style={{color: '#fff', fontSize: 12, paddingTop: 2,fontWeight: 'bold'}}>{date}</Text>
      </View>
      </TouchableOpacity>
    )
  }

  modalBack = () => {
    if(this.state.student.length > 0){
      this.setState({visibleModal: false})
    }
    else{
      this.setState({visibleModal: false})
      setTimeout(() => this.props.navigation.goBack(), 500)
    }
  }

  renderModalContent = () => {
    return(
      <View style={styles.content}>
        <Text style={{fontStyle: 'italic', padding: 20, textAlign: 'center'}} >Vui lòng chọn học sinh: </Text>
        <FlatList 
          data={this.state.listStudent}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch', paddingTop: 30}}>
          <TouchableOpacity onPress={() => {this.modalBack()}}>
            <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold'}}>ĐÓNG</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderModalContent1 = () => {
    return(
      <View style={styles.content}>
        <Text style={{fontStyle: 'italic', padding: 20, textAlign: 'center'}} >Vui lòng chọn năm học: </Text>
        <RadioForm
          radio_props={this.state.namhocs}
          initial={-1}
          onPress={(value) => {this.setState({value:value})}}
          buttonSize={15}
          buttonWrapStyle={{padding: 20}}
        />
        <Text style={{fontStyle: 'italic', padding: 20, textAlign: 'center'}} >Vui lòng chọn học kỳ: </Text>
        <RadioForm
          radio_props={this.state.hockys}
          initial={-1}
          onPress={(value) => {this.setState({value1:value})}}
          buttonSize={15}
          buttonWrapStyle={{padding: 20}}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch', paddingTop: 30}}>
          <TouchableOpacity onPress={() => {this.setNamHoc()}}>
            <Text style={{fontSize: 14, color: '#66BB6A', fontWeight: '600'}}>CHỌN</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderModalContent2 = () => {
    var date = moment(this.state.student.NgaySinh).format('L')
    return(
        <View style={{flex: 1, backgroundColor: '#2AA5FF'}}>
        <Icon name='close' size={30} color='#fff' containerStyle={{alignSelf: 'flex-end', padding: 10, paddingTop: 33}} onPress={() => this.setState({visibleModal2: false})} />
        <View style={{flexDirection: 'row', padding: 10, paddingTop: 10, alignItems: 'center',}}>
          <View style={{height: 80, width: 80, borderRadius: 80, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
            <Icon type='font-awesome' name='user' size={24} color='#fff' />
          </View>
          <View style={{paddingStart: 10}}>
            <Text style={{color: '#fff', fontSize: 18, fontWeight: 'bold'}}>{this.state.student.Ten}</Text>
            <Text style={{color: '#fff', fontSize: 14, fontWeight: 'bold'}}>ID {this.state.student.ID}</Text>
          </View>
          
          </View>
          <View style={{padding: 10, margin: 10, backgroundColor: '#fff', borderRadius: 10}}>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Trường</Text>
              <Text>{this.state.Truong}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Lớp</Text>
              <Text>{this.state.Lop}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Khối</Text>
              <Text>{this.state.student.Grade}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Năm học</Text>
              <Text>{this.state.NamHoc}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Giới tính</Text>
              <Text>{this.state.student.GioiTinh}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Ngày sinh</Text>
              <Text>{date}</Text>
            </View>
          </View>
        </View>
    )
  }
  


  renderData = () => {
    if(!this.state.loading){
      return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={{height: 90, alignSelf: 'stretch', flexDirection: 'row', paddingTop: 35, paddingStart: 20, paddingEnd: 20, justifyContent: 'space-between', backgroundColor: '#2AA5FF'}}>
          <View style={{paddingTop: 10}}>
          <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{}} />
          </View>
          <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => {this.setState({visibleModal2: true})}} style={{height: 40, width: 40, borderRadius: 50, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
            <Icon type='font-awesome' name='user' size={18} color='#fff' />
          </TouchableOpacity>
          <View style={{paddingStart: 10, paddingEnd: 10}}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>{this.state.student.Ten}</Text>
            <Text style={{color: '#fff', fontSize: 12, paddingTop: 2,fontWeight: 'bold'}}>{this.state.HocKy}, {this.state.NamHoc}</Text>
          </View>
          <Icon type='font-awesome' name='chevron-down' size={20} color='#fff' containerStyle={{paddingTop: 8}} onPress={() => this.showStudents()} />
          </View>
          <View style={{paddingTop: 10}}>
          <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{}} />
          </View>
        </View>
        <ScrollableTabView
          style={{flex: 1,}}
          initialPage={0}
          tabBarPosition='bottom'
          renderTabBar={() => <GD_TabBar {...this.props}/>}
        >
          <ScrollView style={styles.tabView}>
            <GD_HomeTab {...this.props}/>
          </ScrollView>
          <ScrollView style={styles.tabView}>
            <GD_PointTab {...this.props}/> 
          </ScrollView>
          <ScrollView style={styles.tabView}>
            <GD_ClassTab {...this.props}/>
          </ScrollView>
          <ScrollView style={styles.tabView}>
            <GD_MenuTab {...this.props}/>
          </ScrollView>
        </ScrollableTabView>
        <Modal
          backdropTransitionOutTiming={0}
          isVisible={this.state.visibleModal2}
          style={{margin: 0}}
          hideModalContentWhileAnimating={true}>
          {this.renderModalContent2()}
        </Modal>
      </View>
      )
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#2AA5FF'}}>Đang lấy dữ liệu</Text>
        </View>
      )
    }
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {this.renderData()}
        <Modal
          backdropTransitionOutTiming={0}
          isVisible={this.state.visibleModal}
          style={{margin: 0}}
          hideModalContentWhileAnimating={true}>
          {this.renderModalContent()}
        </Modal>
        <Modal
          backdropTransitionOutTiming={0}
          isVisible={this.state.visibleModal1}
          style={{margin: 0}}
          hideModalContentWhileAnimating={true}>
          {this.renderModalContent1()}
        </Modal>
        
      </View>
    );
  }
}
