import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, TouchableOpacity, Image, ToastAndroid, Linking, ImageBackground, Animated, PermissionsAndroid, ScrollView, RefreshControl, Platform} from "react-native";
import {Text, Button, Icon, ButtonGroup} from "react-native-elements";
import styles from "./Styles/TĐTM_DetailBodyStyles"
import images from "../Themes/Images"
import Modal from "react-native-modal";
import SwipeableGmail from "./SwipeableGmail"
import {  RectButton } from 'react-native-gesture-handler';
import EmptyFlatlist from './EmptyFlatlist'
import {requestPOST, requestGET} from '../Api/Basic'
import {TĐTM_URL} from '../Config/server'
import Toast, {DURATION} from 'react-native-easy-toast'

const buttons = [ 'Phổ biến', 'Gần nhất']

export default class TĐTM_DetailBody extends Component {

    constructor () {
        super()
        this.state = {
            selectedIndex: 0,
            name: '',
            data: [],
            visibleModal: false,
            detail: [],
            history_TĐTM: [],
            refreshing: true,
            dataPP: [],
            IDLV: '',
            dataN: []
        }
        this.updateIndex = this.updateIndex.bind(this)
    }

    componentWillMount = async() => {
        //PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        var name = this.props.navigation.getParam('name')
        var IDLV = this.props.navigation.getParam('IDLV')
        var data1 = await requestGET(`${TĐTM_URL}/hotlines/popular/${IDLV}`)
        var dataPP = data1.data?data1.data:[]
        this.setState({name: name, dataPP: dataPP, data: dataPP, refreshing: false, IDLV: IDLV})
    }

    updateData = (selectedIndex) => {
        if(selectedIndex == 0) {
            this.setState({data: this.state.dataPP,refreshing: false})
          }
          else if(selectedIndex == 1) {
              navigator.geolocation.getCurrentPosition(async(position) => {
                  if (position) {
                    console.log(position)
                    var userLat = position.coords.latitude
                    var userlong = position.coords.longitude
                    var idcat = this.state.IDLV
                    var data1 = await requestGET(`${TĐTM_URL}/hotlines/near/distance?categoryid=${idcat}&lat=${userLat}&long=${userlong}`)
                    var dataN = data1.data?data1.data:[]
                    this.setState({
                      data: dataN,refreshing: false, dataN: dataN
                    });
                  }
                },
                (error) => {
                  console.log(error)
                  this.refs.toast.show('Không lấy được vị trí hiện tại do chưa bật GPS', ToastAndroid.LONG)
                  this.setState({
                      data: this.state.dataPP,refreshing: false, selectedIndex: 0
                  })
                }
                );
          }
    }

    onRefresh = () => {
        this.setState({
            refreshing: true,
        }, async() => {
            this.updateData(this.state.selectedIndex)
        })
    }
      updateIndex (selectedIndex) {
        this.setState({ selectedIndex,refreshing: true})
        this.updateData(selectedIndex)
      }

    dialCall = (number) => {
        let phoneNumber = '';
        if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
        else {phoneNumber = `telprompt:${number}`; }
        Linking.openURL(phoneNumber);
     };

     /*
     addPID = async(PID) => {
        var history_TĐTM = await AsyncStorage.getItem('history_TĐTM')
        var arr = history_TĐTM ? JSON.parse(history_TĐTM) : [];
        arr.push(PID)
        var arr1 = [...new Set([...arr])]
        this.setState({history_TĐTM: arr1})
        AsyncStorage.setItem('history_TĐTM', JSON.stringify(arr1));
     }

     removePID = async(PID) => {
        var arr = this.state.history_TĐTM
        var data = this.state.data
        var i = arr.indexOf(PID);
        if (i != -1) {
          arr.splice(i,1);
        }
        await AsyncStorage.setItem('history_TĐTM', JSON.stringify(arr));
        this.setState({history_TĐTM: arr})
        if(this.state.name === 'Yêu thích'){
            var j = data.findIndex((i) => {return i.PID === PID})
            data.splice(j,1)
            this.setState({data: data})
        }
    renderFavorite = (PID) => {
        if(this.state.history_TĐTM.includes(PID)){
            return(
                <Icon onPress = {() => this.removePID(PID)} size={30} name='favorite' color='#f44336' />
            )
          }
          else{
            return(
                <Icon onPress = {() => this.addPID(PID)} size={30} name='favorite-border' />
            )
          }
    }
     }*/
    
    SwipeableRow = ({ item, index }) => {
        var item1 = item.HotLine
        return (
            <SwipeableGmail item={item1}>
              <RectButton style={styles.rectButton} onPress={() => this.setState({visibleModal: true, detail: item1})}>
                <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item1})} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItem1s: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', flex: 1}}>
                        <Image resizeMode='stretch' style={{width: 60, height: 60}} source={images.background.service} />
                        <View>
                            <Text style={{fontSize: 16, paddingStart: 10, padding: 5, fontWeight: 'bold'}}>{item1.Title}</Text>
                            <Text style={{fontSize: 12, paddingStart: 10, color: '#9E9E9E'}}>{item1.Phone}</Text>
                        </View>
                    </View>
                    <View style={{height: 40, width: 40, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent', borderWidth: 1, borderColor: '#9E9E9E'}}>
                        <Icon size={18} type='font-awesome' name='info' color='#9E9E9E' />
                    </View>
                </TouchableOpacity>
                </RectButton>
            </SwipeableGmail>
          );
      }

      _renderModalContent = () => {
        if(this.state.detail){
        const {detail} = this.state
        return(
          <View style={styles.content2}>
            <ImageBackground imageStyle={{}} source={images.background.TĐTM_bg} style={{height: 150, justifyContent: 'flex-end', padding: 10}}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                        <Image resizeMode='cover' source={images.background.service} style={{width: 80, height: 80, borderRadius: 40}} />
                        <Text style={{padding: 10, fontWeight: 'bold'}}>{detail.Title}</Text>
                    </View>
                    
                </View>
            </ImageBackground>
            <View style={{padding: 20}}>
                <Text style={styles.section}>SĐT</Text>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.label}>{detail.Phone}</Text>
                    <TouchableOpacity onPress={() => this.dialCall(detail.Phone)} style={{alignItems: 'center', justifyContent:'center', height: 50, width: 50, borderRadius: 50, backgroundColor: '#4CAF50'}}>
                        <Icon size={24} name='phone' color='#fff' />
                    </TouchableOpacity>
                </View>
                <Text style={styles.section}>Thông tin</Text>
                <Text style={styles.label}>{detail.Detail}</Text>
                <Text style={styles.section}>Địa chỉ</Text>
                <Text style={styles.label}>{detail.Address}</Text>
            </View>
            <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
                 <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 10, alignSelf: 'flex-end'}}>ĐÓNG</Text>
            </TouchableOpacity>
          </View>
        )
        }
      }

      navigateToMap = () => {
        var data = this.state.data
        var dataMap = []
        data.map((item) => {
            dataMap.push({name: item.HotLine.Title, address: item.HotLine.Address, location: {latitude: Number(item.HotLine.Lat),longitude: Number(item.HotLine.Long)}, distance: item.Distance})
        })
        this.props.navigation.navigate('MapViewScreen', {data: dataMap})
      }

    renderBody(){
        if(!this.state.refreshing){
          return(
            <View style={{flex: 1}}>
            <FlatList 
                data={this.state.data}
                renderItem={this.SwipeableRow}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={EmptyFlatlist}
                ItemSeparatorComponent={() => <View style={styles.separator} />}
                extraData={this.state}
            /> 
          </View>
          )
        }
      }

    

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}>
                <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
                <Text style={styles.title}>{this.state.name}</Text>
                <Icon onPress={() => {this.navigateToMap()}} name='map' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            <ScrollView style={{flex: 1}}
                refreshControl={
                    <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.onRefresh(this.state.status)}
                    />
                }
                >
                <ButtonGroup
                    onPress={this.updateIndex}
                    selectedIndex={this.state.selectedIndex}
                    buttons={buttons}
                    containerStyle={{height: 35, backgroundColor: '#fff', borderWidth: 0, elevation: 3, margin: 10}}
                    buttonStyle={{}}
                    textStyle={{fontSize: 12}}
                    innerBorderStyle={{width: 0.2}}
                />
                {this.renderBody()}
            </ScrollView>
            <Modal
                onBackdropPress={() => this.setState({visibleModal: false})}
                backdropTransitionOutTiming={0}
                isVisible={this.state.visibleModal}
                style={{margin: 0}}
                hideModalContentWhileAnimating={true}>
            {this._renderModalContent()}
            </Modal>
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
