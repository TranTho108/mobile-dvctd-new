import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/NN_CSCN_MainBodyStyles"
import {NN_CSCN} from "../Data/NN_Data"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';


export default class NN_CSCN_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          
        };
      }

    _renderItem = ({item,index}) => {
    return(
        <TouchableOpacity onPress={() => this.setState({})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#45A659', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='warehouse' color='#fff' size={18} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
            <Text style={{fontSize: 16, fontWeight: 'bold', padding: 5}}>{item.name}</Text>
            <View style={{flexDirection: 'row', padding: 5}}><Text style={{color: '#757575', width: 50}}>Địa chỉ</Text><Text style={{color: '#757575', fontWeight: 'bold', flex: 1}}>{item.address}</Text></View>
        </View>
        </TouchableOpacity>
    )
    }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Cơ sở chăn nuôi</Text>
          <Icon onPress={() => this.props.navigation.navigate('MapViewScreen', {data: NN_CSCN})} name='map' color='white' containerStyle={{paddingEnd: 20}} />
        </View>
        <FlatList 
            data={NN_CSCN}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{padding: 10}}
        />
    </View>
    );
  }
}
