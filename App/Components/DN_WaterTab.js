import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  View,
  processColor,
  LayoutAnimation,
  ScrollView,
  Image,
  FlatList,
  TouchableOpacity,
  Switch,
  ToastAndroid,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import {Text, Button, Icon, Avatar} from "react-native-elements";
import Modal from "react-native-modal";
import images from "../Themes/Images"
import {N_Data, N_CB} from '../Data/DN_Data'
import { LineChart } from "react-native-charts-wrapper";
import EmptyFlatlist from './EmptyFlatlist'
import {requestPOST, requestGET} from '../Api/Basic'
import {DN_URL} from '../Config/server'
import Toast, {DURATION} from 'react-native-easy-toast'

const greenBlue = "rgb(26, 182, 151)";
const petrel = "rgb(59, 145, 153)";

export default class DN_WaterTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dataTB: [],
      visibleModal: false,
      detail: [],
      dataCB: [],
      visibleModal1: false,
      detail1: [],
      switchValue: false,
      loading: true,
      dataBGGD: [],
      dataBGDN: [],
      dataBD: [],
      dataMonth: []
    };
  }


  componentWillMount = async() => {
    var soHoKhau = await AsyncStorage.getItem('soHoKhau')
    var dataBGGD = await requestGET(`${DN_URL}/quotations/latest?serviceId=2&customerId=1`)
    var dataBGGD1 = dataBGGD.data?dataBGGD.data[0].Quotations:[]
    var dataBGDN = await requestGET(`${DN_URL}/quotations/latest?serviceId=2&customerId=2`)
    var dataBGDN1 = dataBGDN.data?dataBGDN.data[0].Quotations:[]
    var dataTB = await requestGET(`${DN_URL}/services/2/notifications`)
    var dataTB1 = dataTB.data?dataTB.data:[]
    var dataBD = await requestGET(`${DN_URL}/bill/services/2/households/${soHoKhau}?numbermonth=6`)
    var dataBD1 = dataBD.data?dataBD.data.reverse():[]
    var data2 = []
    var data3 = []
    var data4 = []
    dataBD1.map((item,index) => {
      data2.push({y: item.MoneyPay,x: index, marker: ''})
      var month = new Date(item.FromDate).getMonth()+1
      data3.push(`Th${month}`)
      if(item.Warning){data4.push({title: `Cảnh báo sử dụng nước bất thường tháng ${month}`, body: item.Warning})}
    })
    this.setState({dataTB: dataTB1, dataCB: data4,dataBGGD: dataBGGD1, dataBGDN: dataBGDN1, dataBD: data2, dataMonth: data3, loading: false})
  }

  toggleSwitch = (value) => {
    if(value){
      this.refs.toast.show('Thông báo sẽ được hiển thị trước khi cắt nước 30 phút', ToastAndroid.LONG)
    }
    this.setState({switchValue: value})
 }

  _renderItem = ({item,index}) => {
    return(
      <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 30,width: 30, borderRadius: 30, backgroundColor: 'rgb(26, 182, 151)', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='tint' color='#fff' size={14} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
          <Text style={{fontSize: 14, color: 'black'}}>{item.Name}</Text>
          <Text style={{color: '#757575'}}>{item.Content}</Text>
          <Text style={{textAlign: 'right', fontStyle: 'italic',color: '#757575', fontSize: 12, paddingTop: 5}}>{item.CreatedAt}</Text>
        </View>
      </TouchableOpacity>
    )
  }
  _renderItem2 = ({item,index}) => {
    var Price = item.Price.toLocaleString()
    return(
      <View style={styles.row}>
        <Text>{item.Name}</Text>
        <Text>{Price}</Text>
      </View>
    )
  }

  _renderItem1 = ({item,index}) => {
    return(
      <TouchableOpacity onPress={() => this.setState({visibleModal1: true, detail1: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 30,width: 30, borderRadius: 30, backgroundColor: 'rgb(26, 182, 151)', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='exclamation' color='#fff' size={14} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
          <Text style={{fontSize: 14, color: 'black'}}>{item.title}</Text>
          <Text numberOfLines={2} style={{color: '#757575'}}>{item.body}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  _renderModalContent1 = () => {
    if(this.state.detail1){
    const {detail1} = this.state
    return(
      <View style={styles.content2}>
        <ScrollView style={{padding: 20}}>
          <Text style={styles.title5}>Chi tiết cảnh báo</Text>
          <Text style={styles.section}>Tên</Text>
          <Text style={styles.label}>{detail1.title}</Text>
          <Text style={styles.section}>Nội dung</Text>
          <Text style={styles.label}>{detail1.body}</Text>
        </ScrollView>
        <TouchableOpacity onPress={() => {this.setState({visibleModal1: false})}}>
             <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
        </TouchableOpacity>
      </View>
    )
    }
  }

  renderNoti = (Time) => {
    if(new Date(Time).getTime() > new Date().getTime()){
      return(
        <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
          <Text style={styles.section}>Thông báo thiết bị</Text>
          <Switch
            onValueChange = {this.toggleSwitch}
            value = {this.state.switchValue}
          />
        </View>
      )
    }
  }

  _renderModalContent = () => {
    if(this.state.detail){
    const {detail} = this.state
    return(
      <View style={styles.content2}>
        <ScrollView style={{padding: 20}}>
          <Text style={styles.title5}>Chi tiết thông báo</Text>
          <Text style={styles.section}>Tên</Text>
          <Text style={styles.label}>{detail.Name}</Text>
          <Text style={styles.section}>Nội dung</Text>
          <Text style={styles.label}>{detail.Content}</Text>
          <Text style={styles.section}>Thời gian</Text>
          <Text style={styles.label}>{detail.Time}</Text>
          {this.renderNoti(detail.Time)}
        </ScrollView>
        <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
             <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
        </TouchableOpacity>
      </View>
    )
    }
  }

  renderBody = () => {
    if(!this.state.loading){
      return(
      <View>
        <Text style={{paddingTop: 10, paddingBottom: 10, fontWeight: 'bold'}}>Cảnh báo</Text>
          <FlatList 
            data={this.state.dataCB}
            renderItem={this._renderItem1}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent = {EmptyFlatlist}
          />
          <Text style={{paddingTop: 10, paddingBottom: 10, fontWeight: 'bold'}}>Thông báo</Text>
          <FlatList 
              data={this.state.dataTB}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent = {EmptyFlatlist}
            />
          <Text style={{paddingTop: 10, paddingBottom: 10, fontWeight: 'bold'}}>Tiền nước trong 6 tháng gần nhất</Text>
          <LineChart
            style={styles.chart}
            data={{
              dataSets: [
                {
                  values: this.state.dataBD,
                  label: "",
                  config: {
                    mode: "CUBIC_BEZIER",
                    drawValues: false,
                    lineWidth: 2,
                    drawCircles: true,
                    circleColor: processColor(petrel),
                    drawCircleHole: false,
                    circleRadius: 5,
                    highlightColor: processColor("transparent"),
                    color: processColor(petrel),
                    drawFilled: true,
                    fillGradient: {
                      colors: [processColor(petrel), processColor(greenBlue)],
                      positions: [0, 0.5],
                      angle: 90,
                      orientation: "TOP_BOTTOM"
                    },
                    fillAlpha: 1000,
                    valueTextSize: 15
                  }
                },
              ]
            }}
            chartDescription={{ text: "" }}
            legend={{
              enabled: false
            }}
            marker={{
              enabled: true,
              markerColor: processColor("white"),
              textColor: processColor("black")
            }}
            xAxis={{
              enabled: true,
              granularity: 1,
              drawLabels: true,
              position: "BOTTOM",
              drawAxisLine: true,
              drawGridLines: false,
              fontFamily: "HelveticaNeue-Medium",
              fontWeight: "bold",
              textSize: 12,
              textColor: processColor("gray"),
              valueFormatter: this.state.dataMonth
            }}
            yAxis={{
              left: {
                enabled: true,
                granularity: 1,
                drawLabels: true,
                drawGridLines: false,
                fontFamily: "HelveticaNeue-Medium",
                fontWeight: "bold",
                textSize: 12,
              },
              right: {
                enabled: false
              }
            }}
            autoScaleMinMaxEnabled={true}
            animation={{
              durationX: 0,
              durationY: 1500,
              easingY: "EaseInOutQuart"
            }}
            drawGridBackground={false}
            drawBorders={false}
            touchEnabled={true}
            dragEnabled={false}
            scaleEnabled={false}
            scaleXEnabled={false}
            scaleYEnabled={false}
            pinchZoom={false}
            doubleTapToZoomEnabled={false}
            dragDecelerationEnabled={true}
            dragDecelerationFrictionCoef={0.99}
            keepPositionOnRotation={false}
          />
          <Text style={{paddingTop: 10, paddingBottom: 10, fontWeight: 'bold'}}>Biểu giá nước hiện tại(hộ gia đình)</Text>
          
          <View style={{}}>
            <FlatList 
              data={this.state.dataBGGD}
              renderItem={this._renderItem2}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
            />
              <View style={{padding: 10}}>
                  <Text style={{fontStyle: 'italic', textAlign: 'right', fontSize: 11}}>Đơn vị tính: nghìn đồng/kWh</Text>
              </View>

          </View>
          <Text style={{paddingTop: 10, paddingBottom: 10, fontWeight: 'bold'}}>Biểu giá nước hiện tại(doanh nghiệp)</Text>
          
          <View style={{}}>
            <FlatList 
              data={this.state.dataBGDN}
              renderItem={this._renderItem2}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
            />
              <View style={{padding: 10}}>
                  <Text style={{fontStyle: 'italic', textAlign: 'right', fontSize: 11}}>Đơn vị tính: nghìn đồng/kWh</Text>
              </View>

          </View>
          <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
          <Modal
            onBackdropPress={() => this.setState({visibleModal1: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal1}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent1()}
          </Modal>
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
      </View>
      )
    }
  }

  render() {
    return (
        <View style={styles.container}>
          {this.renderBody()}
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 10
  },
  chart: {
    height: 250
  },
  row: {
    justifyContent: 'space-between', 
    flexDirection: 'row', 
    borderBottomColor: '#e8e8e8', 
    borderBottomWidth: 0.5, 
    padding: 10
  },
  section: {
    fontSize: 14,
    color: '#9E9E9E',
    paddingBottom: 5
},
label: {
    fontSize: 14,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
},
content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    minHeight: 100,
    padding: 10,
},
title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
});