import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/ATTP_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {ATTP_DANHMUC} from '../Data/ATTP_Data'


export default class ATTP_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          
        };
      }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>An toàn thực phẩm</Text>
          <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
        </View>
        <ScrollView>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    flexWrap: 'wrap',
                }}>
                {ATTP_DANHMUC.map(item => (
                    <View key={item.id} style={{width: '50%'}}>
                    <TouchableOpacity onPress={() => {this.props.navigation.navigate(item.navigate)}} style={styles.view1}>
                        <Image source={item.icon} style={{width: 60, height: 60}} />
                        <Text style={{paddingTop: 10, height: 50, textAlign: 'center'}}>{item.name}</Text>
                    </TouchableOpacity>
                    </View>
                ))}
            </View>               
            
        </ScrollView>
    </View>
    );
  }
}
