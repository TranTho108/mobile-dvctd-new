import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ToastAndroid, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Image} from "react-native-elements";
import styles from "./Styles/SignUp_IDTabStyles"
import images from '../Themes/Images'
import LinearGradient from 'react-native-linear-gradient';
import axios from "axios"
import {CD_URL} from '../Config/server'
import * as Animatable from 'react-native-animatable';
import {parseString} from "react-native-xml2js"
import Toast, {DURATION} from 'react-native-easy-toast'
 

export default class SignUp_IDTab extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          id : '',
          loading: false,
        };
      }

    checkContent = async(id) => {
      if(id){
        if(id.length == 9 || id.length == 12 || id.length == 8){
          this.setState({loading: true})
            var a = await axios({
              method: 'PUT',
              url: `${CD_URL}/CheckUserRegister`,
              data: JSON.stringify(
                  {	
                    user: id
                  }
              ),
              headers: {'Content-Type': 'application/json'},
            })
          .then(response => {
              console.log(response.data)
              return response.data
          })
          .catch((err) => this.refs.toast.show('Lỗi kết nối', ToastAndroid.LONG) + setTimeout(() => {this.setState({loading: false})}, 1000) + console.log(err))
          if(a){
            if(a.error.code == 200){
              this.props.handleChangeData(a.data,id)
              setTimeout(() => this.setState({loading: false}) + this.props.handleChangePage(1), 1000)
            }
            else{
              setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show(a.error.userMessage, ToastAndroid.LONG)}, 1000)
            }
          }
          else{
            setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show('Lỗi kết nối', ToastAndroid.LONG)}, 1000)
          }
        }
        else{
          this.refs.toast.show('ID đăng nhập phải là số căn cước công dân (12 chữ số) hoặc Chứng minh nhân dân (9 chữ số)', ToastAndroid.LONG)
        }
      }
      else{
        this.refs.toast.show('Vui lòng nhập ID', ToastAndroid.LONG)
      }
    }

    renderButtonLogin = () => {
      if(!this.state.loading){
        return(
          <Button
          title='Tiếp tục'
          buttonStyle={{borderRadius: 20,paddingStart: 100,paddingEnd: 100}}
          containerStyle={{marginTop: 30,height: 50}}
          onPress={() => this.checkContent(this.state.id)}
      />
        )
      }
      else{
        return(
          <Animatable.View animation='fadeIn' style={{alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{marginTop: 30}} color='#1E88E5' />
            <Text style={{textAlign: 'center', paddingTop: 10}}>Vui lòng đợi trong giây lát</Text>
          </Animatable.View>
        )
      }
    }


  render() {
    return (
        <View style={styles.container}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' size={30} color='#616161' containerStyle={{position: 'absolute', top: 40, left: 10}} />
            <Image source={images.background.cccd} style={{padding: 10, width: 120, height: 120}} />

            <Text style={{padding: 10, fontSize: 16, fontWeight: 'bold'}}>ID Đăng Nhập</Text>
            <Text style={{padding: 5, textAlign: 'center', paddingStart: 20, paddingEnd: 20}}>ID đăng nhập là sô Căn cước công dân hoặc Chứng minh nhân dân được cấp</Text>
            <TextInput keyboardType='number-pad' placeholderTextColor='#9E9E9E' style={{color: '#757575', borderBottomWidth: 0.5, borderBottomColor: '#757575', margin: 10, alignSelf: 'stretch'}} value={this.state.id} placeholder='Nhập ID' onChangeText={(text) => this.setState({id: text})}/>
            {this.renderButtonLogin()}
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
