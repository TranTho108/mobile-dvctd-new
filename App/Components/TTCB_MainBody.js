import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/TTCB_MainBodyStyles'
import LinearGradient from 'react-native-linear-gradient';
import TTCB_TabBar from './TTCB_TabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TTCB_HomeTab from './TTCB_HomeTab'
import TTCB_CategoryTab from './TTCB_CategoryTab'

export default class TTCB_MainBody extends Component {


  
  render() {
   return (
    <View style={styles.container}>
      <View style={styles.linearGradient1}>
        <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='black' containerStyle={{paddingStart: 20}} />
        <Text style={styles.title}>Thông tin cảnh báo</Text>
        <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='black' containerStyle={{paddingEnd: 20}} />
      </View>
    <ScrollableTabView
     style={{flex: 1}}
     tabBarPosition='bottom'
     initialPage={0}
     renderTabBar={() => <TTCB_TabBar {...this.props}/>}
    >
      <View tabLabel="Trang chủ" style={styles.tabView}>
        <TTCB_HomeTab {...this.props}/>
      </View>
      <View tabLabel="Chuyên mục" style={styles.tabView}>
        <TTCB_CategoryTab {...this.props}/>
      </View>
    </ScrollableTabView>
    </View>
    );
  }
}
