import React, { Component } from "react";
import { View, Text, AsyncStorage, FlatList, Dimensions, ActivityIndicator, TouchableOpacity, TextInput, PermissionsAndroid, Platform, Animated, RefreshControl, Image, ToastAndroid, ImageBackground} from "react-native";
import { Button, Icon} from "react-native-elements";
import axios from "axios"
import styles from "./Styles/DVC_TKHS_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import DVC_TKHS_TabBar from './DVC_TKHS_TabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import DVC_TKHS_PersonalTab from './DVC_TKHS_PersonalTab'
import DVC_TKHS_FollowTab from './DVC_TKHS_FollowTab'
 
import images from '../Themes/Images'

export default class DVC_TKHS_MainBody extends Component {

  constructor(props) {
    super(props);
    this.state = {
        
      }
    }

  render() {
    return (
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <ImageBackground resizeMode='cover' source={images.background.trongdong} style={{flex:1}} imageStyle= {{opacity:0.1}} >
            <View style={styles.linearGradient1}><Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='white' /><Text style={styles.title}>Hồ sơ theo dõi</Text></View>
            <LinearGradient
              colors={['rgba(0,0,0,0.1)', 'transparent']}
              style={{
                  left: 0,
                  right: 0,
                  height: 5,
              }}
            />
            <ScrollableTabView
            style={{flex: 1}}
            tabBarPosition='bottom'
            initialPage={0}
            renderTabBar={() => <DVC_TKHS_TabBar {...this.props}/>}
            >
              <View tabLabel="Cá nhân" style={styles.tabView}>
                <DVC_TKHS_PersonalTab {...this.props}/>
              </View>
              <View tabLabel="Theo dõi" style={styles.tabView}>
                <DVC_TKHS_FollowTab {...this.props}/>
              </View>
            </ScrollableTabView>
          </ImageBackground>
        </View>
    );
  }
}
