import React, { Component } from "react";
import { View, AsyncStorage, ImageBackground, ScrollView, TouchableOpacity, RefreshControl, FlatList, Image, StatusBar} from "react-native";
import {Text, Button, Icon, Divider} from "react-native-elements";
import styles from "./Styles/YT_BHYT_MainBodyStyles.js"
import images from "../Themes/Images"
import axios from "axios"
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
var moment = require('moment')
moment.locale('vi')
import {dataMenuTD} from "../Data/MenuData"

export default class MainBody_NotifyTab extends Component {

    constructor(props) {
        super(props);
      
        this.state = {
          
        };
    }

    componentWillMount = async() => {
        
    }
    
  render() {
    return (
        <View style={styles.container}>
            <View style={{paddingTop: 30}}>
                <Text style={{fontWeight: 'bold', fontSize: 24, padding: 10}}>Thông báo</Text>
                <Divider style={{backgroundColor: '#A9AAAE'}} />
            </View>
            <View style={{flex: 1}}>
            <ScrollableTabView
                style={{flex: 1}}
                tabBarPosition='top'
                initialPage={0}
                tabBarUnderlineStyle={{height: 1}}
                renderTabBar={() => <ScrollableTabBar />}
                >
                    {dataMenuTD.map((i) => (
                        <View tabLabel={i.name} key={i.appid} style={styles.tabView}>
                            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                <Image source={images.background.responsive} style={{height: 100, width: 100}} />
                                <Text style={{padding: 10}}>Không có dữ liệu</Text>
                            </View>
                        </View>
                    ))}
            </ScrollableTabView>
            </View>
        </View>
    );
  }
}
