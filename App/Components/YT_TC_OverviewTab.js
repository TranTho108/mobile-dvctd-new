import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ScrollView, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_TC_OverviewTabStyles"
import {} from '../Data/YT_TC_Data'
import Modal from "react-native-modal";
import axios from 'axios'

export default class YT_TC_OverviewTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        data: [],
        visibleModal: false,
        id: this.props.id
    }
  }

  setData = async(id) => {
    var tokenYT = await AsyncStorage.getItem('tokenYT')
    var config = {
      headers: {'Authorization': "bearer " + tokenYT}
    }
    var KHANG_NGUYEN = await axios.get(`http://api-stc.vncdc.gov.vn/lich_su_tiem/khang_nguyen?doi_tuong_id=${id}`, 
      config
    )
    .then((res) => {
      return res.data.data
    })
    if(KHANG_NGUYEN){
      var data1 = []
      console.log(KHANG_NGUYEN)
      KHANG_NGUYEN.map((item) => {
          data1.push(item.khang_nguyen_id)
      })
      data1 = [...new Set(data1)].sort((a,b) => {return a-b})
      var data2 = []
      data1.map((i) => {
          data2.push({khang_nguyen_id: i, ten_khang_nguyen: '', data: []})
      })
      data2.map((i) => {
          KHANG_NGUYEN.map((j) => {
              if(i.khang_nguyen_id == j.khang_nguyen_id){
                  i.data.push(j)
                  i.ten_khang_nguyen = j.ten_khang_nguyen
              }
      })
      })
      this.setState({data: data2})
    }
  }

  componentWillMount = () => {
    this.setData(this.props.id)
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.id !== this.state.id) {
      this.setData(nextProps.id)
    }
  }

  _renderItem = ({item,index}) => {
    return(
      <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between', alignItems: 'center'}}>
        <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#9aca40', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='syringe' color='#fff' size={18} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>{item.ten_khang_nguyen}</Text>
          <Text style={{color: '#757575'}}>Số mũi: {item.data.length}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  renderKN = (item) => {
    if(item.trang_thai >1){
        return(
            <View style={{backgroundColor: '#c6e4cb', padding: 10, margin: 10, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch', alignItems: 'center'}}>
                <Text style={{color: '#58815f'}}>Đã tiêm</Text>
                <Text style={{color: '#58815f'}}>{item.ngay_tiem}</Text>
            </View>
        )
    }
    else{
        return(
            <View style={{backgroundColor: '#ffe082', padding: 10, margin: 10, borderRadius: 5, justifyContent: 'center', alignSelf: 'stretch', alignItems: 'center'}}>
                <Text style={{color: '#ff8f00'}}>Chưa tiêm</Text>
            </View>
        )
    }
  }

  _renderItem1 = ({item,index}) => {
    return(
      <View>
          <Text style={styles.section}>Mũi {index+1}</Text>
          {this.renderKN(item)}
      </View>
    )
  }

_renderModalContent = () => {
    if(this.state.detail){
    const {detail} = this.state
    return(
      <View style={styles.content2}>
        <ScrollView style={{padding: 20}}>
          <Text style={styles.title5}>Thông tin kháng nguyên</Text>
          <Text style={styles.section}>Tên</Text>
          <Text style={styles.label}>{detail.ten_khang_nguyen}</Text>
          <FlatList 
              data={this.state.detail.data}
              renderItem={this._renderItem1}
              keyExtractor={(item, index) => index.toString()}
            />
        </ScrollView>
        <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
             <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
        </TouchableOpacity>
      </View>
    )
    }
    else{
        return(
          <View style={styles.content2}>
          <ActivityIndicator size='large' color='#2196F3' />
          <Text style={{ textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
          </View>
        )
      }
  }
    
  render() {
    return (
        <View style={styles.container}>
            <FlatList 
              data={this.state.data}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{padding: 10}}
            />
            <Modal
                onBackdropPress={() => this.setState({visibleModal: false})}
                backdropTransitionOutTiming={0}
                isVisible={this.state.visibleModal}
                style={{margin: 0}}
                hideModalContentWhileAnimating={true}>
            {this._renderModalContent()}
            </Modal>
        </View>
    );
  }
}
