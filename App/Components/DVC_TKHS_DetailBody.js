import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ActivityIndicator, ScrollView, TouchableOpacity, ToastAndroid, ImageBackground} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import axios from "axios"
import styles from "./Styles/DVC_TKHS_MainBodyStyles"
import {parseString} from "react-native-xml2js"
import SearchFileByCode from "../Api/GetFileByCode"
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import Timeline from 'react-native-timeline-listview'
import DVC_TKHS_DetailTabBar from './DVC_TKHS_DetailTabBar';
import LinearGradient from 'react-native-linear-gradient';
import Toast, {DURATION} from 'react-native-easy-toast'

export default class DVC_TKHS_DetailBody extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data: [],
        timeline: [],
        history: [],
        follow: false,
        token:'',
        followHide: this.props.navigation.state.params.followHide
      }
    }

  componentWillMount() {
    var data = [];
    if(this.props.navigation.state.params.token){
      this.setState({token : this.props.navigation.state.params.token})
    }
    data = this.props.navigation.state.params.data
    var timeline =[];
    data.QuaTrinhXuLy.map((data) => {
      timeline.push({"time": data.NgayThucHien, "title": data.NoiDung, "description": "Người gửi : "+ data.NguoiGui + "\nNgười nhận : " + data.NguoiNhan })
    });
    this.setState({data: data, timeline: timeline})
    this._getHistory(data.MaHoSo);
  }

  _getHistory = async(MaHoSo) => {
    await AsyncStorage.getItem('history')
    .then((history) => {
        const arr = history ? JSON.parse(history) : [];
        this.setState({history: arr})
        if(arr.includes(MaHoSo)){
          this.setState({follow: true})
        }
  })
    .catch((err)=> console.log(err))
  }

  _pushItem = async (MaHoSo,MaDonVi) => {
    /*const xmlReqBody = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">   <soap:Body>     <SaveToken xmlns="http://schemas.microsoft.com/sharepoint/soap/">       <maHoSo>`+ MaHoSo +`</maHoSo><maDonVi>`+ MaDonVi +`</maDonVi><token>`+ this.state.token +`</token>     </SaveToken>   </soap:Body> </soap:Envelope>`
    await axios('http://dichvucong.thanhhoa.gov.vn/_layouts/tandan/dvc/DVCService.asmx?wsdl', {
      method: 'POST',
      headers: {
        "Content-Type": 'text/xml; charset="utf-8"',
      },
      data: xmlReqBody,
      dataType: "xml",
    })
    .then((response)=> console.log(response))
    .catch((err)=> console.log(err))*/
      const arr = this.state.history
      arr.push(MaHoSo)
      const arr1 = [...new Set([...arr])]
      await AsyncStorage.setItem('history', JSON.stringify(arr1));
      this.refs.toast.show(`Đã thêm hồ sơ vào danh sách theo dõi`, ToastAndroid.SHORT);
      this.setState({follow: true})
  }

  _deleteItem = async (MaHoSo) => {
    var arr = this.state.history
    var i = arr.indexOf(MaHoSo);
    if (i != -1) {
      arr.splice(i,1);
    }
    await AsyncStorage.setItem('history', JSON.stringify(arr));
    this.refs.toast.show(`Đã bỏ theo dõi hồ sơ`, ToastAndroid.SHORT);
    this.setState({follow: false})
  }

  _checkItem = (MaHoSo,MaDonVi) => {
    if(!this.state.followHide){
    if(this.state.follow){
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#FF7043', flexDirection: 'row', width: null, margin: 20, alignItems: 'center', justifyContent: 'center', height: 30}} onPress = {() => this._deleteItem(MaHoSo)}>
            <Icon 
              name='delete-sweep'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}> BỎ THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
    else{
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#03A9F4', flexDirection: 'row', width: null, margin: 20, alignItems: 'center', justifyContent: 'center', height: 30}} onPress = {() => this._pushItem(MaHoSo,MaDonVi)}>
            <Icon 
              name='near-me'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}>  THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
  }
  }

  render() {
    const data = this.state.data
    return (
      <View style={styles.container}>
          <View style={styles.linearGradient2}>
            <Icon containerStyle={{alignSelf: 'flex-start'}} onPress={() => this.props.navigation.goBack()} size={28} name='arrow-back' color='white' />
            <Text style={styles.title1}>{data.MaHoSo}</Text>
            <Text style={styles.section}>Mã hồ sơ</Text>
            <View style={{flexDirection: 'row-reverse', justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={{width: 50, height: 50, backgroundColor: '#fff', borderRadius: 50, alignItems: 'center', justifyContent: 'center', marginEnd: 10}}>
                <Icon name='file-alt' type='font-awesome' color='#00897B' />
              </View>
              <View style={{flex: 1, maxWidth: 250}}>
                <Text style={styles.title2}>{data.TenHoSo}</Text>
                <Text style={styles.section}>Tên hồ sơ</Text>
              </View>
            </View>
          </View>
          <LinearGradient
            colors={['rgba(0,0,0,0.1)', 'transparent']}
            style={{
                left: 0,
                right: 0,
                height: 5,
            }}
          />
      <ScrollableTabView
            style={{}}
            initialPage={0}
            tabBarPosition='top'
            tabBarActiveTextColor='#00897B'
            tabBarInactiveTextColor={'#e8e8e8'}
            tabBarUnderlineStyle={{backgroundColor: '#00897B',}}
            //renderTabBar={() => <DVC_TKHS_DetailTabBar {...this.props}/>}
          >
          <ScrollView tabLabel='Chi tiết hồ sơ' style={{backgroundColor: 'transparent', flex: 1}}>
            <View style={{margin: 15,borderBottomWidth: 1, borderBottomColor: '#e8e8e8'}}>
              <View style={{flexDirection: 'row',}}>
                <Icon name='user-alt' type='font-awesome' color='#757575' />
                <View style={{paddingTop: 5}}><Text style={{color: '#757575', fontSize: 16, paddingStart: 10}}>Chủ hồ sơ </Text><Text style={{color: 'black',padding: 10}}>{data.ChuHoSo}</Text></View>
              </View>
            </View>
            
            <View style={{margin: 15,borderBottomWidth: 1, borderBottomColor: '#e8e8e8'}}>
              <View style={{flexDirection: 'row',}}>
                <Icon name='user-edit' type='font-awesome' color='#757575' />
                <View style={{paddingTop: 5}}><Text style={{color: '#757575', fontSize: 16, paddingStart: 10}}>Người nộp </Text><Text style={{color: 'black',padding: 10}}>{data.NguoiNop}</Text></View>
              </View>
            </View>

            <View style={{margin: 15,borderBottomWidth: 1, borderBottomColor: '#e8e8e8'}}>
              <View style={{flexDirection: 'row',}}>
                <Icon name='building' type='font-awesome' color='#757575' />
                <View style={{paddingTop: 5}}><Text style={{color: '#757575', fontSize: 16, paddingStart: 10}}>Đơn vị </Text><Text style={{color: 'black',padding: 10}}>{data.DonVi}</Text></View>
              </View>
            </View>

            <View style={{margin: 15,borderBottomWidth: 1, borderBottomColor: '#e8e8e8'}}>
              <View style={{flexDirection: 'row',}}>
                <Icon name='code-branch' type='font-awesome' color='#757575' />
                <View style={{paddingTop: 5}}><Text style={{color: '#757575', fontSize: 16, paddingStart: 10}}>Trạng Thái </Text><Text style={{color: 'black',padding: 10}}>{data.TrangThaiMobile}</Text></View>
              </View>
            </View>

            <View style={{margin: 15,borderBottomWidth: 1, borderBottomColor: '#e8e8e8'}}>
              <View style={{flexDirection: 'row',}}>
                <Icon name='calendar-alt' type='font-awesome' color='#757575' />
                <View style={{paddingTop: 5}}><Text style={{color: '#757575', fontSize: 16, paddingStart: 10}}>Ngày tiếp nhận </Text><Text style={{color: 'black',padding: 10}}>{data.NgayTiepNhan}</Text></View>
              </View>
            </View>

            <View style={{margin: 15,borderBottomWidth: 1, borderBottomColor: '#e8e8e8'}}>
              <View style={{flexDirection: 'row',}}>
                <Icon name='calendar-alt' type='font-awesome' color='#757575' />
                <View style={{paddingTop: 5}}><Text style={{color: '#757575', fontSize: 16, paddingStart: 10}}>Ngày hẹn trả </Text><Text style={{color: 'black',padding: 10}}>{data.NgayHenTra}</Text></View>
              </View>
            </View>



            {this._checkItem(data.MaHoSo, data.MaDonVi)}
          </ScrollView>
            <View tabLabel='Quá trình xử lý'>
            <ScrollView style={{padding: 10}}>
            <Timeline
              circleSize={20}
              circleColor='rgb(45,156,219)'
              lineColor='rgb(45,156,219)'
              timeContainerStyle={{minWidth:50,}}
              timeStyle={{textAlign: 'center', backgroundColor:'rgb(45,156,219)', color:'white', borderRadius:7, padding: 2}}
              descriptionStyle={{color:'gray'}}
              titleStyle={{color: 'black'}}
              data={this.state.timeline}
              innerCircle={'dot'}
              
            />
            </ScrollView>
            </View>
          </ScrollableTabView>
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
      </View>
    );
    }
}
