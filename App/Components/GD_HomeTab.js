import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ActivityIndicator, TouchableOpacity} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/GD_HomeTabStyles"
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import Modal from 'react-native-modal';
import RadioForm from 'react-native-simple-radio-button';
var moment = require('moment')
moment.locale('vi')

export default class GD_HomeTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        dataNoti: [],
        loading: true,
        dataTKB: [],
        dataTKBs: [],
        dataDay: [],
        day: '',
        value: '',
        visibleModal: false
    }
  }

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem('userToken')
    var NamHocID = await AsyncStorage.getItem('NamHocID')
    var HocKyID = await AsyncStorage.getItem('HocKyID')
    var studentID = await AsyncStorage.getItem('studentID')
    var body = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID}
    var data1 = await requestPOST(`${GD_URL}/ThongBao`, body)
    var data2 = data1.data?data1.data:[]
    var body1 = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID, 'HocKyID': HocKyID}
    var data3 = await requestPOST(`${GD_URL}/ThoiKhoaBieu`, body1)
    var data4 = data3.data?data3.data:[]
    var dataDay = []
    data4.forEach(i => {
      dataDay.push({label: `Thứ ${i.Thu}`, value: Number(i.Thu)})
    });
    this.setState({loading: false, dataNoti: data2.slice(0,3), dataTKBs: data4, dataDay: dataDay, day: dataDay.length>0?dataDay[0].label:'', dataTKB: data4.length>0?data4[0].List:[]})
  }

  _renderItem = ({item,index}) => {
    var date = moment(item.NgayThongBao).format('L')
    return(
      <View style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 30,width: 30, borderRadius: 30, backgroundColor: '#2AA5FF', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='bell' color='#fff' size={14} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
          <Text style={{fontSize: 14, color: 'black'}}>{item.TieuDe}</Text>
          <Text style={{color: '#757575'}}>{item.NoiDung}</Text>
          <Text style={{textAlign: 'right', fontStyle: 'italic',color: '#757575', fontSize: 12, paddingTop: 5}}>{date}</Text>
        </View>
      </View>
    )
  }

  _renderItem1 = ({item,index}) => {
    return(
      <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, borderBottomColor: '#e8e8e8', borderBottomWidth: 1, padding: 5}}>
        <Text>Tiết {item.TietHoc}</Text>
        <Text>{item.MonHoc}</Text>
      </View>
    )
  }

  setDay = (value) => {
    this.state.dataTKBs.map((i) => {
      if(Number(i.Thu) == value){
        this.setState({dataTKB: i.List, day: `Thứ ${value}`, visibleModal: false})
      }
    })
  }

  renderModalContent = () => {
    return(
      <View style={styles.content}>
        <RadioForm
          radio_props={this.state.dataDay}
          initial={-1}
          onPress={(value) => {this.setState({value:value})}}
          buttonSize={15}
          buttonWrapStyle={{padding: 20}}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch', paddingTop: 20}}>

          <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
            <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold'}}>ĐÓNG</Text>
          </TouchableOpacity>
          
          <TouchableOpacity onPress={() => {this.setDay(this.state.value)}}>
            <Text style={{fontSize: 14, color: '#66BB6A', fontWeight: '600'}}>CHỌN</Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }

  renderBody = () => {
    if(!this.state.loading){
      return(
        <View style={{padding: 20, flex: 1}}>
            <Text style={{fontSize: 20, color: 'black'}}>Thông báo mới nhất</Text>
            <FlatList 
              data={this.state.dataNoti}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
            />
            <Text style={{fontSize: 20, paddingTop: 20, paddingBottom: 20, color: 'black'}}>Thời khoá biểu</Text>
              {this.state.dataTKBs.length>0?<TouchableOpacity onPress={() => this.setState({visibleModal: true})} style={{width: 150, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', padding: 5, borderColor: '#e8e8e8', borderWidth: 1, borderRadius: 5, alignSelf: 'center'}}>
                <Text>{this.state.day}    </Text>
                <Icon name='angle-down' type='font-awesome' />
              </TouchableOpacity>:<></>}
            <FlatList 
              data={this.state.dataTKB}
              renderItem={this._renderItem1}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
            />
            <Modal
              backdropTransitionOutTiming={0}
              isVisible={this.state.visibleModal}
              style={{margin: 0}}
              hideModalContentWhileAnimating={true}>
              {this.renderModalContent()}
            </Modal>
          </View>
      )
    }
    else{
      return(
        <View style={{padding: 20, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
        </View>
      )
    }
  }
    
  render() {
    return (
        <View style={styles.container}>
          {this.renderBody()}
        </View>
    );
  }
}
