import React, { Component } from "react";
import {View,ScrollView,ActivityIndicator, FlatList, TouchableOpacity, AsyncStorage} from "react-native";
import {Text, Button, Icon, Avatar, Input} from "react-native-elements";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Modal from 'react-native-modal';
import {THANH_VIEN} from '../Data/YT_TC_Data'
import styles from './Styles/YT_TC_MainBodyStyles'
import YT_TC_HistoryTab from './YT_TC_HistoryTab'
import YT_TC_FigureTab from './YT_TC_FigureTab'
import YT_TC_OverviewTab from './YT_TC_OverviewTab'
import YT_TC_TabBar from './YT_TC_TabBar'
import axios from 'axios'

export default class YT_TC_MainBody extends Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      people: [],
      visibleModal: false,
      visibleModal1: false,
      peopleData: [],
      phoneNumber: '',
      pass: '',
      visibleModal2: false,
      modal2Loading: false,
      error: ''
    };
  }

  login = async(phoneNumber,pass) => {
    if(!phoneNumber){
      var error = 'Vui lòng nhập đầy đủ thông tin'
      this.setState({modal2Loading: false, error: error})
    }
    else{
    this.setState({modal2Loading: true, error: ''})
    var body = {"phoneNumber":phoneNumber,"pass":pass,"osType":"","osVersion":"","deviceId":"","notificationToken":""}
    var data = await axios(`http://api-stc.vncdc.gov.vn/auth`, {
      method: 'POST',
      data: body
    })
    .then((res) => {
      return res.data
    })
    .catch((err) => {return {code: 2}})
    if(data.code == 1){
      await AsyncStorage.multiSet([
        ['phoneNumberYT', phoneNumber],
        ['passYT', pass],
        ['tokenYT', data.data.token],
      ])
      var people1 = await this.getPeople(data.data.token)
      this.setState({peopleData: people1.data,visibleModal2: false,modal2Loading: false})
      setTimeout(() => this.setState({visibleModal: true}), 500)
    }
    else{
      var error = 'Đăng nhập không thành công'
      this.setState({modal2Loading: false, error: error})
    }
    }
  }

  getToken = async() => {
    var phoneNumber = await AsyncStorage.getItem('phoneNumberYT')
    var pass = await AsyncStorage.getItem('passYT')
    var body = {"phoneNumber":phoneNumber,"pass":pass,"osType":"","osVersion":"","deviceId":"","notificationToken":""}
    var token = await axios(`http://api-stc.vncdc.gov.vn/auth`, {
      method: 'POST',
      data: body
    })
    .then((res) => {
      return res.data.data.token
    })
    .catch((err) => console.log(err))
    return token
  }

  setToken = async(tokenYT) => {
    await AsyncStorage.setItem('tokenYT', tokenYT)
  }

  getPeople = async(tokenYT)=> {
    var config = {
      headers: {'Authorization': "bearer " + tokenYT}
    }
    var people = await axios.get(`http://api-stc.vncdc.gov.vn/thanh_vien?theo_doi=1`, 
      config
    )
    .then((res) => {
      return res.data
    })
    .catch((err) => console.log(err))
    console.log(people)
    return people
  }

  componentWillMount = async() => {
    var tokenYT = await AsyncStorage.getItem('tokenYT')
    if(!tokenYT){
      this.setState({visibleModal2: true})
    }
    else{
      var people1 = await this.getPeople(tokenYT)
      if(people && people1.code == 1){
        this.setState({peopleData: people1.data})
      }
      else{
        tokenYT = await this.getToken()
        await this.setToken(tokenYT)
        people1 = await this.getPeople(tokenYT)
        this.setState({peopleData: people1.data})
      }
      var medicalID = await AsyncStorage.getItem('medicalID')
      if(medicalID){
        var people = this.state.peopleData.filter(i => i.doi_tuong_id == Number(medicalID))
        this.setState({people: people[0], visibleModal: false})
        setTimeout(() => this.setState({loading: false,}), 1000)
      }
      else{
        setTimeout(() => this.setState({visibleModal: true}), 1000)
      }
    }
  }

  changePeople = async(ID) => {
    this.setState({loading: true})
    var people = this.state.peopleData.filter(i => i.doi_tuong_id == ID)
    this.setState({people: people[0], visibleModal: false})
    await AsyncStorage.setItem('medicalID', ID.toString())
    setTimeout(() => this.setState({loading: false,}), 1000)
  }

  _renderItem = ({item}) => {
    return(
      <TouchableOpacity onPress={() => this.changePeople(item.doi_tuong_id)} style={{flexDirection: 'row', padding: 10, margin: 10, backgroundColor: '#9aca40', borderRadius: 10}}>
      <View style={{height: 40, width: 40, borderRadius: 50, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
        <Icon type='font-awesome' name='user' size={18} color='#fff' />
      </View>
      <View style={{paddingStart: 10}}>
        <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>{item.ho_ten}</Text>
        <Text style={{color: '#fff', fontSize: 12, paddingTop: 2,fontWeight: 'bold'}}>{item.ngay_sinh}</Text>
      </View>
      </TouchableOpacity>
    )
  }

  renderButton = () => {
    if(this.state.people.doi_tuong_id){
      return(
      <TouchableOpacity style={{padding: 10, paddingTop: 20, alignSelf: 'flex-end'}} onPress={() => {this.setState({visibleModal: false})}}>
        <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold'}}>ĐÓNG</Text>
      </TouchableOpacity>
      )
    }
  }

  renderModalContent = () => (
    <View style={styles.content}>
      <Text style={{fontStyle: 'italic', paddingBottom: 20}} >Vui lòng chọn : </Text>
      <FlatList 
        data={this.state.peopleData}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
      {this.renderButton()}
    </View>
  )

  renderModalContent1 = () => {
    console.log(this.state.people.doi_tuong_id)
    return(
        <View style={{flex: 1, backgroundColor: '#9aca40'}}>
        <Icon name='close' size={30} color='#fff' containerStyle={{alignSelf: 'flex-end', padding: 10, paddingTop: 33}} onPress={() => this.setState({visibleModal1: false})} />
        <View style={{flexDirection: 'row', padding: 10, paddingTop: 10, alignItems: 'center',}}>
          <View style={{height: 80, width: 80, borderRadius: 80, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
            <Icon type='font-awesome' name='user' size={24} color='#fff' />
          </View>
          <View style={{paddingStart: 10}}>
            <Text style={{color: '#fff', fontSize: 18, fontWeight: 'bold'}}>{this.state.people.ho_ten}</Text>
            <Text style={{color: '#fff', fontSize: 14, fontWeight: 'bold'}}>ID {this.state.people.doi_tuong_id}</Text>
          </View>
          
          </View>
          <View style={{padding: 10, margin: 10, backgroundColor: '#fff', borderRadius: 10, }}>
            <View style={{flexDirection: 'row', padding: 10,}}>
              <Text style={{color: '#616161', width: 100}}>Địa chỉ</Text>
              <Text style={{ flex: 1}}>{this.state.people.tam_tru_dia_chi}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Điện thoại</Text>
              <Text>{this.state.people.dien_thoai}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Cơ sở đăng ký</Text>
              <Text>{this.state.people.ten_co_so}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Giới tính</Text>
              <Text>{this.state.people.gioi_tinh}</Text>
            </View>
            <View style={{flexDirection: 'row', padding: 10}}>
              <Text style={{color: '#616161', width: 100}}>Ngày sinh</Text>
              <Text>{this.state.people.ngay_sinh}</Text>
            </View>
          </View>
        </View>
    )
  }

  renderModalContent2 = () => (
    <View style={styles.content}>
      <Text style={{fontStyle: 'italic', padding: 20, textAlign: 'center'}} >Vui lòng nhập tài khoản trên hệ thống sổ tiêm chủng quốc gia</Text>
      <Input
        placeholder='Số điện thoại'
        leftIcon={{ type: 'font-awesome', name: 'mobile-alt', color: '#9E9E9E' }}
        onChangeText={(value) => {this.setState({phoneNumber: value})}}
        errorMessage={this.state.error}
        leftIconContainerStyle={{paddingEnd: 10}}
        containerStyle={{padding: 10}}
      />
      <Input
        placeholder='Mật khẩu'
        leftIcon={{ type: 'font-awesome', name: 'lock', color: '#9E9E9E' }}
        onChangeText={(value) => {this.setState({pass: value})}}
        errorMessage={this.state.error}
        leftIconContainerStyle={{paddingEnd: 10}}
        secureTextEntry
        containerStyle={{padding: 10}}
      />
        <Button
          type='outline'
          title='Đăng nhập'
          loading={this.state.modal2Loading}
          containerStyle={{marginTop: 30, height: 50}}
          onPress={() => this.login(this.state.phoneNumber,this.state.pass)}
        />
        <Button
          type='outline'
          title='Quay lại'
          containerStyle={{marginTop: 10, height: 50}}
          onPress={() => {this.setState({visibleModal2: false}) + setTimeout(() => this.props.navigation.goBack(),500)}}
          titleStyle={{color: '#f44336'}}
        />
    </View>
  )

  renderData = () => {
    if(!this.state.loading){
      return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={{height: 90, alignSelf: 'stretch', flexDirection: 'row', paddingTop: 35, paddingStart: 20, paddingEnd: 20, justifyContent: 'space-between', backgroundColor: '#9aca40'}}>
          <View style={{paddingTop: 10}}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{}} />
          </View>
          <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => this.setState({visibleModal1: true})} style={{height: 40, width: 40, borderRadius: 50, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
            <Icon name='face' size={18} color='#fff' />
          </TouchableOpacity>
          <View style={{paddingStart: 10, paddingEnd: 10}}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>{this.state.people.ho_ten}</Text>
            <Text style={{color: '#fff', fontSize: 12, paddingTop: 2,fontWeight: 'bold'}}>{this.state.people.ngay_sinh}</Text>
          </View>
          </View>
          <View style={{paddingTop: 10}}>
          <Icon onPress={() => {}} name='cached' color='#fff' containerStyle={{}} onPress={() => this.setState({visibleModal: true})} />
          </View>
        </View>
        <ScrollableTabView
          style={{flex: 1,}}
          initialPage={0}
          tabBarPosition='bottom'
          renderTabBar={() => <YT_TC_TabBar {...this.props}/>}
        >
          <ScrollView style={styles.tabView} tabLabel="Kháng nguyên">
            <YT_TC_OverviewTab id={this.state.people.doi_tuong_id} />
          </ScrollView>
          <ScrollView style={styles.tabView} tabLabel="Lịch sử tiêm">
            <YT_TC_HistoryTab id={this.state.people.doi_tuong_id} />
          </ScrollView>
          <ScrollView style={styles.tabView} tabLabel="Phác đồ">
            <YT_TC_FigureTab />
          </ScrollView>
        </ScrollableTabView>
        <Modal
          backdropTransitionOutTiming={0}
          isVisible={this.state.visibleModal1}
          style={{margin: 0}}
          hideModalContentWhileAnimating={true}>
          {this.renderModalContent1()}
        </Modal>
      </View>
      )
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#9aca40' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#9aca40'}}>Đang lấy dữ liệu</Text>
        </View>
      )
    }
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {this.renderData()}
        <Modal
          backdropTransitionOutTiming={0}
          isVisible={this.state.visibleModal}
          style={{margin: 0}}
          hideModalContentWhileAnimating={true}>
          {this.renderModalContent()}
        </Modal>
        <Modal
          backdropTransitionOutTiming={0}
          isVisible={this.state.visibleModal2}
          style={{margin: 0}}
          hideModalContentWhileAnimating={true}>
          {this.renderModalContent2()}
        </Modal>
      </View>
    );
  }
}
