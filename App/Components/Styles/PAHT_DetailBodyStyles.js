import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
    container: {
        flex: 1
    },
    view1: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'flex-start',
        paddingStart: 10,
        borderBottomColor: 'rgba(0, 0, 0, .05)',
        borderBottomWidth: 2,
        paddingTop: 20
    },
    view2: {
        flexDirection: 'row', 
        alignItems: 'center', 
        backgroundColor: '#2E7D32', 
        height: 32, 
        alignSelf: 'baseline',
        borderRadius: 2,
        marginTop: 20, 
        paddingStart: 10,
        paddingEnd: 10
    },
    text1: {
        fontSize: 16, 
        color: '#424242'
    },
    text2: {
        fontSize: 18, 
        color: 'black', 
        fontWeight: '400',
        margin: 10,
    },
    text3: {
        textAlign: 'right', 
        color: '#304FFE', 
        fontStyle: 'italic'
    },
    view3: {
        position: 'absolute', 
        top: 120, 
        left: 20, 
        right: 0, 
        bottom: 0, 
        height: 20
    },
    text4: {
        color: '#fff', 
        fontSize: 16, 
        fontWeight: 'bold'
    },
    view4: {
        margin: 10, 
        backgroundColor: '#E0E0E0', 
        padding: 10, 
        borderWidth: 1, 
        borderStyle: 'dashed'
    },
    text5: {
        textAlign: 'center', 
        fontSize: 18, 
        color: 'black', 
        fontWeight: 'bold'
    },
    view5: {
        flexDirection: 'row', 
        alignItems: 'center', 
        flex: 1,
    },
    view6: {
        flexDirection: 'row', 
        alignItems: 'center', 
        flex: 6/10,
    },
    renderImage: {
        height: 90, 
        width: 90, 
        resizeMode: 'cover',
        aspectRatio:1,
        margin: 10,
    },
    viewSelectedfile:{
        paddingTop: 10,
        flex: 1, 
        flexDirection: 'row', 
    },
    textInput: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eaeaea',
        backgroundColor: '#FAFAFA',
        margin: 10,
        flex: 9.5/10,
        height: 30
    },
    flatList: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    title: {
        fontSize: 18, 
        color: 'black', 
        fontWeight: '600',
        padding: 10,
        paddingStart: 20
      },
      linearGradient1: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'flex-start',
        paddingStart: 20,
        paddingTop: 30,
        paddingBottom: 10,
        backgroundColor: '#fff'
      },
  });