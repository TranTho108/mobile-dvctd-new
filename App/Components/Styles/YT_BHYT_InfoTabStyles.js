import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
    container: {
      backgroundColor: "#fff",
      flex: 1
    },
    view1: {
        padding: 10, 
        borderColor: '#e8e8e8', 
        borderWidth: 1, 
        borderRadius: 10, 
        margin: 10, 
        marginTop: 30
    },
    view2: {
        position: 'absolute', 
        top: -18, 
        left: 10, 
        backgroundColor: '#fff',
        padding: 5
    },
    view3: {
        flexDirection: 'row', 
        padding: 5
    },
    text1: {
        width: 100
    },
    text2: {
        flex: 1,
        paddingStart: 10,
        color: 'black'
    }
  });