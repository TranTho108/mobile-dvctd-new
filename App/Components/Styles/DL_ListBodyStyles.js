import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#EEEEEE",
    flex: 1
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#D84315'
  },
  view1: {
    margin: 10, 
    backgroundColor: '#fff', 
    borderRadius: 5, 
    flexDirection: 'row'
  },
  view2: {
    flex: 2/3, 
    padding: 10, 
    alignItems: 'flex-start'
  },
  view3: {
    flexDirection: 'row', 
    paddingTop: 5, 
    alignItems: 'center'
  },
  text1: {
    paddingStart: 10, 
    fontSize: 14, 
    color: '#9E9E9E', 
    flex: 1
  }
})
