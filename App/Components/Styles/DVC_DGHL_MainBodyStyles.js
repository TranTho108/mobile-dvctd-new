import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  tabView: {
    flex: 1,
    padding: 10,
    paddingTop: 30,
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'flex-start',
    paddingStart: 20,
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#00897B'
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: 'bold',
    padding: 10,
    paddingStart: 20
  },
})
