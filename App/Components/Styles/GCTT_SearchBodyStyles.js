import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#2856C6'
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
  },
  view1: {
    alignItems: 'center', 
    justifyContent: 'center', 
    backgroundColor: '#fff', 
    borderRadius: 4, 
    padding: 20,
    margin: 10
  },
  section: {
    fontSize: 14,
    color: '#9E9E9E',
    paddingBottom: 5
    },
    label: {
        fontSize: 14,
        color: 'black',
        paddingBottom: 10,
        paddingStart: 10
    },
    content2: {
        backgroundColor: 'white',
        justifyContent: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        margin: 20,
        minHeight: 100,
        padding: 10,
    },
    title5: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 20,
        textAlign: 'center'
  },
  content: {
    backgroundColor: 'white',
    margin: 10,
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    minHeight: 100,
    padding: 20
  },
  title1: {
    flex: 1/4,
    paddingStart: 5
  },
  title3: {
    flex: 1/4,
    paddingStart: 5
  },
  title2: {
    flex: 1/4,
    fontWeight: '600'
  },
  textInput: {
    borderRadius: 5,
    borderBottomWidth: 1,
    borderColor: '#e8e8e8',
    height: 30,
    margin: 5
  },
})
