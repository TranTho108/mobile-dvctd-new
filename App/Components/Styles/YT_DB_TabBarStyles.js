import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
    tab: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',

    },
    tabs: {
      height: 60,
      flexDirection: 'row',
      backgroundColor: '#fff',
    },
  });