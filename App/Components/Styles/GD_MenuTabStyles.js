import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  tabView: {
    flex: 1
  },
  view1: {
    flexDirection: 'row', alignItems: 'center', padding: 20
  },
  text1: {
    fontSize: 16, color: '#323232', paddingStart: 20
  },
  text2: {
    fontSize: 16, color: '#fff', paddingStart: 20, fontWeight: 'bold'
  },
  text3: {
    fontSize: 16, color: '#fff', fontWeight: 'bold'
  },
  date: {
    fontSize: 10,
  },
  content: {
    backgroundColor: 'white',
    margin: 10,
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    minHeight: 100,
    padding: 20
  },
})
