import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: "#F4F6F9",
    // margin: 5,
    flex: 1,
  },
  text1: {
    fontSize: 15,
    color: 'black',
    fontWeight: '600',
  },
  text2: {
    fontSize: 12,
    color: 'black',
    marginBottom: 10,
  },
  section: {
    fontSize: 14,
    color: '#9E9E9E',
    paddingBottom: 5
  },
  label: {
    fontSize: 14,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    minHeight: 100,
    padding: 10,
  },
  title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
  containerFlatList: {
    backgroundColor: '#fff',
    borderRadius: 8,
    borderWidth: 0.4,
    borderColor: '#e8e8e8',
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  tabView: {
    flex: 1,
    margin:10, 
  },
})

