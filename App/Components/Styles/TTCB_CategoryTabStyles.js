import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  }
})
