import { StyleSheet,Platform, Dimensions } from 'react-native'

const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 73
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default StyleSheet.create({
  container:{
    flex: 1
  },
  text: {
    fontSize: 20, 
    color: 'black'
  },
  textInput: {
    borderRadius: 5,
    borderBottomWidth: 0.5,
    borderColor: '#e8e8e8',
    margin: 10,
    height: 30
  },
  picker: {
    height: 40,
  },
  pickerview: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    margin: 10,
    width: 300
  },
  qrscan: {
    margin : 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  listrow:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 5,
    backgroundColor: '#FAFAFA',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    flex: 1
  },
  ngay: {
    color: '#1976D2',
    fontStyle: 'italic',
    },
  ma: {
    fontSize: 16,
    color: '#0D47A1',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  chu: {
    fontSize: 12,
    color: 'black',
  },
  nguoinop: {
    fontSize: 12,
    color: 'black',
  },
  ten: {
    paddingStart: 5,
    fontSize: 12,
    color: 'black',
  },
  trangthai: {
    fontSize: 14,
    color: '#4caf50',
  },
  place: {
    padding: 20,
  },
  container1  : {
    backgroundColor: '#fff',
    margin:10,
    overflow:'hidden',
},
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  titleContainer : {
    flexDirection: 'row'
},
title1       : {
    flex    : 1,
    padding : 10,
    color   :'#2a2f43',
    fontWeight:'bold'
},
button      : {

},
buttonImage : {
    width   : 30,
    height  : 25
},
body        : {
    padding     : 10,
    paddingTop  : 0
},
place: {
  padding: 20,
},
fill: {
  flex: 1,
},
content: {
  flex: 1,
},
header: {
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
  backgroundColor: '#00897B',
  overflow: 'hidden',
  height: HEADER_MAX_HEIGHT,
},
backgroundImage: {
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
  width: null,
  height: HEADER_MAX_HEIGHT,
  backgroundColor: '#00897B'
},
bar: {
  backgroundColor: 'transparent',
  marginTop: Platform.OS === 'ios' ? 28 : 38,
  height: 32,
  alignItems: 'center',
  justifyContent: 'center',
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
},
title: {
  fontSize: 18, 
  color: 'white', 
  fontWeight: '600',
  padding: 10,
  paddingStart: 20
},
row: {
  height: 40,
  margin: 16,
  backgroundColor: '#D3D3D3',
  alignItems: 'center',
  justifyContent: 'center',
},
linearGradient1: {
  flexDirection: 'row', 
  alignItems: 'center', 
  justifyContent: 'flex-start',
  paddingStart: 20,
  paddingTop: 30,
  paddingBottom: 10,
  backgroundColor: '#00897B'
},
})
