import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  },
  linearGradient2: {
    paddingStart: 20,
    paddingTop: 30,
    paddingBottom: 20,
  },
  title1: {
    fontSize: 24, 
    color: 'white', 
    fontWeight: '600',
    paddingStart: 10,
    paddingTop: 10
  },
  title2: {
    fontSize: 24, 
    color: 'white', 
    fontWeight: '600',
    paddingStart: 10,
    paddingTop: 10,
  },
  section: {
    color: 'white',
    paddingStart: 10
  },
  tabView: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  }
})
