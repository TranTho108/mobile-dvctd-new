import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  tabView: {
    flex: 1
  },
  view1: {
    flexDirection: 'row', padding: 5, backgroundColor: '#2AA5FF', borderRadius: 20, paddingStart: 10, paddingEnd: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    width: 180
  }
})
