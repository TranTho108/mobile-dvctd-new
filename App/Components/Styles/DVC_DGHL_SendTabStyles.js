import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "transparent",
    flex: 1
  },
  content1: {
    backgroundColor: 'white',
    margin: 20,
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    minHeight: 100
  },
})
