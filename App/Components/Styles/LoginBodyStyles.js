import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  text: {
    fontSize: Fonts.size.medium,
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  input: {
    height: 40,
    borderColor: '#888888',
    marginTop: 30,
    borderBottomWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text2: {
    fontSize: Fonts.size.medium,
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    textAlign: 'right'
  },
  text3: {
    fontSize: Fonts.size.medium,
    color: '#fff',
  },
  text4:{
    fontSize: Fonts.size.medium,
    color: '#4FC173',
    fontWeight: '600'
  },
  text5:{
    fontSize: Fonts.size.medium,
    textDecorationLine: 'underline',
    color: '#4FC173',
    fontWeight: '600'
  },
  title2:{
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
    padding: 10,
    textAlign: 'center'
  },
})
