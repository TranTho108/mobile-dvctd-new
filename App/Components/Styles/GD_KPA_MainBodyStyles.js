import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  tabView: {
    flex: 1
  },
  view1: {
    flexDirection: 'row', alignItems: 'center', padding: 20
  },
  text1: {
    fontSize: 16, color: '#323232', paddingStart: 20
  },
  text2: {
    fontSize: 16, color: '#fff', paddingStart: 20, fontWeight: 'bold'
  },
  text3: {
    fontSize: 16, color: '#fff', fontWeight: 'bold'
  },
  date: {
    fontSize: 20,
  },
  section: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingBottom: 5
  },
  label: {
    fontSize: 14,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    marginTop: 40,
    minHeight: 100,
    padding: 10,
  },
  title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
})
