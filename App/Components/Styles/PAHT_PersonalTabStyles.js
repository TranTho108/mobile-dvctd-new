import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centered: {
    alignItems: 'center'
  },
  loginButton:{
    borderRadius: 5,
    backgroundColor: '#80256D',
    margin: 20
  },
  view: {
    borderRadius: 2,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    marginTop: 20,
  },
  view1: {
    backgroundColor: '#fff', 
    minHeight: 100, 
    padding: 10
  },
  view2: {
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  nameText:{
    paddingTop: 2, 
    paddingLeft: 5, 
    color: 'black'
  },
  statusText: {
    textAlign: 'right', 
    color: '#43A047', 
    fontStyle: 'italic'
  },
  view3: {
    position: 'absolute', 
    top: -20, 
    left: 10, 
    flexDirection: 'row', 
    alignItems: 'center', 
    backgroundColor: '#2E7D32', 
    height: 32, 
    //alignSelf: 'baseline',
    borderRadius: 2,
  },
  view4: {
    position: 'absolute', 
    top: 120, 
    left: 20, 
    right: 0, 
    bottom: 0
  },
  tieudeText: {
    color: '#fff',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    flex: 1,
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#fff',
    justifyContent: 'space-between'
  },
  title: {
    fontSize: 18, 
    color: 'black', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  }
})
