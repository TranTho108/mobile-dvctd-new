import { StyleSheet,Platform, Dimensions } from 'react-native'

const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const DEVICE_HEIGHT = Dimensions.get('window').height

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    flex: 1
  },
  listrow:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 5,
    backgroundColor: '#FAFAFA',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    flex: 1
  },
  ngay: {
    color: '#1976D2',
    fontStyle: 'italic',
    },
  ma: {
    fontSize: 15,
    color: '#0D47A1',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  chu: {
    fontSize: 12,
    color: 'black',
  },
  nguoinop: {
    fontSize: 12,
    color: 'black',
  },
  ten: {
    paddingStart: 5,
    fontSize: 12,
    color: 'black',
  },
  trangthai: {
    fontSize: 12,
    color: '#4caf50',
  },
  empty: {
    fontSize: 16,
    color: 'black',
  },
  listrow1:{
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderTopWidth: 2,
    borderColor: '#eaeaea',
    margin: 10,
  },
  text1: {
    color: 'black',
    marginLeft: 10
  },
  row: {
    flex: 1, 
    flexDirection: 'row'
  },
  place: {
    padding: 20,
  },
  fill: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#9aca40',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    backgroundColor: '#9aca40'
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    minHeight: DEVICE_HEIGHT - HEADER_MIN_HEIGHT,
  },
  scrollViewContent1: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    minHeight: DEVICE_HEIGHT - HEADER_MIN_HEIGHT,
    alignItems: 'center',
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  linearGradient2: {
    paddingStart: 20,
    paddingTop: 30,
    paddingBottom: 20,
    backgroundColor: '#9aca40'
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#9aca40'
  },
  title1: {
    fontSize: 24, 
    color: 'white', 
    fontWeight: '600',
    paddingStart: 10,
    paddingTop: 10
  },
  title2: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    paddingStart: 10,
    paddingTop: 10,
  },
  section: {
    color: 'white',
    paddingStart: 10
  },
  tabView: {
    flex: 1,
    //padding: 10,
  },
})
