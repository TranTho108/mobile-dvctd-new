import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'flex-start',
    paddingStart: 20,
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#D84315'
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  },
  view1: {
    flex: 1/2, 
    alignItems: 'center', 
    justifyContent: 'center', 
    margin: 10, 
    marginEnd: 5, 
    backgroundColor: '#fff', 
    borderRadius: 4, 
    padding: 20
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    minHeight: 100,
  },
  title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
  rectButton: {
    flex: 1,
    backgroundColor: 'white',
  },
  separator: {
    backgroundColor: 'rgb(200, 199, 204)',
  },
  section: {
    fontSize: 16,
    color: '#9E9E9E',
    paddingBottom: 5
  },
  label: {
    fontSize: 16,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
  },
})