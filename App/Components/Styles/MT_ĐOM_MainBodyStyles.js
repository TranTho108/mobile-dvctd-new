import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#EEEEEE",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#0271FE'
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
  },
  view1: {
    flex: 1/2, 
    alignItems: 'center', 
    justifyContent: 'center', 
    margin: 10, 
    marginEnd: 5, 
    backgroundColor: '#fff', 
    borderRadius: 4, 
    padding: 20
  },
  section: {
    fontSize: 14,
    color: '#9E9E9E',
    paddingBottom: 5
  },
  label: {
    fontSize: 14,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    minHeight: 100,
    padding: 10,
  },
  title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
})
