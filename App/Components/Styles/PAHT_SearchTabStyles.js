import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centered: {
    alignItems: 'center'
  },
  loginButton:{
    borderRadius: 5,
    backgroundColor: '#80256D',
    margin: 20
  },
  text: {
    color: 'black',
    fontSize: 16
  },
  content1: {
    backgroundColor: 'white',
    margin: 20,
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    minHeight: 100
  },
  view: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    marginTop: 40,
  },
  view1: {
    backgroundColor: '#fff', 
    minHeight: 100, 
    padding: 8
  },
  view2: {
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  nameText:{
    paddingTop: 2, 
    paddingLeft: 5, 
    color: 'black'
  },
  statusText: {
    textAlign: 'right', 
    color: '#43A047', 
    fontStyle: 'italic'
  },
  view3: {
    position: 'absolute', 
    top: -20, 
    left: 10, 
    flexDirection: 'row', 
    alignItems: 'center', 
    backgroundColor: '#2E7D32', 
    height: 32, 
    //alignSelf: 'baseline',
    borderRadius: 2,
  },
  view4: {
    position: 'absolute', 
    top: 120, 
    left: 20, 
    right: 0, 
    bottom: 0
  },
  tieudeText: {
    color: '#fff',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    flex: 1,
  },
})
