import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
    container: {
      backgroundColor: "#fff",
      flex: 1
    },
    icon: {
      width: 300,
      height: 300,
      alignSelf: 'center',
    },
    tabView: {
      flex: 1,
      //padding: 10,
    },
    linearGradient1: {
      flexDirection: 'row', 
      alignItems: 'center', 
      justifyContent: 'space-between',
      paddingTop: 30,
      paddingBottom: 10,
      backgroundColor: '#FFEB3B'
    },
    title: {
      fontSize: 18, 
      color: 'black', 
      fontWeight: '600',
      padding: 10,
      paddingStart: 20
    },
    row: {
      height: 40,
      margin: 16,
      backgroundColor: '#D3D3D3',
      alignItems: 'center',
      justifyContent: 'center',
    },
  
  });