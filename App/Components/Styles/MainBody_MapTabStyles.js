import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  view1: {
    backgroundColor: '#fff',
    shadowColor: '#BDBDBD',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 8,
    shadowOpacity: 1.0,
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'center', 
    margin: 20, 
    borderRadius: 5,
    marginTop: -20,
    height: 40
  }
})
