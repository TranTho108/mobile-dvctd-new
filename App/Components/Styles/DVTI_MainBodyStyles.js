import { StyleSheet,Dimensions } from 'react-native'
const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = width - 80;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    position: "absolute",
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    borderRadius: 5,
    elevation: 2,
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    width: CARD_WIDTH,
    overflow: "hidden",
  },
  view3: {
    flexDirection: 'row', 
    paddingTop: 5, 
    alignItems: 'center'
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: 140,
    alignSelf: "center",
  },
  textContent: {
    padding: 10,
    flex: 1,
  },
  cardtitle: {
    fontSize: 14,
    marginTop: 5,
    fontWeight: "bold",
  },
  cardloai: {
    fontSize: 14,
  },
  cardDescription: {
    fontSize: 12,
    color: "#444",
    flex: 1
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: "rgba(130,4,150, 0.3)",
    position: "absolute",
    borderWidth: 1,
    borderColor: "rgba(130,4,150, 0.5)",
  },
  title3: {
    fontSize: 16,
    fontWeight: "600",
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    minHeight: 100,
    padding: 10,
    marginTop: 40
},
title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
  modalText: {
    fontSize: 14,
    marginTop: 5,
    fontWeight: "bold",
    textShadowColor: 'rgba(0, 0, 0, 0.15)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 5
  },
})
