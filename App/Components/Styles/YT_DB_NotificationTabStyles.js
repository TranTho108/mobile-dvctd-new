import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20,
    color: 'black'
  },
  container: {
    flex: 1
  },
  text1: {
    fontSize: 15,
    color: 'black',
    fontWeight: '600',
  },
  text2: {
    fontSize: 12,
    color: 'black',
    marginBottom: 10,
  },
  section: {
    fontSize: 14,
    color: '#9E9E9E',
    paddingBottom: 5
  },
  label: {
    fontSize: 14,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20,
    minHeight: 100,
    padding: 10,
  },
  title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
  containerFlatList: {
    backgroundColor: '#fff',
    borderRadius: 8,
    borderWidth: 0.4,
    borderColor: '#e8e8e8',
    padding: 10,
    margin: 5
  },
  containerNotif: {
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: '#EEA80B',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingStart: 15,
    paddingEnd: 15,
    margin: 10,
  },
  containerUnNotif: {
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingStart: 15,
    paddingEnd: 15,
    margin: 10,
  }
})
