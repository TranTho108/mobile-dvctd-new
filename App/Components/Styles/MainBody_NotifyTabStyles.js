import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
})
