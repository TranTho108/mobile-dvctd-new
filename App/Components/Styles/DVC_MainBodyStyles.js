import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#EEEEEE",
    flex: 1
  },
  linearGradient: {
    borderRadius: 40, 
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
    width: 80
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#00897B'
  },
  view1: {
    alignItems: 'center',
    paddingTop: 30
  },
  text: {
    color: '#616161',
    textAlign: 'center',
    paddingTop: 5,
    width: 80,
    height: 40,
    fontSize: 14,
  },
  text1: {
    color: '#fff',
    textAlign: 'center',
    padding: 10,
    fontSize: 18,
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  }
})
