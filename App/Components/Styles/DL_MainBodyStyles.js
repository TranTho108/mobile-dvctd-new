import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  },
  view1: {
    position: 'absolute', 
    top: -25, 
    padding: 5, 
    paddingStart: 50, 
    paddingEnd: 50, 
    borderRadius: 20, 
    backgroundColor: '#f44336', 
    justifyContent: 'center', 
    alignItems: 'center', 
    alignSelf: 'center',
    zIndex: 1
  },
  view2: {
    margin: 10,
    borderWidth: 1, 
    borderRadius: 5, 
    borderColor: '#e8e8e8', 
    padding: 20, 
    alignItems: 'center',
    shadowColor: "#000000",
    shadowOpacity: 0.2,
    shadowRadius: 4,
    shadowOffset: {height: 0,width: 0}}
})
