import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: '#fff'
  },
  view1: {
    flexDirection: 'row', 
    backgroundColor: '#008b00',
    alignItems: 'flex-end', 
    justifyContent: 'flex-start',
    paddingStart: 10,
    borderBottomColor: 'rgba(0, 0, 0, .05)',
    borderBottomWidth: 2,
    height: 75
  },
  view2: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingBottom: 5,
  },
  text2: {
    fontSize: 18, 
    color: '#fff', 
    fontWeight: '400',
    margin: 10,
  },

})
