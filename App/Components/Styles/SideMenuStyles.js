import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
 
export default StyleSheet.create({
   container: {
       flex: 1
   },
   title4:{
       fontSize: Fonts.size.medium,
       color: 'black',
       textShadowColor: 'rgba(0, 0, 0, 0.6)',
       textShadowOffset: {width: -1, height: 1},
       textShadowRadius: 10,
     },
   title1: {
       color: '#424242',
       fontSize: 14,
       fontWeight:'600',
       paddingStart: 20
   },
   view1:{
       padding: 10,
       paddingBottom: 20,
       paddingLeft: 15,
       flexDirection: 'row',
       alignItems: 'center'
   },
   viewBorder:{
       paddingTop: 20,
       paddingBottom: 20,
       paddingLeft: 15,
       flexDirection: 'row',
       alignItems: 'center',
       borderTopWidth: 0.6,
       borderTopColor: '#e8e8e8',
   },
   icon1:{
       width: 50
   },
   section: {
       fontSize: 14,
       color: '#9E9E9E',
       paddingBottom: 5
   },
   label: {
       fontSize: 14,
       color: 'black',
       paddingBottom: 10
   },
   content2: {
       backgroundColor: 'white',
       justifyContent: 'center',
       borderRadius: 4,
       borderColor: 'rgba(0, 0, 0, 0.1)',
       margin: 20
   },
   title5: {
       fontWeight: 'bold',
       color: 'black',
       fontSize: 20,
       textAlign: 'center'
     },
 });
 

