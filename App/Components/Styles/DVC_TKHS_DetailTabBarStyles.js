import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    tab: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',

    },
    tabs: {
      height: 60,
      flexDirection: 'row',
      paddingTop: 5,
      borderTopWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      //borderTopColor: '#e8e8e8',
      backgroundColor: 'transparent',
    },
  });
