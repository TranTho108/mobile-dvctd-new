import { StyleSheet } from 'react-native'
 
export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  linearGradient: {
    borderRadius: 40, 
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
    width: 80
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'flex-start',
    paddingStart: 20,
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#00897B'
  },
  view1: {
    alignItems: 'center',
    paddingTop: 30
  },
  text: {
    color: 'black',
    textAlign: 'center',
    paddingTop: 5,
    width: 80,
    height: 40,
    fontSize: 14,
  },
  text1: {
    color: '#fff',
    textAlign: 'center',
    padding: 10,
    fontSize: 18,
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
    paddingStart: 20
  },
  bottomModal: {
    justifyContent: 'flex-end',
    //margin: 20,
  },
  content: {
    backgroundColor: 'white',
    padding: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    minHeight: 100
  },
  content1: {
    backgroundColor: 'white',
    margin: 20,
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    minHeight: 100
  },
  buttoninModal:{
    padding: 5, 
    alignSelf: 'stretch', 
    borderBottomWidth: 0.5, 
    borderBottomColor: '#e8e8e8'
  },
  viewVideo: {
    height: 175, 
    width: 350, 
    margin: 10
  },
  iconVideo:{
    position: 'absolute', 
    right: 1, 
    top: 1,
  },
  renderImage: {
    height: 100, 
    width: 100, 
    resizeMode: 'cover'
  },
  iconImage:{
    position: 'absolute', 
    right: 1, 
    top: 1,
  },
  viewSelectedfile:{
    paddingTop: 10,
    flex: 1, 
    flexDirection: 'row', 
    flexWrap: 'wrap', 
    alignItems: 'flex-start', 
    justifyContent: 'center',
  },
  text2: {
    fontSize: 18, 
    color: 'black', 
    fontWeight: '400',
    margin: 10,
  },
  viewBody: {
    padding: 10
  },
})
 
 

