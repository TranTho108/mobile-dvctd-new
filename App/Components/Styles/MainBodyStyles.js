import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  text: {
    color: 'white',
    textAlign: 'center',
    paddingTop: 5,
    width: 90,
    height: 40,
    fontSize: Fonts.size.medium,
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  text2: {
    fontSize: Fonts.size.medium,
    fontSize: 20, 
    color: 'white',
    textAlign: 'center',
    paddingTop: 5
  },
  content1: {
    backgroundColor: 'white',
    margin: 20,
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  content2: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    margin: 20
  },
  view1: {
    flex: 1/4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  view2: {
    backgroundColor: '#1E88E5', 
    borderRadius: 15, 
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 60
    //overlayColor: '#fff'
  },
  view3: {
    backgroundColor: '#7B96B0', 
    borderRadius: 15,
    opacity: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
    //overlayColor: '#fff'
  },
  icon1:{
    paddingEnd: 10
  },
  title1:{
    fontSize: Fonts.size.medium,
    color: '#fff',
    fontWeight: '600',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  title2:{
    color: '#fff', 
    fontSize: 24, 
    fontWeight: 'bold',
    padding: 10
  },
  title3:{
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.6)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    width: 80,
    textAlign: 'center'
  },
  view4:{
    padding: 10
  },
  view5:{
    backgroundColor: '#fff', 
    borderRadius: 20,
    height: 40, 
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  linearGradient: {
    borderRadius: 15, 
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 60,
    backgroundColor: '#fff'
  },
  linearGradient1: {
    borderRadius: 15, 
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 60,
    opacity: 0.4
  },
  title4:{
    fontSize: Fonts.size.medium,
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.6)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  title5: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    textAlign: 'center'
  },
  section: {
    fontSize: 14,
    color: '#9E9E9E',
    paddingBottom: 5
  },
  label: {
    fontSize: 14,
    color: 'black',
    paddingBottom: 10,
    paddingStart: 10
  },
  leftAction: {
    flex: 1,
    backgroundColor: 'transparent'
  }
})
