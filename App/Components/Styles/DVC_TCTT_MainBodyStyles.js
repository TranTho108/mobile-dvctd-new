import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  tabView: {
    flex: 1,
    padding: 10,
    paddingTop: 30,
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'flex-start',
    paddingStart: 20,
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#00897B'
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: 'bold',
    padding: 10,
    paddingStart: 20
  },
  title1: {
    fontWeight: '600'
  },
  text: {
    flex: 1
  },
  ngay: {
    textAlign: 'right',
    fontStyle: 'italic',
    fontSize: 12
  },
  kyhieu: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14
  },
  section: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingBottom: 5
  },
  label: {
      fontSize: 14,
      color: 'black',
      paddingBottom: 10,
      paddingStart: 10
  },
  content2: {
      backgroundColor: 'white',
      justifyContent: 'center',
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)',
      margin: 20,
      marginTop: 40,
      minHeight: 100,
      padding: 10
  },
  title5: {
      fontWeight: 'bold',
      color: 'black',
      fontSize: 20,
      textAlign: 'center'
  },
  html: {
    paddingBottom: 10,
    paddingStart: 10
  }
})