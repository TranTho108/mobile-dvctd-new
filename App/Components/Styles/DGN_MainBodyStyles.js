import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title: {
    fontWeight: '600',
    color: 'black'
  },
  description: {
    flex: 1
  },
  linearGradient1: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between',
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#D84315'
  },
  title: {
    fontSize: 18, 
    color: 'white', 
    fontWeight: '600',
    padding: 10,
  },
  view1: {
    flex: 1/2, 
    alignItems: 'center', 
    justifyContent: 'center', 
    margin: 10, 
    marginEnd: 5, 
    backgroundColor: '#fff', 
    borderRadius: 4, 
    padding: 20
  },
})
