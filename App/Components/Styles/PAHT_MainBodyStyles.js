import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
    container: {
        marginTop: 30,
      },
      icon: {
        width: 300,
        height: 300,
        alignSelf: 'center',
      },
      tabView: {
        flex: 1,
      },
      card: {
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: 'rgba(0,0,0,0.1)',
        margin: 5,
        height: 150,
        padding: 15,
        shadowColor: '#ccc',
        shadowOffset: { width: 2, height: 2, },
        shadowOpacity: 0.5,
        shadowRadius: 3,
      },
      linearGradient1: {
        flexDirection: 'row', 
        alignItems: 'center', 
        paddingTop: 30,
        paddingBottom: 10,
        backgroundColor: '#fff',
        justifyContent: 'space-between'
      },
      title: {
        fontSize: 18, 
        color: 'black', 
        fontWeight: '600',
        padding: 10,
        paddingStart: 20
      }
  });