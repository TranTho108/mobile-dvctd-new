import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  title1:{
    color: '#fff',
    fontWeight: '600',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  title2:{
    color: '#fff',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  viewHeader: {
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingVertical: 5,
    paddingStart: 5,
    paddingEnd: 15,
    marginVertical: 10,
    marginStart: 10,
    flexDirection: 'row',
    borderLeftWidth: 2,
    borderLeftColor: '#717EF2',
  },
  textHeaderTitle: {fontSize: 18, color: '#3D4458', fontWeight: '500'},
  textHeaderAll: {color: '#90caf9', fontStyle: 'italic'},
  viewIcon: {
    marginHorizontal: 10,
    padding: 15,
    backgroundColor: '#FBEDEB',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textCount: {fontWeight: 'bold', fontSize: 17, color: '#3D4458'},
  textTitle: {color: '#B2B4C6'},
})
