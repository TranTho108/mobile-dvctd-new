import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, FlatList} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/GCTT_NMH_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {GCTT_DANHMUC} from '../Data/GCTT_Data'
import {requestGET, requestPOST} from "../Api/Basic"
import {GCTT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import Modal from "react-native-modal";


export default class GCTT_NMH_MainBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data:[],
        loading: true,
        name: this.props.navigation.getParam('name'),
        isLoading: true,
        detail: []
    }
  }

    componentWillMount = async() => {
        var userToken = await AsyncStorage.getItem('userToken')
        var name = this.props.navigation.getParam('name')
        var IDNMH = this.props.navigation.getParam('IDNMH')
        var body = {'NhomMatHangIDs': IDNMH}
        var data1 = await requestPOST(`${GCTT_URL}/MatHangs/mGetList`, body)
        var data = data1.data?data1.data:[]
        this.setState({data: data, loading: false})
    }

    _displayModal = async(id) => {
        this.setState({visibleModal: true})
        var body = {'MatHangID': id}
        var data1 = await requestPOST(`${GCTT_URL}/GCTTs/mGetList`, body)
        var data2 = data1.data[0]?data1.data[0]:[]
        this.setState({detail: data2, isLoading: false})
    }

    _renderModalContent = () => {
        if(!this.state.isLoading && this.state.detail ){
        const {detail} = this.state
        console.log(detail)
        var TuGia = detail.TuGia?detail.TuGia.toLocaleString():0
        var DenGia = detail.DenGia?detail.DenGia.toLocaleString():0
        var TrungBinhGia = detail.TrungBinhGia?detail.TrungBinhGia.toLocaleString():0
        return(
          <View style={styles.content2}>
            <ScrollView style={{padding: 20}}>
              <Text style={styles.title5}>Chi tiết giá mặt hàng</Text>
              <Text style={styles.section}>Tên mặt hàng</Text>
              <Text style={styles.label}>{detail.TenMatHang}</Text>
              <Text style={styles.section}>Từ giá</Text>
              <Text style={styles.label}>{TuGia}</Text>
              <Text style={styles.section}>Đến giá</Text>
              <Text style={styles.label}>{DenGia}</Text>
              <Text style={styles.section}>Trung bình giá</Text>
              <Text style={styles.label}>{TrungBinhGia}</Text>
              <Text style={styles.section}>Đơn vị tính</Text>
              <Text style={styles.label}>{detail.DonViTinh}</Text>
              <Text style={styles.section}></Text>
              <Text style={styles.label}></Text>
            </ScrollView>
            <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
                 <Text style={{fontSize: 14, color: '#2089dc', fontWeight: '600', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
               </TouchableOpacity>
          </View>
      );}
      else{
        return(
          <View style={styles.content2}>
            <ActivityIndicator size='large' color='#2856C6' />
            <Text style={{ textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
          </View>
        )
      }
      }

    renderEmpty = () => {
      if(this.state.data.length > 0){
        return(
          <ScrollView>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    flexWrap: 'wrap',
                }}>
                {this.state.data.map(item => 
                (
                    <View key={item.ID} style={{width: '50%'}}>
                    <TouchableOpacity onPress={() => {this._displayModal(item.ID)}} style={styles.view1}>
                        <Image source={images.background.box} style={{width: 60, height: 60}} />
                        <Text style={{paddingTop: 10}}>{item.Ten}</Text>
                    </TouchableOpacity>
                    </View>
                ))
                }
            </View>               
            
        </ScrollView>
        )
      }
      else{
        return(
          <EmptyFlatlist />
        )
      }
    }

    renderBody = () => {
      if(!this.state.loading){
        return(
          <>
          {this.renderEmpty()}
          </>
        )
      }
      else{
        return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2856C6' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#2856C6'}}>Đang lấy dữ liệu</Text>
        </View>
        )
      }
    }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
            <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>{this.state.name}</Text>
          <Icon name='home' color='transparent' containerStyle={{paddingEnd: 20}} />
        </View>
        {this.renderBody()}
        <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
            {this._renderModalContent()}
        </Modal>
    </View>
    );
  }
}
