import {StyleSheet,View,Image,TouchableOpacity,Animated} from 'react-native';
import React, { Component } from "react";
import {Text, Button, Icon, Tooltip} from "react-native-elements";

class Panel extends Component{
    constructor(props){
        super(props);
        this.state = {
            title       : props.title,
            expanded    : false,
        }
    }

    toggle(){
        let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded : !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
        if (!this.state.maxHeight) {
          this.setState({
            maxHeight: event.nativeEvent.layout.height,
          });
        }
      }
    
      _setMinHeight(event) {
        if (!this.state.minHeight) {
          this.setState({
            minHeight: event.nativeEvent.layout.height,
            animation: new Animated.Value(event.nativeEvent.layout.height),
          });
        }
      }

    render(){
        let icon = 'chevron-down';

        if(this.state.expanded){
            icon = 'chevron-up';
        }

        return (
            <Animated.View 
                style={[styles.container,{height: this.state.animation}]}>
                <TouchableOpacity onPress={this.toggle.bind(this)} style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.title}>{this.state.title}</Text>
                        <Text style={{}}>{this.props.dtb}</Text>
                    </View>
                    <Icon name={icon} type='font-awesome' size={20} />
                </TouchableOpacity>
                
                <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>

            </Animated.View>
        );
    }
}



var styles = StyleSheet.create({
    container   : {
        backgroundColor: '#fff',
        margin:10,
        overflow:'hidden'
    },
    titleContainer : {
        flexDirection: 'row',
        padding: 10,
        alignSelf: 'stretch',
        justifyContent: 'space-between'
    },
    title       : {
        fontSize: 16,
        width: 130
    },
    body        : {
        backgroundColor: '#f7f7f7',
        borderRadius: 5,
        padding: 5
    }
});

export default Panel;