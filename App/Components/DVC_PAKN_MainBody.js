import React, { Component } from "react";
import { View, ImageBackground, AsyncStorage, TouchableOpacity, TextInput, ScrollView, Image, StatusBar,Dimensions, ToastAndroid, ActivityIndicator, Keyboard} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import Modal from 'react-native-modal';
import styles from "./Styles/DVC_PAKN_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
const captcha = Math.floor(100000 + Math.random() * 900000)
const height = Dimensions.get('window').height
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios'
import Toast, {DURATION} from 'react-native-easy-toast'
 
import * as Animatable from 'react-native-animatable';
import images from '../Themes/Images'
import {DVC_URL} from '../Config/server'
import {requestPOST, requestGET} from '../Api/Basic'
 
export default class DVC_PAKN_MainBody extends Component {
  
  constructor(props) {
    super(props);
 
    this.state = {
      token: '',
      userToken: '',
      dinhkem: '',
      tieude: '',
      recaptcha: null,
      noidung: '',
      images1: [],
      visibleModal: false,
      visibleModal_1: false,
      isLoading: false,
    };
  }
 
  componentWillMount = async() =>{
    var userToken = await AsyncStorage.getItem("userToken")
    if(userToken){
      var token = await AsyncStorage.getItem("token")
      this.setState({token: token, userToken: userToken})
    }
  }
 
  arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
      for(var j=i+1; j<a.length; ++j) {
          if(a[i].uri === a[j].uri)
              a.splice(j--, 1);
      }
    }
 
    return a;
  }
 
  renderModalContent = () => (
    <View style={styles.content}>
    <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
      <Button
        type='clear'
        buttonStyle={{height: 40,}}
        containerStyle={styles.buttoninModal}
        onPress={this.pickMultiple.bind(this)}
        icon={
        <Icon
          name="photo-library"
          size={20}
          color="#2089dc"
        />
        }
        title=" Chọn từ thư viện"
      />
      <Button
        onPress={() => this.pickSingleWithCamera(false,mediaType='photo')}
        type='clear'
        buttonStyle={{height: 40}}
        containerStyle={styles.buttoninModal}
        icon={
        <Icon
          name="add-a-photo"
          size={20}
          color="#2089dc"
        />
        }
        title=" Chụp ảnh"
      />
      <Button
        onPress={() => this.setState({ visibleModal: false })}
        title="Thoát"
        buttonStyle={{height: 40}}
        containerStyle={styles.buttoninModal}
        type='clear'
        icon={
          <Icon
            name="exit-to-app"
            size={20}
            color="#2089dc"
          />
          }
      />
    </View>
  );
 
  cleanupSingleImage = (uri) => {
    this.setState({images1: this.state.images1.filter(function(item) {
      return item.uri !== uri
    })})
  }
 
  pickMultiple() {
    ImagePicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      includeBase64: true,
    }).then(images2 => {
      var images3 = images2.map(i => {
        return {uri: i.path, data: [i.data], type: i.mime};
      })
      var images4 = this.arrayUnique(this.state.images1.concat(images3));
      this.setState({
        images1: images4,
        visibleModal: false
      });
    })
    .catch(e => console.log(e));
  }
 
  pickSingleWithCamera(cropping, mediaType) {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      mediaType,
      includeBase64: true
    }).then(i => {
      var image1 = {uri: i.path, data: [i.data], type: i.mime}
      var images2 = this.arrayUnique(this.state.images1.concat(image1));
      this.setState({
        images1: images2,
        visibleModal: false
      });
    }).catch(e => console.log(e));
  }
 
  renderImage(image) {
    const uri = image.uri
    return (
      <View style= {{margin: 10}}>
        <Image style={styles.renderImage} source={image} />
        <Icon 
          name='check-box'
          onPress={() => this.cleanupSingleImage(uri)}
          size={28}
          color='#E53935'
          containerStyle={styles.iconImage}
        />
      </View>
      )
  }
 
  checkContent = (token, userToken, tieude, noidung, images1, recaptcha) => {
    Keyboard.dismiss()
    if(!tieude || !noidung){
      this.refs.toast.show('Vui lòng nhập đầy đủ thông tin', ToastAndroid.LONG);
    }
    else if(Number(recaptcha) > captcha || Number(recaptcha) < captcha){
      this.refs.toast.show('Vui lòng nhập lại mã xác nhận', ToastAndroid.LONG);
    }
    else{
      this.setState({visibleModal_1: true, isLoading: true})
      this.sendContent(token, userToken, tieude, noidung, images1)
    }
  }
 
  sendContent = async(token, userToken, tieude, noidung, images1) => {
    var arr = {"FilesAttach": []}
    if(images1){
    await Promise.all(images1.map(async (i) => {
      var base64 = i.data[0]
      var uri = i.uri
      var nameimage = uri.substr(-20,20)
      arr.FilesAttach.push({Type: "Image", Name: nameimage, Base64: base64})
    }))}
    var dinhkem = JSON.stringify(arr)
    var body = {
      "Token": this.state.userToken,
      "TieuDe": tieude,
      "NoiDung": noidung,
      "DinhKem":  dinhkem,
      "ViTri":""
    }
    var data1 = await requestPOST(`${DVC_URL}/SendSatisfactionRating`, body)
    var data = data1.data?data1.data:[]
      if(data.length > 0){
        setTimeout(() => this.setState({ isLoading: false}),2000)
      }
      else{
        this.setState({visibleModal_1: false})
        this.refs.toast.show('Xảy ra lỗi trong quá trình gửi', ToastAndroid.LONG)
      }
  }
 
  renderModalContent_1 = () => {
    if(!this.state.isLoading){
      return(
      <Animatable.View animation='fadeIn' style={styles.content1}>
        <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
        <Icon
          containerStyle={{marginTop: 20}}
          name="check-circle"
          size={60}
          color="#28a745"
        />
        <Text style={{textAlign: 'center', color: 'black', fontSize: 16, margin: 10}}>Phản ánh được gửi thành công</Text>
        <Button
          onPress={() => {this.setState({visibleModal_1: false})}}
          buttonStyle={{height: 40, margin: 10, marginLeft: 20, marginRight: 20, backgroundColor: '#28a745', borderRadius: 5}}
          title=" ĐÓNG"
          containerStyle={{marginBottom: 20}}
        />        
      </Animatable.View>
      );}
    else{
      return(
        <View style={styles.content1}>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
          <ActivityIndicator size="large" color="black" />
          <Text style={{color: 'black', textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
      </View>
      );
    }
  }
 
  render() {
    const {token, userToken, tieude, noidung, dinhkem, images1, recaptcha} = this.state
    return (
      <View style={styles.container}>
      <ImageBackground resizeMode='cover' source={images.background.trongdong} style={{flex:1}} imageStyle= {{opacity:0.1}} >
      <View style={styles.linearGradient1}>
        <Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='white' />
        <Text style={styles.title}>Phản ánh kiến nghị</Text>
      </View>
      
      <LinearGradient
        colors={['rgba(0,0,0,0.1)', 'transparent']}
        style={{
            left: 0,
            right: 0,
            height: 5,
        }}
      />
      <ScrollView style={styles.viewBody}>
        <View style={{}}>
          <Text style={{fontSize: 16, color: 'black'}}>Phản ánh kiến nghị về việc</Text>
          <TextInput placeholderTextColor='#e8e8e8' value={this.state.tieude} textContentType='name' style={{color: '#424242'}} placeholder='Nhập tiêu đề phản ánh, kiến nghị' onChangeText={(text) => this.setState({tieude: text})}/>
        </View>
        <View style={{}}>
          <Text style={{fontSize: 16, color: 'black'}}>Nội dung phản ánh, kiến nghị</Text>
          <TextInput returnKeyType='done' blurOnSubmit={true} onSubmitEditing={() => Keyboard.dismiss()} placeholderTextColor='#e8e8e8' value={this.state.noidung} style={{flex: 1, height: 150}} placeholder='Nhập nội dung phản ánh, kiến nghị' multiline={true} textAlignVertical='top' onChangeText={(text) => this.setState({noidung: text})}/>
        </View>
        <View style={{}}>
          <Text style={{fontSize: 16, color: 'black'}}>Mã xác nhận</Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextInput keyboardType='number-pad' placeholderTextColor='#e8e8e8' maxLength={6} value={this.state.recaptcha} style={{color: '#424242', minWidth: 200}} placeholder='Nhập mã xác nhận' onChangeText={(text) => this.setState({recaptcha: text})}/>
            <View style={{padding: 10}}><Text style={{fontStyle:'italic', color: 'black', fontSize: 18}}>{captcha}</Text></View>
          </View>
        </View>
 
        <View style={{paddingTop: 30}}>             
          <Button
              type='outline'
              buttonStyle={{height: 50}}
              containerStyle={{padding: 10}}
              onPress={() => this.setState({ visibleModal: true })}
              icon={
              <Icon
                name="add-a-photo"
                size={16}
                color="#2089dc"
              />
              }
              title=" Thêm hình ảnh"
          />
          <Modal
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            onSwipeComplete={() => this.setState({ visibleModal: false })}
            swipeDirection={['up', 'left', 'right', 'down']}
            style={styles.bottomModal}
            hideModalContentWhileAnimating={true}
            onBackButtonPress={() => this.setState({ visibleModal: false })}
            onBackdropPress={() => this.setState({ visibleModal: false })}
            deviceHeight={height+100}
          >
            {this.renderModalContent()}
          </Modal>
          
          <Modal
            style={{ margin: 0, flex: 1 }}
            backdropColor="rgba(0,0,0,0.8)"
            transparent={true}
            //backdropOpacity={0.8}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={0}
            animationOutTiming={0}
            backdropTransitionInTiming={600}
            backdropTransitionOutTiming={600}
            isVisible={this.state.visibleModal_1}
            onBackButtonPress={() => this.setState({ visibleModal_1: false })}
            deviceHeight={height+100}
          >
          {this.renderModalContent_1()}
          </Modal>
          <View style={styles.viewSelectedfile}>
            {this.state.images1 ? this.state.images1.map(i => <View key={i.uri}>{this.renderImage(i)}</View>) : null}
          </View>
        </View>
 
        <Button
          title='GỬI'
          onPress= {() => {this.checkContent(token, userToken, tieude, noidung, images1, recaptcha)}}
          buttonStyle={{borderRadius: 20, backgroundColor: '#00897B'}}
          containerStyle={{marginTop: 10, marginBottom: 30}}
        />
      </ScrollView>
      <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
      </ImageBackground>
        </View>
    );
  }
}
 

