import React, { Component } from "react";
import { View, Text, AsyncStorage, FlatList, Dimensions, ActivityIndicator, TouchableOpacity, TextInput, PermissionsAndroid, Platform, Animated, RefreshControl, Image, ToastAndroid, ImageBackground} from "react-native";
import { Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_TKHS_MainBodyStyles"
import SearchFileByCode from "../Api/GetFileByCode"
import * as Animatable from 'react-native-animatable';
import EmptyFlatlist from "./EmptyFlatlist"
import Toast, {DURATION} from 'react-native-easy-toast'
 
import images from '../Themes/Images'

export default class DVC_TKHS_FollowTab extends Component {

  constructor(props) {
    super(props);
    this.state = {
        history: [],
        data: [],
        loading: true,
        empty: false,
        data1: [],
        token: '',
        refreshing: false,
      }
    }


    getFile = async() => {
      await AsyncStorage.getItem('history')
      .then(async(history) => {
        const arr = history ? JSON.parse(history) : [];
        console.log(arr)
        if(arr.length > 0){
        arr.forEach(async(item) => {
          var file = await SearchFileByCode(item)
          this.setState({data: this.state.data.concat(file), data1: this.state.data.concat(file), loading: false})
        })
        this.setState({history: arr})
        }
        else{
          this.setState({loading: false})
        }
      })
    }

    componentWillMount = async() =>{
      await this.getFile()
  }

  deleteItem = async (MaHoSo) => {
    this.refs.toast.show(`Đã bỏ theo dõi hồ sơ ${MaHoSo}`, ToastAndroid.LONG)
    var data = this.state.data.filter(item => {
      const MaHoSo1 = item.MaHoSo
      return MaHoSo1 !== MaHoSo
    })
    var arr = this.state.history
    var i = arr.indexOf(MaHoSo);
    if (i != -1) {
      arr.splice(i,1);
    }
    this.setState({data: data})
    await AsyncStorage.setItem('history', JSON.stringify(arr));
  }

  _renderItem = ({item,index}) => {
    return (
      <Animatable.View delay={index*300} animation='fadeInRight'>
        <TouchableOpacity onPress = {() => {this.props.navigation.navigate("DVC_TKHS_DetailScreen", {data: item})}} style={{flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#e8e8e8', paddingBottom: 10}} >
          <View>
            <View style={{backgroundColor: '#00897B', margin: 10, borderRadius: 5, alignItems: 'center'}}><Icon name='file-alt' type='font-awesome' color='#fff' containerStyle={{padding: 20}} /></View>
          </View>
          <View style={{margin: 10, flex: 1}}>
            <Text style={{fontWeight: 'bold', fontSize: 20,color: 'black'}}>{item.MaHoSo}</Text>
            <Text style={{ color: 'black', paddingTop: 10}}>{item.TenHoSo}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}><Icon color='#e8e8e8' size={20} name='user-alt' type='font-awesome' /><Text style={{paddingStart: 5}}>{item.ChuHoSo}</Text></View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}><Icon color='#e8e8e8' size={20} name='calendar-alt' type='font-awesome' /><Text style={{paddingStart: 5}}>{item.NgayTiepNhan}</Text></View>
            </View>
            <View style= {{ alignItems: 'center', paddingTop: 10, alignSelf: 'stretch'}}>
              <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#FF7043', padding: 5, alignItems: 'center', flexDirection: 'row',alignSelf: 'stretch', justifyContent: 'center'}} onPress = {() => this.deleteItem(item.MaHoSo)}>
                <Icon 
                  name='delete-sweep'
                  size={16}
                  color= '#fff'
                />
                <Text style={{color: '#fff', fontSize: 12}}> BỎ THEO DÕI</Text>
            </TouchableOpacity>
        </View>
          </View>
        </TouchableOpacity>
        </Animatable.View>
  )}


  _renderScrollViewContent() {
    if(this.state.loading){
      return ( 
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size='large' style={{}} color='#00897B' />
        <Text style={{textAlign: 'center', paddingTop: 10, color: '#00897B'}}>Đang lấy dữ liệu</Text>
      </View>
      )
    }
    else{
    return (
      <View style={{}}>
        <FlatList
          data={this.state.data}
          renderItem={(item,index) => this._renderItem(item,index)}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
          ListEmptyComponent={EmptyFlatlist}
        />
      </View>
    );}
  }



  render() {
    return (
        <View style={{flex: 1}}>
          {this._renderScrollViewContent()}
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
