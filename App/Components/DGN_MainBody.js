import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, TouchableOpacity, Image, Linking, Platform} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DGN_MainBodyStyles"
import images from "../Themes/Images"
import {DGN_Data} from "../Data/DGN_Data"

export default class DGN_MainBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
        
    }
  }

  navigateFavorite = async() => {
    var history_DGN = await AsyncStorage.getItem('history_DGN')
    var history_DGN1 = history_DGN?JSON.parse(history_DGN):[]
    var data = []
    history_DGN1.map((i) => 
      {
        DGN_Data.map((j) => {
          j.data.map((h) => {
            if(h.PID === i){
              data.push(h)
            }
          })
        })
      }
    )
    this.props.navigation.navigate('DGN_DetailScreen', {name: 'Yêu thích', data: data})
  }

  _renderItem = ({item}) => {
      return(
          <TouchableOpacity onPress={() => this.props.navigation.navigate('DGN_DetailScreen', {name: item.name, data: item.data})} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItems: 'center'}}>
              <Icon type='font-awesome' name={item.icon} containerStyle={{padding: 10, width: 60}} color='#616161' />
              <Text style={{fontSize: 18,color: '#212121'}}>{item.name}</Text>
          </TouchableOpacity>
      )
  }

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}>
              <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
              <Text style={styles.title}>Đường dây nóng</Text>
              <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            <TouchableOpacity onPress={() => this.navigateFavorite()} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItems: 'center'}}>
              <Icon type='font-awesome' name='heart' containerStyle={{padding: 10, width: 60}} color='#616161' />
              <Text style={{fontSize: 18, color: '#212121'}}>Yêu thích</Text>
            </TouchableOpacity>
            <FlatList 
                data={DGN_Data}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
            />        
        </View>
    );
  }
}