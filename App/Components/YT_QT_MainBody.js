import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_QT_MainBodyStyles"
import {TYT_Data} from "../Data/YT_TYT_Data"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import Modal from "react-native-modal";
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import {YT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'


export default class YT_QT_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          data: [],
          loading: true,
          visibleModal: false,
          detail: []
        };
      }

    componentWillMount = async() => {
      var data1 = await requestGET(`${YT_URL}/DiaChiYTes/mGetListQT`)
      var data2 = data1.data?data1.data:[]
      this.setState({data: data2, loading: false})
    }

    _renderItem = ({item,index}) => {
    return(
        <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#9aca40', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='clinic-medical' color='#fff' size={18} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
            <Text style={{fontSize: 16, fontWeight: 'bold', padding: 5}}>{item.Ten}</Text>
            <View style={{flexDirection: 'row', padding: 5}}><Text style={{color: '#757575', width: 50}}>SĐT</Text><Text style={{color: '#757575', fontWeight: 'bold', flex: 1}}>{item.SoDienThoai}</Text></View>
            <View style={{flexDirection: 'row', padding: 5}}><Text style={{color: '#757575', width: 50}}>Địa chỉ</Text><Text style={{color: '#757575', fontWeight: 'bold', flex: 1}}>{item.DiaChi}</Text></View>
        </View>
        </TouchableOpacity>
    )
    }

    navigateToMap = () => {
      var data = this.state.data
      var dataMap = []
      data.map((item) => {
          dataMap.push({name: item.Ten, address: item.DiaChi, location: {latitude: Number(item.LocationLatitude),longitude: Number(item.LocationLongitude)}})
      })
      this.props.navigation.navigate('MapViewScreen', {data: dataMap})
    }

    _renderModalContent = () => {
      if(this.state.detail){
      const {detail} = this.state
      return(
        <View style={styles.content2}>
          <ScrollView style={{padding: 20}}>
            <Text style={styles.title5}>Chi tiết quầy thuốc</Text>
            <Text style={styles.section}>Tên</Text>
            <Text style={styles.label}>{detail.Ten}</Text>
            <Text style={styles.section}>Thông tin chung</Text>
            <Text style={styles.label}>{detail.ThongTinChung}</Text>
            <Text style={styles.section}>Địa chỉ</Text>
            <Text style={styles.label}>{detail.DiaChi}</Text>
            <Text style={styles.section}>Người liên hệ</Text>
            <Text style={styles.label}>{detail.NguoiLienHe}</Text>
            <Text style={styles.section}>SĐT</Text>
            <Text style={styles.label}>{detail.SoDienThoai}</Text>
            <Text style={styles.section}></Text>
            <Text style={styles.label}></Text>
          </ScrollView>
          <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
               <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
          </TouchableOpacity>
        </View>
      )
      }
    }

    renderBody(){
      if(!this.state.loading){
        return(
          <View style={{flex: 1}}>
          <FlatList 
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{padding: 10}}
            ListEmptyComponent={EmptyFlatlist}
        />
        </View>
        )
      }
      else{
        return(
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#9aca40' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#9aca40'}}>Đang lấy dữ liệu</Text>
          </View>
        )
      }
    }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Quầy thuốc</Text>
          <Icon onPress={() => this.navigateToMap()} name='map' color='white' containerStyle={{paddingEnd: 20}} />
        </View>
        {this.renderBody()}
        <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
    </View>
    );
  }
}
