import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, Image, ActivityIndicator, ImageBackground, TouchableOpacity} from "react-native";
import {Text, Button, Icon, Rating} from "react-native-elements";
import styles from "./Styles/DL_ListBodyStyles"
import {DL_URL} from '../Config/server'
import {requestPOST, requestGET} from '../Api/Basic'
import EmptyFlatlist from './EmptyFlatlist'
import * as Animatable from 'react-native-animatable';

export default class DL_ListBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          loading: true,
          name: this.props.navigation.getParam('TieuDe'),
          ID: '',
          data: []
        };
      }

      componentWillMount = async() => {
        var name = this.props.navigation.getParam('TieuDe')
        var ID = this.props.navigation.getParam('ID')
        var arr = []
        arr.push(ID)
        var body = {"maxa": '', "loaidulich": arr}
        var data1 = await requestPOST(`${DL_URL}/GetDiemDuLichs`, body)
        var data2 = data1.data?data1.data:[]
        this.setState({data: data2, loading: false})
      }

      navigateToMap = () => {
        var data = this.state.data
        var dataMap = []
        data.map((item) => {
            dataMap.push({name: item.Ten, address: item.DiaChi, location: {latitude: Number(item.LatitudeLongtitude),longitude: Number(item.LatitudeLongtitude)}, distance: ''})
        })
        this.props.navigation.navigate('MapViewScreen', {data: dataMap})
      }

      _renderItem = ({item,index}) => {
        return(
                <TouchableOpacity onPress={() => this.props.navigation.navigate("DL_DetailScreen", {ID: item.ID, Ten: item.Ten})}>
                <Animatable.View delay={index*200} animation='bounceInRight' style={styles.view1}>
                <Image onError={({ nativeEvent: {error} }) => console.log(error)} source={{uri: item.AnhDaiDien}} style={{flex: 1/3, borderRadius: 5, height: 150}} />
                <View style={styles.view2}>
                    <Text style={{fontWeight: '600', fontSize: 18}}>{item.Ten}</Text>
                    <Rating
                        imageSize={20}
                        readonly
                        startingValue={item.DanhGia}
                        style={{paddingTop: 5}}
                        size={18}
                    />
                    <View style={styles.view3}>
                        <Icon type='font-awesome' name='map-marker-alt' size={18} color='#9E9E9E' containerStyle={{width: 25}} />
                        <Text style={styles.text1}>{item.DiaChi}</Text>
                    </View>
                </View>
            </Animatable.View>
            </TouchableOpacity>
        )
    }

      renderBody(){
        if(!this.state.loading){
          return(
            <View style={{flex: 1}}>
                <FlatList 
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    extraData={this.state}
                    ListEmptyComponent={EmptyFlatlist}
                />
            </View> 
          )
        }
        else{
          return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator size='large' style={{}} color='#9aca40' />
              <Text style={{textAlign: 'center', paddingTop: 10, color: '#9aca40'}}>Đang lấy dữ liệu</Text>
            </View>
          )
        }
      }

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}>
                <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
                <Text style={styles.title}>{this.state.name}</Text>
                <Icon onPress={() => {}} name='map' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            {this.renderBody()}
        </View>
    );
  }
}