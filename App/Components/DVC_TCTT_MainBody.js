import React, { Component } from "react";
import { View, AsyncStorage, TextInput, ActivityIndicator, Keyboard, ImageBackground, FlatList, ScrollView, TouchableOpacity, StatusBar, Dimensions, ToastAndroid } from "react-native";
import {Text, Button, Icon, Input} from "react-native-elements";
import styles from "./Styles/DVC_TCTT_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import ModalSelector from 'react-native-modal-selector'
 
import {TKHS_URL} from '../Config/server'
import Modal from "react-native-modal";
import axios from "axios"
import {parseString} from "react-native-xml2js"
import images from '../Themes/Images'
import * as Animatable from 'react-native-animatable';
import HTMLView from 'react-native-htmlview';
const height = Dimensions.get('window').height
import {requestPOST, requestGET} from '../Api/Basic'
import {DVC_URL} from '../Config/server'
import Toast, {DURATION} from 'react-native-easy-toast'

var index = 0;
const data = [
    { key: index++, section: true, label: 'Đơn Vị' },
    { key: index++, label: 'Sở Xây Dựng', customKey: 'SXD' },
    { key: index++, label: 'Sở Y tế', customKey: 'SYT' },
    { key: index++, label: 'Sở Thông tin và Truyền Thông', customKey: 'STTTT' },
    { key: index++, label: 'Cấp huyện', customKey: 'QH' },
    { key: index++, label: 'Cấp xã', customKey: 'XP' },
    { key: index++, label: 'Ban QL KKT Nghi Sơn và các Khu công nghiệp', customKey: 'KCN' },
    { key: index++, label: 'Sở Tài Chính', customKey: 'STC' },
    { key: index++, label: 'Sở Kế hoạch và Đầu tư', customKey: 'SKHDT' },
    { key: index++, label: 'Sở Nông nghiệp và Phát triển nông thôn', customKey: 'SNN' },
    { key: index++, label: 'Sở Tư pháp', customKey: 'STP' },
    { key: index++, label: 'Sở Công Thương', customKey: 'SCT' },
    { key: index++, label: 'Sở Giao thông vận tải', customKey: 'SGT' },
    { key: index++, label: 'Sở Nội vụ', customKey: 'SNV' },
    { key: index++, label: 'Thanh tra Tỉnh', customKey: 'TT' },
    { key: index++, label: 'Sở Giáo dục và Đào tạo', customKey: 'SGD' },
    { key: index++, label: 'Sở Tài nguyên và Môi trường', customKey: 'STN' },
    { key: index++, label: 'Sở Khoa học và Công nghệ', customKey: 'SKH' },
    { key: index++, label: 'Sở Lao động - Thương binh và Xã hội', customKey: 'SLT' },
    { key: index++, label: 'Sở Văn hoá, Thể thao và Du lịch', customKey: 'SVH' },
    { key: index++, label: 'Công an tỉnh', customKey: 'CAT' },
    { key: index++, label: 'Ngân hàng Nhà nước tỉnh', customKey: 'NH' },
    { key: index++, label: 'Cục thuế tỉnh', customKey: 'CT' },
    { key: index++, label: 'Kho bạc Nhà nước Tỉnh', customKey: 'KB' },
    { key: index++, label: 'Văn phòng Uỷ ban', customKey: 'VPUB' },
    { key: index++, label: 'Sở Ngoại vụ', customKey: 'SNgV' },
    { key: index++, label: 'Công ty Điện lực Thanh Hoá', customKey: 'DL' },
    { key: index++, label: 'Ban Dân tộc', customKey: 'BDT' },


];


export default class DVC_TCTT_MainBody extends Component {

  constructor(props) {
    super(props);

    this.state = {
        tuKhoa: '',
        maNhom: '',
        isloading: false,
        result: [],
        visibleModal: false,
        detail: []
    }
}

  renderSearch = () => {
    if(this.state.isloading){
      return(
        <View>
        <ActivityIndicator size='large' style={{marginTop: 20}} color='#1E88E5' />
        <Text style={{textAlign: 'center', paddingTop: 10}}>Đang tiến hành tìm kiếm</Text>
        </View>
      )
    }
    else{
      return(
        <TouchableOpacity onPress={() => this.checkContent(this.state.tuKhoa, this.state.maNhom)} style={{height: 60, width: 60, borderRadius: 60, backgroundColor: '#00897B', alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: 10}} >
          <Icon name='search' color='#fff' />
        </TouchableOpacity>
      )
    }
  }

  checkContent = (tuKhoa,maNhom) => {
    Keyboard.dismiss()
    if(!maNhom){
      this.refs.toast.show('Vui lòng chọn đơn vị tra cứu', ToastAndroid.LONG)
    }
    else if (tuKhoa.length < 6){
      this.refs.toast.show('Từ khoá quá ngắn', ToastAndroid.LONG)
    }
    else{
      this.setState({isloading: true})
      this.searchContent(tuKhoa,maNhom)
    }
  }

  _renderItem = ({item,index}) => {
    return(
      <TouchableOpacity onPress = {() => {this.setState({visibleModal: true, detail: item})}} style={{padding: 10}}>
      <Animatable.View delay={index*200} animation='bounceInRight' style={{flexDirection: 'row', backgroundColor: '#FFECB3', padding: 10, alignSelf: 'stretch', alignItems: 'center', borderRadius: 4, borderWidth: 0.5, borderColor: '#FFB300'}} >
        <View style={{height: 40, width: 40, borderRadius: 40, justifyContent: 'flex-start', alignItems: 'center', justifyContent: 'center', backgroundColor: '#FFB74D', marginEnd: 10}}><Icon type='font-awesome' color='#fff' name='clipboard' />
        </View>
        <View style={{flex: 1, padding: 5}}>
        <Text style={styles.kyhieu}>{item.Ma}</Text>
        <Text style={styles.text}>{item.Ten}</Text>
        </View>
      </Animatable.View>
      </TouchableOpacity>

    )
  }

  searchContent = async(tuKhoa, maNhom) => {
    this.setState({result: []})
    var data = await requestGET(`${DVC_URL}/SearchProcedureByKey/${maNhom}/${tuKhoa}`)
    if(!data.data || data.data.DSThuTuc.length < 1){
      this.refs.toast.show(`Không tìm thấy kết quả`, ToastAndroid.LONG);
      this.setState({isloading: false})
    }
    else{
      this.refs.toast.show(`Đã tìm thấy `+ data.data.DSThuTuc.length +` thủ tục.`, ToastAndroid.LONG);
      this.setState({result: data.data.DSThuTuc,count: data.data.DSThuTuc, isloading: false})
    }
  }

  _renderModalContent = () => {
    const {detail} = this.state
    return(
      <View style={styles.content2}>
        <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
        <ScrollView showsVerticalScrollIndicator={false} style={{padding: 10}}>
          <Text style={styles.title5}>Chi tiết thủ tục</Text>
          <Text style={styles.section}>Tên</Text>
          <Text style={styles.label}>{detail.Ten}</Text>
          <Text style={styles.section}>Mã</Text>
          <Text style={styles.label}>{detail.Ma}</Text>
          <Text style={styles.section}>Thời hạn xử lý</Text>
          <Text style={styles.label}>{detail.ThoiHanXuLy}</Text>
          <Text style={styles.section}>Thành phần hồ sơ</Text>
          <View style={styles.html}>
          <HTMLView
            value={detail.ThanhPhanHoSo}
          />
          </View>
          <Text style={styles.section}>Lệ phí</Text>
          <Text style={styles.label}>{detail.LePhi}</Text>
          <Text style={styles.section}>Cơ quan thực hiện</Text>
          <View style={styles.html}>
          <HTMLView
            value={detail.CoQuanThucHien}
          />
          </View>
          <Text style={styles.section}>Đối tượng thực hiện</Text>
          <View style={styles.html}>
          <HTMLView
            value={detail.DoiTuongThucHien}
          />
          </View>
        </ScrollView>
        <TouchableOpacity style={{padding: 5}} onPress={() => {this.setState({visibleModal: false})}}>
             <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', alignSelf: 'flex-end'}}>ĐÓNG</Text>
        </TouchableOpacity>
      </View>
  );
  }
    
  render() {
    
    return (
        <View style={styles.container}>
          <ImageBackground resizeMode='cover' source={images.background.trongdong} style={{flex:1}} imageStyle= {{opacity:0.1}} >
          <View style={styles.linearGradient1}><Icon onPress={() =>  this.props.navigation.goBack()} name='arrow-back' color='white' /><Text style={styles.title}>Tra cứu thủ tục hành chính</Text></View>
          <LinearGradient
            colors={['rgba(0,0,0,0.1)', 'transparent']}
            style={{
                left: 0,
                right: 0,
                height: 5,
            }}
          />
          <ScrollView style={{padding: 10}}>
          <View style={{flexDirection: 'row', alignItems: 'center',paddingBottom: 10,}}>
            <Icon name='account-balance' size={24} containerStyle={{width: 35}} />
            <Text style={{ color: 'black', fontWeight: '600', fontSize: 15}} >Đơn vị tra cứu </Text>
          </View>
          <ModalSelector
            sectionStyle={{borderBottomWidth: 0}}
            sectionTextStyle={{fontWeight: 'bold', fontSize: 16}}
            optionStyle={{borderBottomColor: '#e8e8e8', padding: 10, paddingBottom: 15, borderBottomWidth: 0.5}}
            optionTextStyle={{textAlign: 'left'}}
            optionContainerStyle={{marginTop: 20}}
            style={{padding: 10}}
            cancelText='Thoát'
            animationType='fade'
            data={data}
            initValue="Chọn đơn vị"
            onChange={(option)=>{this.setState({maNhom: option.customKey})}} />
          <View style={{flexDirection: 'row', alignItems: 'center',paddingBottom: 10,}}>
            <Icon name='keyboard' size={24} containerStyle={{width: 35}} />
            <Text style={{ color: 'black', fontWeight: '600', fontSize: 15}} >Từ khoá tra cứu </Text>
          </View>
          <TextInput autoCapitalize='none' placeholderTextColor='#e8e8e8' value={this.state.tuKhoa} style={{color: '#424242', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8', margin: 10}} placeholder='Nhập từ khoá' onChangeText={(text) => this.setState({tuKhoa: text})}/>
            {this.renderSearch()}
            <FlatList
              data={this.state.result}
              renderItem={(item,index) => this._renderItem(item,index)}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              contentContainerStyle={{paddingTop: 30, paddingBottom: 30}}
            />
          </ScrollView>
            <Modal
                onBackdropPress={() => this.setState({visibleModal: false})}
                backdropTransitionOutTiming={0}
                isVisible={this.state.visibleModal}
                style={{margin: 0}}
                hideModalContentWhileAnimating={true}
                deviceHeight={height+100}>
              {this._renderModalContent()}
              </Modal>
              <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
            </ImageBackground>
        </View>
    );
  }
}