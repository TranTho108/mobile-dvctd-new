import React, { Component } from "react";
import { View, ImageBackground,ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/SignUpBodyStyles"
import ScrollableTabView from 'react-native-scrollable-tab-view';
import SignUp_IDTab from './SignUp_IDTab'
import SignUp_OTPTab from './SignUp_OTPTab'
import SignUp_PassTab from './SignUp_PassTab'
import LinearGradient from 'react-native-linear-gradient';
import images from '../Themes/Images'

export default class SignUpBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      page : 0,
      countdown: '',
      ID: ''
    };
  }

  componentWillMount = () =>{
    
  }

  changePage(page){
    this.setState({page: page});
  }

  changeData = (countdown,ID) =>{
    this.setState({countdown,ID})
  }

  render() {
    return (
        <View style={styles.container}>
          <ScrollableTabView
            initialPage={0}
            page={this.state.page}
            locked={true}
            renderTabBar={() => <View />}
          >
            <View tabLabel="Nhập ID đăng nhập" style={styles.tabView}>
                <SignUp_IDTab {...this.props} handleChangeData={this.changeData.bind(this)} handleChangePage={this.changePage.bind(this)}/>
            </View>
            <View tabLabel="Nhập mã OTP" style={styles.tabView}>
                <SignUp_OTPTab {...this.props} handleChangePage={this.changePage.bind(this)} countdown={this.state.countdown} ID={this.state.ID}/>
            </View>
            <View tabLabel="Nhập mật khẩu" style={styles.tabView}>
                <SignUp_PassTab {...this.props} ID={this.state.ID}/>
            </View>
          </ScrollableTabView>
        </View>
    );
  }
}