import React, { Component } from 'react'
import { ScrollView, Text, Image, View, FlatList, TouchableOpacity,ActivityIndicator, ImageBackground, RefreshControl } from 'react-native'
import {ButtonGroup, Icon} from 'react-native-elements'
import {SERVER_URL, HOST} from '../Config/server'
import {fetchTest} from "../Api/Test"
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
// Styles
import styles from './Styles/PAHT_HomeTabStyles'
import EmptyFlatlist from './EmptyFlatlist'


const buttons = [ 'Mới nhất', 'Phổ biến', 'Đang xử lý', 'Đã xử lý']

export default class PAHT_HomeTab extends Component {

  constructor () {
    super()
    this.state = {
      selectedIndex: 0,
      data: [],
      dataall: [],
      status: 'new',
      refreshing: true
    }
    this.updateIndex = this.updateIndex.bind(this)
  }
  getData = async(status) =>{
    var data1 = await fetchTest(status)
    var data2 = data1.length>0?JSON.parse(data1):[]
    this.setState({data: data2.data,refreshing: false})
  }
  componentWillMount = async() =>{
    var data1 = await fetchTest('new')
    var data2 = data1.length>0?JSON.parse(data1):[]
    this.setState({data: data2.data,dataall: data2.data,refreshing: false})
  }
  onRefresh = (status) => {
    this.setState({
        refreshing: true,
    }, async() => {
      var data1 = await fetchTest(status)
      var data2 = data1.length>0?JSON.parse(data1):[]
      this.setState({data: data2.data,refreshing: false})
    })
}
  updateIndex (selectedIndex) {
    this.setState({ selectedIndex,refreshing: true})
    if(selectedIndex == 0) {
      this.setState({data: this.state.dataall,refreshing: false,status: 'new'})
    }
    else if(selectedIndex == 1) {
      this.getData('mostview')
      this.setState({status: 'mostview'})
    }
    else if(selectedIndex == 2) {
      this.getData('notanswered ')
      this.setState({status: 'notanswered'})
    }
    else if(selectedIndex == 3) {
      this.getData('answered ')
      this.setState({status: 'answered'})
    }
  }

  _renderItem = ({item,index}) => {
    var a = item.anhdaidien;
    var url = a?`${HOST}${a}`:'https://images2.alphacoders.com/552/552466.jpg'
    return (
    <Animatable.View delay={index*300} animation='zoomInLeft' style={styles.view}>
    <TouchableOpacity onPress = {() => {this.props.navigation.navigate('PAHT_DetailScreen', {item: item.id})}}>
          <View >
            <ImageBackground
              resizeMode='cover'
              style={{height: 150, flex: 1}}
              source={{uri: url}}>
                <View style={{position: 'absolute',bottom:0,left:0, padding: 10}}><Text style={styles.tieudeText}>{item.tieude}</Text></View>
              </ImageBackground>
            <View style={styles.view1}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name='account-circle' color='#e2e2e2'/>
                  <Text style={styles.nameText}>Người dân</Text>
                </View>
                <Text style={{fontStyle: 'italic'}}>{item.thoigiangui}</Text>
              </View>
              <Text numberOfLines={3} style={{paddingTop: 10, fontSize: 13}}>{item.noidung}</Text>
              <Text style={styles.statusText}>{item.tinhtrang}</Text>
            </View>
            <View style={styles.view3}><Icon name='bookmark' color='white' style={{}}/><Text style={{color: '#fff', padding: 5, fontSize: 13}}>Phản ánh hiện trường</Text></View>
          </View>
    </TouchableOpacity>
    </Animatable.View>
    )}

    renderBody(){
      if(!this.state.refreshing){
        return(
          <FlatList
            extraData={this.state}
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={EmptyFlatlist}
          />
        )
      }
    }

  render () {
    const { selectedIndex, data } = this.state
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={{flex: 1, margin: 10}}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh(this.state.status)}
            />
          }
        >
          <Text style={{color: 'black'}}>Danh sách phản ánh</Text>
          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            containerStyle={{height: 35, backgroundColor: '#fff', borderWidth: 0, elevation: 3}}
            buttonStyle={{}}
            textStyle={{fontSize: 12}}
            innerBorderStyle={{width: 0.2}}
          />
        {this.renderBody()}
        </ScrollView>
      </View>
    )
  }
}
