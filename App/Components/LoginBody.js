import React, { Component } from "react";
import { View, ImageBackground, TextInput, TouchableOpacity,ScrollView, AsyncStorage, ActivityIndicator, Image, ToastAndroid, TouchableWithoutFeedback, Keyboard} from "react-native";
import {Text, Button, Icon, Input} from "react-native-elements";
import styles from "./Styles/LoginBodyStyles"
import axios from 'axios'
import {parseString} from "react-native-xml2js"
import { StackActions, NavigationActions } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
import * as Animatable from 'react-native-animatable';
import ToggleSwitch from './ToggleSwitch';
import TouchID from 'react-native-touch-id';
import images from "../Themes/Images"
import {CD_URL} from '../Config/server'
import Toast, {DURATION} from 'react-native-easy-toast'
import firebase from 'react-native-firebase';

const optionalConfigObject = {
  title: 'Xác thực vân tay', // Android
  imageColor: '#e00606', // Android
  imageErrorColor: '#ff0000', // Android
  sensorDescription: 'Touch sensor', // Android
  sensorErrorDescription: 'Thất bại', // Android
  cancelText: 'Hủy', // Android
  fallbackLabel: '', // iOS (if empty, then label is hidden)
  unifiedErrors: false, // use unified error messages (default false)
  passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};
 

export default class LoginBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      hide: false,
      username: '',
      password: '',
      loadingLogin: false,
      checkXacThucVanTay: false
    };
  }

  componentWillMount = () =>{
    this.getUsername()
    this.getXacThuc()
  }

  eyepass = () =>{
    if(this.state.hide){
        return(
            <Icon name='eye' type='font-awesome' size={18} color='#EEEEEE' onPress={() => {this.setState({hide: !this.state.hide})}} />
        )
    }
    else{
        return(
            <Icon name='eye-slash' type='font-awesome' size={18} color='#BDBDBD' onPress={() => {this.setState({hide: !this.state.hide})}} />
        )
    }
  }

  getUsername = async () => {
    try {
      const value = await AsyncStorage.getItem('username');
      if (value !== null) {
        this.setState({username: value})
      }
    } catch (e) {
      // error reading value
    }
  };

  getXacThuc = async () => {
    try {
      const value = await AsyncStorage.getItem('XacThucVanTay');
      var value1 = JSON.parse(value)
      if (value1) {
          this.setState({checkXacThucVanTay: true})
          this.refs.toast.show('Sử dụng vân tay để đăng nhập nhanh', ToastAndroid.LONG)
      }
    } catch (e) {
      // error reading value
    }
  };


  getPassword = async () =>{
    try {
        const value = await AsyncStorage.getItem('password');
        if (value !== null) {
            this.setState({password: value});
            this.login(this.state.username, value);
        }
      } catch (e) {
        // error reading value
      }
  };



  DangNhapBangVanTay = () =>{
    TouchID.isSupported()
    .then(this.authenticate())
    .catch(error => {
      console.log('TouchID not supported');
      //AlertIOS.alert('Authenticated Successfully');
    });
  };

  authenticate = () => {
    return TouchID.authenticate(
      'Xác thực dấu vân tay của bạn để tiếp tục',
      optionalConfigObject,
    )
      .then(success => {
        
        this.getPassword();
      })
      .catch(error => {
        console.log('Xac thuc that bai');
        console.log(error);
        //AlertIOS.alert(error.message);
      });
  };

  login = async(username,password) => {
    Keyboard.dismiss()
    if(!username || !password){
      this.refs.toast.show('Vui lòng nhập tài khoản và mật khẩu', ToastAndroid.LONG)
    }
    else{
    this.setState({loadingLogin: true})
    var deviceToken = await firebase.messaging().getToken();
    console.log(deviceToken)
    var body = {'user': username, 'pass': password, 'tokenDevice': deviceToken}
    var a = await axios(`${CD_URL}/GetTokenKey`, {
      method: 'POST',
      data: body,
    })
    .then((response) => {
      return response.data
    })
    .catch(() => this.refs.toast.show('Đăng nhập thất bại', ToastAndroid.LONG) + setTimeout(() => {this.setState({loadingLogin: false})}, 1000))
    if(a.error.code == 200){
      await AsyncStorage.multiSet([
        ['userToken', a.data.token],
        ['deviceToken', deviceToken],
        ['userInfo', JSON.stringify(a.data)],
        ['identifier', a.data.identifier],
        ['soHoKhau', a.data.soHoKhau],
        ['userName', a.data.fullName],
        ['username', username],
        ['password', password],
      ])
      firebase.messaging().subscribeToTopic('DVCTD');
      this.refs.toast.show('Đăng nhập thành công', ToastAndroid.LONG)
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'AppNavigation' })],
      });
      SplashScreen.show()
      setTimeout(() => {this.props.navigation.dispatch(resetAction)}, 1000)
    }
    else{
      var waring = a?a.error.userMessage: 'Vui lòng thử lại sau'
      setTimeout(() => {this.setState({loadingLogin: false}) + this.refs.toast.show(waring, ToastAndroid.LONG)}, 1000)
    }
  }
  }

  render() {
    const{username,password} = this.state
    return (
        <View style={styles.container}>
            <ImageBackground onLoadEnd={() => SplashScreen.hide()} resizeMode='cover' style={{flex: 1, opacity: 0.9}} source={images.background.bg}>
            <ScrollView contentContainerStyle={{flexGrow: 1}}
              keyboardShouldPersistTaps='handled'
            >
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20}}>
                    <Input 
                        autoCapitalize="none"
                        inputContainerStyle={styles.input}
                        placeholder='ID đăng nhập'
                        placeholderTextColor='#CCCCCC'
                        leftIcon={{ type: 'font-awesome', name: 'user', color: '#fff'}}
                        containerStyle={{ alignItems: 'center' }}
                        inputStyle={{color: '#fff', paddingStart: 10}}
                        onChangeText={(text) => this.setState({username: text})}
                        value={this.state.username}
                    />
                    <Input
                        autoCapitalize="none"
                        inputContainerStyle={styles.input}
                        placeholder='Mật khẩu'
                        placeholderTextColor='#CCCCCC'
                        leftIcon={{ type: 'font-awesome', name: 'key', color: '#fff' }}
                        secureTextEntry={!this.state.hide}
                        containerStyle={{ alignItems: 'center' }}
                        rightIcon={this.eyepass()}
                        inputStyle={{color: '#fff', paddingStart: 10}}
                        onChangeText={(text) => this.setState({password: text})}
                        value={this.state.password}
                    />
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <Button
                        type='outline'
                        title='Đăng nhập'
                        buttonStyle={{backgroundColor: '#fff', borderRadius: 20, width: 150}}
                        containerStyle={{marginTop: 30, height: 50, margin: 10}}
                        onPress={() => this.login(username,password)}
                        loading={this.state.loadingLogin}
                      />
                      <Button
                        type='outline'
                        title='Đăng ký'
                        buttonStyle={{backgroundColor: '#fff', borderRadius: 20, width: 150}}
                        titleStyle={{color: '#FF5722'}}
                        containerStyle={{marginTop: 30, height: 50, margin: 10}}
                        onPress={() => this.props.navigation.navigate('SignUpScreen')}
                      />
                    </View>
                    {this.state.checkXacThucVanTay ? (
                    <TouchableOpacity
                      onPress={() => {this.DangNhapBangVanTay();}}>
                      <View style={{height: 50, width: 50, borderRadius: 50, borderColor: '#fff', borderWidth: 0.5, margin:15, justifyContent:'center', alignItems:'center'}}>
                        <Icon name="fingerprint" size={45} color="#fff" />
                      </View>
                    </TouchableOpacity>
                  ) : (
                    <></>
                  )}
                    <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', margin: 10}} onPress={()=> this.login('036090006275','default')}><Text style={styles.text5}>Đăng nhập với tài khoản khách </Text></TouchableOpacity>
                </View>
                
            </ScrollView>
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
            </ImageBackground>
        </View>
    );
  }
}
