import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, Image, ActivityIndicator, ImageBackground, ScrollView, Dimensions} from "react-native";
import {Text, Button, Icon, Rating, Divider} from "react-native-elements";
import styles from "./Styles/DL_DetailBodyStyles"
import {DL_URL} from '../Config/server'
import {requestPOST, requestGET} from '../Api/Basic'
import EmptyFlatlist from './EmptyFlatlist'
import * as Animatable from 'react-native-animatable';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from './Styles/SliderEntryStyles';
import SliderEntry from './SliderEntry';

export default class DL_DetailBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          loading: true,
          data: [],
          name: this.props.navigation.getParam('Ten'),
          dataAnh: [],
          slider1ActiveSlide: 0
        };
      }

      componentWillMount = async() => {
        var ID = this.props.navigation.getParam('ID')
        var body = {"id": ID}
        var data1 = await requestPOST(`${DL_URL}/GetDiemDuLich`, body)
        console.log(data1.data)
        var data2 = data1.data?data1.data:[]
        var c = []
        c.push({title: 'Tư liệu ảnh', subtitle: '', illustration: data2.AnhDaiDien})
        if(data1.data && data1.data.TuLieuAnh){
            var a = data1.data.TuLieuAnh
            a.map((i) => {
                i.Anhs.map((j) => {
                    c.push({title: i.TrichYeu, subtitle: i.NoiDung, illustration: j})
                })
            })
        }
        console.log(c)
        this.setState({data: data2, loading: false, dataAnh: c})
      }

      navigateToMap = () => {
        var data = this.state.data
        var dataMap = []
        data.map((item) => {
            dataMap.push({name: item.Ten, address: item.DiaChi, location: {latitude: Number(item.LatitudeLongtitude),longitude: Number(item.LatitudeLongtitude)}, distance: ''})
        })
        this.props.navigation.navigate('MapViewScreen', {data: dataMap})
      }


        _renderItemWithParallax ({item, index}, parallaxProps) {
            return (
                <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
                />
            );
        }

        _renderItem ({item, index}) {
            return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
        }


      renderBody(){
        
        if(!this.state.loading){
            const {data} = this.state
          return(
                <View style={{flex: 1}}>
                    <ScrollView>
                    <Carousel
                        ref={c => this._slider1Ref = c}
                        data={this.state.dataAnh}
                        renderItem={this._renderItemWithParallax}
                        sliderWidth={sliderWidth}
                        itemWidth={itemWidth}
                        hasParallaxImages={true}
                        firstItem={0}
                        inactiveSlideScale={0.94}
                        inactiveSlideOpacity={0.7}
                        // inactiveSlideShift={20}
                        containerCustomStyle={styles.slider}
                        contentContainerCustomStyle={styles.sliderContentContainer}
                        loop={true}
                        loopClonesPerSide={2}
                        autoplay={true}
                        autoplayDelay={500}
                        autoplayInterval={3000}
                        onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                    />
                    <Pagination
                        dotsLength={this.state.dataAnh.length}
                        activeDotIndex={this.state.slider1ActiveSlide}
                        containerStyle={styles.paginationContainer}
                        dotColor={'#fff'}
                        dotStyle={styles.paginationDot}
                        inactiveDotColor={'#e8e8e8'}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                        carouselRef={this._slider1Ref}
                        tappableDots={!!this._slider1Ref}
                    />
                    <View style= {{padding: 10}}>
                        <Text style={{color: 'black', fontSize: 20, fontWeight: 'bold'}}>{data.Ten}</Text>
                        <Rating
                            type='custom'
                            imageSize={24}
                            readonly
                            startingValue={data.DanhGia?data.DanhGia:0}
                            style={{padding: 5, alignSelf: 'flex-start'}}
                            size={18}
                        />
                        <Divider style={{backgroundColor: '#BDBDBD', height: 0.5, margin: 10}} />
                        <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>Giới thiệu</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon type='font-awesome' name='bookmark' color='#2196F3' />
                            <Text style={{flex: 1, padding: 10}}>{data.GioiThieu}</Text>
                        </View>
                        <Divider style={{backgroundColor: '#BDBDBD', height: 0.5, margin: 10}} />
                        <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>Thông tin</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon type='font-awesome' name='map-marked' color='#2196F3' />
                            <Text style={{flex: 1, padding: 10}}>{data.DiaChi}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon type='font-awesome' name='phone' color='#2196F3' />
                            <Text style={{flex: 1, padding: 10}}>{data.DienThoai}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon type='font-awesome' name='envelope' color='#2196F3' />
                            <Text style={{flex: 1, padding: 10}}>{data.Email}</Text>
                        </View>
                        <Divider style={{backgroundColor: '#BDBDBD', height: 0.5, margin: 10}} />
                        <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>Thông tin khác</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon type='font-awesome' name='info-circle' color='#2196F3' />
                            <Text style={{flex: 1, padding: 10}}>{data.ThongTinKhac}</Text>
                        </View>
                    </View>
                    </ScrollView>
                </View>
          )
        }
        else{
          return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator size='large' style={{}} color='#9aca40' />
              <Text style={{textAlign: 'center', paddingTop: 10, color: '#9aca40'}}>Đang lấy dữ liệu</Text>
            </View>
          )
        }
      }

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}>
                <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
                <Text numberOfLines={1} style={styles.title}>Chi tiết địa điểm</Text>
                <Icon onPress={() => {}} name='map' color='transparent' containerStyle={{paddingEnd: 20}} />
            </View>
            {this.renderBody()}
        </View>
    );
  }
}