import React, { Component } from "react";
import {StyleSheet,View,ScrollView,StatusBar, ImageBackground, TouchableOpacity, ActivityIndicator} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/MainBody_MapTabStyles'
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import {MapData} from "../Data/MapData"
import {QLDB_URL, HOST_YT} from "../Config/server"
import {requestGET, requestPOST} from "../Api/Basic"
import EmptyFlatlist from './EmptyFlatlist'

export default class MainBody_MapTab extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            loading: true,
            dataInfo: [],
            dataChild: []
        };
    }

  componentWillMount = async() => {
    var data1 = await requestGET(`${QLDB_URL}/DiaBanApi/mGetAreaInfo?AreaCode=36`)
    var dataInfo = data1.data[0]?data1.data[0]:[]
    var data2 = await requestGET(`${QLDB_URL}/DiaBanApi/mGetChildByCode?AreaCode=36`)
    var dataChild = data2?data2:[]
    this.setState({dataChild: dataChild, dataInfo: dataInfo, loading: false})
  }
  
  render() {
   const {dataChild, dataInfo} = this.state
   var urlAnh1 = dataInfo.UrlAnhDaiDien?`${HOST_YT}${dataInfo.UrlAnhDaiDien.split(",")[0]}`:'https://vneconomy.mediacdn.vn/zoom/480_270/2019/3/29/tp-hcm-155384260439116565666-crop-1553842610657853611450.jpg'
   return (
    <View style={styles.container}>
        {this.state.loading?
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#212121' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#212121'}}>Đang lấy dữ liệu</Text>
        </View>
        :
        <ScrollView>
            <ImageBackground source={{uri: urlAnh1}} style={{height: 250}} />
            <View style={styles.view1}>
                <Icon type='font-awesome' name='map' size={18} color='#FF9800' />
                <Text style={{fontWeight: 'bold'}}>      Bản đồ hành chính {dataInfo.Area.AreaName}</Text>
            </View>
            <View style={{padding: 15}}>
                <Text style={{color: '#FF9800'}}>Dư địa chỉ</Text>
                <Text style={{fontSize: 40, padding: 5, paddingLeft: 0}}>{dataInfo.Area.AreaName}</Text>
                <Text numberOfLines={3} style={{lineHeight: 30,}}>{dataInfo.TongQuan_GioiThieu}</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('BDHC_UnitScreen', {data: dataInfo})} style={{flexDirection: 'row', paddingTop: 10, alignItems: 'center'}}>
                    <Text style={{color: '#FF9800', fontWeight: '600'}}>Xem chi tiết </Text>
                    <Icon type='font-awesome' name='chevron-right' size={18} color='#FF9800' />
                </TouchableOpacity>
                <Text style={{fontSize: 18, paddingTop: 30, paddingBottom: 10, fontWeight: '600'}}>Các đơn vị hành chính</Text>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    flexWrap: 'wrap',
                }}>
                {dataChild.map(item => {
                    var urlAnh = item.Url_AnhDaiDien?`${HOST_YT}${item.Url_AnhDaiDien.split(",")[0]}`:'https://vneconomy.mediacdn.vn/zoom/480_270/2019/3/29/tp-hcm-155384260439116565666-crop-1553842610657853611450.jpg'
                    return(
                    <TouchableOpacity key={item.ID} onPress={() => this.props.navigation.navigate('BDHC_UnitChildScreen', {data: item})} style={{justifyContent: 'center', padding: 5}}>
                        <ImageBackground source={{uri: urlAnh}} style={{height: 80, width: 100, overflow: 'hidden', borderTopLeftRadius: 10, borderBottomRightRadius: 20}} />
                        <Text style={{paddingTop: 10, fontWeight: '600', width: 100, height: 50}}>{item.AreaName}</Text>
                        <Text style={{paddingTop: 5, color: '#757575', paddingBottom: 10}}></Text>
                    </TouchableOpacity>
                )})}
                </View>
                </View>
        </ScrollView>
        }
    </View>
    );
  }
}
