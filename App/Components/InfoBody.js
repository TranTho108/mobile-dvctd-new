import React, { Component } from "react";
import { View, AsyncStorage, ImageBackground, ScrollView, TouchableOpacity, RefreshControl, FlatList, Image, StatusBar} from "react-native";
import {Text, Button, Icon, Divider} from "react-native-elements";
import styles from "./Styles/InfoBodyStyles.js"
import images from "../Themes/Images"
import axios from "axios"
import ScrollableTabView from 'react-native-scrollable-tab-view';
var moment = require('moment')
moment.locale('vi')

export default class InfoBody extends Component {

    constructor(props) {
        super(props);
      
        this.state = {
            userInfo: []
        };
    }

    componentWillMount = async() => {
        var userToken = await AsyncStorage.getItem("userToken")
        if(userToken){
            userInfo = await AsyncStorage.getItem("userInfo")
            var info = JSON.parse(userInfo)
            this.setState({userInfo: info})
        }

    }
    
  render() {
      const {userInfo} = this.state
    return (
        <View style={styles.container}>
            <View style={{paddingTop: 30}}>
                <View style={{flexDirection: 'row', padding: 10}}>
                    <Icon name='arrow-back' size={30} onPress={() => this.props.navigation.goBack()} />
                    <Text style={{fontWeight: 'bold', fontSize: 24, paddingStart: 10}}>Tài khoản của tôi</Text>
                </View>
            </View>
            <View style={{flex: 1}}>
                <ScrollView>
                <Divider style={{backgroundColor: '#F6F6F6', height: 8}} />
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8'}}>
                    <Text style={{color: '#616161'}}>ID đăng nhập</Text>
                    <Text>{userInfo.identifier}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8'}}>
                    <Text style={{color: '#616161'}}>Họ & tên</Text>
                    <Text>{userInfo.fullName}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch'}}>
                    <Text style={{color: '#616161'}}>SĐT</Text>
                    <Text>{userInfo.phoneNumber}</Text>
                </View>
                <Divider style={{backgroundColor: '#F6F6F6', height: 8}} />
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8'}}>
                    <Text style={{color: '#616161'}}>Giới tính</Text>
                    <Text>{userInfo.sex}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8'}}>
                    <Text style={{color: '#616161'}}>Ngày sinh</Text>
                    <Text>{userInfo.birthday}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch'}}>
                    <Text style={{color: '#616161'}}>Email</Text>
                    <Text>{userInfo.email}</Text>
                </View>
                <Divider style={{backgroundColor: '#F6F6F6', height: 8}} />
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, padding: 10, alignSelf: 'stretch'}}>
                    <Text style={{color: '#616161'}}>Địa chỉ</Text>
                    <Text>{userInfo.address}</Text>
                </View>
                </ScrollView>
            </View>
        </View>
    );
  }
}
