import React, { Component } from "react";
import { View, AsyncStorage, FlatList, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Tooltip} from "react-native-elements";
import styles from "./Styles/GD_MenuTabStyles"
import moment from 'moment'
import Modal from "react-native-modal";
import 'moment/locale/vi'
moment.locale('vi')
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import RadioForm from 'react-native-simple-radio-button';

export default class GD_MenuTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        dataNoti: [],
        dataCheck: [],
        value: '',
        value1: '',
        visibleModal1: false,
        namhocs: [],
        hockys: [],
        modalLoading: true
    }
  }

  componentWillMount = () => {

  }

  showNamHoc = async() => {
    this.setState({visibleModal1: true})
    var data1 = await requestGET(`${GD_URL}/GetNamHocs`)
    var data2 = data1.data?data1.data:[]
    var radio_props = []
    data2.forEach((i) => {
      radio_props.push({label: i.TieuDe, value: i.ID})
    })
    var data3 = await requestGET(`${GD_URL}/GetHocKys`)
    var data4 = data3.data?data3.data:[]
    var radio_props1 = []
    data4.forEach((i) => {
      radio_props1.push({label: i.TieuDe, value: i.ID})
    })
    this.setState({namhocs: radio_props, hockys: radio_props1, modalLoading: false})
  }

  setNamHoc = async() => {
    if(this.state.value && this.state.value1){
      var a = this.state.namhocs.findIndex(x => x.value === this.state.value)
      var b = this.state.hockys.findIndex(x => x.value === this.state.value1)
      await AsyncStorage.multiSet([
        ['NamHocID', this.state.value.toString()],
        ['HocKyID', this.state.value1.toString()],
        ['NamHoc', this.state.namhocs[a].label],
        ['HocKy', this.state.hockys[b].label],
      ])
      this.setState({visibleModal1: false})
      setTimeout(() => this.props.navigation.replace('GD_MainScreen'), 200)
    }
  }

  renderModalContent1 = () => {
    if(!this.state.modalLoading){
    return(
      <View style={styles.content}>
        <Text style={{fontStyle: 'italic', padding: 20, textAlign: 'center'}} >Vui lòng chọn năm học: </Text>
        <RadioForm
          radio_props={this.state.namhocs}
          initial={-1}
          onPress={(value) => {this.setState({value:value})}}
          buttonSize={15}
          buttonWrapStyle={{padding: 20}}
        />
        <Text style={{fontStyle: 'italic', padding: 20, textAlign: 'center'}} >Vui lòng chọn học kỳ: </Text>
        <RadioForm
          radio_props={this.state.hockys}
          initial={-1}
          onPress={(value) => {this.setState({value1:value})}}
          buttonSize={15}
          buttonWrapStyle={{padding: 20}}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch', paddingTop: 20}}>

          <TouchableOpacity onPress={() => {this.setState({visibleModal1: false})}}>
            <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold'}}>ĐÓNG</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {this.setNamHoc()}}>
            <Text style={{fontSize: 14, color: '#66BB6A', fontWeight: '600'}}>CHỌN</Text>
          </TouchableOpacity>
          
        </View>
      </View>
    )
    }
    else {
      return(
        <View style={styles.content}>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#2AA5FF'}}>Đang lấy dữ liệu</Text>
          </View>
        </View>
      )
    }
  }
    
  render() {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GD_TKB_MainScreen')} style={styles.view1}>
              <Icon type='font-awesome' name='calendar-alt' color='#73809B' size={30} containerStyle={{padding: 10}}/>
              <Text style={styles.text1}>Thời khoá biểu</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GD_DD_MainScreen')} style={styles.view1}>
              <Icon type='font-awesome' name='calendar-check' color='#73809B' size={30} containerStyle={{padding: 10}}/>
              <Text style={styles.text1}>Điểm danh</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GD_TB_MainScreen')}  style={styles.view1}>
              <Icon type='font-awesome' name='bell' color='#73809B' size={30} containerStyle={{padding: 10}}/>
              <Text style={styles.text1}>Thông báo</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GD_KPA_MainScreen')}  style={styles.view1}>
              <Icon type='font-awesome' name='utensils' color='#73809B' size={30} containerStyle={{padding: 10}}/>
              <Text style={styles.text1}>Khẩu phần ăn</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.showNamHoc()}}  style={styles.view1}>
              <Icon type='font-awesome' name='cog' color='#73809B' size={30} containerStyle={{padding: 10}}/>
              <Text style={styles.text1}>Chọn lại năm học</Text>
            </TouchableOpacity>
            <Modal
              backdropTransitionOutTiming={0}
              isVisible={this.state.visibleModal1}
              style={{margin: 0}}
              hideModalContentWhileAnimating={true}>
              {this.renderModalContent1()}
            </Modal>
        </View>
    );
  }
}
