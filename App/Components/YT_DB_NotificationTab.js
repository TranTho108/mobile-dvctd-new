import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_QT_MainBodyStyles"
import images from "../Themes/Images"
import Modal from "react-native-modal";
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import {YT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'


export default class YT_DB_NotificationTab extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          data: [],
          loading: true,
          visibleModal: false,
          detail: []
        };
      }

    componentWillMount = async() => {
      var data1 = await requestGET(`${YT_URL}/ThongBaoDichBenhs/mGetListThongBao`)
      var data2 = data1.data?data1.data:[]
      this.setState({data: data2, loading: false})
    }

    _renderItem = ({item,index}) => {
    return(
        <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#9aca40', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='bell' color='#fff' size={18} /></View>
        <View style={{flex: 1, paddingStart: 10}}>
            <Text style={{fontSize: 16, fontWeight: 'bold'}}>{item.TieuDe}</Text>
            <Text style={{color: '#757575', flex: 1}}>{item.NoiDung}</Text>
            <Text style={{color: '#757575', flex: 1, textAlign: 'right', fontStyle: 'italic'}}>{item.Created}</Text>
        </View>
        </TouchableOpacity>
    )
    }

    _renderModalContent = () => {
      if(this.state.detail){
      const {detail} = this.state
      return(
        <View style={styles.content2}>
          <ScrollView showsVerticalScrollIndicator={false} style={{padding: 20}}>
            <Text style={styles.title5}>Chi tiết thông báo</Text>
            <Text style={styles.section}>Tiêu đề</Text>
            <Text style={styles.label}>{detail.TieuDe}</Text>
            <Text style={styles.section}>Nội dung</Text>
            <Text style={styles.label}>{detail.NoiDung}</Text>
            <Text style={styles.section}>Ngày bắt đầu</Text>
            <Text style={styles.label}>{detail.NgayBatDau}</Text>
            <Text style={styles.section}></Text>
            <Text style={styles.label}></Text>
          </ScrollView>
          <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
               <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
          </TouchableOpacity>
        </View>
      )
      }
    }

    renderBody(){
      if(!this.state.loading){
        return(
          <View style={{flex: 1}}>
          <FlatList 
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{padding: 10}}
            ListEmptyComponent={EmptyFlatlist}
        />
        </View>
        )
      }
      else{
        return(
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#9aca40' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#9aca40'}}>Đang lấy dữ liệu</Text>
          </View>
        )
      }
    }

  render() {
    return (
        <View style={styles.container}>
        {this.renderBody()}
        <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
    </View>
    );
  }
}
