import React, { Component } from "react";
import { View, AsyncStorage, FlatList, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon, Tooltip} from "react-native-elements";
import styles from "./Styles/GD_MenuTabStyles"
import moment from 'moment'
import 'moment/locale/vi'
moment.locale('vi')
import {requestPOST, requestGET} from '../Api/Basic'
import {GD_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'

export default class GD_TKB_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            dataDD: [],
            countNNCP: 0,
            countNNKP: 0,
            countNDH: 0,
            loading: true
        }
      }
    
      componentWillMount = async() => {
        var userToken = await AsyncStorage.getItem('userToken')
        var NamHocID = await AsyncStorage.getItem('NamHocID')
        var studentID = await AsyncStorage.getItem('studentID')
        var body = {'token': userToken, 'HocSinhID': studentID, 'NamHocID': NamHocID}
        var data1 = await requestPOST(`${GD_URL}/DiemDanhHocSinh`, body)
        var data2 = data1.data?data1.data:[]
        this.setState({dataDD: data2, loading: false})
      }


    _renderItem1 = ({item,index}) => {
        return(
          <View>
            <Text style={{textAlign: 'center', padding: 10}}>{item.month}</Text>
            <FlatList 
              data={item.data}
              renderItem={this._renderItem2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
            />
          </View>
        )
      }
    
      _renderItem2 = ({item,index}) => {
        var day = moment(item.NgayDiemDanh).format('L')
        var dayString = moment(item.NgayDiemDanh, "DD-MM-YYYY").format('dddd')
        return(
          <View style={{margin: 10, flexDirection: 'row', alignSelf: 'stretch', borderRadius: 5, borderWidth: 0.3, borderColor: '#e8e8e8'}}>
          <View style={{flex: 1/4, backgroundColor: '#FAFAFA', alignItems: 'center', justifyContent: 'center', padding: 10}}>
            <Text style={styles.date}>{day}</Text>
            <Text>{dayString}</Text>
          </View>
          <View style={{flex: 3/4}}>
          <View style={{justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{padding: 5, backgroundColor: '#2AA5FF', borderRadius: 30}}><Icon name='cloud-sun' type='font-awesome' color='#fff' size={10} /></View>
              <Text style={{paddingStart: 10}}>Buổi {item.BuoiHoc}</Text>
            </View>
            <View>
              <Text style={{color: '#4CAF50', fontStyle: 'italic'}}>{item.TrangThaiDiemDanh}</Text>
            </View>
          </View>
          </View>
        </View>
        )
      }

    renderBody = () => {
      if(!this.state.loading){
        return(
          <View style={{flex: 1}}>
            <View style={{padding: 10}}>
              <View style={{flexDirection: 'row', paddingBottom: 5}}><Text>Tổng số ngày đi học: </Text><Text style={{fontWeight: 'bold'}}>{this.state.dataDD.length}</Text></View>
              <View style={{flexDirection: 'row'}}><Text>Số ngày nghỉ: </Text><Text style={{fontWeight: 'bold'}}>{this.state.countNNCP + this.state.countNNKP}</Text><Text style={{color: '#FF9800'}}> (không phép {this.state.countNNKP})</Text></View>
              </View>
              <FlatList 
                  data={this.state.dataDD}
                  renderItem={this._renderItem2}
                  keyExtractor={(item, index) => index.toString()}
                  extraData={this.state}
                  ListEmptyComponent={EmptyFlatlist}
              />
            </View>
        )
      }
      else{
        return(
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
          </View>
        )
      }
    }
    
  render() {
    return (
        <View style={{backgroundColor: '#fff', flex: 1,}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#2AA5FF', padding: 15,paddingTop: 35}}>
            <Icon onPress={() => this.props.navigation.goBack()} name='chevron-left' type='font-awesome' color='#fff' size={24} />
            <Text style={styles.text3}>Điểm danh</Text>
            <Icon name='arrow-left' type='font-awesome' color='transparent' size={24} />
            </View>
            {this.renderBody()}
        </View>
    );
  }
}
