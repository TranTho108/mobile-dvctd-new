import React, { Component } from "react";
import { View, ImageBackground, TouchableOpacity, Image, ToastAndroid, ScrollView} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import firebase from 'react-native-firebase';
import * as Animatable from 'react-native-animatable';
import Toast, {DURATION} from 'react-native-easy-toast'

export default class DVC_MainBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      token: ''
    };
  }

  componentWillMount = async() =>{
    await firebase.messaging().getToken()
    .then(fcmToken => {
      if (fcmToken) {
        this.setState({token: fcmToken})
      } else {
        
      } 
    })
    .catch((err) => console.log(err))
  }

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}>
              <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
              <Text style={styles.title}>Dịch vụ hành chính công</Text>
              <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            <View style={{flex: 1}}>
              <View style={{flex: 3/10}} ><ImageBackground resizeMode='cover' source={images.background.DVC_background} style={{flex:1}}></ImageBackground></View>
              <View style={{flex: 7/10}}>
                <View style={{position: 'absolute', top: -60, left: 0, right: 0,bottom: 0, backgroundColor: 'transparent', margin: 20}}>
                <View>
                <ScrollView contentContainerStyle={{flexGrow:1}}>
                  <View style={{flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center'}}>
                    <Animatable.View delay={200} animation='bounceInLeft' style={{backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', flex: 1/2, margin: 5}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("DVC_TKHS_SearchScreen", {token: this.state.token})} style={{padding: 20}}>
                      <Icon name="search" type="font-awesome" color="#616161" size={28} containerStyle={styles.icon} />
                      <Text style={styles.text}>Tra cứu hồ sơ</Text>
                    </TouchableOpacity>
                    </Animatable.View>

                    <Animatable.View delay={200} animation='bounceInRight' style={{backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', flex: 1/2, margin: 5}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('DVC_TKHS_MainScreen')} style={{padding: 20}}>
                      <Icon name="list" type="font-awesome" color="#616161" size={28} containerStyle={styles.icon} />
                      <Text style={styles.text}>Hồ sơ theo dõi</Text>
                      </TouchableOpacity>
                    </Animatable.View>

                  </View>

                  <View style={{flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center'}}>

                  
                  <Animatable.View delay={200} animation='bounceInLeft' style={{backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', flex: 1/2, margin: 5, }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DVC_TCTT_MainScreen')} style={{padding: 20}}>
                      <Icon name="book" type="font-awesome" color="#616161" size={28} containerStyle={styles.icon} />
                      <Text style={styles.text}>Tra cứu thủ tục</Text>
                    </TouchableOpacity>
                    </Animatable.View>

                    <Animatable.View delay={200} animation='bounceInRight' style={{backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', flex: 1/2, margin: 5,}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DVC_PAKN_MainScreen')} style={{padding: 20}}>
                    <Icon name="user-edit" type="font-awesome" color="#616161" size={28} containerStyle={styles.icon} />
                    <Text style={styles.text}>Phản ánh kiến nghị</Text>
                    </TouchableOpacity>
                    </Animatable.View>
                  </View>

                  <View style={{flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center'}}>

                    <Animatable.View delay={200} animation='bounceInRight' style={{backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', flex: 1/2, margin: 5, }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DVC_DGHL_MainScreen')} style={{padding: 20}}>
                      <Icon name="user-check" type="font-awesome" color="#616161" size={28} containerStyle={styles.icon} />
                      <Text style={styles.text}>Đánh giá hài lòng</Text>
                    </TouchableOpacity>
                    </Animatable.View>
                  </View>
                  </ScrollView>
                </View>
                </View>
              </View>
            </View>
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
