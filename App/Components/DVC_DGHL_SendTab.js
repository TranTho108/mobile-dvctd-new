import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, Image, Dimensions, ActivityIndicator, ToastAndroid} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_DGHL_SendTabStyles"
import images from '../Themes/Images'
import RadioForm from 'react-native-simple-radio-button';
import Modal from 'react-native-modal';
import axios from "axios"
import {DVC_URL} from '../Config/server'
import * as Animatable from 'react-native-animatable';
import {parseString} from "react-native-xml2js"
import Toast, {DURATION} from 'react-native-easy-toast'
const height = Dimensions.get('window').height
const radio_props = [
  {label: 'Rất hài lòng', value: 'Rất hài lòng' },
  {label: 'Hài lòng', value: 'Hài lòng' },
  {label: 'Không hài lòng', value: 'Không hài lòng' }
];

export default class DVC_DGHL_SendTab extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          data : [],
          value: '',
          isLoading: true,
          visibleModal_1: false
        };
      }
    

    handleChange(page){
        this.props.handleChangePage(page);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.data !== null){
          console.log(nextProps.data)
          this.setState({data: nextProps.data})
        }
      }
    
      renderModalContent_1 = () => {
        if(!this.state.isLoading){
          return(
          <Animatable.View animation='fadeIn' style={styles.content1}>
            <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
            <Icon
              containerStyle={{marginTop: 20}}
              name="check-circle"
              size={60}
              color="#28a745"
            />
            <Text style={{textAlign: 'center', color: 'black', fontSize: 16, margin: 10}}>Đánh giá được gửi thành công</Text>
            <Button
              onPress={() => {this.setState({visibleModal_1: false}) + this.props.handleChangePage(0)}}
              buttonStyle={{height: 40, margin: 10, marginLeft: 20, marginRight: 20, backgroundColor: '#28a745', borderRadius: 5}}
              title=" ĐÓNG"
              containerStyle={{marginBottom: 20}}
            />        
          </Animatable.View>
          );}
        else{
          return(
            <Animatable.View animation='fadeIn' style={styles.content1}>
              <StatusBar backgroundColor="rgba(0, 0, 0, 0.8)" barStyle="dark-content" translucent={true} />
              <ActivityIndicator size="large" color="black" />
              <Text style={{color: 'black', textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
          </Animatable.View>
          );
        }
      }

    checkContent = () => {
      if(!this.state.value){
        this.refs.toast.show('Vui lòng đánh giá', ToastAndroid.LONG)
      }
      else{
        this.setState({visibleModal_1: true})
        this.sendDG()
      }
    }

    sendDG = async() => {
      var body = {
        "IdHoSo": this.state.data.Id,
        "DanhGia1": this.state.value,
        "DanhGia2": this.state.value,
        "DanhGia3": this.state.value
      }
      var data1 = await requestPOST(`${DVC_URL}/SendSatisfactionRating`, body)
      var data = data1.data?data1.data:[]
      if(data.length > 0){
        setTimeout(() => this.setState({ isLoading: false}),2000)
      }
      else{
        this.setState({visibleModal_1: false})
        this.refs.toast.show('Xảy ra lỗi trong quá trình đánh giá', ToastAndroid.LONG)
      }
    }

  render() {
    return (
        <View style={styles.container}>
          <View style={{paddingTop: 30}}>
            
            <Text style={{fontWeight: 'bold'}}>Mã hồ sơ</Text>
            <Text style={{paddingStart: 10}}>{this.state.data.MaHoSo}</Text>
            <Text style={{fontWeight: 'bold'}}>Chủ hồ sơ</Text>
            <Text style={{paddingStart: 10}}>{this.state.data.ChuHoSo}</Text>
            <Text style={{fontWeight: 'bold'}}>Ông/Bà đánh giá như thế nào về thái độ công chức khi tiếp nhận và hướng dẫn giải quyết hồ sơ?</Text>
            <RadioForm
              radio_props={radio_props}
              initial={-1}
              buttonColor={'#616161'}
              buttonSize={10}
              style={{padding: 10}}
              buttonWrapStyle={{padding: 20}}
              animation={true}
              onPress={(value) => {this.setState({value:value})}}
            />
          </View>
            <Button
                type='outline'
                title='Gửi'
                buttonStyle={{borderRadius: 20, backgroundColor: '#00897B'}}
                containerStyle={{marginTop: 10, marginBottom: 30}}
                titleStyle={{color: '#fff'}}
                onPress={() => this.checkContent()}
            />
            <Modal
              backdropTransitionOutTiming={0}
              isVisible={this.state.visibleModal_1}
              style={{margin: 0}}
              hideModalContentWhileAnimating={true}
              deviceHeight={height+100}
            >
            {this.renderModalContent_1()}
          </Modal>
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
