import React, { Component } from "react";
import { View, AsyncStorage, ImageBackground, ScrollView, TouchableOpacity, RefreshControl, FlatList, Image, StatusBar} from "react-native";
import {Text, Button, Icon, Divider} from "react-native-elements";
import styles from "./Styles/MainBody_HomeTabStyles.js"
import images from "../Themes/Images"
import {dataMenuTD, dataTT} from "../Data/MenuData"
import axios from "axios"
var moment = require('moment')
moment.locale('vi')

export default class MainBody_HomeTab extends Component {

    constructor(props) {
        super(props);
      
        this.state = {
          userInfo: [],
          refreshing: false,
          weather: [],
          temp: ''
        };
    }

    componentWillMount = async() => {
        var userToken = await AsyncStorage.getItem("userToken")
        var temp = ''
        var list = await axios.get('https://api.openweathermap.org/data/2.5/weather?lat=21.02&lon=105.84&APPID=f27de506261598e9f157ed99451eaeed&lang=vi')
        .then(function (response) {
        // handle success
            return response.data;
        })
        .then(function (data) {
            temp = Math.ceil(data.main.temp - 273.15)
            return data.weather
        })
        .catch(function (error) {
        // handle error
            console.log(error);
        })
        if(userToken){
            userInfo = await AsyncStorage.getItem("userInfo")
            info = JSON.parse(userInfo)
            this.setState({userInfo: info, weather: list, temp: temp})
        }
    }

    _renderItem1 = ({item}) => {
      return (
        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: 'column',
            width: 300,
            margin: 10,
            backgroundColor: '#FFFFFF',
    
            borderRadius: 8,
    
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
    
            elevation: 5,
          }}>
          <Image
            resizeMode="cover"
            style={{
              flex: 0.6,
              height: 150,
              overflow: 'hidden',
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}
            source={{uri: item.imageurl}}
          />
          <View style={{height: 80, padding: 10}}>
            <Text style={{fontSize: 14}}>{item.title}</Text>
          </View>
        </TouchableOpacity>
      );
    };

    _renderItem = ({item}) => {
        var description =  item.description.charAt(0).toUpperCase() + item.description.slice(1);
        return (
          <View style={{alignItems:'center', flexDirection: 'row'}}>
            <Image
              resizeMode='contain'
               style={{width: 40,height: 40}}
              source={{uri: 'http://openweathermap.org/img/w/'+item.icon+'.png'}} />
            <Text style={{paddingStart: 20}}>{this.state.temp}&#8451;, {description}</Text>
          </View>
        )
        }
      
      renderWeather = () => {
        if(this.state.weather){
          return (
              <View style={{justifyContent: 'center'}}>
                <FlatList
                  horizontal
                  data={this.state.weather}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
          )
        }
      }
    
  render() {
    return (
        <View style={styles.container}>
            <ImageBackground source={images.background.bg_top} style={{flex: 1/5}}>
                <View style={{paddingTop: 40, padding: 10, flexDirection: 'row'}}>
                    <Text style={styles.title1}>   Xin chào, </Text><Text style={styles.title2}>{this.state.userInfo.fullName}</Text>
                </View>
            </ImageBackground>
            <View style={{marginTop: -50, backgroundColor: 'transparent', flex: 1}}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                      tintColor='#fff'
                      refreshing={this.state.refreshing}
                      onRefresh={() => {
                        this.setState({refreshing: true});
                        setTimeout(() => this.setState({refreshing: false}), 1000);
                      }}
                      // Android offset for RefreshControl
                      progressViewOffset={20}
                    />
                  }
            >
                <View style={{margin: 15, borderRadius: 10, backgroundColor: '#fff', padding: 10, shadowColor: 'rgba(171, 180, 189, 0.35)',shadowOffset: {width: 0,height: 1,},shadowOpacity: 1, elevation: 5}}>
                    <Text
                        style={{fontWeight: 'bold', color: '#565A96', paddingBottom: 10}}>
                        {moment().format('dddd')}, {moment().format('LL')}
                    </Text>
                    {this.renderWeather()}
                    <Divider style={{backgroundColor: '#4849A1'}} />
                    <Text style={{textAlign: 'center', padding: 10, color: '#A9AAAE'}}>Không có thông báo mới</Text>
                </View>
                <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    flexWrap: 'wrap',
                    backgroundColor: '#fff',
                }}>
                {dataMenuTD.map(item => (
                    <TouchableOpacity key={item.appid} onPress={() => this.props.navigation.navigate(item.navigate)} style={{justifyContent: 'center',alignItems: 'center', padding: 5}}>
                    <View  style={{justifyContent: 'center',alignItems: 'center', height: 50, width: 50, backgroundColor: item.color, borderRadius: 20}}>
                        <Icon name={item.icon} type="font-awesome" color="#fff" size={20} containerStyle={styles.icon} />
                        </View>
                        <Text style={{width: 90, textAlign: 'center', paddingTop: 10, height: 50}}>{item.name}</Text>
                    </TouchableOpacity>
                ))}
                </View>
                <View style={styles.viewHeader}>
                  <Text style={styles.textHeaderTitle}>Tin tức </Text>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {}}>
                    <Text style={styles.textHeaderAll}>Tất cả</Text>
                  </TouchableOpacity>
                </View>
                <FlatList
                  horizontal
                  scrollEnabled
                  scrollEventThrottle={16}
                  showsHorizontalScrollIndicator={false}
                  snapToAlignment="center"
                  data={dataTT}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={this._renderItem1}
                />
            </ScrollView>
            </View>
        </View>
    );
  }
}
