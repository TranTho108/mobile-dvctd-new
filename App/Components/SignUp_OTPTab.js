import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ToastAndroid, ActivityIndicator, Keyboard, Alert} from "react-native";
import {Text, Button, Icon, Image} from "react-native-elements";
import styles from "./Styles/SignUp_OTPTabStyles"
import images from '../Themes/Images'
import LinearGradient from 'react-native-linear-gradient';
import axios from "axios"
import {CD_URL} from '../Config/server'
import * as Animatable from 'react-native-animatable';
import {parseString} from "react-native-xml2js"
import CountDown from 'react-native-countdown-component'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import Toast, {DURATION} from 'react-native-easy-toast'

export default class SignUp_OTPTab extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          otp : '',
          loading: false,
          countdown: this.props.countdown,
          ID: this.props.ID,
          running: true
        };
      }
      

    checkContent = async(ID,otp) => {
      if(this.state.otp){
        this.setState({loading: true})
            var a = await axios({
              method: 'GET',
              url: `${CD_URL}/CheckVerificationCode/${ID}/${otp}`,
            })
          .then(response => {
              console.log(response.data)
              return response.data
          })
          .catch(() => this.refs.toast.show('Lỗi kết nối', ToastAndroid.LONG + setTimeout(() => {this.setState({loading: false})}, 1000), ToastAndroid.LONG))
          if(a){
            if(a.error.code == 200){
              setTimeout(() => this.setState({loading: false, running: false}) + this.props.handleChangePage(2), 1000)
            }
            else{
              setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show(a.error.userMessage, ToastAndroid.LONG)}, 1000)
            }
          }
          else{
            setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show('Lỗi kết nối', ToastAndroid.LONG)}, 1000)
          }
      }
      else{
        this.refs.toast.show('Vui lòng nhập mã OTP', ToastAndroid.LONG)
      }
    }

    renderButtonLogin = () => {
      if(!this.state.loading){
        return(
          <Button
          title='Xác nhận'
          buttonStyle={{borderRadius: 20,paddingStart: 100,paddingEnd: 100}}
          containerStyle={{marginTop: 50,height: 50}}
          onPress={() => this.checkContent()}
      />
        )
      }
      else{
        return(
          <Animatable.View animation='fadeIn' style={{alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{marginTop: 30}} color='#1E88E5' />
            <Text style={{textAlign: 'center', paddingTop: 10}}>Vui lòng đợi trong giây lát</Text>
          </Animatable.View>
        )
      }
    }

    alertTimeOut = () => {
      Alert.alert(
        'OTP',
        'OTP đã hết hạn.Vui lòng thử lại',
        [
          {text: 'OK', onPress: () => {this.props.navigation.goBack()}},
        ],
        {cancelable: false},
      );
    }


  render() {
    return (
        <View style={styles.container}>
            <Image source={images.background.messaging} style={{padding: 10, width: 120, height: 120}} />

            <Text style={{padding: 10, fontSize: 16, fontWeight: 'bold'}}>Mã OTP</Text>
            <Text style={{padding: 5, textAlign: 'center', paddingStart: 20, paddingEnd: 20}}>Mã OTP đã được gửi đến số điện thoại của ban. Vui lòng nhập mã OTP để xác thực việc đăng ký</Text>
            <OTPInputView
                style={{width: '80%', height: 150}}
                pinCount={4}
                code={this.state.otp}
                onCodeChanged = {code => { this.setState({otp: code})}}
                autoFocusOnLoad
                secureTextEntry
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled = {(code => {
                  Keyboard.dismiss()
              })}
            />
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text>Mã OTP sẽ hết hạn sau: </Text>
              <CountDown
                until={this.state.countdown}
                size={16}
                onFinish={() => {}}
                digitStyle={{backgroundColor: 'transparent'}}
                digitTxtStyle={{color: '#FF9800'}}
                timeToShow={['M', 'S']}
                timeLabels={{m: '', s: ''}}
                separatorStyle={{color: '#FF9800'}}
                showSeparator
                onFinish={() => this.alertTimeOut()}
                running={this.state.running}
              />
            </View>
            {this.renderButtonLogin()}
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
