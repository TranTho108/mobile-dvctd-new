import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ScrollView, TouchableOpacity, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/YT_TC_HistoryTabStyles"
import {VACCIN} from '../Data/YT_TC_Data'
import Modal from "react-native-modal";
import axios from 'axios'

export default class YT_TC_HistoryTab extends Component {

  constructor(props) {
    super(props);

    this.state = {
        data: [],
        visibleModal: false
    }
  }

  setData = async(id) => {
    var tokenYT = await AsyncStorage.getItem('tokenYT')
    var config = {
      headers: {'Authorization': "bearer " + tokenYT}
    }
    var LICH_SU_TIEM = await axios.get(`http://api-stc.vncdc.gov.vn/lich_su_tiem/vacxin?doi_tuong_id=${id}`, 
      config
    )
    .then((res) => {
      return res.data.data
    })
    if(LICH_SU_TIEM){
      this.setState({data: LICH_SU_TIEM})
    }
  }

  componentWillMount = () => {
    this.setData(this.props.id)
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.id !== this.state.id) {
      this.setData(nextProps.id)
    }
  }

  _renderItem = ({item,index}) => {
    return(
      <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
        <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#9aca40', justifyContent: 'center', alignItems: 'center'}}><Text style={{color: '#fff'}}>{item.thu_tu_mui_tiem}</Text></View>
        <View style={{flex: 1, paddingStart: 10}}>
          <Text style={{fontSize: 14, color: 'black'}}>{item.ten_vacxin}</Text>
          <Text style={{color: '#757575'}}>{item.co_so_tiem_chung}</Text>
          <Text style={{textAlign: 'right', fontStyle: 'italic',color: '#757575', fontSize: 12, paddingTop: 5}}>{item.ngay_tiem}</Text>
        </View>
      </TouchableOpacity>
    )
  }

_renderModalContent = () => {
    if(this.state.detail){
    const {detail} = this.state
    var tt = detail.trang_thai == 2?'Đã tiêm':'Chưa tiêm'
    return(
      <View style={styles.content2}>
        <ScrollView style={{padding: 20}}>
          <Text style={styles.title5}>Thông tin mũi tiêm</Text>
          <Text style={styles.section}>Vắc xin</Text>
          <Text style={styles.label}>{detail.ten_vacxin}</Text>
          <Text style={styles.section}>Kháng nguyên</Text>
          <Text style={styles.label}>{detail.khang_nguyen}</Text>
          <Text style={styles.section}>Ngày tiêm</Text>
          <Text style={styles.label}>{detail.ngay_tiem}</Text>
          <Text style={styles.section}>Lô vắc xin</Text>
          <Text style={styles.label}>{detail.lo_vacxin}</Text>
          <Text style={styles.section}>Trạng thái</Text>
          <Text style={styles.label}>{tt}</Text>
          <Text style={styles.section}>Cơ sở tiêm chủng</Text>
          <Text style={styles.label}>{detail.co_so_tiem_chung}</Text>
          <Text style={styles.section}>Phản ứng sau tiêm</Text>
        </ScrollView>
        <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
             <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
        </TouchableOpacity>
      </View>
    )
    }
    else{
        return(
          <View style={styles.content2}>
          <ActivityIndicator size='large' color='#2196F3' />
          <Text style={{ textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
          </View>
        )
      }
  }
    
  render() {
    return (
        <View style={styles.container}>
            <FlatList 
              data={this.state.data}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{padding: 10}}
            />
            <Modal
                onBackdropPress={() => this.setState({visibleModal: false})}
                backdropTransitionOutTiming={0}
                isVisible={this.state.visibleModal}
                style={{margin: 0}}
                hideModalContentWhileAnimating={true}>
            {this._renderModalContent()}
            </Modal>
        </View>
    );
  }
}
