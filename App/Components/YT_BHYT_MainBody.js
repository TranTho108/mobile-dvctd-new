import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  WebView
} from 'react-native';
import {Text, Button, Icon} from "react-native-elements";
import styles from './Styles/YT_BHYT_MainBodyStyles'
import LinearGradient from 'react-native-linear-gradient';
import YT_BHYT_TabBar from './YT_BHYT_TabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import YT_BHYT_InfoTab from './YT_BHYT_InfoTab'
import YT_BHYT_SearchTab from './YT_BHYT_SearchTab'

export default class YT_BHYT_MainBody extends Component {

  renderLoading = () => {
    return(
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#212121' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#212121'}}>Đang lấy dữ liệu</Text>
        </View>
    )
  }
  
  render() {
   return (
    <View style={styles.container}>
      <View style={styles.linearGradient1}>
        <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
        <Text style={styles.title}>Bảo hiểm y tế</Text>
        <Icon name='home' color='transparent' containerStyle={{paddingEnd: 20}} />
      </View>
      <WebView
        source={{uri: 'https://www.baohiemxahoi.gov.vn/tracuu/pages/tra-cuu-thoi-han-su-dung-the-bhyt.aspx'}}
      />
    </View>
    );
  }
}
