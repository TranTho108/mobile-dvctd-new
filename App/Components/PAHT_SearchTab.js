import React, { Component } from 'react'
import { ScrollView, Image, View, TextInput, CheckBox, ActivityIndicator, Dimensions, ToastAndroid, FlatList, TouchableOpacity, StatusBar, ImageBackground, Keyboard} from 'react-native'
import {ButtonGroup, Icon, Button, Text} from 'react-native-elements'
import Modal from 'react-native-modal';
import {SERVER_URL, HOST} from '../Config/server'
import {fetchTest3, fetchTest4} from "../Api/Test"
 
// Styles
import styles from './Styles/PAHT_SearchTabStyles'
import * as Animatable from 'react-native-animatable';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import Toast, {DURATION} from 'react-native-easy-toast'

export default class PAHT_SearchTab extends Component {

  constructor() {
    super();
    this.state = {
      visibleModal: false,
      searchKey: '',
      data: []
    }
  }

  renderModalContent = () => {
      return(
        <View style={styles.content1}>
          <StatusBar backgroundColor="rgba(0,0,0,0.8)" barStyle= 'dark-content' translucent={true} />
          <ActivityIndicator size="large" color="black" />
          <Text style={{textAlign: 'center'}}>Đang tìm kiếm dữ liệu...</Text>
      </View>
      );
  }

  _renderItem = ({item,index}) => {
    var a = item.anhdaidien;
    var url = a?`${HOST}${a}`:'https://images2.alphacoders.com/552/552466.jpg'
    return (
    <Animatable.View delay={index*300} animation='zoomInLeft' style={styles.view}>
    <TouchableOpacity onPress = {() => {this.props.navigation.navigate('PAHT_DetailScreen', {item: item.id})}}>
          <View >
            <ImageBackground
              resizeMode='cover'
              style={{height: 150, flex: 1}}
              source={{uri: url}}>
                <View style={{position: 'absolute',bottom:0,left:0, padding: 10}}><Text style={styles.tieudeText}>{item.tieude}</Text></View>
              </ImageBackground>
            <View style={styles.view1}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name='account-circle' color='#e2e2e2'/>
                  <Text style={styles.nameText}>Người dân</Text>
                </View>
                <Text style={{fontStyle: 'italic'}}>{item.thoigiangui}</Text>
              </View>
              <Text numberOfLines={3} style={{paddingTop: 10, fontSize: 13}}>{item.noidung}</Text>
              <Text style={styles.statusText}>{item.tinhtrang}</Text>
            </View>
            <View style={styles.view3}><Icon name='bookmark' color='white' style={{}}/><Text style={{color: '#fff', padding: 5, fontSize: 13}}>Phản ánh hiện trường</Text></View>
          </View>
    </TouchableOpacity>
    </Animatable.View>
    )}


  searchFunction = async(searchKey) => {
    Keyboard.dismiss()
    this.setState({data: []})
    if(searchKey === ''){
      this.refs.toast.show('Vui lòng nhập từ khóa', ToastAndroid.LONG);
    }
    else{
      this.setState({visibleModal: true})
      var data1 = await fetchTest3(searchKey)
      var data2 = data1.length>0?JSON.parse(data1):[]
      var dataSearch = data2.data?data2.data:[]
      if(dataSearch.length > 0){
        console.log(dataSearch)
        setTimeout(() => {this.setState({visibleModal: false, data: dataSearch}) + this.refs.toast.show(`Đã tìm thấy ${dataSearch.length} phản ánh`, ToastAndroid.LONG);}, 1000)
      }
      else{
        setTimeout(() => {this.setState({visibleModal: false}) + this.refs.toast.show('Không tìm thấy nội dung phù hợp', ToastAndroid.LONG);}, 1000)
      }
    }
  }

  render () {
    const { searchKey } = this.state
    return (
      <View style={styles.mainContainer}>
      <ScrollView style={{flex: 1, padding: 10}}
        keyboardDismissMode='on-drag'
        keyboardShouldPersistTaps='always'
      >
        <View style={{height: 50, marginTop: 30}}>
          <TextInput style={{flex: 1}} placeholder='Nhập nội dung tìm kiếm...' onChangeText={(text) => {this.setState({searchKey: text})}}/>
        </View>
        <Button
          type='outline'
          buttonStyle={{height: 50}}
          containerStyle={{padding: 10}}
          onPress={() => this.searchFunction(searchKey)}
          icon={
          <Icon
            name="search"
            size={16}
            color="#2089dc"
          />
          }
          title=" Tìm kiếm"
        />
      <Modal
        style={{ margin: 0, flex: 1 }}
        backdropColor="rgba(0,0,0,0.8)"
        transparent={true}
        //backdropOpacity={0.8}
        animationIn="slideInLeft"
        animationOut="slideOutRight"
        hideModalContentWhileAnimating={true}
        backdropTransitionOutTiming={0}
        isVisible={this.state.visibleModal}
        onBackButtonPress={() => this.setState({ visibleModal: false })}
        deviceHeight={height+100}
      >
        {this.renderModalContent()}
      </Modal>
      <View style={{flex: 1, padding: 5}}>
          <FlatList
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            //ListEmptyComponent={this.showEmptyListView()}
          />
      </View>  
      </ScrollView>
      <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
      </View>
    )
  }
}
