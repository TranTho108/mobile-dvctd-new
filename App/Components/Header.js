import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/HeaderStyle"

export default class Header extends Component {
    
  render() {
    return (
        <View style={styles.view1}>
        <View style={styles.view2}>
        <Icon 
          name='keyboard-arrow-left'
          size={34}
          color='#fff'
          onPress={() =>  this.props.navigation.goBack()}
        />
        <Text style={styles.text2}>{this.props.name}</Text>
        </View>
        </View>
    );
  }
}
