import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, FlatList} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/GCTT_NMH_MainBodyStyles"
import LinearGradient from 'react-native-linear-gradient';
import images from "../Themes/Images"
import * as Animatable from 'react-native-animatable';
import {GCTT_DANHMUC} from '../Data/GCTT_Data'
import {requestGET, requestPOST} from "../Api/Basic"
import {GCTT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'
import Modal from 'react-native-modal';
import RadioForm from 'react-native-simple-radio-button';

var moment = require('moment')
moment.locale('vi')


export default class GCTT_NMH_MainBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data:[],
        loading: true,
        name: this.props.navigation.getParam('name'),
        value: '',
        radio_props: [],
        data1: [],
        Nhom: '',
        visibleModal1: false,
        visibleModal: false,
        detail: []
    }
  }

    componentWillMount = async() => {
        var IDLVMH = this.props.navigation.getParam('IDLVMH')
        var body = {'NhomLinhVucMatHangIDs': IDLVMH}
        var data1 = await requestPOST(`${GCTT_URL}/NhomMatHangs/mGetList`, body)
        var data = data1.data?data1.data:[]
        var IDNMH = []
        var radio_props = [{label: 'Tất cả', value: ''}]
        data.forEach(i => {
          IDNMH.push(i.ID)
          radio_props.push({label: i.Ten, value: i.ID})
        });
        if(IDNMH.length > 0){
          var body1 = {'NhomMatHangIDs': IDNMH.toString()}
          var data2 = await requestPOST(`${GCTT_URL}/GCTTs/mGetListMatHang`, body1)
          var data3 = data2.data?data2.data:[]
          this.setState({data: data3, data1: data3, loading: false, radio_props: radio_props, Nhom: 'Tất cả'})
        }
        else{
          this.setState({loading: false})
        }
    }

    timeUpdate = () => {
      var time = moment().format('LT')
      return(
        <Text style={{fontStyle: 'italic', textAlign: 'right', padding: 5}}>Cập nhật lúc {time}</Text>
      )
    }

    _renderItem1 = ({item,index}) => {
      var TrungBinhGia = item.TrungBinhGia?item.TrungBinhGia.toLocaleString():0
      return(
        <TouchableOpacity onPress={() => this.setState({detail: item, visibleModal1: true})} style={{flexDirection: 'row', flex: 1, margin: 10, borderBottomColor: '#e8e8e8', borderBottomWidth: 1, padding: 5,}}>
          <Text style={styles.title1}>{item.TenMatHang}</Text>
          <Text style={styles.title3}>{TrungBinhGia}</Text>
          <Text style={styles.title3}>{item.DonViTinh}</Text>
          <Text style={styles.title1}>{item.DiaDiemKhaoSat}</Text>
        </TouchableOpacity>
      )
    }
  
    setNhom = (value) => {
      if(value){
        var a = this.state.radio_props.find(x => x.value == value)
        var data = []
        this.state.data.map((i) => {
          if(i.IDNhomMatHang == value){
            data.push(i)
          }
        })
        this.setState({data1: data, Nhom: a.label,visibleModal: false})
      }
      else{
        this.setState({data1: this.state.data, Nhom: 'Tất cả',visibleModal: false})
      }
    }
  
    renderModalContent = () => {
      return(
        <View style={styles.content}>
          <RadioForm
            radio_props={this.state.radio_props}
            initial={-1}
            onPress={(value) => {this.setState({value:value})}}
            buttonSize={15}
            buttonWrapStyle={{padding: 20}}
          />
          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch', paddingTop: 20}}>
  
            <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
              <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold'}}>ĐÓNG</Text>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => {this.setNhom(Number(this.state.value))}}>
              <Text style={{fontSize: 14, color: '#66BB6A', fontWeight: '600'}}>CHỌN</Text>
            </TouchableOpacity>
  
          </View>
        </View>
      )
    }

  renderModalContent1 = () => {
      if(this.state.detail ){
      const {detail} = this.state
      var TuGia = detail.TuGia?detail.TuGia.toLocaleString():0
      var DenGia = detail.DenGia?detail.DenGia.toLocaleString():0
      var TrungBinhGia = detail.TrungBinhGia?detail.TrungBinhGia.toLocaleString():0
      return(
        <View style={styles.content2}>
          <ScrollView style={{padding: 20}}>
            <Text style={styles.title5}>Chi tiết giá mặt hàng</Text>
            <Text style={styles.section}>Tên mặt hàng</Text>
            <Text style={styles.label}>{detail.TenMatHang}</Text>
            <Text style={styles.section}>Từ giá</Text>
            <Text style={styles.label}>{TuGia}</Text>
            <Text style={styles.section}>Đến giá</Text>
            <Text style={styles.label}>{DenGia}</Text>
            <Text style={styles.section}>Trung bình giá</Text>
            <Text style={styles.label}>{TrungBinhGia}</Text>
            <Text style={styles.section}>Đơn vị tính</Text>
            <Text style={styles.label}>{detail.DonViTinh}</Text>
            <Text style={styles.section}>Địa điểm</Text>
            <Text style={styles.label}>{detail.DiaDiemKhaoSat}</Text>
            <Text style={styles.section}></Text>
            <Text style={styles.label}></Text>
          </ScrollView>
          <TouchableOpacity onPress={() => {this.setState({visibleModal1: false})}}>
               <Text style={{fontSize: 14, color: '#2089dc', fontWeight: '600', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
             </TouchableOpacity>
        </View>
    );}
    }

    renderBody = () => {
      if(!this.state.loading){
        return(
          <View style={{flex: 1}}>
            {this.state.data.length>0?
            <>
            <View style={{flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center' }}>
              <Text style={{fontStyle: 'italic'}}>Nhóm mặt hàng:</Text>
              <TouchableOpacity onPress={() => this.setState({visibleModal: true})} style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', padding: 5, paddingStart: 10, paddingEnd: 10, borderColor: '#e8e8e8', borderWidth: 1, borderRadius: 5, margin: 10}}>
                <Text>{this.state.Nhom}    </Text>
                <Icon name='angle-down' type='font-awesome' />
              </TouchableOpacity>
            </View>
              <View style={{flexDirection: 'row', margin: 10, borderBottomColor: '#e8e8e8', borderBottomWidth: 1, padding: 5}}>
                <Text style={[styles.title1, {fontWeight: 'bold'}]}>Mặt hàng</Text>
                <Text style={[styles.title3, {fontWeight: 'bold'}]}>Giá trung bình</Text>
                <Text style={[styles.title3, {fontWeight: 'bold'}]}>Đơn vị tính</Text>
                <Text style={[styles.title1, {fontWeight: 'bold'}]}>Địa điểm</Text>
              </View>
            </>
              :<></>}
            <FlatList 
              data={this.state.data1}
              renderItem={this._renderItem1}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
              ListFooterComponent={this.timeUpdate}
            />
            <Modal
              backdropTransitionOutTiming={0}
              isVisible={this.state.visibleModal}
              style={{margin: 0}}
              hideModalContentWhileAnimating={true}>
              {this.renderModalContent()}
            </Modal>
            <Modal
              backdropTransitionOutTiming={0}
              isVisible={this.state.visibleModal1}
              style={{margin: 0}}
              hideModalContentWhileAnimating={true}>
              {this.renderModalContent1()}
            </Modal>
          </View>
        )
      }
      else{
        return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2856C6' />
          <Text style={{textAlign: 'center', paddingTop: 10, color: '#2856C6'}}>Đang lấy dữ liệu</Text>
        </View>
        )
      }
    }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
            <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>{this.state.name}</Text>
          <Icon name='home' color='transparent' containerStyle={{paddingEnd: 20}} />
        </View>
        {this.renderBody()}
    </View>
    );
  }
}
