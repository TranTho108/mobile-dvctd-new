import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, FlatList, TouchableOpacity, Image, Linking, Platform, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/TĐTM_MainBodyStyles"
import images from "../Themes/Images"
import {TĐTM_Data} from "../Data/TĐTM_Data"
import {requestPOST, requestGET} from '../Api/Basic'
import {TĐTM_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'

export default class TĐTM_MainBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data:[],
        loading: true
    }
  }

  componentWillMount = async() => {
    var data1 = await requestGET(`${TĐTM_URL}/categories`)
    var data = data1.data?data1.data:[]
    this.setState({data: data, loading: false})
  }

  navigateFavorite = async() => {
    
  }

  _renderItem = ({item}) => {
      return(
          <TouchableOpacity onPress={() => this.props.navigation.navigate('TĐTM_DetailScreen', {name: item.Title, IDLV: item.ID})} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItems: 'center'}}>
              <Icon type='font-awesome' name={item.IconClass} containerStyle={{padding: 10, width: 60}} color='#616161' />
              <Text style={{fontSize: 18}}>{item.Title}</Text>
          </TouchableOpacity>
      )
  }

  renderBody = () => {
    if(!this.state.loading){
      return(
        <FlatList 
          data={this.state.data}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={EmptyFlatlist}
        /> 
      )
    }
    else{
      return(
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size='large' style={{}} color='#D84315' />
        <Text style={{textAlign: 'center', paddingTop: 10, color: '#D84315'}}>Đang lấy dữ liệu</Text>
      </View>
      )
    }
  }

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.linearGradient1}>
              <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='white' containerStyle={{paddingStart: 20}} />
              <Text style={styles.title}>Tổng đài thông minh</Text>
              <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='white' containerStyle={{paddingEnd: 20}} />
            </View>
            <TouchableOpacity onPress={() => {}} style={{flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#e8e8e8', alignItems: 'center'}}>
              <Icon type='font-awesome' name='heart' containerStyle={{padding: 10, width: 60}} color='#616161' />
              <Text style={{fontSize: 18}}>Yêu thích</Text>
            </TouchableOpacity>
            {this.renderBody()}
        </View>
    );
  }
}
