import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';
import {ButtonGroup, Icon} from 'react-native-elements'

import styles from './Styles/PAHT_MainBodyStyles'

import PAHT_TabBar from './PAHT_TabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import PAHT_HomeTab from './PAHT_HomeTab'
import PAHT_PersonalTab from './PAHT_PersonalTab'

export default class PAHT_MainBody extends Component {
  render() {
   return (
    <View style={{flex: 1}}>
      <View style={styles.linearGradient1}>
        <Icon onPress={() => this.props.navigation.openDrawer()} name='menu' color='black' containerStyle={{paddingStart: 20}}/>
        <Text style={styles.title}>Phản ánh hiện trường</Text>
        <Icon onPress={() => this.props.navigation.navigate('MainScreen')} name='home' color='black' containerStyle={{paddingEnd: 20}} />
      </View>
      <ScrollableTabView
      style={{flex: 1}}
      tabBarPosition='bottom'
      initialPage={0}
      renderTabBar={() => <PAHT_TabBar {...this.props}/>}
      >
        <View tabLabel="Trang chủ" style={styles.tabView}>
          <PAHT_HomeTab {...this.props}/>
        </View>
        <View tabLabel="Cá nhân" style={styles.tabView}>
         <PAHT_PersonalTab {...this.props}/>
        </View>
      </ScrollableTabView>
    </View>
    );
  }
}
