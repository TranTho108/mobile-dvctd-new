import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  TouchableOpacity
} from 'react-native';
import {ButtonGroup, Icon} from 'react-native-elements'
import * as Animatable from 'react-native-animatable';

import styles from './Styles/PAHT_PersonalTabStyles'
import EmptyFlatlist from './EmptyFlatlist'
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {requestPOST, requestGET} from '../Api/Basic'
import {} from '../Config/server'

export default class PAHT_PersonalTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
        dataDTL: [],
        dataCTL: [],
        loading: true
    }
  }

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem('userToken')
    var body = {'token': userToken, 'DaTraLoi': true }
    var body1 = {'token': userToken, 'DaTraLoi': false }
    var data1 = await requestPOST(`https://pakn.tandan.com.vn/_layouts/TD.WcfApi2010/ServicePAKN.svc/CongDanGetPAKN`, body)
    var data2 = data1.data?data1.data:[]
    var data3 = await requestPOST(`https://pakn.tandan.com.vn/_layouts/TD.WcfApi2010/ServicePAKN.svc/CongDanGetPAKN`, body1)
    var data4 = data3.data?data3.data:[]
    this.setState({loading: false, dataDTL: data2, dataCTL: data4})
  }

  _renderItem = ({item,index}) => {
    var a = item.AnhDaiDien;
    var url = a?`${AnhDaiDien}`:'https://images2.alphacoders.com/552/552466.jpg'
    return (
    <Animatable.View delay={index*300} animation='zoomInLeft' style={styles.view}>
    <TouchableOpacity onPress = {() => {this.props.navigation.navigate('PAHT_DetailScreen', {item: item.ID})}}>
          <View >
            <ImageBackground
              resizeMode='cover'
              style={{height: 150, flex: 1}}
              source={{uri: url}}>
                <View style={{position: 'absolute',bottom:0,left:0, padding: 10}}><Text style={styles.tieudeText}>{item.TieuDe}</Text></View>
              </ImageBackground>
            <View style={styles.view1}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name='account-circle' color='#e2e2e2'/>
                  <Text style={styles.nameText}>Người dân</Text>
                </View>
                <Text style={{fontStyle: 'italic'}}>{item.ThoiGianGui}</Text>
              </View>
              <Text numberOfLines={3} style={{paddingTop: 10, fontSize: 13}}>{item.NoiDung}</Text>
              <Text style={styles.statusText}>{item.TrangThai}</Text>
            </View>
            <View style={styles.view3}><Icon name='bookmark' color='white' style={{}}/><Text style={{color: '#fff', padding: 5, fontSize: 13}}>Phản ánh hiện trường</Text></View>
          </View>
    </TouchableOpacity>
    </Animatable.View>
    )}

  renderBody = () => {
    if(!this.state.loading){
      return(
        <ScrollableTabView
          style={{flex: 1}}
          tabBarPosition='top'
          initialPage={0}
          tabBarActiveTextColor='#FD3434'
          tabBarUnderlineStyle={{height: 1, backgroundColor: '#FD3434'}}
        >
          <View tabLabel="Đã trả lời" style={styles.tabView}>
            <FlatList
              data={this.state.dataDTL}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
              contentContainerStyle={{padding: 20}}
            />
          </View>
          <View tabLabel="Chưa trả lời" style={styles.tabView}>
            <FlatList
              data={this.state.dataCTL}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
              contentContainerStyle={{padding: 20}}
            />
          </View>
        </ScrollableTabView>
      )
    }
    else{
      return(
        <View style={{padding: 10, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size='large' style={{}} color='#2AA5FF' />
        </View>
      )
    }
  }


  render() {
   return (
    <View style={{flex: 1}}>
      {this.renderBody()}
    </View>
    );
  }
}
