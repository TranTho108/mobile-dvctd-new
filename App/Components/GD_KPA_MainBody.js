import React, { Component } from "react";
import { View, AsyncStorage, FlatList, TouchableOpacity, ScrollView} from "react-native";
import {Text, Button, Icon, Tooltip} from "react-native-elements";
import styles from "./Styles/GD_KPA_MainBodyStyles"
import {KPA_Data} from "../Data/GD_Data"
import moment from 'moment'
import 'moment/locale/vi'
moment.locale('vi')
import Modal from "react-native-modal";

export default class GD_KPA_MainBody extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
            detail: [],
            visibleModal: false
        }
      }


    _renderItem = ({item,index}) => {
        return(
          <TouchableOpacity onPress={() => this.setState({visibleModal: true, detail: item})} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between', }}>
            <View style={{height: 30,width: 30, borderRadius: 30, backgroundColor: '#2AA5FF', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='utensils' color='#fff' size={14} /></View>
            <View style={{flex: 1, paddingStart: 10}}>
              <Text style={{fontSize: 14, color: 'black'}}>Thông báo khẩu phần ăn</Text>
              <Text style={{color: '#757575'}}>{item.Name}</Text>
              <Text style={{textAlign: 'right', fontStyle: 'italic',color: '#757575', fontSize: 12, paddingTop: 5}}>{item.Time}</Text>
            </View>
          </TouchableOpacity>
        )
    }

    _renderModalContent = () => {
        if(this.state.detail && this.state.detail.Data){
        const {detail} = this.state
        return(
          <View style={styles.content2}>
            <ScrollView showsVerticalScrollIndicator={false} style={{padding: 20}}>
              <Text style={styles.title5}>Khẩu phần ăn tuần {detail.Week}</Text>
              {detail.Data.map(item1 => (
                <View>
                    <Text style={styles.section}>{item1.Day}</Text>
                    {item1.Food.map(item2 => (
                        <Text style={styles.label}>{item2}</Text>
                    ))}
                </View>
              ))}
            </ScrollView>
            <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
                 <Text style={{fontSize: 14, color: '#2089dc', fontWeight: 'bold', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
            </TouchableOpacity>
          </View>
        )
        }
        else{
            return(<></>)
        }
      }
    
  render() {
    return (
        <View style={{backgroundColor: '#fff', flex: 1,}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#2AA5FF', padding: 15, paddingTop: 35}}>
          <Icon onPress={() => this.props.navigation.goBack()} name='chevron-left' type='font-awesome' color='#fff' size={24} />
          <Text style={styles.text3}>Khẩu phần ăn</Text>
          <Icon name='arrow-left' type='font-awesome' color='transparent' size={24} />
        </View>
        <FlatList 
          data={KPA_Data}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
          contentContainerStyle={{padding: 10}}
        />
        <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
      </View>
    );
  }
}
