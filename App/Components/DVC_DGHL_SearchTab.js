import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ToastAndroid, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVC_DGHL_SearchTabStyles"
import {DVC_URL} from '../Config/server'
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import Toast, {DURATION} from 'react-native-easy-toast'

export default class DVC_DGHL_SearchTab extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          mahs : '',
          loading: false,
          data: []
        };
      }

    checkContent = () => {
      if(this.state.mahs.length == 0){
        this.refs.toast.show(`Nhập mã hồ sơ`, ToastAndroid.LONG)
      }
      else if(this.state.mahs.length < 5 || this.state.mahs.length > 26){
        this.refs.toast.show(`Mã hồ sơ phải chứa từ 5-26 ký tự`, ToastAndroid.LONG)
      }
      else{
        this.setState({loading: true})
        this.getDataHS()
      }
    }

    getDataHS = async() => {
      var data1 = await requestGET(`${DVC_URL}/GetDocInfoSatisfactionRating/${this.state.mahs}`)
      var data = data1.data?data1.data:[]
        if(data.length > 0){
          setTimeout(() => {
            this.setState({loading: false})
            this.handleChange(1,data)
          }, 1000)
        }
        else{
          setTimeout(() => {this.setState({loading: false}) + this.refs.toast.show('Mã hồ sơ không tồn tại hoặc đã được sử dụng để đánh giá!', ToastAndroid.LONG)}, 1000)
        }
    }

    handleChange(page,data){
      this.props.handleChangePage(page);
      this.props.handleChangeData(data)
    }

    renderButtonLogin = () => {
      if(!this.state.loading){
        return(
          <Button
          title='Tiếp tục'
          buttonStyle={{backgroundColor: '#fff', borderRadius: 20,paddingStart: 30,paddingEnd: 30}}
          containerStyle={{marginTop: 30,height: 50}}
          type='outline'
          onPress={() => this.checkContent()}
      />
        )
      }
      else{
        return(
          <Animatable.View animation='fadeIn' style={{alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{marginTop: 30}} color='#1E88E5' />
            <Text style={{textAlign: 'center', paddingTop: 10}}>Đang lấy dữ liệu hồ sơ</Text>
          </Animatable.View>
        )
      }
    }


  render() {
    return (
        <View style={styles.container}>
          <View style={{flexDirection: 'row', alignItems: 'center',paddingBottom: 10,}}>
            <Icon name='bookmark' size={24} containerStyle={{width: 35}} />
            <Text style={{ color: 'black', fontWeight: '600', fontSize: 15}} >Hồ sơ đánh giá </Text>
          </View>
            <TextInput placeholderTextColor='#e8e8e8' style={{color: '#424242', borderBottomWidth: 0.3, borderBottomColor: '#e8e8e8', margin: 10}} value={this.state.mahs} placeholder='Nhập mã hồ sơ' onChangeText={(text) => this.setState({mahs: text})}/>
            {this.renderButtonLogin()}
            <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='center'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}
