import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements'

import styles from './Styles/DVC_TKHS_DetailTabBarStyles'

class DVC_TKHS_DetailTabBar extends React.Component {
  icons = [];
  constructor(props) {
    super(props);
    this.icons = [];
  }

  iconColor(progress) {
    const red = 59 + (204 - 59) * progress;
    const green = 89 + (204 - 89) * progress;
    const blue = 152 + (204 - 152) * progress;
    return `rgb(${red}, ${green}, ${blue})`;
  }

  textStyle = (active,i) => {
    return {
      fontSize: 12,
      color: active === i ? '#09af00' : 'rgb(204,204,204)'
    }
  }

  render() {
    var logo = ['description', 'sync']
    return <View style={[styles.tabs, this.props.style, ]}>
      {this.props.tabs.map((tab, i) => {
        icon = logo[i]

        return <TouchableOpacity key={tab} onPress={() => this.props.goToPage(i)} style={{ flex: 1, alignItems: 'center', justifyContent: 'center',}}>
          <Icon
            name={icon}
            size={28}
            color={this.props.activeTab === i ? '#09af00' : 'rgb(204,204,204)'}
            ref={(icon) => { this.icons[i] = icon; }}
          />
          <Text style={this.textStyle(this.props.activeTab,i)}>{tab}</Text>
        </TouchableOpacity>;
      })}
    </View>;
  }
}

export default DVC_TKHS_DetailTabBar;
