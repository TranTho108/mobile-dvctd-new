import React, { Component } from "react";
import { View, AsyncStorage, StatusBar, TextInput, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/MT_ĐOM_MainBodyStyles"
import images from "../Themes/Images"
import Modal from "react-native-modal";
import * as Animatable from 'react-native-animatable';
import {requestPOST, requestGET} from '../Api/Basic'
import {MT_URL} from '../Config/server'
import EmptyFlatlist from './EmptyFlatlist'


export default class MT_ĐOM_MainBody extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          data: [],
          loading: true,
          visibleModal: false,
          detail: []
        };
      }

    componentWillMount = async() => {
      var data1 = await requestGET(`${MT_URL}/diemonhiems`)
      var data2 = data1.data?data1.data:[]
      this.setState({data: data2, loading: false})
    }

    _renderItem = ({item,index}) => {
      return(
          <TouchableOpacity onPress={() => this._displayModal(item.ID,item)} style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10, flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between'}}>
          <View style={{height: 40,width: 40, borderRadius: 40, backgroundColor: '#0271FE', justifyContent: 'center', alignItems: 'center'}}><Icon type='font-awesome' name='crosshairs' color='#fff' size={18} /></View>
          <View style={{flex: 1, paddingStart: 10}}>
              <Text style={{fontSize: 16, fontWeight: 'bold', padding: 5}}>{item.Title}</Text>
              <View style={{flexDirection: 'row', padding: 5}}><Text style={{color: '#757575', width: 100}}>Nguyên nhân</Text><Text style={{color: '#757575', fontWeight: 'bold', flex: 1}}>{item.NguyenNhan}</Text></View>
          </View>
          </TouchableOpacity>
      )
      }

    navigateToMap = () => {
      var data = this.state.data
      var dataMap = []
      data.map((item) => {
          dataMap.push({name: item.Title, address: '', location: {latitude: Number(item.LocationLatitude),longitude: Number(item.LocationLongitude)}})
      })
      this.props.navigation.navigate('MapViewScreen', {data: dataMap})
    }

    _renderItem1 = ({item,index}) => {
      return(
        <TouchableOpacity style={{marginTop: 10, backgroundColor: '#f7f7f7', padding: 10,borderRadius: 5, justifyContent: 'center'}}>
          <View style={{flex: 1, paddingStart: 10}}>
            <Text style={{fontSize: 14, color: 'black'}}>{item.Description}</Text>
            <Text numberOfLines={2} style={{color: '#757575'}}>{item.Time}</Text>
          </View>
        </TouchableOpacity>
      )
    }

  _displayModal = async(id,data) => {
      this.setState({visibleModal: true})
      var data1 = await requestGET(`${MT_URL}/lichsudiemonhiems/diemonhiem/${id}`)
      var history = data1.data?data1.data:[]
      this.setState({detail: data, isLoading: false, history: history})
  }

  _renderModalContent = () => {
      if(!this.state.isLoading && this.state.detail && this.state.history){
      const {detail} = this.state
      return(
        <View style={styles.content2}>
          <ScrollView style={{padding: 20}}>
            <Text style={styles.title5}>Chi tiết địa điểm</Text>
            <Text style={styles.section}>Tên</Text>
            <Text style={styles.label}>{detail.Title}</Text>
            <Text style={styles.section}>Nguyên nhân</Text>
            <Text style={styles.label}>{detail.NguyenNhan}</Text>
            <Text style={styles.section}>Lịch sử</Text>
            <FlatList 
              data={this.state.history}
              renderItem={this._renderItem1}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={EmptyFlatlist}
            />
            <Text style={styles.section}></Text>
            <Text style={styles.label}></Text>
          </ScrollView>
          <TouchableOpacity onPress={() => {this.setState({visibleModal: false})}}>
               <Text style={{fontSize: 14, color: '#2089dc', fontWeight: '600', padding: 5, alignSelf: 'flex-end'}}>ĐÓNG</Text>
             </TouchableOpacity>
        </View>
    );}
    else{
      return(
        <View style={styles.content2}>
          <ActivityIndicator size='large' color='#0271FE' />
          <Text style={{ textAlign: 'center'}}>Vui lòng đợi trong giây lát</Text>
        </View>
      )
    }
    }

    renderBody(){
      if(!this.state.loading){
        return(
          <View style={{flex: 1}}>
          <FlatList 
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{padding: 10}}
            ListEmptyComponent={EmptyFlatlist}
        />
        </View>
        )
      }
      else{
        return(
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large' style={{}} color='#0271FE' />
            <Text style={{textAlign: 'center', paddingTop: 10, color: '#0271FE'}}>Đang lấy dữ liệu</Text>
          </View>
        )
      }
    }

  render() {
    return (
        <View style={styles.container}>
        <View style={styles.linearGradient1}>
          <Icon onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' containerStyle={{paddingStart: 20}} />
          <Text style={styles.title}>Địa điểm ô nhiễm</Text>
          <Icon onPress={() => this.navigateToMap()} name='map' color='white' containerStyle={{paddingEnd: 20}} />
        </View>
        {this.renderBody()}
        <Modal
            onBackdropPress={() => this.setState({visibleModal: false})}
            backdropTransitionOutTiming={0}
            isVisible={this.state.visibleModal}
            style={{margin: 0}}
            hideModalContentWhileAnimating={true}>
          {this._renderModalContent()}
          </Modal>
    </View>
    );
  }
}
