import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements'

import styles from './Styles/GD_TabBarStyles'

class GD_TabBar extends React.Component {
  icons = [];
  constructor(props) {
    super(props);
    this.icons = [];
  }


  textStyle = (active,i) => {
    return {
      fontSize: 12,
      color: active === i ? '#2AA5FF' : 'rgb(204,204,204)'
    }
  }

  render() {
    var logo = ['school', 'award', 'user-friends', 'buromobelexperte']
    return <View style={[styles.tabs, this.props.style]}>
      {this.props.tabs.map((tab, i) => {
        icon = logo[i]

        return <TouchableOpacity key={i} onPress={() => this.props.goToPage(i)} style={{ flex: 1, alignItems: 'center', justifyContent: 'center',}}>
          <Icon
            name={icon}
            size={24}
            color={this.props.activeTab === i ? '#2AA5FF' : 'rgb(204,204,204)'}
            ref={(icon) => { this.icons[i] = icon; }}
            type='font-awesome'
          />
        </TouchableOpacity>;
      })}
    </View>;
  }
}

export default GD_TabBar;
