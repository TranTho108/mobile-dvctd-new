import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DVC_TCTT_MainBody from "../Components/DVC_TCTT_MainBody";

export default class DVC_TCTT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <DVC_TCTT_MainBody {...this.props}/>
        </View>
    );
  }
}
