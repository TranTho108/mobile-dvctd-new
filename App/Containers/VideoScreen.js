import React, { Component } from 'react'
import { ScrollView, Text, Image, View, StatusBar } from 'react-native'
import {Icon} from 'react-native-elements'
import Video from 'react-native-video';

export default class VideoScreen extends Component {
  constructor () {
    super()
    this.state = {
      url: ''
    }
  }
  componentWillMount = async() =>{
    if(this.props.navigation.getParam('url')){
      var url = this.props.navigation.getParam('url')
      console.log(url)
      this.setState({url: url})
    }
  }

  render () {
    return (
      <View style={{flex:1, backgroundColor: 'black',}}>
        <StatusBar backgroundColor='transparent' barStyle= 'light-content' translucent={true} />
        <Video source={{uri: 'https://www.radiantmediaplayer.com/media/bbb-360p.mp4'}}   // Can be a URL or a local file.
            ref={(ref) => {
                this.player = ref
            }}
            controls={true}   
            style={{flex: 1}}
            full={true}
            paused={false}
            onError={(err) => console.log(err)}
                   
         />
        <View style={{position: 'absolute', top: 30, left: 10, height: 30}}><Icon style={{}} size={34} onPress={() => this.props.navigation.goBack()} name='arrow-back' color='white' /></View>
      </View>
    )
  }
}
