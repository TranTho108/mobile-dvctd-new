import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import BDHC_UnitBody from "../Components/BDHC_UnitBody";

export default class BDHC_UnitScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <BDHC_UnitBody {...this.props}/>
        </View>
    );
  }
}
