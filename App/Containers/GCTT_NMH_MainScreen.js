import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GCTT_NMH_MainBody from "../Components/GCTT_NMH_MainBody";

export default class GCTT_NMH_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GCTT_NMH_MainBody {...this.props}/>
        </View>
    );
  }
}
