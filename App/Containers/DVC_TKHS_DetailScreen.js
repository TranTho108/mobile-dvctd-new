import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import Header from "../Components/Header"
import DVC_TKHS_DetailBody from "../Components/DVC_TKHS_DetailBody";

export default class DVC_TKHS_DetailScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <DVC_TKHS_DetailBody {...this.props}/>
        </View>
    );
  }
}
