import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import YT_DB_MainBody from "../Components/YT_DB_MainBody";

export default class YT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0)" barStyle="light-content" translucent={true} />
          <YT_DB_MainBody {...this.props}/>
        </View>
    );
  }
}
