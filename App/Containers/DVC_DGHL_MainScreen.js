import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DVC_DGHL_MainBody from "../Components/DVC_DGHL_MainBody";

export default class DVC_DGHLMainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <DVC_DGHL_MainBody {...this.props}/>
        </View>
    );
  }
}
