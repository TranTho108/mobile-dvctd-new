import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import ATTP_TTTT_MainBody from "../Components/ATTP_TTTT_MainBody";

export default class ATTP_TTTT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <ATTP_TTTT_MainBody {...this.props}/>
        </View>
    );
  }
}
