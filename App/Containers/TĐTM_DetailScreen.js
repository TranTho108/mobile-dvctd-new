import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import TĐTM_DetailBody from "../Components/TĐTM_DetailBody";

export default class TĐTM_DetailScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <TĐTM_DetailBody {...this.props}/>
        </View>
    );
  }
}
