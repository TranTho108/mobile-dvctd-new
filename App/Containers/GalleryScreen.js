import React, { Component } from 'react'
import { ScrollView, Text, Image, View, StatusBar, ActivityIndicator  } from 'react-native'
import { Images } from '../Themes'
import {Icon} from 'react-native-elements'
import ImageViewer from 'react-native-image-zoom-viewer';

export default class GalleryScreen extends Component {
    constructor (props) {
        super(props);
        this.state = {
            index: 0,
            images: [],
            title: ''
        };
        this.onChangeImage = this.onChangeImage.bind(this);
    }

    componentWillMount(){
        if(this.props.navigation.getParam('images')){
            console.log(this.props.navigation.getParam('images'))
            this.setState({images: this.props.navigation.getParam('images'), title: this.props.navigation.getParam('title')})
        }
    }

    onChangeImage (index) {
        this.setState({ index });
    }

    renderError () {
        return (
            <View style={{ flex: 1, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                 <Text style={{ color: 'white', fontSize: 15, fontStyle: 'italic' }}>Không load được ảnh.Vui lòng thử lại</Text>
            </View>
        );
    }

    get caption () {
        const { images, index } = this.state;
        return (
            <View style={{ bottom: 0, height: 65, backgroundColor: 'rgba(0, 0, 0, 0.7)', width: '100%', position: 'absolute', justifyContent: 'center' }}>
                <Text style={{ textAlign: 'center', color: 'white', fontSize: 15, fontStyle: 'italic' }}>{this.state.title}</Text>
            </View>
        );
    }


    render () {
        return (
            <View style={{ flex: 1}} >
                <StatusBar backgroundColor='transparent' barStyle= 'light-content' translucent={true} />
                <ImageViewer 
                    renderHeader={() => (<View style={{position: 'absolute', top: 38, left: 10, zIndex: 14}}><Icon size={30} name='arrow-back'  color= 'white' onPress={() => this.props.navigation.goBack()}/></View>)}
                    imageUrls={this.state.images}
                />
            </View>
        );
    }
}

