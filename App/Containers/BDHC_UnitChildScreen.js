import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import BDHC_UnitChildBody from "../Components/BDHC_UnitChildBody";

export default class BDHC_UnitChildScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <BDHC_UnitChildBody {...this.props}/>
        </View>
    );
  }
}
