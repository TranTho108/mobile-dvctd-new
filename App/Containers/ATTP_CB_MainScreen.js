import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import ATTP_CB_MainBody from "../Components/ATTP_CB_MainBody";

export default class ATTP_CB_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <ATTP_CB_MainBody {...this.props}/>
        </View>
    );
  }
}
