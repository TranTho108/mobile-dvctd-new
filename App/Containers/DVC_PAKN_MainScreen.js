import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DVC_PAKN_MainBody from "../Components/DVC_PAKN_MainBody";

export default class DVC_PAKN_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <DVC_PAKN_MainBody {...this.props}/>
        </View>
    );
  }
}
