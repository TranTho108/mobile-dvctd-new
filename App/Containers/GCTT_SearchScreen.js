import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GCTT_SearchBody from "../Components/GCTT_SearchBody";

export default class GCTT_SearchScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GCTT_SearchBody {...this.props}/>
        </View>
    );
  }
}
