import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import { StackActions, NavigationActions, DrawerActions } from 'react-navigation';

export default class LoadingScreen extends Component {

  componentWillMount = async() => {
    var userToken = await AsyncStorage.getItem("userToken")
    if(userToken){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'AppNavigation' })],
        });
        this.props.navigation.dispatch(resetAction)
    }
    else{
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
        });
        this.props.navigation.dispatch(resetAction)
    }
  }


  render() {
    return (
        <View>
        </View>
    );
  }
}
