import React, { Component } from "react";
import { View,TouchableOpacity, StyleSheet, PermissionsAndroid, ToastAndroid} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import MapView,{Marker, PROVIDER_GOOGLE} from "react-native-maps";
import Toast, {DURATION} from 'react-native-easy-toast'


export default class MapViewScreen extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            markers: this.props.navigation.getParam('data'),
            loading: true
        };
      }

      componentWillMount = () => {
        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      }

      componentDidMount = () => {
        navigator.geolocation.getCurrentPosition(position => {
          if (position) {
            this.setState({
              region: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05,
              },
              loading: false
            });
          }
        },
        (error) => {
          this.refs.toast.show('Không lấy được vị trí hiện tại do chưa bật GPS', ToastAndroid.LONG)
          console.log(error)
          this.setState({
            region: {
              latitude: this.state.markers[0].location.latitude,
              longitude: this.state.markers[0].location.longitude,
              latitudeDelta: 0.05,
              longitudeDelta: 0.05,
            },
            loading: false
          })
        }
        );
      }

      onMarkerPressed(marker) {
        this[marker].showCallout();
      }
    
      renderCallout(marker) {
        var distance = marker.distance?Number(marker.distance).toFixed(2):''
        return (
          <MapView.Callout tooltip>
            <View style={{padding: 10, backgroundColor: '#fff', alignItems: 'center', width: 200}}>
              <Text style={{fontWeight: 'bold', textAlign: 'center'}}>{marker.name}</Text>
              <Text style={{fontSize: 10}}>{marker.address}</Text>
              <Text style={{}}>Khoảng cách:  {distance}km</Text>
            </View>
          </MapView.Callout>
        );
      }

      _renderModalContent = () => {
        return(
          <View style={styles.content2}>
            <ActivityIndicator size="large" color="black" />
            <Text style={{textAlign: 'center'}}>Đang lấy vị trí của bạn</Text>
          </View>
        )
      }


      renderMap = () => {
        if(!this.state.loading){
          return(
            <MapView
              ref={map => this.map = map}
              showsUserLocation={true}
              showsMyLocationButton={true}
              initialRegion={this.state.region}
              style={styles.container}
                >
                {this.state.markers.map((marker, index) => {
                    return (
                    <MapView.Marker
                        coordinate={marker.location}
                        key={marker.ID}
                        title={marker.name}
                        centerOffset={{ x: 0, y: 0 }}
                        onPress={() => this.onMarkerPressed(`marker + ${index}`)}
                        ref={(map) => { this[`marker + ${index}`] = map; }}
                      >
                        {this.renderCallout(marker)}
                      </MapView.Marker>
                    );
                })}
            </MapView>
          )
        }
      }

  render() {
    return (
        <View style={styles.container}>
        {this.renderMap()}
        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{padding: 5, position: 'absolute', top: 35, left: 10, flexDirection: 'row', borderRadius: 5, borderWidth: 0.5, borderColor: '#e8e8e8', alignItems: 'center'}}>
            <Icon size={18} containerStyle={{paddingEnd: 10}} name='chevron-left' type='font-awesome' color='#424242' />
            <Text style={{color: '#424242'}}>Quay lại</Text>
          </TouchableOpacity>
          <Toast
              ref="toast"
              style={{backgroundColor:'rgba(0,0,0,0.6)'}}
              position='bottom'
              fadeInDuration={750}
              fadeOutDuration={1000}
            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
      markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
})