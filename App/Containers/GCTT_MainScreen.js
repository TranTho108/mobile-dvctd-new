import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GCTT_MainBody from "../Components/GCTT_MainBody";

export default class GCTT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content"/>
          <GCTT_MainBody {...this.props}/>
        </View>
    );
  }
}
