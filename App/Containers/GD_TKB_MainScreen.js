import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GD_TKB_MainBody from "../Components/GD_TKB_MainBody";

export default class GD_TKB_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GD_TKB_MainBody {...this.props}/>
        </View>
    );
  }
}
