import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import MT_LTGR_MainBody from "../Components/MT_LTGR_MainBody";

export default class MT_LTGR_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <MT_LTGR_MainBody {...this.props}/>
        </View>
    );
  }
}
