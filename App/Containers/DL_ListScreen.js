import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DL_ListBody from "../Components/DL_ListBody";

export default class DL_ListScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <DL_ListBody {...this.props}/>
        </View>
    );
  }
}
