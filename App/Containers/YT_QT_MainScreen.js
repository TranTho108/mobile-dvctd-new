import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import YT_QT_MainBody from "../Components/YT_QT_MainBody";

export default class YT_QT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <YT_QT_MainBody {...this.props}/>
        </View>
    );
  }
}
