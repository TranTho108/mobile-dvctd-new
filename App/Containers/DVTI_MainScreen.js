import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DVTI_MainBody from "../Components/DVTI_MainBody";

export default class DVTI_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <DVTI_MainBody {...this.props}/>
        </View>
    );
  }
}
