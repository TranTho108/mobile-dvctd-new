import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GD_DD_MainBody from "../Components/GD_DD_MainBody";

export default class GD_DD_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GD_DD_MainBody {...this.props}/>
        </View>
    );
  }
}
