import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DN_MainBody from "../Components/DN_MainBody";

export default class DN_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content"/>
          <DN_MainBody {...this.props}/>
        </View>
    );
  }
}
