import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import MT_ĐXLR_MainBody from "../Components/MT_ĐXLR_MainBody";

export default class MT_ĐXLR_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <MT_ĐXLR_MainBody {...this.props}/>
        </View>
    );
  }
}
