import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DL_DetailBody from "../Components/DL_DetailBody";

export default class DL_DetailScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <DL_DetailBody {...this.props}/>
        </View>
    );
  }
}
