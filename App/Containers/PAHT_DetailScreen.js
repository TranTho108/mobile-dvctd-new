import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import PAHT_DetailBody from "../Components/PAHT_DetailBody";

export default class PAHT_DetailScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0)" barStyle="dark-content" translucent={true} />
          <PAHT_DetailBody {...this.props}/>
        </View>
    );
  }
}
