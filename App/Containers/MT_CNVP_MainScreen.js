import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import MT_CNVP_MainBody from "../Components/MT_CNVP_MainBody";

export default class MT_CNVP_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <MT_CNVP_MainBody {...this.props}/>
        </View>
    );
  }
}
