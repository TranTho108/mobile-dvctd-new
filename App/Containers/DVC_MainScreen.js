import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DVC_MainBody from "../Components/DVC_MainBody";

export default class DVC_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <DVC_MainBody {...this.props}/>
        </View>
    );
  }
}
