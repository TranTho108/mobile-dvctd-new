import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import MT_MainBody from "../Components/MT_MainBody";

export default class MT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content"/>
          <MT_MainBody {...this.props}/>
        </View>
    );
  }
}
