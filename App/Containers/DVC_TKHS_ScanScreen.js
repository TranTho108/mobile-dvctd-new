import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DVC_TKHS_ScanBody from "../Components/DVC_TKHS_ScanBody";

export default class DVC_TKHS_ScanScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <DVC_TKHS_ScanBody {...this.props}/>
        </View>
    );
  }
}
