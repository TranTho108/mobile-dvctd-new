import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GD_TB_MainBody from "../Components/GD_TB_MainBody";

export default class GD_TB_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GD_TB_MainBody {...this.props}/>
        </View>
    );
  }
}
