import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import TTCB_DetailBody from "../Components/TTCB_DetailBody";

export default class TTCB_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="rgba(0, 0, 0,0)" barStyle="dark-content" translucent={true} />
          <TTCB_DetailBody {...this.props}/>
        </View>
    );
  }
}
