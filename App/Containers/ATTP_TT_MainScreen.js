import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import ATTP_TT_MainBody from "../Components/ATTP_TT_MainBody";

export default class ATTP_TT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <ATTP_TT_MainBody {...this.props}/>
        </View>
    );
  }
}
