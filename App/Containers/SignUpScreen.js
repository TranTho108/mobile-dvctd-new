import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import Header from "../Components/Header"
import SignUpBody from "../Components/SignUpBody"

export default class SignUpScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0)" barStyle="dark-content" translucent={true} />
          <SignUpBody {...this.props}/>
        </View>
    );
  }
}
