import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import ATTP_MainBody from "../Components/ATTP_MainBody";

export default class ATTP_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content"/>
          <ATTP_MainBody {...this.props}/>
        </View>
    );
  }
}
