import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import InfoBody from "../Components/InfoBody";

export default class InfoScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <InfoBody {...this.props}/>
        </View>
    );
  }
}
