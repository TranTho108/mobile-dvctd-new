import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import TTCB_MainBody from "../Components/TTCB_MainBody";

export default class TTCB_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="rgba(0, 0, 0,0)" barStyle="dark-content" translucent={true} />
          <TTCB_MainBody {...this.props}/>
        </View>
    );
  }
}
