import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import TTCB_ListBody from "../Components/TTCB_ListBody";

export default class TĐTM_DetailScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <TTCB_ListBody {...this.props}/>
        </View>
    );
  }
}
