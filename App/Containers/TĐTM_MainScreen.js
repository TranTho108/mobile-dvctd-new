import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import TĐTM_MainBody from "../Components/TĐTM_MainBody";

export default class TĐTM_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content"/>
          <TĐTM_MainBody {...this.props}/>
        </View>
    );
  }
}
