import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import Head from "../Components/Header"
import DVC_TKHS_MainBody from "../Components/DVC_TKHS_MainBody";

export default class DVC_TKHS_MainScreen extends Component {
  
  render() {
    return (
        <View style={styles.container}>
          <DVC_TKHS_MainBody  {...this.props} />
        </View>
    );
  }
}
