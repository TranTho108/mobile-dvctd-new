import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DGN_DetailBody from "../Components/DGN_DetailBody";

export default class DGN_DetailScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <DGN_DetailBody {...this.props}/>
        </View>
    );
  }
}
