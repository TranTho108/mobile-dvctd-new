import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GCTT_MH_MainBody from "../Components/GCTT_MH_MainBody";

export default class GCTT_MH_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GCTT_MH_MainBody {...this.props}/>
        </View>
    );
  }
}
