import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import MT_ĐOM_MainBody from "../Components/MT_ĐOM_MainBody";

export default class MT_ĐOM_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <MT_ĐOM_MainBody {...this.props}/>
        </View>
    );
  }
}
