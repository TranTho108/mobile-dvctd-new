import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import Header from "../Components/Header"
import DVC_TKHS_SearchBody from "../Components/DVC_TKHS_SearchBody"

export default class DVC_TKHS_SearchScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <DVC_TKHS_SearchBody {...this.props}/>
        </View>
    );
  }
}
