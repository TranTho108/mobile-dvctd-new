import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import MainBody1 from "../Components/MainBody1";

export default class MainScreen extends Component {
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0)" barStyle="light-content" translucent={true} />
          <MainBody1 {...this.props}/>
        </View>
    );
  }
}
