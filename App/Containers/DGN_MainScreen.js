import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import DGN_MainBody from "../Components/DGN_MainBody";

export default class DGN_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <DGN_MainBody {...this.props}/>
        </View>
    );
  }
}
