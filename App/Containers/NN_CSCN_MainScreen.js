import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import NN_CSCN_MainBody from "../Components/NN_CSCN_MainBody";

export default class NN_CSCN_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <NN_CSCN_MainBody {...this.props}/>
        </View>
    );
  }
}
