import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import GD_KPA_MainBody from "../Components/GD_KPA_MainBody";

export default class GD_KPA_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <GD_KPA_MainBody {...this.props}/>
        </View>
    );
  }
}
