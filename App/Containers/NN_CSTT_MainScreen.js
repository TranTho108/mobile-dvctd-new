import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import NN_CSTT_MainBody from "../Components/NN_CSTT_MainBody";

export default class NN_CSTT_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <NN_CSTT_MainBody {...this.props}/>
        </View>
    );
  }
}
