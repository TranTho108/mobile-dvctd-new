import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import NN_MainBody from "../Components/NN_MainBody";

export default class NN_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content"/>
          <NN_MainBody {...this.props}/>
        </View>
    );
  }
}
