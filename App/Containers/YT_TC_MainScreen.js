import React, { Component } from "react";
import { View, AsyncStorage, StatusBar} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/DVCMainScreenStyles"
import YT_TC_MainBody from "../Components/YT_TC_MainBody";

export default class YT_TC_MainScreen extends Component {
    
  render() {
    return (
        <View style={styles.container}>
          <YT_TC_MainBody {...this.props}/>
        </View>
    );
  }
}
