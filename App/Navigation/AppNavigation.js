import React from "react";
import { Dimensions, View, TouchableOpacity, Image } from 'react-native'
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import { Icon, Text } from "react-native-elements"
import DVC_TKHS_MainScreen from "../Containers/DVC_TKHS_MainScreen"
import DVC_TKHS_SearchScreen from "../Containers/DVC_TKHS_SearchScreen"
import DVC_TKHS_ScanScreen from "../Containers/DVC_TKHS_ScanScreen"
import DVC_TKHS_DetailScreen from "../Containers/DVC_TKHS_DetailScreen"
import DVC_PAKN_MainScreen from "../Containers/DVC_PAKN_MainScreen";
import DVC_DGHL_MainScreen from "../Containers/DVC_DGHL_MainScreen";
import DVC_MainScreen from "../Containers/DVC_MainScreen";
import PAHT_MainScreen from "../Containers/PAHT_MainScreen";
import LoginScreen from "../Containers/LoginScreen";
import SignUpScreen from "../Containers/SignUpScreen";
import MainScreen from "../Containers/MainScreen";
import PAHT_RequestScreen from "../Containers/PAHT_RequestScreen";
import PAHT_DetailScreen from "../Containers/PAHT_DetailScreen";
import VideoScreen from "../Containers/VideoScreen";
import GalleryScreen from "../Containers/GalleryScreen";
import SideMenu from '../Components/SideMenu'
import LoadingScreen from '../Containers/LoadingScreen'
import TTCB_MainScreen from '../Containers/TTCB_MainScreen'
import DVC_TCTT_MainScreen from '../Containers/DVC_TCTT_MainScreen'
import DL_MainScreen from '../Containers/DL_MainScreen'
import DL_ListScreen from '../Containers/DL_ListScreen'
import DL_DetailScreen from '../Containers/DL_DetailScreen'
import TĐTM_MainScreen from '../Containers/TĐTM_MainScreen'
import DN_MainScreen from '../Containers/DN_MainScreen'
import DVTI_MainScreen from '../Containers/DVTI_MainScreen'
import TĐTM_DetailScreen from '../Containers/TĐTM_DetailScreen'
import GD_MainScreen from '../Containers/GD_MainScreen'
import GD_TKB_MainScreen from '../Containers/GD_TKB_MainScreen'
import GD_DD_MainScreen from '../Containers/GD_DD_MainScreen'
import GD_TB_MainScreen from '../Containers/GD_TB_MainScreen'
import YT_MainScreen from '../Containers/YT_MainScreen'
import YT_DB_MainScreen from '../Containers/YT_DB_MainScreen'
import YT_BHYT_MainScreen from '../Containers/YT_BHYT_MainScreen'
import YT_TC_MainScreen from '../Containers/YT_TC_MainScreen'
import YT_QT_MainScreen from '../Containers/YT_QT_MainScreen'
import InfoScreen from '../Containers/InfoScreen'
import YT_TYT_MainScreen from '../Containers/YT_TYT_MainScreen'
import GCTT_MainScreen from '../Containers/GCTT_MainScreen'
import GCTT_SearchScreen from '../Containers/GCTT_SearchScreen'
import MT_MainScreen from '../Containers/MT_MainScreen'
import ATTP_MainScreen from '../Containers/ATTP_MainScreen'
import NN_MainScreen from '../Containers/NN_MainScreen'
import NN_LTLG_MainScreen from '../Containers/NN_LTLG_MainScreen'
import MapViewScreen from '../Containers/MapViewScreen'
import NN_CSCN_MainScreen from '../Containers/NN_CSCN_MainScreen'
import NN_CSTT_MainScreen from '../Containers/NN_CSTT_MainScreen'
import GD_KPA_MainScreen from '../Containers/GD_KPA_MainScreen'
import MT_LTGR_MainScreen from '../Containers/MT_LTGR_MainScreen'
import MT_CNVP_MainScreen from '../Containers/MT_CNVP_MainScreen'
import MT_ĐXLR_MainScreen from '../Containers/MT_ĐXLR_MainScreen'
import MT_ĐOM_MainScreen from '../Containers/MT_ĐOM_MainScreen'
import GCTT_NMH_MainScreen from '../Containers/GCTT_NMH_MainScreen'
import GCTT_MH_MainScreen from '../Containers/GCTT_MH_MainScreen'
import TTCB_ListScreen from '../Containers/TTCB_ListScreen'
import ATTP_CSKD_MainScreen from '../Containers/ATTP_CSKD_MainScreen'
import ATTP_TTTT_MainScreen from '../Containers/ATTP_TTTT_MainScreen'
import ATTP_TT_MainScreen from '../Containers/ATTP_TT_MainScreen'
import ATTP_CB_MainScreen from '../Containers/ATTP_CB_MainScreen'
import BDHC_UnitScreen from '../Containers/BDHC_UnitScreen'
import BDHC_UnitChildScreen from '../Containers/BDHC_UnitChildScreen'

const {height,width} = Dimensions.get('window');

const AppNavigator = createStackNavigator({
    DVC_TKHS_MainScreen: { screen: DVC_TKHS_MainScreen},
    DVC_TKHS_SearchScreen: { screen: DVC_TKHS_SearchScreen},
    DVC_TKHS_ScanScreen: { screen: DVC_TKHS_ScanScreen},
    DVC_TKHS_DetailScreen: { screen: DVC_TKHS_DetailScreen},
    MainScreen: { screen: MainScreen},
    DVC_PAKN_MainScreen: { screen : DVC_PAKN_MainScreen},
    DVC_DGHL_MainScreen: { screen: DVC_DGHL_MainScreen},
    DVC_MainScreen: { screen: DVC_MainScreen},
    PAHT_MainScreen: { screen: PAHT_MainScreen},
    PAHT_RequestScreen: { screen: PAHT_RequestScreen},
    PAHT_DetailScreen:{ screen: PAHT_DetailScreen},
    GalleryScreen: {screen: GalleryScreen},
    VideoScreen: {screen: VideoScreen},
    TTCB_MainScreen: { screen: TTCB_MainScreen},
    DVC_TCTT_MainScreen: { screen: DVC_TCTT_MainScreen},
    DL_MainScreen: { screen: DL_MainScreen},
    TĐTM_MainScreen: {screen: TĐTM_MainScreen},
    DN_MainScreen: {screen: DN_MainScreen},
    DVTI_MainScreen: {screen: DVTI_MainScreen},
    TĐTM_DetailScreen: { screen: TĐTM_DetailScreen},
    GD_MainScreen: {screen: GD_MainScreen},
    GD_TKB_MainScreen: {screen: GD_TKB_MainScreen},
    GD_DD_MainScreen: {screen: GD_DD_MainScreen},
    GD_TB_MainScreen: {screen: GD_TB_MainScreen},
    YT_MainScreen: {screen: YT_MainScreen},
    YT_DB_MainScreen: {screen: YT_DB_MainScreen},
    YT_BHYT_MainScreen: {screen: YT_BHYT_MainScreen},
    YT_TC_MainScreen: {screen: YT_TC_MainScreen},
    YT_QT_MainScreen: {screen: YT_QT_MainScreen},
    InfoScreen: {screen: InfoScreen},
    YT_TYT_MainScreen: {screen: YT_TYT_MainScreen},
    GCTT_MainScreen: {screen: GCTT_MainScreen},
    GCTT_SearchScreen: {screen: GCTT_SearchScreen},
    MT_MainScreen: {screen: MT_MainScreen},
    ATTP_MainScreen: {screen: ATTP_MainScreen},
    NN_MainScreen: {screen: NN_MainScreen},
    NN_LTLG_MainScreen: {screen: NN_LTLG_MainScreen},
    MapViewScreen: {screen: MapViewScreen},
    NN_CSCN_MainScreen: {screen: NN_CSCN_MainScreen},
    NN_CSTT_MainScreen: {screen: NN_CSTT_MainScreen},
    GD_KPA_MainScreen: {screen: GD_KPA_MainScreen},
    MT_LTGR_MainScreen: {screen: MT_LTGR_MainScreen},
    MT_ĐXLR_MainScreen:{screen: MT_ĐXLR_MainScreen},
    MT_ĐOM_MainScreen: {screen: MT_ĐOM_MainScreen},
    MT_CNVP_MainScreen: {screen: MT_CNVP_MainScreen},
    GCTT_NMH_MainScreen: {screen: GCTT_NMH_MainScreen},
    GCTT_MH_MainScreen: {screen: GCTT_MH_MainScreen},
    TTCB_ListScreen: {screen: TTCB_ListScreen},
    ATTP_CSKD_MainScreen: {screen: ATTP_CSKD_MainScreen},
    ATTP_TTTT_MainScreen: {screen: ATTP_TTTT_MainScreen},
    ATTP_TT_MainScreen: {screen: ATTP_TT_MainScreen},
    DL_ListScreen: {screen: DL_ListScreen},
    ATTP_CB_MainScreen: {screen: ATTP_CB_MainScreen},
    DL_DetailScreen: {screen: DL_DetailScreen},
    BDHC_UnitScreen: {screen: BDHC_UnitScreen},
    BDHC_UnitChildScreen: {screen: BDHC_UnitChildScreen}
},
{
    headerMode: 'none',
    initialRouteName: "MainScreen",
}
);
const AppNavigation = createDrawerNavigator({
    AppNavigator: {screen: AppNavigator},

},
{
    drawerLockMode: '',
    contentComponent: props => <SideMenu {...props} />,
    drawerWidth: width*0.8,
}
)
const AppDrawer = createStackNavigator({
    AppNavigation: {screen: AppNavigation},
    LoginScreen: { screen: LoginScreen},
    SignUpScreen: { screen: SignUpScreen},
    LoadingScreen: { screen: LoadingScreen}
},
{
    headerMode: 'none',
    initialRouteName: 'LoadingScreen'
},
)
const AppContainer = createAppContainer(AppDrawer);


export default AppContainer;
