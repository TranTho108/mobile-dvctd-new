const GCTT_DANHMUC = [
    {
        id: 0,
        name: 'Tìm kiếm',
        icon: require("../Images/loupe.png"),
    },
    {
        id: 1,
        name: 'Nông sản',
        icon: require("../Images/vegetables.png"),
    },
    {
        id: 2,
        name: 'Thuỷ sản',
        icon: require("../Images/shrimp.png"),
    },
    {
        id: 3,
        name: 'Xăng dầu',
        icon: require("../Images/oil.png"),
    },
    {
        id: 4,
        name: 'Giá điện',
        icon: require("../Images/idea.png"),
    },
    {
        id: 5,
        name: 'Giá nước',
        icon: require("../Images/drop.png"),
    },
    {
        id: 6,
        name: 'Giá gas',
        icon: require("../Images/gas.png"),
    },
    {
        id: 7,
        name: 'Giá vàng',
        icon: require("../Images/gold.png"),
    },
    {
        id: 8,
        name: 'Ngoại tệ',
        icon: require("../Images/money.png"),
    },
    {
        id: 9,
        name: 'Khác',
        icon: require("../Images/box.png"),
    },
]

export {
    GCTT_DANHMUC
}