const THANH_VIEN = [
    {
        "doi_tuong_id": 14768502,
        "ma_doi_tuong": "101081319870138",
        "ho_ten": "ĐINH THỊ KIM CHUNG",
        "ngay_sinh": "08/11/1987",
        "gioi_tinh": 1,
        "dan_toc": "Kinh",
        "dien_thoai": "0976811087",
        "co_so_id": 713,
        "ten_co_so": "TYT Phường Định Công",
        "ho_khau_tinh_id": 101,
        "ho_khau_huyen_id": 10108,
        "ho_khau_xa_id": 1010813,
        "ho_khau_thon_ap_id": null,
        "ho_khau_tinh": "Hà Nội",
        "ho_khau_huyen": "Hoàng Mai",
        "ho_khau_xa": "Định Công",
        "ho_khau_thon_ap": null,
        "ho_khau_dia_chi": "Số 12 Hẻm 7 Ngách 385 Ngõ 192 Lê Trọng Tấn",
        "tam_tru_tinh_id": 101,
        "tam_tru_huyen_id": 10108,
        "tam_tru_xa_id": 1010813,
        "tam_tru_thon_ap_id": null,
        "tam_tru_tinh": "Hà Nội",
        "tam_tru_huyen": "Hoàng Mai",
        "tam_tru_xa": "Định Công",
        "tam_tru_thon_ap": null,
        "tam_tru_dia_chi": "Số 12 Hẻm 7 Ngách 385 Ngõ 192 Lê Trọng Tấn",
        "theo_doi": 1,
        "hien_thi_mac_dinh": "0",
        "avatar": ""
    },
    {
        "doi_tuong_id": 14768717,
        "ma_doi_tuong": "101081320130624",
        "ho_ten": "NGUYỄN HỮU TRÍ DŨNG",
        "ngay_sinh": "22/06/2013",
        "gioi_tinh": 0,
        "dan_toc": "Kinh",
        "dien_thoai": null,
        "co_so_id": 713,
        "ten_co_so": "TYT Phường Định Công",
        "ho_khau_tinh_id": 101,
        "ho_khau_huyen_id": 10108,
        "ho_khau_xa_id": 1010813,
        "ho_khau_thon_ap_id": null,
        "ho_khau_tinh": "Hà Nội",
        "ho_khau_huyen": "Hoàng Mai",
        "ho_khau_xa": "Định Công",
        "ho_khau_thon_ap": null,
        "ho_khau_dia_chi": "Số 12 Hẻm 7 Ngách 385 Ngõ 192 Lê Trọng Tấn",
        "tam_tru_tinh_id": 101,
        "tam_tru_huyen_id": 10108,
        "tam_tru_xa_id": 1010813,
        "tam_tru_thon_ap_id": null,
        "tam_tru_tinh": "Hà Nội",
        "tam_tru_huyen": "Hoàng Mai",
        "tam_tru_xa": "Định Công",
        "tam_tru_thon_ap": null,
        "tam_tru_dia_chi": "Số 12 Hẻm 7 Ngách 385 Ngõ 192 Lê Trọng Tấn",
        "theo_doi": 1,
        "hien_thi_mac_dinh": "1",
        "avatar": ""
    },
    {
        "doi_tuong_id": 17388773,
        "ma_doi_tuong": "101081320190301",
        "ho_ten": "Nguyễn Hữu An Nhiên",
        "ngay_sinh": "09/03/2019",
        "gioi_tinh": 1,
        "dan_toc": "Kinh",
        "dien_thoai": "0976811087",
        "co_so_id": 713,
        "ten_co_so": "TYT Phường Định Công",
        "ho_khau_tinh_id": 101,
        "ho_khau_huyen_id": 10108,
        "ho_khau_xa_id": 1010813,
        "ho_khau_thon_ap_id": null,
        "ho_khau_tinh": "Hà Nội",
        "ho_khau_huyen": "Hoàng Mai",
        "ho_khau_xa": "Định Công",
        "ho_khau_thon_ap": null,
        "ho_khau_dia_chi": "Số 12 /7 Ngách 385 Ngõ 192 Lê Trọng Tấn, Phường Định Công, Quận Hoàng Mai, Tp. Hà Nội",
        "tam_tru_tinh_id": 101,
        "tam_tru_huyen_id": 10108,
        "tam_tru_xa_id": 1010813,
        "tam_tru_thon_ap_id": null,
        "tam_tru_tinh": "Hà Nội",
        "tam_tru_huyen": "Hoàng Mai",
        "tam_tru_xa": "Định Công",
        "tam_tru_thon_ap": null,
        "tam_tru_dia_chi": "Số 12 /7 Ngách 385 Ngõ 192 Lê Trọng Tấn, Phường Định Công, Quận Hoàng Mai, Tp. Hà Nội",
        "theo_doi": 1,
        "hien_thi_mac_dinh": "0",
        "avatar": ""
    }
]

const KHANG_NGUYEN = [
    {
        "lich_su_tiem_id": 108363913,
        "khang_nguyen_id": 16,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Rota",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 109889272,
        "khang_nguyen_id": 20,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm tai giữa cấp tính",
        "trang_thai": 2,
        "ngay_tiem": "16:08 23/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 109889272,
        "khang_nguyen_id": 21,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Phế cầu xâm nhập",
        "trang_thai": 2,
        "ngay_tiem": "16:08 23/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 123349391,
        "khang_nguyen_id": 17,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Cúm",
        "trang_thai": 2,
        "ngay_tiem": "16:12 15/09/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 131758224,
        "khang_nguyen_id": 18,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Não mô cầu",
        "trang_thai": 2,
        "ngay_tiem": "14:53 23/11/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 111893836,
        "khang_nguyen_id": 16,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Rota",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 113319020,
        "khang_nguyen_id": 20,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm tai giữa cấp tính",
        "trang_thai": 2,
        "ngay_tiem": "10:19 23/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 113319020,
        "khang_nguyen_id": 21,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Phế cầu xâm nhập",
        "trang_thai": 2,
        "ngay_tiem": "10:19 23/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 128987545,
        "khang_nguyen_id": 17,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Cúm",
        "trang_thai": 2,
        "ngay_tiem": "11:31 03/11/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 118741823,
        "khang_nguyen_id": 16,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Rota",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 121131990,
        "khang_nguyen_id": 20,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm tai giữa cấp tính",
        "trang_thai": 2,
        "ngay_tiem": "11:21 24/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 121131990,
        "khang_nguyen_id": 21,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Phế cầu xâm nhập",
        "trang_thai": 2,
        "ngay_tiem": "11:21 24/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 0
    },
    {
        "lich_su_tiem_id": 107272566,
        "khang_nguyen_id": 270,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Lao",
        "trang_thai": 2,
        "ngay_tiem": "09:30 14/03/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 1
    },
    {
        "lich_su_tiem_id": 108363935,
        "khang_nguyen_id": 3,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm gan B",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 2
    },
    {
        "lich_su_tiem_id": 111893835,
        "khang_nguyen_id": 3,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm gan B",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 2
    },
    {
        "lich_su_tiem_id": 118741788,
        "khang_nguyen_id": 3,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm gan B",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 2
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 3,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm gan B",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 4,
        "thu_tu_hien_thi": 2
    },
    {
        "lich_su_tiem_id": 108363935,
        "khang_nguyen_id": 2,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bại liệt",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 3
    },
    {
        "lich_su_tiem_id": 111893835,
        "khang_nguyen_id": 2,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bại liệt",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 3
    },
    {
        "lich_su_tiem_id": 118741788,
        "khang_nguyen_id": 2,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bại liệt",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 3
    },
    {
        "lich_su_tiem_id": 108363935,
        "khang_nguyen_id": 5,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bạch Hầu",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 4
    },
    {
        "lich_su_tiem_id": 111893835,
        "khang_nguyen_id": 5,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bạch Hầu",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 4
    },
    {
        "lich_su_tiem_id": 118741788,
        "khang_nguyen_id": 5,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bạch Hầu",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 4
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 5,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Bạch Hầu",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 4,
        "thu_tu_hien_thi": 4
    },
    {
        "lich_su_tiem_id": 108363935,
        "khang_nguyen_id": 290,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Ho gà",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 5
    },
    {
        "lich_su_tiem_id": 111893835,
        "khang_nguyen_id": 290,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Ho gà",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 5
    },
    {
        "lich_su_tiem_id": 118741788,
        "khang_nguyen_id": 290,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Ho gà",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 5
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 290,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Ho gà",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 4,
        "thu_tu_hien_thi": 5
    },
    {
        "lich_su_tiem_id": 108363935,
        "khang_nguyen_id": 291,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Uốn ván",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 6
    },
    {
        "lich_su_tiem_id": 111893835,
        "khang_nguyen_id": 291,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Uốn ván",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 6
    },
    {
        "lich_su_tiem_id": 118741788,
        "khang_nguyen_id": 291,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Uốn ván",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 6
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 291,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Uốn ván",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 4,
        "thu_tu_hien_thi": 6
    },
    {
        "lich_su_tiem_id": 108363935,
        "khang_nguyen_id": 172,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Hib",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 7
    },
    {
        "lich_su_tiem_id": 111893835,
        "khang_nguyen_id": 172,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Hib",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 7
    },
    {
        "lich_su_tiem_id": 118741788,
        "khang_nguyen_id": 172,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Hib",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 7
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 8,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Sởi",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 8
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 8,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Sởi",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 8
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 9,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm não Nhật Bản",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 9
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 9,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm não Nhật Bản",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 9
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 9,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Viêm não Nhật Bản",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 3,
        "thu_tu_hien_thi": 9
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 13,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Thương Hàn",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 10
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 6,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Tả",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 11
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 176,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Rubella",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 1,
        "thu_tu_hien_thi": 11
    },
    {
        "lich_su_tiem_id": null,
        "khang_nguyen_id": 6,
        "doi_tuong_id": 17388773,
        "ten_khang_nguyen": "Tả",
        "trang_thai": 1,
        "ngay_tiem": null,
        "thu_tu_mui_tiem": 2,
        "thu_tu_hien_thi": 11
    }
]

const VACCIN = [
    {
        "lich_su_tiem_id": 107272566,
        "doi_tuong_id": 17388773,
        "vacxin_id": 2,
        "ten_vacxin": "BCG",
        "khang_nguyen": "Lao",
        "trang_thai": 2,
        "ngay_tiem": "09:30 14/03/2019",
        "thu_tu_mui_tiem": 1,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 108363913,
        "doi_tuong_id": 17388773,
        "vacxin_id": 18,
        "ten_vacxin": "Rota Teq",
        "khang_nguyen": "Rota",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "co_so_tiem_chung": "Viện đào tạo Y học dự phòng và Y tế công cộng - Trường ĐH Y Hà Nội",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": "R021817",
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 108363935,
        "doi_tuong_id": 17388773,
        "vacxin_id": 21,
        "ten_vacxin": "Infanrix Hexa",
        "khang_nguyen": "Bạch Hầu, Bại liệt, Hib, Ho gà, Uốn ván, Viêm gan B",
        "trang_thai": 2,
        "ngay_tiem": "15:15 09/05/2019",
        "thu_tu_mui_tiem": 1,
        "co_so_tiem_chung": "Viện đào tạo Y học dự phòng và Y tế công cộng - Trường ĐH Y Hà Nội",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": "A21CD510A",
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 109889272,
        "doi_tuong_id": 17388773,
        "vacxin_id": 20,
        "ten_vacxin": "Synflorix",
        "khang_nguyen": "Viêm tai giữa, phế cầu khuẩn",
        "trang_thai": 2,
        "ngay_tiem": "16:08 23/05/2019",
        "thu_tu_mui_tiem": 1,
        "co_so_tiem_chung": "Viện đào tạo Y học dự phòng và Y tế công cộng - Trường ĐH Y Hà Nội",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": "ASPNB202CC",
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 111893836,
        "doi_tuong_id": 17388773,
        "vacxin_id": 18,
        "ten_vacxin": "Rota Teq",
        "khang_nguyen": "Rota",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 111893835,
        "doi_tuong_id": 17388773,
        "vacxin_id": 21,
        "ten_vacxin": "Infanrix Hexa",
        "khang_nguyen": "Bạch Hầu, Bại liệt, Hib, Ho gà, Uốn ván, Viêm gan B",
        "trang_thai": 2,
        "ngay_tiem": "11:01 10/06/2019",
        "thu_tu_mui_tiem": 2,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 113319020,
        "doi_tuong_id": 17388773,
        "vacxin_id": 20,
        "ten_vacxin": "Synflorix",
        "khang_nguyen": "Viêm tai giữa, phế cầu khuẩn",
        "trang_thai": 2,
        "ngay_tiem": "10:19 23/06/2019",
        "thu_tu_mui_tiem": 2,
        "co_so_tiem_chung": "Viện đào tạo Y học dự phòng và Y tế công cộng - Trường ĐH Y Hà Nội",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": "ASPNB202CC",
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 118741823,
        "doi_tuong_id": 17388773,
        "vacxin_id": 18,
        "ten_vacxin": "Rota Teq",
        "khang_nguyen": "Rota",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "co_so_tiem_chung": "Viện đào tạo Y học dự phòng và Y tế công cộng - Trường ĐH Y Hà Nội",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": "S012242",
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 118741788,
        "doi_tuong_id": 17388773,
        "vacxin_id": 21,
        "ten_vacxin": "Infanrix Hexa",
        "khang_nguyen": "Bạch Hầu, Bại liệt, Hib, Ho gà, Uốn ván, Viêm gan B",
        "trang_thai": 2,
        "ngay_tiem": "11:34 05/08/2019",
        "thu_tu_mui_tiem": 3,
        "co_so_tiem_chung": "Viện đào tạo Y học dự phòng và Y tế công cộng - Trường ĐH Y Hà Nội",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": "A21CD426A",
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 121131990,
        "doi_tuong_id": 17388773,
        "vacxin_id": 20,
        "ten_vacxin": "Synflorix",
        "khang_nguyen": "Viêm tai giữa, phế cầu khuẩn",
        "trang_thai": 2,
        "ngay_tiem": "11:21 24/08/2019",
        "thu_tu_mui_tiem": 3,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 123349391,
        "doi_tuong_id": 17388773,
        "vacxin_id": 312,
        "ten_vacxin": "VAXIGRIP 0.5ml ",
        "khang_nguyen": "Cúm",
        "trang_thai": 2,
        "ngay_tiem": "16:12 15/09/2019",
        "thu_tu_mui_tiem": 1,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 128987545,
        "doi_tuong_id": 17388773,
        "vacxin_id": 312,
        "ten_vacxin": "VAXIGRIP 0.5ml ",
        "khang_nguyen": "Cúm",
        "trang_thai": 2,
        "ngay_tiem": "11:31 03/11/2019",
        "thu_tu_mui_tiem": 2,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    },
    {
        "lich_su_tiem_id": 131758224,
        "doi_tuong_id": 17388773,
        "vacxin_id": 24,
        "ten_vacxin": "VA - MENGOC - BC",
        "khang_nguyen": "Não mô cầu",
        "trang_thai": 2,
        "ngay_tiem": "14:53 23/11/2019",
        "thu_tu_mui_tiem": 1,
        "co_so_tiem_chung": "Trung tâm tiêm chủng VNVC 180 Trường Chinh",
        "seo": null,
        "truoc_24h": null,
        "lo_vacxin": null,
        "phan_ung_sau_tiem": {
            "ngay_phan_ung": null,
            "loai_phan_ung": null,
            "ket_qua": null
        }
    }
]

const PHAC_DO = [
    {
        "phac_do_id": 2,
        "doi_tuong_id": null,
        "khang_nguyen_id": 3,
        "ten_khang_nguyen": "Viêm gan B",
        "thu_tu": 1,
        "tong_so_mui": 4,
        "mo_ta": "Viêm gan B là một bệnh gan nghiêm trọng gây ra bởi virus viêm gan B (HBV). Đối với một số người, bệnh viêm gan B lây nhiễm trở thành mãn tính, dẫn đến suy gan, ung thư gan, hoặc xơ gan - một tình trạng gây ra sẹo vĩnh viễn ở gan.",
        "thang_tiem": 0,
    },
    {
        "phac_do_id": 14,
        "doi_tuong_id": null,
        "khang_nguyen_id": 172,
        "ten_khang_nguyen": "Hib",
        "thu_tu": 2,
        "tong_so_mui": 3,
        "mo_ta": "Các bệnh lý thường gặp là viêm màng não mũ, viêm phổi, viêm nắp thanh quản, nhiễm trùng máu... Viêm màng não mủ do vi khuẩn Hib thương gây tử vong cao hoặc dẫn đến các di chứng thần kinh không hồi phục. Vi khuẩn thường gây ra bệnh cho các trẻ em dưới 5 tuổi, trong đó nhóm trẻ em dưới 1 tuổi là đối tượng dễ mắc bệnh nhất.",
        "thang_tiem": 3
    },
    {
        "phac_do_id": 4,
        "doi_tuong_id": null,
        "khang_nguyen_id": 5,
        "ten_khang_nguyen": "Bạch Hầu",
        "thu_tu": 1,
        "tong_so_mui": 4,
        "mo_ta": "Bạch hầu là bệnh do vi khuẩn gây ra và thường lây qua đường hô hấp (hắt hơi, ho...)\nBệnh biểu hiện đặc trưng hội chứng nhiễm trùng nhiễm độc nặng nề với sốt cao, viêm họng nặng kèm giả mạc dẫn đến nguy cơ tử vong do suy hô hấp, tổn thương thần kinh và tim mạch...",
        "thang_tiem": 2
    },
    {
        "phac_do_id": 16,
        "doi_tuong_id": null,
        "khang_nguyen_id": 5,
        "ten_khang_nguyen": "Bạch Hầu",
        "thu_tu": 3,
        "tong_so_mui": 4,
        "mo_ta": "Bạch hầu là bệnh do vi khuẩn gây ra và thường lây qua đường hô hấp (hắt hơi, ho...)\nBệnh biểu hiện đặc trưng hội chứng nhiễm trùng nhiễm độc nặng nề với sốt cao, viêm họng nặng kèm giả mạc dẫn đến nguy cơ tử vong do suy hô hấp, tổn thương thần kinh và tim mạch...",
        "thang_tiem": 4
    },
    {
        "phac_do_id": 17,
        "doi_tuong_id": null,
        "khang_nguyen_id": 290,
        "ten_khang_nguyen": "Ho gà",
        "thu_tu": 3,
        "tong_so_mui": 4,
        "mo_ta": "Ho gà là một bệnh lý truyền qua đường hô hấp & do vi khuẩn ho gà gây ra. Bệnh biểu hiện đặc trưng bằng các cơn ho kịch phát và kéo dài - với nguy cơ co thắt khí - phế quản của trẻ. Trẻ thường tử vong do suy hô hấp hoặc do tổn thương não, đặc biệt nguy cơ này xảy ra rất cao ở những trẻ dưới 6 tháng tuổi.",
        "thang_tiem": 4
    },
    {
        "phac_do_id": 18,
        "doi_tuong_id": null,
        "khang_nguyen_id": 291,
        "ten_khang_nguyen": "Uốn ván",
        "thu_tu": 3,
        "tong_so_mui": 4,
        "mo_ta": "Bệnh Uốn ván do vi trùng uốn ván thường gặp trong đất hoặc bất kỳ phần rỉ sét nào trong môi trường sống. Vi khuẩn thường xâm nhập vào cơ thể qua các vết thương hở như: cắt rốn trong khi sinh, mổ xẻ... hoặc thậm chí khi chỉ bị vết cắt rất nhỏ. Độc tố sinh ra từ vi khuẩn sẽ tấn công vào hệ thống thần kinh và gây hậu quả co thắt toàn bộ các cơ của cơ thể và dẫn đến nguy cơ tử vong rất cao, đặc biệt với trẻ sơ sinh.",
        "thang_tiem": 4
    },
    {
        "phac_do_id": 19,
        "doi_tuong_id": null,
        "khang_nguyen_id": 2,
        "ten_khang_nguyen": "Bại liệt",
        "thu_tu": 3,
        "tong_so_mui": 3,
        "mo_ta": "Bại liệt là bệnh lý thần kinh vô cùng nguy hiểm do vi-rút Bại liệt gây ra. Vi-rút gây tổn thương hệ thống thần kinh dẫn đến nguy cơ tử vong do suy hô hấp. Các di chứng liệt một hoặc cả hai  chi không hồi phục làm trẻ bị tàn tật suốt đời.",
        "thang_tiem": 4
    },
    {
        "phac_do_id": 20,
        "doi_tuong_id": null,
        "khang_nguyen_id": 172,
        "ten_khang_nguyen": "Hib",
        "thu_tu": 3,
        "tong_so_mui": 3,
        "mo_ta": "Các bệnh lý thường gặp là viêm màng não mũ, viêm phổi, viêm nắp thanh quản, nhiễm trùng máu... Viêm màng não mủ do vi khuẩn Hib thương gây tử vong cao hoặc dẫn đến các di chứng thần kinh không hồi phục. Vi khuẩn thường gây ra bệnh cho các trẻ em dưới 5 tuổi, trong đó nhóm trẻ em dưới 1 tuổi là đối tượng dễ mắc bệnh nhất.",
        "thang_tiem": 4
    },
    {
        "phac_do_id": 21,
        "doi_tuong_id": null,
        "khang_nguyen_id": 8,
        "ten_khang_nguyen": "Sởi",
        "thu_tu": 1,
        "tong_so_mui": 2,
        "mo_ta": "Bệnh Sởi là bệnh do vi-rút sởi gây ra phát ban, ho, chảy nước mũi, ngứa mắt và sốt.\nBệnh Sởi có thể dẫn đến nhiễm trùng tai, viêm phổi, động kinh, tổn thương não, và tử vong.",
        "thang_tiem": 9
    },
    {
        "phac_do_id": 22,
        "doi_tuong_id": null,
        "khang_nguyen_id": 8,
        "ten_khang_nguyen": "Sởi",
        "thu_tu": 2,
        "tong_so_mui": 2,
        "mo_ta": "Bệnh Sởi là bệnh do vi-rút sởi gây ra phát ban, ho, chảy nước mũi, ngứa mắt và sốt.\nBệnh Sởi có thể dẫn đến nhiễm trùng tai, viêm phổi, động kinh, tổn thương não, và tử vong.",
        "thang_tiem": 18
    },
    {
        "phac_do_id": 23,
        "doi_tuong_id": null,
        "khang_nguyen_id": 176,
        "ten_khang_nguyen": "Rubella",
        "thu_tu": 1,
        "tong_so_mui": 1,
        "mo_ta": "Vi rút Rubella gây phát ban, viêm khớp (chủ yếu ở phụ nữ) và sốt nhẹ\nRubella là bệnh nguy hiểm và nghiêm trọng đối với phụ nữ mang thai và thai nhi. Nếu một phụ nữ bị Rubella trong khi đang mang thai, bé có thể bị dị tật bẩm sinh như các vấn đề về tim mạch, mất khả năng nghe và nhìn, ảnh hưởng trí não, bị tổn thương gan hoặc  lá lách. Các tổn thương bẩm sinh nghiêm trọng là phổ biến nếu thai phụ bị nhiễm rubella trong thời kỳ đầu mang thai, đặc biệt trong 12 tuần đầu tiên. Bị nhiễm rubella khi mang thai đồng thời có thể gây sẩy thai hoặc sinh non.\nCác bệnh này lây lan từ người này sang người khác qua mầm bệnh lan truyền trong không khí. Bé yêu và thậm chí bạn có thể dễ dàng nhiễm bệnh do ở gần một ai đó đã bị nhiễm bệnh.",
        "thang_tiem": 18
    },
    {
        "phac_do_id": 24,
        "doi_tuong_id": null,
        "khang_nguyen_id": 5,
        "ten_khang_nguyen": "Bạch Hầu",
        "thu_tu": 4,
        "tong_so_mui": 4,
        "mo_ta": "Bạch hầu là bệnh do vi khuẩn gây ra và thường lây qua đường hô hấp (hắt hơi, ho...)\nBệnh biểu hiện đặc trưng hội chứng nhiễm trùng nhiễm độc nặng nề với sốt cao, viêm họng nặng kèm giả mạc dẫn đến nguy cơ tử vong do suy hô hấp, tổn thương thần kinh và tim mạch...",
        "thang_tiem": 18
    },
    {
        "phac_do_id": 25,
        "doi_tuong_id": null,
        "khang_nguyen_id": 290,
        "ten_khang_nguyen": "Ho gà",
        "thu_tu": 4,
        "tong_so_mui": 4,
        "mo_ta": "Ho gà là một bệnh lý truyền qua đường hô hấp & do vi khuẩn ho gà gây ra. Bệnh biểu hiện đặc trưng bằng các cơn ho kịch phát và kéo dài - với nguy cơ co thắt khí - phế quản của trẻ. Trẻ thường tử vong do suy hô hấp hoặc do tổn thương não, đặc biệt nguy cơ này xảy ra rất cao ở những trẻ dưới 6 tháng tuổi.",
        "thang_tiem": 18
    },
    {
        "phac_do_id": 26,
        "doi_tuong_id": null,
        "khang_nguyen_id": 291,
        "ten_khang_nguyen": "Uốn ván",
        "thu_tu": 4,
        "tong_so_mui": 4,
        "mo_ta": "Bệnh Uốn ván do vi trùng uốn ván thường gặp trong đất hoặc bất kỳ phần rỉ sét nào trong môi trường sống. Vi khuẩn thường xâm nhập vào cơ thể qua các vết thương hở như: cắt rốn trong khi sinh, mổ xẻ... hoặc thậm chí khi chỉ bị vết cắt rất nhỏ. Độc tố sinh ra từ vi khuẩn sẽ tấn công vào hệ thống thần kinh và gây hậu quả co thắt toàn bộ các cơ của cơ thể và dẫn đến nguy cơ tử vong rất cao, đặc biệt với trẻ sơ sinh.",
        "thang_tiem": 18
    },
    {
        "phac_do_id": 27,
        "doi_tuong_id": null,
        "khang_nguyen_id": 9,
        "ten_khang_nguyen": "Viêm não Nhật Bản",
        "thu_tu": 1,
        "tong_so_mui": 3,
        "mo_ta": "Viêm não Nhật Bản là bệnh truyền nhiễm do virus cấp tính. Virus viêm não Nhật Bản có ái lực với tế bào thần kinh nên khi xâm nhập vào máu, chúng tấn công vào hệ thần kinh trung ương gây tử vong hoặc để lại di chứng nặng nề.",
        "thang_tiem": 12
    },
    {
        "phac_do_id": 28,
        "doi_tuong_id": null,
        "khang_nguyen_id": 9,
        "ten_khang_nguyen": "Viêm não Nhật Bản",
        "thu_tu": 2,
        "tong_so_mui": 3,
        "mo_ta": "Viêm não Nhật Bản là bệnh truyền nhiễm do virus cấp tính. Virus viêm não Nhật Bản có ái lực với tế bào thần kinh nên khi xâm nhập vào máu, chúng tấn công vào hệ thần kinh trung ương gây tử vong hoặc để lại di chứng nặng nề.",
        "thang_tiem": 12
    },
    {
        "phac_do_id": 29,
        "doi_tuong_id": null,
        "khang_nguyen_id": 9,
        "ten_khang_nguyen": "Viêm não Nhật Bản",
        "thu_tu": 3,
        "tong_so_mui": 3,
        "mo_ta": "Viêm não Nhật Bản là bệnh truyền nhiễm do virus cấp tính. Virus viêm não Nhật Bản có ái lực với tế bào thần kinh nên khi xâm nhập vào máu, chúng tấn công vào hệ thần kinh trung ương gây tử vong hoặc để lại di chứng nặng nề.",
        "thang_tiem": 24
    },
    {
        "phac_do_id": 30,
        "doi_tuong_id": null,
        "khang_nguyen_id": 13,
        "ten_khang_nguyen": "Thương Hàn",
        "thu_tu": 1,
        "tong_so_mui": 1,
        "mo_ta": null,
        "thang_tiem": 36
    },
    {
        "phac_do_id": 31,
        "doi_tuong_id": null,
        "khang_nguyen_id": 6,
        "ten_khang_nguyen": "Tả",
        "thu_tu": 1,
        "tong_so_mui": 2,
        "mo_ta": null,
        "thang_tiem": 24
    },
    {
        "phac_do_id": 1,
        "doi_tuong_id": null,
        "khang_nguyen_id": 270,
        "ten_khang_nguyen": "Lao",
        "thu_tu": 1,
        "tong_so_mui": 1,
        "mo_ta": "Bệnh lao là một bệnh nhiễm khuẩn, một bệnh lây. Nguyên nhân gây bệnh lao là do vi khuẩn lao từ người bệnh sang người lành. Nguồn lây là những bệnh nhân lao nói chung, đặc biệt là lao phổi khạc ra vi khuẩn lao trong đờm tìm thấy được bằng phương pháp nhuộm soi trực tiếp là nguồn lây bệnh nguy hiểm nhất.",
        "thang_tiem": 0
    },
    {
        "phac_do_id": 13,
        "doi_tuong_id": null,
        "khang_nguyen_id": 290,
        "ten_khang_nguyen": "Ho gà",
        "thu_tu": 2,
        "tong_so_mui": 4,
        "mo_ta": "Ho gà là một bệnh lý truyền qua đường hô hấp & do vi khuẩn ho gà gây ra. Bệnh biểu hiện đặc trưng bằng các cơn ho kịch phát và kéo dài - với nguy cơ co thắt khí - phế quản của trẻ. Trẻ thường tử vong do suy hô hấp hoặc do tổn thương não, đặc biệt nguy cơ này xảy ra rất cao ở những trẻ dưới 6 tháng tuổi.",
        "thang_tiem": 3
    },
    {
        "phac_do_id": 15,
        "doi_tuong_id": null,
        "khang_nguyen_id": 3,
        "ten_khang_nguyen": "Viêm gan B",
        "thu_tu": 4,
        "tong_so_mui": 4,
        "mo_ta": "Viêm gan B là một bệnh gan nghiêm trọng gây ra bởi virus viêm gan B (HBV). Đối với một số người, bệnh viêm gan B lây nhiễm trở thành mãn tính, dẫn đến suy gan, ung thư gan, hoặc xơ gan - một tình trạng gây ra sẹo vĩnh viễn ở gan.",
        "thang_tiem": 4
    },
    {
        "phac_do_id": 3,
        "doi_tuong_id": null,
        "khang_nguyen_id": 3,
        "ten_khang_nguyen": "Viêm gan B",
        "thu_tu": 2,
        "tong_so_mui": 4,
        "mo_ta": "Viêm gan B là một bệnh gan nghiêm trọng gây ra bởi virus viêm gan B (HBV). Đối với một số người, bệnh viêm gan B lây nhiễm trở thành mãn tính, dẫn đến suy gan, ung thư gan, hoặc xơ gan - một tình trạng gây ra sẹo vĩnh viễn ở gan.",
        "thang_tiem": 2
    },
    {
        "phac_do_id": 5,
        "doi_tuong_id": null,
        "khang_nguyen_id": 290,
        "ten_khang_nguyen": "Ho gà",
        "thu_tu": 1,
        "tong_so_mui": 4,
        "mo_ta": "Ho gà là một bệnh lý truyền qua đường hô hấp & do vi khuẩn ho gà gây ra. Bệnh biểu hiện đặc trưng bằng các cơn ho kịch phát và kéo dài - với nguy cơ co thắt khí - phế quản của trẻ. Trẻ thường tử vong do suy hô hấp hoặc do tổn thương não, đặc biệt nguy cơ này xảy ra rất cao ở những trẻ dưới 6 tháng tuổi.",
        "thang_tiem": 2
    },
    {
        "phac_do_id": 6,
        "doi_tuong_id": null,
        "khang_nguyen_id": 291,
        "ten_khang_nguyen": "Uốn ván",
        "thu_tu": 1,
        "tong_so_mui": 4,
        "mo_ta": "Bệnh Uốn ván do vi trùng uốn ván thường gặp trong đất hoặc bất kỳ phần rỉ sét nào trong môi trường sống. Vi khuẩn thường xâm nhập vào cơ thể qua các vết thương hở như: cắt rốn trong khi sinh, mổ xẻ... hoặc thậm chí khi chỉ bị vết cắt rất nhỏ. Độc tố sinh ra từ vi khuẩn sẽ tấn công vào hệ thống thần kinh và gây hậu quả co thắt toàn bộ các cơ của cơ thể và dẫn đến nguy cơ tử vong rất cao, đặc biệt với trẻ sơ sinh.",
        "thang_tiem": 2
    },
    {
        "phac_do_id": 7,
        "doi_tuong_id": null,
        "khang_nguyen_id": 172,
        "ten_khang_nguyen": "Hib",
        "thu_tu": 1,
        "tong_so_mui": 3,
        "mo_ta": "Các bệnh lý thường gặp là viêm màng não mũ, viêm phổi, viêm nắp thanh quản, nhiễm trùng máu... Viêm màng não mủ do vi khuẩn Hib thương gây tử vong cao hoặc dẫn đến các di chứng thần kinh không hồi phục. Vi khuẩn thường gây ra bệnh cho các trẻ em dưới 5 tuổi, trong đó nhóm trẻ em dưới 1 tuổi là đối tượng dễ mắc bệnh nhất.",
        "thang_tiem": 2
    },
    {
        "phac_do_id": 8,
        "doi_tuong_id": null,
        "khang_nguyen_id": 2,
        "ten_khang_nguyen": "Bại liệt",
        "thu_tu": 1,
        "tong_so_mui": 3,
        "mo_ta": "Bại liệt là bệnh lý thần kinh vô cùng nguy hiểm do vi-rút Bại liệt gây ra. Vi-rút gây tổn thương hệ thống thần kinh dẫn đến nguy cơ tử vong do suy hô hấp. Các di chứng liệt một hoặc cả hai  chi không hồi phục làm trẻ bị tàn tật suốt đời.",
        "thang_tiem": 2
    },
    {
        "phac_do_id": 9,
        "doi_tuong_id": null,
        "khang_nguyen_id": 3,
        "ten_khang_nguyen": "Viêm gan B",
        "thu_tu": 3,
        "tong_so_mui": 4,
        "mo_ta": "Viêm gan B là một bệnh gan nghiêm trọng gây ra bởi virus viêm gan B (HBV). Đối với một số người, bệnh viêm gan B lây nhiễm trở thành mãn tính, dẫn đến suy gan, ung thư gan, hoặc xơ gan - một tình trạng gây ra sẹo vĩnh viễn ở gan.",
        "thang_tiem": 3
    },
    {
        "phac_do_id": 10,
        "doi_tuong_id": null,
        "khang_nguyen_id": 2,
        "ten_khang_nguyen": "Bại liệt",
        "thu_tu": 2,
        "tong_so_mui": 3,
        "mo_ta": "Bại liệt là bệnh lý thần kinh vô cùng nguy hiểm do vi-rút Bại liệt gây ra. Vi-rút gây tổn thương hệ thống thần kinh dẫn đến nguy cơ tử vong do suy hô hấp. Các di chứng liệt một hoặc cả hai  chi không hồi phục làm trẻ bị tàn tật suốt đời.",
        "thang_tiem": 3
    },
    {
        "phac_do_id": 11,
        "doi_tuong_id": null,
        "khang_nguyen_id": 291,
        "ten_khang_nguyen": "Uốn ván",
        "thu_tu": 2,
        "tong_so_mui": 4,
        "mo_ta": "Bệnh Uốn ván do vi trùng uốn ván thường gặp trong đất hoặc bất kỳ phần rỉ sét nào trong môi trường sống. Vi khuẩn thường xâm nhập vào cơ thể qua các vết thương hở như: cắt rốn trong khi sinh, mổ xẻ... hoặc thậm chí khi chỉ bị vết cắt rất nhỏ. Độc tố sinh ra từ vi khuẩn sẽ tấn công vào hệ thống thần kinh và gây hậu quả co thắt toàn bộ các cơ của cơ thể và dẫn đến nguy cơ tử vong rất cao, đặc biệt với trẻ sơ sinh.",
        "thang_tiem": 3
    },
    {
        "phac_do_id": 12,
        "doi_tuong_id": null,
        "khang_nguyen_id": 5,
        "ten_khang_nguyen": "Bạch Hầu",
        "thu_tu": 2,
        "tong_so_mui": 4,
        "mo_ta": "Bạch hầu là bệnh do vi khuẩn gây ra và thường lây qua đường hô hấp (hắt hơi, ho...)\nBệnh biểu hiện đặc trưng hội chứng nhiễm trùng nhiễm độc nặng nề với sốt cao, viêm họng nặng kèm giả mạc dẫn đến nguy cơ tử vong do suy hô hấp, tổn thương thần kinh và tim mạch...",
        "thang_tiem": 3
    },
    {
        "phac_do_id": 32,
        "doi_tuong_id": null,
        "khang_nguyen_id": 6,
        "ten_khang_nguyen": "Tả",
        "thu_tu": 2,
        "tong_so_mui": 2,
        "mo_ta": null,
        "thang_tiem": 24
    }
]

export {
    THANH_VIEN,
    KHANG_NGUYEN,
    VACCIN,
    PHAC_DO,
}