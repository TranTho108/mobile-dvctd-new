const detailData = [
  {
    'ID': 122154367,
    'Name': 'Nguyễn Văn An',
    'Class': '8A3',
    'Year': '2019-2020',
    'level': 2,
    'School': 'Trung học cơ sở Đoàn Thị Điểm',
    'Grade': 'Khối 8',
    'Sex': 'Nam',
    'Birthday': '02/01/2006',
    'dataNoti': [
      {
        body: 'HS Nguyễn Văn An đi học muộn ngày 11/11/2019',
        time: '11/11/2019 07:54',
        isMuon: true
      },
      {
        body: 'HS Nguyễn Văn An nghỉ học ngày 08/11/2019',
        time: '08/11/2019 08:54',
        isNghi: true
      },
      {
        body: 'HS Nguyễn Văn An được 8.0 điểm (15P) - Địa lý',
        time: '05/10/2019 07:54'
      },
      {
        body: 'HS Nguyễn Văn An được 6.0 điểm (45P) - Toán',
        time: '04/10/2019 03:09'
      },
      {
        body: 'HS Nguyễn Văn An được 9.0 điểm (15P) - Tiếng Anh',
        time: '02/10/2019 04:12'
      },
      {
        body: 'HS Nguyễn Văn An được 8.0 điểm (45P) - Vật lý',
        time: '28/09/2019 07:54'
      },
      {
        body: 'HS Nguyễn Văn An được 6.0 điểm (45P) - Toán',
        time: '11/09/2019 03:09'
      },
      {
        body: 'HS Nguyễn Văn An được 9.0 điểm (15P) - Mĩ Thuật',
        time: '10/09/2019 04:12'
      },
      {
        body: 'HS Nguyễn Văn An được 8.0 điểm (45P) - Ngữ Văn',
        time: '09/09/2019 07:54'
      },
      {
        body: 'HS Nguyễn Văn An được 6.0 điểm (45P) - Toán',
        time: '07/09/2019 03:09'
      },
      {
        body: 'HS Nguyễn Văn An được 9.0 điểm (15P) - Tiếng Anh',
        time: '03/09/2019 04:12'
      },
      {
        body: 'Lớp 8A được nghỉ học ngày 02/09/2019',
        time: '02/09/2019 04:12',
        isNghi: true
      },
    ],
    'dataCheck': [
      {
        month: 'Tháng 11 Năm 2019',
        data: [
          {
            NgayDiemDanh: '11/11/2019',
            GioVao: '7h45',
            GioVe: '17h15',
            TrangThaiDiemDanhID: 4,
            GhiChu: '',

          },
          {
            NgayDiemDanh: '08/11/2019',
            GioVao: '',
            GioVe: '',
            TrangThaiDiemDanhID: 3,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '07/11/2019',
            GioVao: '7h12',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '06/11/2019',
            GioVao: '',
            GioVe: '',
            TrangThaiDiemDanhID: 2,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '05/11/2019',
            GioVao: '7h08',
            GioVe: '17h18',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '04/11/2019',
            GioVao: '7h15',
            GioVe: '17h19',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '01/11/2019',
            GioVao: '7h09',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
        ]
      },
      {
        month: 'Tháng 10 Năm 2019',
        data: [
          {
            NgayDiemDanh: '31/10/2019',
            GioVao: '7h11',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '30/10/2019',
            GioVao: '7h19',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '30/10/2019',
            GioVao: '7h20',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '29/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '28/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '25/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '24/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '23/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '22/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '21/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '18/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '17/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '16/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '15/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '14/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '11/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '10/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '09/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '08/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '07/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '04/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '03/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '02/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '01/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          }
        ]
      },
    ],
    'dataSemester': [
      {
        semester: 1,
        semesterString: 'Học kỳ 1',
        HL: 'Khá',
        HK: 'Tốt',
        DH: 'Tiên tiến',
        dataPoint: [
          {
            'subject': 'Toán',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [8.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [7.3],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Vật lý',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [6.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [5.0, 9.0, 8.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.5, 5.0],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [4.5],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Sinh học',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [10.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [10.0, 9.0, 8.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 9.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [8.3],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Ngữ văn',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [6.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.7, 2.0, 5.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [7.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [5.3],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Địa lý',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [3.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 7.5],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [7.9],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Tiếng anh',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [7.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [4.9],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Lịch sử',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [9.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [7.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [7.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [6.3],
                'multiplier': 3
              },
            ]
          },
        ]
      },
      {
        semester: 2,
        semesterString: 'Học kỳ 2',
        HL: '',
        HK: '',
        DH: '',
        dataPoint: []
      },
      {
        semester: 3,
        semesterString: 'Cả năm',
        HL: '',
        HK: '',
        DH: '',
        dataPoint: []
      },
    ],
    'dataStudent': [
      {
        name: 'Bùi Hoàng An',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Hoàng An',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Phương Anh',
        gt: 'Nữ'
      },
      {
        name: 'Trần Lê Minh Anh',
        gt: 'Nữ'
      },
      {
        name: 'Trần Mai Anh',
        gt: 'Nữ'
      },
      {
        name: 'Vũ Minh Anh',
        gt: 'Nữ'
      },
      {
        name: 'Đỗ Phúc Gia Bảo',
        gt: 'Nam'
      },
      {
        name: 'Vũ Minh Châu',
        gt: 'Nam'
      },
      {
        name: 'Ngô Thành Chung',
        gt: 'Nam'
      },
      {
        name: 'Võ Hoàng Dung',
        gt: 'Nữ'
      },
      {
        name: 'Trương Quan Duy',
        gt: 'Nam'
      },
      {
        name: 'Hoàng Tiến Đạt',
        gt: 'Nam'
      },
      {
        name: 'Vũ Hoàng Hà',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Hồng Hạnh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Đức Hiển',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Quang Huy',
        gt: 'Nam'
      },
      {
        name: 'Phạm Thanh Huy',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Khánh Huyền',
        gt: 'Nữ'
      },
      {
        name: 'Đỗ Nam Khánh',
        gt: 'Nam'
      },
      {
        name: 'Trần Hà Khoa',
        gt: 'Nam'
      },
      {
        name: 'Vương Ngọc Liên',
        gt: 'Nữ'
      },
      {
        name: 'Đào Khánh Linh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Diệu Linh',
        gt: 'Nữ'
      },
      {
        name: 'Trần Hà Linh',
        gt: 'Nữ'
      },
      {
        name: 'Lê Anh Thư',
        gt: 'Nữ'
      },
    ],
    'dataTeacher': [
      {
        name: 'Nguyễn Thị Thu Hà',
        gt: 'Nữ',
        subject: 'Chủ nhiệm, Sinh học',
        phone: '0123456789'
      },
      {
        name: 'Nguyễn Kim Anh',
        gt: 'Nữ',
        subject: 'Thể dục',
        phone: '0123456789'
      },
      {
        name: 'Nguyễn Thanh Bình',
        gt: 'Nam',
        subject: 'Ngữ văn, GDCD',
        phone: '0123456789'
      },
      {
        name: 'Đào Minh Cảnh',
        gt: 'Nam',
        subject: 'Toán',
        phone: '0123456789'
      },
      {
        name: 'Đặng Thị Việt Hà',
        gt: 'Nữ',
        subject: 'Công nghệ',
        phone: '0123456789'
      },
      {
        name: 'Trần Duy Hoàng',
        gt: 'Nam',
        subject: 'Tin học',
        phone: '0123456789'
      },
      {
        name: 'Bùi Thị Huế',
        gt: 'Nữ',
        subject: 'Lịch sử',
        phone: '0123456789'
      },
      {
        name: 'Bành Thị Thanh Huyền',
        gt: 'Nữ',
        subject: 'Vật lý',
        phone: '0123456789'
      },
      {
        name: 'Đào Lan Hương',
        gt: 'Nữ',
        subject: 'Âm nhạc',
        phone: '0123456789'
      },
      {
        name: 'Lã Thị Thuỳ Linh',
        gt: 'Nữ',
        subject: 'Địa lý',
        phone: '0123456789'
      },
      {
        name: 'Bùi Thị Huyền Trang',
        gt: 'Nữ',
        subject: 'Tiếng Anh',
        phone: '0123456789'
      },
    ]
  },
  {
    'ID': 122154488,
    'Name': 'Vũ Thuỳ Linh',
    'Class': '6A1',
    'level': 2,
    'Year': '2019-2020',
    'School': 'Trung học cơ sở Đoàn Thị Điểm',
    'Grade': 'Khối 6',
    'Sex': 'Nữ',
    'Birthday': '18/05/2008',
    'dataNoti': [
      {
        body: 'HS Vũ Thuỳ Linh nghỉ học ngày 08/11/2019',
        time: '08/11/2019 08:30',
        isNghi: true
      },
      {
        body: 'HS Vũ Thuỳ Linh đi học muộn ngày 07/11/2019',
        time: '07/11/2019 07:54',
        isMuon: true
      },
      {
        body: 'HS Vũ Thuỳ Linh được 8.0 điểm (15P) - Địa lý',
        time: '05/10/2019 07:54'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 7.0 điểm (45P) - Toán',
        time: '04/10/2019 03:09'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 9.0 điểm (15P) - Tiếng Anh',
        time: '02/10/2019 04:12'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 10.0 điểm (45P) - Vật lý',
        time: '28/09/2019 07:54'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 6.0 điểm (45P) - Toán',
        time: '11/09/2019 03:09'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 5.0 điểm (15P) - Mĩ Thuật',
        time: '10/09/2019 04:12'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 8.0 điểm (45P) - Ngữ Văn',
        time: '09/09/2019 07:54'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 4.0 điểm (45P) - Toán',
        time: '07/09/2019 03:09'
      },
      {
        body: 'HS Vũ Thuỳ Linh được 8.0 điểm (15P) - Tiếng Anh',
        time: '03/09/2019 04:12'
      },
      {
        body: 'Lớp 6A1 được nghỉ học ngày 07/09/2019',
        time: '07/09/2019 06:12',
        isNghi: true
      },
    ],
    'dataCheck': [
      {
        month: 'Tháng 11 Năm 2019',
        data: [
          {
            NgayDiemDanh: '11/11/2019',
            GioVao: '7h12',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',

          },
          {
            NgayDiemDanh: '08/11/2019',
            GioVao: '',
            GioVe: '',
            TrangThaiDiemDanhID: 2,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '07/11/2019',
            GioVao: '7h30',
            GioVe: '17h13',
            TrangThaiDiemDanhID: 4,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '06/11/2019',
            GioVao: '7h01',
            GioVe: '17h11',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '05/11/2019',
            GioVao: '',
            GioVe: '',
            TrangThaiDiemDanhID: 3,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '04/11/2019',
            GioVao: '7h15',
            GioVe: '17h19',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '01/11/2019',
            GioVao: '7h09',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
        ]
      },
      {
        month: 'Tháng 10 Năm 2019',
        data: [
          {
            NgayDiemDanh: '31/10/2019',
            GioVao: '7h16',
            GioVe: '17h13',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '30/10/2019',
            GioVao: '7h19',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '30/10/2019',
            GioVao: '7h20',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '29/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '28/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '25/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '24/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '23/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '22/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '21/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '18/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '17/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '16/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '15/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '14/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '11/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '10/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '09/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '08/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '07/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '04/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '03/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '02/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '01/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          }
        ]
      },
    ],
    'dataSemester': [
      {
        semester: 1,
        semesterString: 'Học kỳ 1',
        HL: 'Khá',
        HK: 'Tốt',
        DH: 'Tiên tiến',
        dataPoint: [
          {
            'subject': 'Toán',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [7.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [7.0, 6.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [8.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [6.5],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Vật lý',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [8.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [5.0, 9.0, 8.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.5, 5.0],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [4.5],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Sinh học',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [10.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [10.0, 9.0, 8.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 9.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [8.3],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Ngữ văn',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [6.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.7, 2.0, 5.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [7.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [5.3],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Địa lý',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [3.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 7.5],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [7.9],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Tiếng anh',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [7.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [6.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [9.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [4.9],
                'multiplier': 3
              },
            ]
          },
          {
            'subject': 'Lịch sử',
            'data': [
              {
                'type': 'Miệng HS1',
                'point': [9.0],
                'multiplier': 1

              },
              {
                'type': 'Viết HS1',
                'point': [7.0, 7.0, 7.5],
                'multiplier': 1
              },
              {
                'type': 'Viết HS2',
                'point': [7.0, 7.3],
                'multiplier': 2
              },
              {
                'type': 'HK',
                'point': [6.3],
                'multiplier': 3
              },
            ]
          },
        ]
      },
      {
        semester: 2,
        semesterString: 'Học kỳ 2',
        HL: '',
        HK: '',
        DH: '',
        dataPoint: []
      },
      {
        semester: 3,
        semesterString: 'Cả năm',
        HL: '',
        HK: '',
        DH: '',
        dataPoint: []
      },
    ],
    'dataStudent': [
      {
        name: 'Triệu Mai An',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Việt An',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Thị Phương Anh',
        gt: 'Nữ'
      },
      {
        name: 'Dương Lê NGọc Bích',
        gt: 'Nữ'
      },
      {
        name: 'Trần Thị Ánh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Vũ Mai Loan',
        gt: 'Nữ'
      },
      {
        name: 'Đỗ Phúc Gia Bảo',
        gt: 'Nam'
      },
      {
        name: 'Vũ Minh Châu',
        gt: 'Nam'
      },
      {
        name: 'Ngô Thành Chung',
        gt: 'Nam'
      },
      {
        name: 'Võ Hoàng Dung',
        gt: 'Nữ'
      },
      {
        name: 'Trương Quan Duy',
        gt: 'Nam'
      },
      {
        name: 'Hoàng Tiến Đạt',
        gt: 'Nam'
      },
      {
        name: 'Vũ Hoàng Hà',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Hồng Hạnh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Đức Hiển',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Quang Huy',
        gt: 'Nam'
      },
      {
        name: 'Phạm Thanh Huy',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Khánh Huyền',
        gt: 'Nữ'
      },
      {
        name: 'Đỗ Nam Khánh',
        gt: 'Nam'
      },
      {
        name: 'Trần Hà Khoa',
        gt: 'Nam'
      },
      {
        name: 'Vương Ngọc Liên',
        gt: 'Nữ'
      },
      {
        name: 'Đào Khánh Linh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Thuỳ Linh',
        gt: 'Nữ'
      },
      {
        name: 'Vũ Thuỳ Linh',
        gt: 'Nữ'
      },
      {
        name: 'Lê Bảo Tuấn',
        gt: 'Nữ'
      },
    ],
    'dataTeacher': [
      {
        name: 'Nguyễn Văn Hiếu',
        gt: 'Nam',
        subject: 'Chủ nhiệm, Toán học',
        phone: '0123456789'
      },
      {
        name: 'Trần Trung Dung',
        gt: 'Nam',
        subject: 'Thể dục',
        phone: '0123456789'
      },
      {
        name: 'Lê Thế Hoan',
        gt: 'Nam',
        subject: 'Ngữ văn, GDCD',
        phone: '0123456789'
      },
      {
        name: 'Đào Minh Cảnh',
        gt: 'Nam',
        subject: 'Sinh học',
        phone: '0123456789'
      },
      {
        name: 'Đặng Thị Việt Hà',
        gt: 'Nữ',
        subject: 'Công nghệ',
        phone: '0123456789'
      },
      {
        name: 'Trần Duy Hoàng',
        gt: 'Nam',
        subject: 'Tin học',
        phone: '0123456789'
      },
      {
        name: 'Bùi Thị Huế',
        gt: 'Nữ',
        subject: 'Lịch sử',
        phone: '0123456789'
      },
      {
        name: 'Bành Thị Thanh Huyền',
        gt: 'Nữ',
        subject: 'Vật lý',
        phone: '0123456789'
      },
      {
        name: 'Đào Lan Hương',
        gt: 'Nữ',
        subject: 'Âm nhạc',
        phone: '0123456789'
      },
      {
        name: 'Lã Thị Thuỳ Linh',
        gt: 'Nữ',
        subject: 'Địa lý',
        phone: '0123456789'
      },
      {
        name: 'Bùi Thị Huyền Trang',
        gt: 'Nữ',
        subject: 'Tiếng Anh',
        phone: '0123456789'
      },
    ]
  },
  {
    'ID': 122123546,
    'Name': 'Trần Thế An',
    'Class': '4B3',
    'level': 1,
    'Year': '2019-2020',
    'School': 'Tiểu học Chu Văn An',
    'Grade': 'Khối 4',
    'Sex': 'Nam',
    'Birthday': '18/05/2010',
    'dataNoti': [
      {
        body: 'Học sinh được nghỉ lễ, phụ huynh nhắc con ôn tập Toán, Tiếng Việt để chuẩn bị cho thi cuối kỳ 1: Học các bài tập làm văn, làm phiếu ôn tập học kỳ',
        time: '26/12/2019 09:43',
      },
      {
        body: 'Nhà trường TB: Học sinh được nghỉ lễ ngày thứ 4 (01/01/2020). Thứ 5 (02/01/2020) học sinh đi học bình thường. CMHS quan tâm quản lí con vui chơi, an toàn.Trân trọng!',
        time: '26/12/2019 09:43',
        isNghi: true
      },
      {
        body: 'Nhà trường TB: Học sinh bắt đầu nghỉ lễ ngày thứ 7 (31/08/2020) đến hết ngày thứ 2 (02/09/2020). Thứ 3 (03/09/2020) học sinh đi học bình thường. CMHS quan tâm quản lí con vui chơi, an toàn.Trân trọng!',
        time: '30/08/2019 16:32',
        isNghi: true
      },
    ],
    'dataCheck': [
      {
        month: 'Tháng 11 Năm 2019',
        data: [
          {
            NgayDiemDanh: '11/11/2019',
            GioVao: '7h12',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',

          },
          {
            NgayDiemDanh: '08/11/2019',
            GioVao: '',
            GioVe: '',
            TrangThaiDiemDanhID: 2,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '07/11/2019',
            GioVao: '7h30',
            GioVe: '17h13',
            TrangThaiDiemDanhID: 4,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '06/11/2019',
            GioVao: '7h01',
            GioVe: '17h11',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '05/11/2019',
            GioVao: '',
            GioVe: '',
            TrangThaiDiemDanhID: 3,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '04/11/2019',
            GioVao: '7h15',
            GioVe: '17h19',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '01/11/2019',
            GioVao: '7h09',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
        ]
      },
      {
        month: 'Tháng 10 Năm 2019',
        data: [
          {
            NgayDiemDanh: '31/10/2019',
            GioVao: '7h16',
            GioVe: '17h13',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '30/10/2019',
            GioVao: '7h19',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '30/10/2019',
            GioVao: '7h20',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '29/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '28/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '25/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '24/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '23/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '22/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '21/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '18/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '17/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '16/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '15/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '14/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '11/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '10/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '09/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '08/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '07/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '04/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '03/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '02/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          },
          {
            NgayDiemDanh: '01/10/2019',
            GioVao: '7h15',
            GioVe: '17h10',
            TrangThaiDiemDanhID: 1,
            GhiChu: '',
          }
        ]
      },
    ],
    'dataSemester': [
      {
        semester: 1,
        semesterString: 'Kiểm tra giữa học kỳ 1',
        dataCapacity: {
          data: ['Tốt','Tốt','Tốt'],
          description: 'Tự giác hoàn thành các nhiệm vụ học tập, biết chia sẻ thông tin'
        },
        dataQuality: {
          data: ['Tốt','Tốt','Tốt', 'Tốt'],
          description: ''
        },
        dataSubject: [
          {
            subject: 'Tin học',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Tiếng Việt',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Tiếng Anh',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Mỹ thuật',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Đạo đức',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Âm nhạc',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Thể dục',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Lịch sử & địa lý',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Khoa học',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Kỹ thuật',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
          {
            subject: 'Toán học',
            title: 'Hoàn thành tốt',
            description: 'Tiếp thu bài nhanh, nắm vững các kiến thức đã học',
          },
        ]
      },
      {
        semester: 2,
        semesterString: 'Kiểm tra cuối học kỳ 1',
        dataCapacity: {},
        dataQuality: {},
        dataSubject: []
      },
      {
        semester: 3,
        semesterString: 'Kiểm tra giữa học kỳ 2',
        dataCapacity: {},
        dataQuality: {},
        dataSubject: []
      },
      {
        semester: 4,
        semesterString: 'Kiểm tra cuối năm',
        dataCapacity: {},
        dataQuality: {},
        dataSubject: []
      },
    ],
    'dataStudent': [
      {
        name: 'Triệu Mai An',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Việt An',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Thị Phương Anh',
        gt: 'Nữ'
      },
      {
        name: 'Dương Lê NGọc Bích',
        gt: 'Nữ'
      },
      {
        name: 'Trần Thị Ánh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Vũ Mai Loan',
        gt: 'Nữ'
      },
      {
        name: 'Đỗ Phúc Gia Bảo',
        gt: 'Nam'
      },
      {
        name: 'Vũ Minh Châu',
        gt: 'Nam'
      },
      {
        name: 'Ngô Thành Chung',
        gt: 'Nam'
      },
      {
        name: 'Võ Hoàng Dung',
        gt: 'Nữ'
      },
      {
        name: 'Trương Quan Duy',
        gt: 'Nam'
      },
      {
        name: 'Hoàng Tiến Đạt',
        gt: 'Nam'
      },
      {
        name: 'Vũ Hoàng Hà',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Hồng Hạnh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Đức Hiển',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Quang Huy',
        gt: 'Nam'
      },
      {
        name: 'Phạm Thanh Huy',
        gt: 'Nam'
      },
      {
        name: 'Nguyễn Khánh Huyền',
        gt: 'Nữ'
      },
      {
        name: 'Đỗ Nam Khánh',
        gt: 'Nam'
      },
      {
        name: 'Trần Hà Khoa',
        gt: 'Nam'
      },
      {
        name: 'Vương Ngọc Liên',
        gt: 'Nữ'
      },
      {
        name: 'Đào Khánh Linh',
        gt: 'Nữ'
      },
      {
        name: 'Nguyễn Thuỳ Linh',
        gt: 'Nữ'
      },
      {
        name: 'Vũ Thuỳ Linh',
        gt: 'Nữ'
      },
      {
        name: 'Lê Bảo Tuấn',
        gt: 'Nữ'
      },
    ],
    'dataTeacher': [
      {
        name: 'Nguyễn Văn Hiếu',
        gt: 'Nam',
        subject: 'Chủ nhiệm, Toán học',
        phone: '0123456789'
      },
      {
        name: 'Trần Trung Dung',
        gt: 'Nam',
        subject: 'Thể dục',
        phone: '0123456789'
      },
      {
        name: 'Lê Thế Hoan',
        gt: 'Nam',
        subject: 'Ngữ văn, GDCD',
        phone: '0123456789'
      },
      {
        name: 'Đào Minh Cảnh',
        gt: 'Nam',
        subject: 'Sinh học',
        phone: '0123456789'
      },
      {
        name: 'Đặng Thị Việt Hà',
        gt: 'Nữ',
        subject: 'Công nghệ',
        phone: '0123456789'
      },
      {
        name: 'Trần Duy Hoàng',
        gt: 'Nam',
        subject: 'Tin học',
        phone: '0123456789'
      },
      {
        name: 'Bùi Thị Huế',
        gt: 'Nữ',
        subject: 'Lịch sử',
        phone: '0123456789'
      },
      {
        name: 'Bành Thị Thanh Huyền',
        gt: 'Nữ',
        subject: 'Vật lý',
        phone: '0123456789'
      },
      {
        name: 'Đào Lan Hương',
        gt: 'Nữ',
        subject: 'Âm nhạc',
        phone: '0123456789'
      },
      {
        name: 'Lã Thị Thuỳ Linh',
        gt: 'Nữ',
        subject: 'Địa lý',
        phone: '0123456789'
      },
      {
        name: 'Bùi Thị Huyền Trang',
        gt: 'Nữ',
        subject: 'Tiếng Anh',
        phone: '0123456789'
      },
    ]
  }
]

const listData = [
  {
    'ID': 122154367,
    'Name': 'Nguyễn Văn An',
    'Class': '8A3',
    'Year': '2019-2020',
    'School': 'Trung học cơ sở Đoàn Thị Điểm',
  },
  {
    'ID': 122154488,
    'Name': 'Vũ Thuỳ Linh',
    'Class': '6A1',
    'Year': '2019-2020',
    'School': 'Trung học cơ sở Đoàn Thị Điểm',
  },
  {
    'ID': 122123546,
    'Name': 'Trần Thế An',
    'Class': '4A3',
    'Year': '2019-2020',
    'School': 'Tiểu học Chu Văn An',
  }
]

const KPA_Data = [
  {
    'ID': 1,
    'Name': 'Thông báo khẩu phần tuần 18 năm học 2019-2020',
    'Week': 18,
    'Time': '31/12/2019 09:20',
    'Data': [
      {
        'Day': 'Thứ 2',
        'Food': ['Thịt gà xào ngô ngọt','Muối vừng','Rau muống xào','Canh chua me','Cơm trắng','Sữa Kun trái cây']
      },
      {
        'Day': 'Thứ 3',
        'Food': ['Thịt kho tàu','Trứng kho','Bí đỏ xào tỏi','Canh cải nấu thịt','Cơm trắng','Bánh tươi Kinh Đô vị socola']
      },
      {
        'Day': 'Thứ 4',
        'Food': ['Nghỉ lễ']
      },
      {
        'Day': 'Thứ 5',
        'Food': ['Chả mực rim tiêu','Ruốc thịt lợn','Su su, cà rốt xào','Canh óc đậu, cà chua','Cơm trắng','Bánh mặt cười']
      },
      {
        'Day': 'Thứ 6',
        'Food': ['Gà chiên giòn','Đậu sốt cà chua','Giá xào hành','Canh bí xanh nấu bột tôm','Cơm trắng','Sữa chua Vinamilk']
      },
    ]
  }
]

export {
  detailData,
  listData,
  KPA_Data
}