const MapData = [
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Nam Định",
            "MoTa": "Nam Định là một thành phố trực thuộc tỉnh Nam Định, Việt Nam. Đây là một trong những thành phố được Pháp lập ra đầu tiên ở Liên bang Đông Dương. Hiện nay Nam Định là đô thị loại I trực thuộc tỉnh và là tỉnh lỵ của tỉnh Nam Định.",
            "Ma": "356",
            "Cap": "Thành phố",
            "DanSo": "436.294",
            "DienTich": "46,4",
            "Unit": [
                {
                    "PhuongXa": "Hạ Long",
                    "MaPX": "13633",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Trần Tế Xương",
                    "MaPX": "13636",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Vị Hoàng",
                    "MaPX": "13639",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Vị Xuyên",
                    "MaPX": "13642",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Quang Trung",
                    "MaPX": "13645",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Cửa Bắc",
                    "MaPX": "13648",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Nguyễn Du",
                    "MaPX": "13651",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Bà Triệu",
                    "MaPX": "13654",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "TrưThi",
                    "MaPX": "13657",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Phan Đình Phùng",
                    "MaPX": "13660",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Ngô Quyền",
                    "MaPX": "13663",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Trần Hưng Đạo",
                    "MaPX": "13666",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Trần Đăng Ninh",
                    "MaPX": "13669",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Năng Tĩnh",
                    "MaPX": "13672",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Văn Miếu",
                    "MaPX": "13675",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Trần Quang Khải",
                    "MaPX": "13678",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Thống Nhất",
                    "MaPX": "13681",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Lộc Hạ",
                    "MaPX": "13684",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Lộc Vượng",
                    "MaPX": "13687",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Cửa Nam",
                    "MaPX": "13690",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Lộc Hòa",
                    "MaPX": "13693",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Nam Phong",
                    "MaPX": "13696",
                    "Cap": "Xã"
                },
                {
                    "PhuongXa": "Mỹ Xá",
                    "MaPX": "13699",
                    "Cap": "Phường"
                },
                {
                    "PhuongXa": "Lộc An",
                    "MaPX": "13702",
                    "Cap": "Xã"
                },
                {
                    "PhuongXa": "Nam Vân",
                    "MaPX": "13705",
                    "Cap": "Xã"
                }
            ],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Mỹ Lộc",
            "Ma": "358",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Vụ Bản",
            "Ma": "359",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Ý Yên",
            "Ma": "360",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Nghĩa Hưng",
            "Ma": "361",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Nam Trực",
            "Ma": "362",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Trực Ninh",
            "Ma": "363",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Xuân Trường",
            "Ma": "364",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Giao Thủy",
            "Ma": "365",
            "Cap": "Huyện",
            "Unit": [],
        },
        {
            "urlAnh": "https://vneconomy.mediacdn.vn/zoom/480_270/2019/5/28/baitamhaihau1-15590147215421635372927-crop-1559014724396516958364.jpg",
            "Ten": "Hải Hậu",
            "Ma": "366",
            "Cap": "Huyện",
            "Unit": [],
        }
    ]
export {
    MapData
}