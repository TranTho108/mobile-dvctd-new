const YT_DB_Data = [
    {
        'id': '1',
        'name': 'Sốt xuất huyết',
        'location': 'Hà Nội',
        'history':
        {
            'appear': '1/11/2019',
            'isolation': '10/11/2019',
            'title': 'Lịch sử bệnh sốt xuất huyết',
            'content': 'Ghi nhận 20000 trường hợp mắc sốt xuất huyết',
        },
        'notification': [
            {
                'title': 'Thông báo phòng ngừa bệnh sốt xuất huyết',
                'content': 'Đậy kín lu, vại, hồ, bể chứa nước, không tạo nơi cho muỗi đẻ và hàng tuần nên cọ rửa các vật dụng chứa nước; thả cá 7 màu diệt lăng quăng (bọ gậy) để không cho muỗi đẻ trứng;',
            },
            {
                'title': 'Phòng chống các bệnh dịch dễ gặp mùa mưa bão',
                'content': 'Trong mùa mưa bão, Cục Y tế dự phòng - Bộ Y tế mạnh mẽ khuyến cáo người dân triển khai các biện pháp đảm bảo vệ sinh cá nhân, vệ sinh môi trường, an toàn thực phẩm sau những ngày mưa bão.',
            },
            {
                'title': 'Chủ động phòng bệnh sốt xuất huyết',
                'content': 'Bệnh sốt xuất huyết gây ra do các loại vi rút thuộc nhóm đường ruột, gồm có Coxsackie, Echo và các vi rút đường ruột khác, trong đó hay gặp là vi rút đường ruột týp 71 (EV71) và Coxsackie A16. Vi rút EV71 có thể gây các biến chứng nặng và gây tử vong.',
            },
        ]
    },
    {
        'id': '2',
        'name': 'Tay chân miệng',
        'location': 'Nam Định',
        'history':
        {
            'appear': '10/11/2019',
            'isolation': '10/11/2019',
            'title': 'Dịch tay chân miệng bắt đầu xuất hiện',
            'content': 'Ghi nhận 100 trường hợp mắc bệnh tay chân miệng',
        },
        'notification': [
            {
                'title': 'Khuyến cáo phòng chống bệnh tay chân miệng',
                'content': 'Bệnh Tay chân miệng là bệnh truyền nhiễm cấp tính do vi rút thuộc nhóm Enterovirus gây ra, bệnh lây truyền theo đường tiêu hóa, thường gặp ở trẻ nhỏ và có khả năng gây thành dịch lớn.',
            },
            {
                'title': 'Công văn Bộ Y tế gửi Giám đốc Sở Y tế   tỉnh, thành phố tiếp tục đẩy mạnh công tác phòng chống dịch bệnh mùa hè',
                'content': 'Hiện nay thời tiết khí hậu đang chuyển mùa hè - thu, đây cũng là thời điểm thời tiết diễn biến phức tạp, rất thuận lợi cho các dịch bệnh truyền nhiễm phát sinh và phát triển, đặc biệt là các bệnh lây truyền qua đường tiêu hóa, đường hô hấp, bệnh do muỗi truyền như bệnh ..',
            },
            {
                'title': 'Tay chân miệng giảm mạnh, nhưng vẫn tiềm ẩn nhiều nguy cơ bùng phát dịch',
                'content': 'Theo thông báo của Tổ chức Y tế thế giới, năm 2018 tại một số quốc gia trong khu vực châu Á và châu Mỹ La tinh, sốt xuất huyết hiện nay đa số giảm so với cùng kỳ năm 2017 tuy nhiên cũng đã bắt đầu có dấu hiệu gia tăng so với các tuần đầu năm.',
            },
        ]
    },
    {
        'id': '3',
        'name': 'Tay chân miệng',
        'location': 'Nam Định',
        'history':
        {
            'appear': '11/11/2019',
            'isolation': '20/11/2019',
            'title': 'Dịch tay chân miệng bắt đầu xuất hiện',
            'content': 'Ghi nhận 100 trường hợp mắc bệnh tay chân miệng',
        },
        'notification': [
            {
                'title': 'Khuyến cáo phòng chống bệnh tay chân miệng',
                'content': 'Bệnh Tay chân miệng là bệnh truyền nhiễm cấp tính do vi rút thuộc nhóm Enterovirus gây ra, bệnh lây truyền theo đường tiêu hóa, thường gặp ở trẻ nhỏ và có khả năng gây thành dịch lớn.',
            },
            {
                'title': 'Công văn Bộ Y tế gửi Giám đốc Sở Y tế   tỉnh, thành phố tiếp tục đẩy mạnh công tác phòng chống dịch bệnh mùa hè',
                'content': 'Hiện nay thời tiết khí hậu đang chuyển mùa hè - thu, đây cũng là thời điểm thời tiết diễn biến phức tạp, rất thuận lợi cho các dịch bệnh truyền nhiễm phát sinh và phát triển, đặc biệt là các bệnh lây truyền qua đường tiêu hóa, đường hô hấp, bệnh do muỗi truyền như bệnh ..',
            },
            {
                'title': 'Tay chân miệng giảm mạnh, nhưng vẫn tiềm ẩn nhiều nguy cơ bùng phát dịch',
                'content': 'Theo thông báo của Tổ chức Y tế thế giới, năm 2018 tại một số quốc gia trong khu vực châu Á và châu Mỹ La tinh, sốt xuất huyết hiện nay đa số giảm so với cùng kỳ năm 2017 tuy nhiên cũng đã bắt đầu có dấu hiệu gia tăng so với các tuần đầu năm.',
            },
        ]
    },
];

module.exports = YT_DB_Data;
