const dataMenuTD = [
    {
      appid: '1',
      menumain: 1,
      name: 'Dịch vụ hành chính công',
      navigate: 'DVC_MainScreen',
      count: 0,
      icon: 'university',
      color: '#0E2D7D',
      datamenu: [],
    },
    {
      appid: '5',
      menumain: 1,
      name: 'Tổng đài thông minh',
      navigate: 'TĐTM_MainScreen',
      count: 0,
      icon: 'phone-volume',
      color: '#DD0E2C',
      datamenu: [],
    },
    {
      appid: '8',
      name: 'Giáo dục',
      navigate: 'GD_MainScreen',
      count: 0,
      icon: 'graduation-cap',
      color: '#0271FE',
      datamenu: [],
    },
    {
      appid: '9',
      name: 'Y tế',
      navigate: 'YT_MainScreen',
      count: 0,
      icon: 'hospital-alt',
      color: '#AF1A16',
      datamenu: [],
    },
    {
      appid: '2',
      menumain: 1,
      name: 'Phản ánh hiện trường',
      navigate: 'PAHT_MainScreen',
      count: 0,
      icon: 'mail-bulk',
      color: '#5B63EC',
      datamenu: [],
    },
    {
      appid: '3',
      menumain: 1,
      name: 'Du lịch',
      navigate: 'DL_MainScreen',
      count: 0,
      icon: 'umbrella-beach',
      color: '#FC7D2E',
      datamenu: [],
    },
    {
      appid: '4',
      menumain: 1,
      name: 'Thông tin cảnh báo',
      navigate: 'TTCB_MainScreen',
      count: 0,
      icon: 'exclamation-triangle',
      color: '#FFC815',
      datamenu: [],
    },
    {
      appid: '6',
      menumain: 1,
      name: 'Điện nước',
      navigate: 'DN_MainScreen',
      count: 0,
      icon: 'clipboard-list',
      color: '#29AAE1',
      datamenu: [],
    },
    {
      appid: '10',
      name: 'Nông nghiệp',
      navigate: 'NN_MainScreen',
      count: 0,
      icon: 'tractor',
      color: '#45A659',
      datamenu: [],
    },
    {
      appid: '12',
      name: 'Giá cả thị trường',
      navigate: 'GCTT_MainScreen',
      count: 0,
      icon: 'chart-line',
      color: '#2856C6',
      datamenu: [],
    },
    {
      appid: '13',
      name: 'Môi trường',
      navigate: 'MT_MainScreen',
      count: 0,
      icon: 'cannabis',
      color: '#0271FE',
      datamenu: [],
    },
    {
      appid: '14',
      name: 'An toàn thực phẩm',
      navigate: 'ATTP_MainScreen',
      count: 0,
      icon: 'user-shield',
      color: '#FC7D2E',
      datamenu: [],
    },
  ];

const dataTT = [
  {
    imageurl:
      'http://xatm.tandan.com.vn/portal/Photos/2018-10/_w/hh_o5PPCoxsm0uultUb_png.jpg',
    title: 'Nông thôn mới phát triển bền vững trên nền tảng khoa học',
  },
  {
    imageurl:
      'http://xatm.tandan.com.vn/portal/Photos/2018-10/_w/nguyenhuyhoang_gvWYVSuNlEWZo5IA_jpg.jpg',
    title: 'Kình ngư Việt Nam lập kỳ tích tại Olympic trẻ Argentina',
  },
  {
    imageurl:
      'http://xatm.tandan.com.vn/portal/Photos/2018-10/_w/Bacgiang_YuzuIEmro0GaefGw_JPG.jpg',
    title: 'Kiện toàn tổ chức, nhân sự 6 tỉnh thành',
  },
  {
    imageurl:
      'https://znews-photo.zadn.vn/w1024/Uploaded/ovhpaob/2020_01_02/thu_tuong.jpg',
    title: 'Thủ tướng: Một số đường dây có lãnh đạo cấp tỉnh bảo kê',
  },
]

export {
    dataMenuTD,
    dataTT
}