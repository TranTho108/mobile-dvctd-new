const D_Data = [
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 31/12/2019 thời gian từ 13h-14h để sửa chữa đường dây',
        dateCĐ: '12/31/2019',
        timeCĐ: '13h-14h',
        time: '31/12/2019 11:00'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 28/11/2019 thời gian từ 9h-11h để sửa chữa đường dây',
        dateCĐ: '11/28/2019',
        timeCĐ: '9h-11h',
        time: '28/11/2019 03:00'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 19/11/2019 thời gian từ 20h-21h để sửa chữa đường dây',
        dateCĐ: '11/19/2019',
        timeCĐ: '20h-21h',
        time: '19/11/2019 16:00'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 01/11/2019 thời gian từ 21h30-22h30 do đường dây gặp sự cố',
        dateCĐ: '11/01/2019',
        timeCĐ: '21h30-22h30',
        time: '01/11/2019 21:40'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 25/10/2019 thời gian từ 15h-17h để sửa chữa bốt điện',
        dateCĐ: '10/25/2019',
        timeCĐ: '15h-17h',
        time: '25/10/2019 12:00'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 15/10/2019 thời gian từ 09h-10h',
        dateCĐ: '10/15/2019',
        timeCĐ: '09h-10h',
        time: '15/10/2019 06:00'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 09/10/2019 thời gian từ 06h-09h',
        dateCĐ: '10/09/2019',
        timeCĐ: '06h-09h',
        time: '09/10/2019 04:00'
    },
    {
        title: 'Thông báo cắt điện',
        body: 'Thông báo cắt điện ngày 05/10/2019 thời gian từ 14h-16h',
        dateCĐ: '10/05/2019',
        timeCĐ: '14h-16h',
        time: '05/10/2019 10:00'
    },
]

const D_CB = [
    {
        title: 'Cảnh báo sử dụng điện bất thường tháng 10',
        body: 'Mức sử dụng điện trong tháng 10 lớn hơn 10% so với trung bình 6 tháng trước đó. Vui lòng kiểm tra lại các thiết bị điện đang sử dụng.',
        level: 'Trung bình',
        time: '05/11/2019 05:12'
    }
]

const N_CB = []

const N_Data =[
    {
        title: 'Thông báo cắt nước',
        body: 'Thông báo cắt nước ngày 25/12/2019 thời gian từ 10h-11h để sửa chữa đường ống',
        dateCĐ: '12/25/2019',
        timeCĐ: '10h-11h',
        time: '25/12/2019 09:00'
    },
    {
        title: 'Thông báo cắt nước',
        body: 'Thông báo cắt nước ngày 28/11/2019 thời gian từ 9h-16h để sửa chữa đường ống',
        dateCĐ: '11/28/2019',
        timeCĐ: '9h-16h',
        time: '28/11/2019 03:00'
    },
    {
        title: 'Thông báo cắt nước',
        body: 'Thông báo cắt điện ngày 19/10/2019 thời gian từ 08h-21h để sửa chữa đường ống',
        dateCĐ: '10/19/2019',
        timeCĐ: '08h-21h',
        time: '19/10/2019 06:00'
    },
    {
        title: 'Thông báo cắt nước',
        body: 'Thông báo cắt điện ngày 01/10/2019 thời gian từ 12h30-18h30 do đường ống bị vỡ',
        dateCĐ: '10/01/2019',
        timeCĐ: '12h30-18h30',
        time: '01/10/2019 21:40'
    },
    {
        title: 'Thông báo cắt nước',
        body: 'Thông báo cắt điện ngày 25/09/2019 thời gian từ 09h-17h để sửa chữa đường ống',
        dateCĐ: '09/25/2019',
        timeCĐ: '09h-17h',
        time: '25/09/2019 08:00'
    },
]

export {
    D_Data,
    D_CB,
    N_CB,
    N_Data
}