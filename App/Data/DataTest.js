const PAKN_LIST =  [
    {
        "id": "1",
        "tieude": "Bóng đèn công viên bị hư",
        "chuyenmuc": "Cảnh quan môi trường",
        "thoigiangui": "2019-06-14T15:26:00Z",
        "noidung": "abc",
        "tinhtrang": "Đang xử lý",
        "nguoigui": "Người dân"
    },
    {
        "id": "2",
        "tieude": "Bóng đèn công viên bị hư",
        "chuyenmuc": "abc",
        "thoigiangui": "2019-06-14T15:26:00Z",
        "noidung": "abc",
        "tinhtrang": "Đang xử lý",
        "nguoigui": "Người dân"
    },
    {
        "id": "3",
        "tieude": "abc",
        "chuyenmuc": "Cảnh quan môi trường",
        "thoigiangui": "2019-06-14T15:26:00Z",
        "noidung": "abc",
        "tinhtrang": "Đang xử lý",
        "nguoigui": "Người dân"
    },  
  ]

const PAKN_DETAIL = 
    [{
        "id":"6",
        "tieude":"abc",
        "noidung":"abc",
        "thoigiangui":"2019-06-14T15:26:00Z",
        "mediagui":["abc","abc"],
        "vitri":"Quận Đống Đa - Hà Nội",
        "chuyenmuc":"Vệ sinh môi trường",
        "luotxem":"22",
        "ngayxuly":"2019-06-14",
        "coquanxuly":"abc",
        "noidungxuly":"abc",
        "binhluan":[{id:"10",name:"abc",content:"abc",time:"2019-06-14T15:26:00Z"}],
        "mediaketqua":["abc","abc"]           
    }]

const TTCB_LIST = [
    {
        "id": "1",
        "tieude": "abc",
        "thoigiangui": "2019-06-14T15:26:00Z",
        "donvigui":"abc",
        "vitri":"abc" 
    },
    {
        "id": "1",
        "tieude": "abc",
        "thoigiangui": "2019-06-14T15:26:00Z",
        "donvigui":"abc",
        "vitri":"abc" 
    },
    {
        "id": "1",
        "tieude": "abc",
        "thoigiangui": "2019-06-14T15:26:00Z",
        "donvigui":"abc",
        "vitri":"abc" 
    },
  
]
const TTCB_DETAIL = 
    [{
        "id":"6",
        "tieude":"abc",
        "noidung":"abc",
        "thoigiangui":"2019-06-14T15:26:00Z",
        "mediagui":["abc","abc"],
        "chuyenmuc":"Giao thông đô thị",
        "luotxem":"22",
        "donvigui":"Sở giao thông vận tải",
        "mucdo":"Thông tin",
        "vitri":"Quận Đống Đa - Hà Nội"
    }]

export {
    PAKN_LIST,
    PAKN_DETAIL,
    TTCB_LIST,
    TTCB_DETAIL
};