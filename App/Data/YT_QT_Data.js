const QT_Data = [
    {
        ID: 1,
        name: 'Quầy Thuốc Tây Nghiêm Thanh Sơn',
        address: '7, Phố Khâm Thiên, Quận Đống Đa, Khâm Thiên, Đống Đa, Hà Nội, Việt Nam',
        phone: '+84 91 741 06 66',
        location: {latitude: 21.0193171,longitude: 105.8399409},
        pharmacist: [
            {
                ID: 100123,
                name: 'Vũ Thị Thanh',
                level: '',
                certificate: ''
            },
            {
                ID: 100124,
                name: 'Lâm Thuý Thảo',
                level: '',
                certificate: ''
            },
        ]
    },
    {
        ID: 2,
        name: 'Quầy Thuốc Tây Cao Dương Đức',
        address: '102E2 Thái Thịnh, Láng Hạ, Đống Đa, Hà Nội, Việt Nam',
        phone: '+84 97 543 68 90',
        location: {latitude: 21.0143992,longitude: 105.814776},
        pharmacist: []
    },
    {
        ID: 3,
        name: 'Quầy thuốc Trung Nghĩa',
        address: '27/86 Chùa Hà, Cầu Giấy, Hanoi, Hà Nội, Việt Nam',
        phone: '+84 91 508 53 09',
        location: {latitude: 21.036819,longitude: 105.7968045},
        pharmacist: []
    },
    {
        ID: 4,
        name: 'Quầy Thuốc Anh Đức',
        address: '19 Ngõ 110 Trần Duy Hưng, Trung Hoà, Cầu Giấy, Hà Nội, Việt Nam',
        phone: '+84 98 539 11 55',
        location: {latitude: 21.0116402,longitude: 105.7992662},
        pharmacist: []
    },
    {
        ID: 5,
        name: 'Quầy Thuốc 5G',
        address: '95 Láng Hạ, Đống Đa, Hà Nội, Việt Nam',
        phone: '+84 98 876 00 11',
        location: {latitude: 21.0147459,longitude: 105.8140465},
        pharmacist: []
    },

]

export {
    QT_Data
}