const MT_DANHMUC = [
    {
        id: 0,
        name: 'Lịch thu gom rác',
        navigate: 'MT_LTGR_MainScreen',
        icon: require("../Images/calendar.png"),
    },
    {
        id: 1,
        name: 'Địa điểm xử lý rác',
        navigate: 'MT_ĐXLR_MainScreen',
        icon: require("../Images/landfill.png"),
    },
    {
        id: 2,
        name: 'Địa điểm ô nhiễm',
        navigate: 'MT_ĐOM_MainScreen',
        icon: require("../Images/trash.png"),
    },
    {
        id: 3,
        name: 'Cá nhân vi phạm',
        navigate: 'MT_CNVP_MainScreen',
        icon: require("../Images/litter.png"),
    }
]

const MT_ĐXLR = [
    {
        ID: 1,
        name: 'Điểm xử lý rác Hàng Bột',
        address: '212 Phố Tôn Đức Thắng, Hàng Bột, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.024125,longitude: 105.8326251},
    },
    {
        ID: 2,
        name: 'Điểm xử lý rác Văn Chương',
        address: '22 Ngõ Văn Chương 1, Văn Chương, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.0223506,longitude: 105.8364443},
    },
    {
        ID: 3,
        name: 'Điểm xử lý rác Khâm Thiên',
        address: '113 Khâm Thiên, Thổ Quan, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.019285,longitude: 105.833832},
    },
]


const MT_ĐOM = [
    {
        ID: 1,
        name: 'Điểm ô nhiễm khu vực Tôn Đức Thắng Tôn Đức Thắng',
        address: '212 Phố Tôn Đức Thắng, Hàng Bột, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.024125,longitude: 105.8326251},
    },
]

export {
    MT_DANHMUC,
    MT_ĐOM,
    MT_ĐXLR
}