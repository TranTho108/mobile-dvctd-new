const TYT_Data = [
    {
        ID: 1,
        name: 'Trạm y tế phường Hàng Bột',
        address: '107 Phố Tôn Đức Thắng, Hàng Bột, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.024115,longitude: 105.8326241},
    },
    {
        ID: 2,
        name: 'Trạm Y Tế Phường Văn Chương',
        address: '53 Ngõ Văn Chương 2, Văn Chương, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.0223516,longitude: 105.8364433},
    },
    {
        ID: 3,
        name: 'Trạm Y Tế Phường Thổ Quan',
        address: '292 Khâm Thiên, Thổ Quan, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.019295,longitude: 105.833822},
    },
    {
        ID: 4,
        name: 'Trạm Y Tế Phường Hàng Trống',
        address: '11 Phủ Doãn, Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam',
        location: {latitude: 21.0298803,longitude: 105.8478286},
    },
    {
        ID: 5,
        name: 'Trạm Y tế phường Văn Miếu',
        address: 'Số 130 Phố Nguyễn Khuyến, Văn Miếu, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.0287463,longitude: 105.8372632},
    },

]

export {
    TYT_Data
}