const DGN_Data = [
    {
        ID: 1,
        name: 'Bảo hiểm',
        data: [
            {
                PID: 1,
                name: 'Bảo hiểm Nhân thọ CHUBB',
                phone: '0123456789',
                img: 'https://www.chubb.com/_global-assets/images/social_sharing_chubb-logo.jpg'
            },
            {
                PID: 2,
                name: 'Bảo hiểm Nhân thọ Hanwha',
                phone: '0123456789',
                img: 'https://baohiemcenter.com/wp-content/uploads/2018/12/hanwha.jpg'
            },
            {
                PID: 3,
                name: 'Bảo Việt Nhân Thọ',
                phone: '0123456789',
                img: 'https://cbamekong.org/wp-content/uploads/2019/07/Bao-Viet-life.png'
            }
        ],
        icon: 'shield-alt'
    },
    {
        ID: 2,
        name: 'Bất động sản',
        data: [
            {
                PID: 4,
                name: 'PROPZY',
                phone: '0123456789',
                img: 'https://cdn.propzy.vn/default/propzy-logo.png'
            },
        ],
        icon: 'warehouse'
    },
    {
        ID: 3,
        name: 'Chăm sóc sức khoẻ',
        data: [
            {
                PID: 5,
                name: 'Vinmec',
                phone: '0123456789',
                img: 'https://www.vinmec.com/static/img/vinmec-facebook.f6895e5f77a5.jpg'
            },
        ],
        icon: 'briefcase-medical'
    },
    {
        ID: 4,
        name: 'Du lịch',
        data: [
            {
                PID: 6,
                name: 'MELIA Ha Noi',
                phone: '0123456789',
                img: 'https://pbs.twphone.com/profile_images/1052406928884846593/3WXVa8B9.jpg'
            },
        ],
        icon: 'globe-asia'
    },
    {
        ID: 5,
        name: 'Dịch vụ',
        data: [],
        icon: 'sitemap'
    },
    {
        ID: 6,
        name: 'Giải trí',
        data: [],
        icon: 'cocktail'
    },
    {
        ID: 7,
        name: 'Hành chính',
        data: [],
        icon: 'university'
    },
    {
        ID: 8,
        name: 'Kinh doanh',
        data: [],
        icon: 'briefcase'
    },
    {
        ID: 9,
        name: 'Thương mại',
        data: [],
        icon: 'store'
    },
    {
        ID: 10,
        name: 'Tiêu dùng',
        data: [],
        icon: 'shopping-cart'
    },
    {
        ID: 11,
        name: 'Truyền thông',
        data: [],
        icon: 'microphone'
    },
    {
        ID: 12,
        name: 'Tài chính',
        data: [],
        icon: 'coins'
    },
    {
        ID: 13,
        name: 'Viễn thông',
        data: [
            {
                PID: 7,
                name: 'FPT Telecom',
                phone: '0123456789',
                img: 'https://static.ybox.vn/2018/7/6/1532794142338-1532397916746-1532343362372-FPT%20telecom.png'
            },
            {
                PID: 8,
                name: 'Mobifone',
                phone: '0123456789',
                img: 'http://beta.kontum.udn.vn/uploads/qhdn/2016_01/tong-dai-mobifone-1.jpg'
            },
        ],
        icon: 'broadcast-tower'
    },
    {
        ID: 14,
        name: 'Vận tải',
        data: [],
        icon: 'truck'
    },
    {
        ID: 15,
        name: 'Ăn uống',
        data: [],
        icon: 'utensils'
    },
]

export {
    DGN_Data
}