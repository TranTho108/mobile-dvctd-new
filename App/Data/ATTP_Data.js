const ATTP_DANHMUC = [
    {
        id: 0,
        name: 'Cơ sở kinh doanh',
        icon: require("../Images/architecture.png"),
        navigate: 'ATTP_CSKD_MainScreen'
    },
    {
        id: 1,
        name: 'Thanh tra ATVSTP',
        icon: require("../Images/contract.png"),
        navigate: 'ATTP_TT_MainScreen'
    },
    {
        id: 2,
        name: 'Thông tin tuyên truyền',
        icon: require("../Images/promotion.png"),
        navigate: 'ATTP_TTTT_MainScreen'
    },
    {
        id: 3,
        name: 'Cảnh báo ATVSTP',
        icon: require("../Images/warning.png"),
        navigate: 'ATTP_CB_MainScreen'
    }
]

export {
    ATTP_DANHMUC
}