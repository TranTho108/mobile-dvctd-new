const NN_DANHMUC = [
    {
        id: 0,
        name: 'Lịch thuỷ lợi, giống',
        navigate: 'NN_LTLG_MainScreen',
        icon: require("../Images/calendar.png"),
    },
    {
        id: 1,
        name: 'Cảnh báo dịch bệnh',
        navigate: 'NN_CBDB_MainScreen',
        icon: require("../Images/virus.png"),
    },
    {
        id: 2,
        name: 'Cơ sở chăn nuôi',
        navigate: 'NN_CSCN_MainScreen',
        icon: require("../Images/cow.png"),
    },
    {
        id: 3,
        name: 'Cơ sở trồng trọt',
        navigate: 'NN_CSTT_MainScreen',
        icon: require("../Images/vegetables.png"),
    }
]

const NN_CSCN = [
    {
        ID: 1,
        name: 'Cơ sở chăn nuôi Hàng Bột',
        address: '212 Phố Tôn Đức Thắng, Hàng Bột, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.024125,longitude: 105.8326251},
    },
    {
        ID: 2,
        name: 'Cơ sở chăn nuôi Văn Chương',
        address: '22 Ngõ Văn Chương 1, Văn Chương, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.0223506,longitude: 105.8364443},
    },
    {
        ID: 3,
        name: 'Cơ sở chăn nuôi Khâm Thiên',
        address: '113 Khâm Thiên, Thổ Quan, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.019285,longitude: 105.833832},
    },
]


const NN_CSTT = [
    {
        ID: 1,
        name: 'Cơ sở trồng trọt Tôn Đức Thắng',
        address: '212 Phố Tôn Đức Thắng, Hàng Bột, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.024125,longitude: 105.8326251},
    },
    {
        ID: 2,
        name: 'Cơ sở trồng trọt Vũ Phan',
        address: '22 Ngõ Văn Chương 1, Văn Chương, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.0223506,longitude: 105.8364443},
    },
    {
        ID: 3,
        name: 'Cơ sở trồng trọt Gia Huy',
        address: '113 Khâm Thiên, Thổ Quan, Đống Đa, Hà Nội, Việt Nam',
        location: {latitude: 21.019285,longitude: 105.833832},
    },
]

export {
    NN_DANHMUC,
    NN_CSCN,
    NN_CSTT
}