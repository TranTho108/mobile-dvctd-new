const SERVER_URL = 'https://pakn.tandan.com.vn/_tandan/gopy.svc'
const HOST = 'https://pakn.tandan.com.vn'
const DVC_URL = 'http://dichvucong.tandan.com.vn/_layouts/tandan.dvc.api/APIDVCService.svc'
const QLDL_URL = 'http://qldlapi.tandan.com.vn/QLDLAPI/PublicAPI'
const DN_URL = 'http://dieuhanhtm.tandan.com.vn/_layouts/15/TD.DN.Service/WCFService.svc'
const CB_URL = 'http://dieuhanhtm.tandan.com.vn/_layouts/15/TD.CB.Service/WCFService.svc'
const TĐTM_URL = 'http://dieuhanhtm.tandan.com.vn/_layouts/15/TD.LH.Service/WCFService.svc'
const GCTT_URL = 'https://qlyt.tandan.com.vn/_vti_bin/TD.XaThongMinh.GiaCaThiTruong.WCF/GCTTService.svc'
const YT_URL = 'https://qlyt.tandan.com.vn/_vti_bin/TD.XaThongMinh.YTe.WCF/XtmYteService.svc'
const CD_URL = 'https://qldcapi.tandan.com.vn/_layouts/15/TD.QLDC.Service/WCFService.svc'
const ATTP_URL = 'https://vsattp.tandan.com.vn/_layouts/15/TD.VSATTP.Service/WCFService.svc'
const MT_URL = 'https://mt.tandan.com.vn/_layouts/15/TD.MT.Service/WCFService.svc'
const DL_URL = 'http://qldl.tandan.com.vn/_layouts/15/TD.QLDL.Service/WCFService.svc'
const GD_URL = 'http://slldt.tandan.com.vn/_layouts/15/TD.QLGD.Service/WCFService.svc'
const QLDB_URL = 'https://qlyt.tandan.com.vn/_vti_bin/TD.XaThongMinh.DiaBan.WCF/XtmDiaBanService.svc'
const HOST_YT = 'https://qlyt.tandan.com.vn'
export {
  SERVER_URL,
  HOST,
  QLDL_URL,
  DN_URL,
  CB_URL,
  DVC_URL,
  TĐTM_URL,
  GCTT_URL,
  YT_URL,
  CD_URL,
  ATTP_URL,
  MT_URL,
  DL_URL,
  GD_URL,
  QLDB_URL,
  HOST_YT
};