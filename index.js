import { AppRegistry, Text } from 'react-native'
import AppContainer from './App/Navigation/AppNavigation'
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

AppRegistry.registerComponent('intership', () => AppContainer)
